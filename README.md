# Bevos Course Guide

UT Course Guide

People:
- Katelyn Ashok (kba488): kba488 - Phase 4 project leader
- Sahil Jain (sj29344): sahiljain11 - Phase 1 project leader
- Rachel Li (rl32784): rachelli0603
- Adeet Parikh (aap3756): N-Krypt - Phase 2 project leader
- Lauren Warhola (lmw2975): lwarhola - Phase 3 project leader

Git SHA: a6ae0dfc5c03bcd96ef53a02b99e9d6bdd77a4c5

Gitlab Pipelines: https://gitlab.com/bevos-crew/bevos-course-guide/-/pipelines/423966947

Links to website:
- Website: [https://www.bevoscourseguide.me](https://www.bevoscourseguide.me)

- Postman: [https://documenter.getpostman.com/view/5971933/UUy4ckKn](https://documenter.getpostman.com/view/5971933/UUy4ckKn)


| Names             | Phase I Estimate  | Phase I Actual |
| ----------------- | ----------------- | -------------- | 
| Adeet Parikh      | 20 hrs            | 35 hrs         |
| Sahil Jain        | 20 hrs            | 30 hrs         |
| Rachel Li         | 15 hrs            | 30 hrs         |
| Lauren Warhola    | 15 hrs            | 30 hrs         |
| Katelyn Ashok     | 15 hrs            | 30 hrs         |

| Names             | Phase II Estimate | Phase II Actual |
| ----------------- | ----------------- | --------------- | 
| Adeet Parikh      | 45 hrs            | 80 hrs          |
| Sahil Jain        | 55 hrs            | 75 hrs          |
| Rachel Li         | 55 hrs            | 75 hrs          |
| Lauren Warhola    | 50 hrs            | 75 hrs          |
| Katelyn Ashok     | 50 hrs            | 75 hrs          |

| Names             | Phase III Estimate  | Phase III Actual |
| ----------------- | ------------------- | ---------------- | 
| Adeet Parikh      | 20 hrs              | 35 hrs           |
| Sahil Jain        | 20 hrs              | 45 hrs           |
| Rachel Li         | 15 hrs              | 30 hrs           |
| Lauren Warhola    | 15 hrs              | 30 hrs           |
| Katelyn Ashok     | 15 hrs              | 30 hrs           |

| Names             | Phase IV Estimate | Phase IV Actual |
| ----------------- | ----------------- | --------------- | 
| Adeet Parikh      |  8 hrs            |  8 hrs          |
| Sahil Jain        | 10 hrs            | 10 hrs          |
| Rachel Li         |  4 hrs            |  8 hrs          |
| Lauren Warhola    |  8 hrs            |  8 hrs          |
| Katelyn Ashok     | 10 hrs            |  8 hrs          |


Comments:
None
