import React from "../../node_modules/react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
} from "../../node_modules/@material-ui/core";
import { makeStyles } from "../../node_modules/@material-ui/styles";

import HomeIcon from "../../node_modules/@material-ui/icons/Home";

import { Link } from "../../node_modules/react-router-dom";

const useStyles = makeStyles(() => ({
  textButtonStyle: {
    fontSize: 20,
    fontWeight: 500,
  }
}));


function NavBar() {

  const classes = useStyles();

  const iconStyle = {
    margin: 0,
    padding: 0,
  };
  const toolBarStyle = {
    justifyContent: "space-between",
    marginLeft: 10,
    marginRight: 200,
  };

  return (
    <AppBar position="static" color="secondary">
      <Toolbar style={toolBarStyle}>
        <IconButton
          edge="start"
          color="primary"
          aria-label="menu"
          style={iconStyle}
          component={Link}
          to="/Home"
        >
          <HomeIcon />
        </IconButton>
        <Button component={Link} to="/Courses">
          <Typography variant="h5" color="primary" className={classes.textButtonStyle}>
            Courses
            </Typography>
        </Button>
        <Button component={Link} to="/Professors">
          <Typography variant="h5" color="primary" className={classes.textButtonStyle}>
            Professors
            </Typography>
        </Button>
        <Button color="primary" component={Link} to="/Departments">
          <Typography variant="h5" color="primary" className={classes.textButtonStyle}>
            Departments
            </Typography>
        </Button>
        <Button color="primary" component={Link} to="/Visualizations">
          <Typography variant="h5" color="primary" className={classes.textButtonStyle}>
            Visualizations
            </Typography>
        </Button>
        <Button color="primary" component={Link} to="/AboutUs">
          <Typography variant="h5" color="primary" className={classes.textButtonStyle}>
            About Us
            </Typography>
        </Button>
      </Toolbar>
    </AppBar>
  );
}

export default NavBar;
