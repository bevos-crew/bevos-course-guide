FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive

#COPY ./badproxy /etc/apt/apt.conf.d/99fixbadproxy

RUN apt-get clean && apt-get update
RUN apt-get install -y python3
RUN apt-get install -y python3-pip python3-dev build-essential vim
RUN apt-get install -y libmysqlclient-dev libpq-dev postgresql

COPY . usr/src/backend
COPY requirements.txt usr/src/backend/requirements.txt

WORKDIR /usr/src/backend

RUN pip install --upgrade pip
RUN pip install psycopg2-binary
RUN pip install black \
    coverage \
    mypy \
    numpy \
    pylint \
    marshmallow-sqlalchemy \
    requests

RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["python3", "app.py"]