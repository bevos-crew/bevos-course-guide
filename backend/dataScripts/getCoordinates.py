import requests
from requests.auth import HTTPBasicAuth
import time
import json
import os
import pandas as pd
from dotenv import load_dotenv

load_dotenv()

def getCoordinates(query):
    headers = {'Accept': 'application/json'}
    params = {"key": os.getenv('GOOGLENEWKEY'), "address": query}

    response = requests.get('https://maps.googleapis.com/maps/api/geocode/json', headers=headers, params=params)

    geoDict = {}
    geoDict["address"]  = ''
    geoDict["lat"]      = ''
    geoDict["lng"]      = ''

    try: 
        #print(response.url)
        #print(response.content)
        geoInfo = json.loads(response.content)['results'][0]
    except: return geoDict

    geoDict["address"] = geoInfo["formatted_address"]
    geoDict["lat"] = geoInfo['geometry']["location"]['lat']
    geoDict["lng"] = geoInfo['geometry']["location"]['lng']

    print(geoDict)
    return geoDict


#print(getCoordinates("UT Tower"))