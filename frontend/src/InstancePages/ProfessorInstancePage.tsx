import { Card, CardMedia, Typography, Grid, List, ListItem, Box } from "@material-ui/core";
import React, { useEffect } from "react";
import '../fonts.css';
import { Link, useParams } from "react-router-dom";
import { IProfessor } from "../Components/Interfaces"

import axios from "axios";
import { useStyle, infoTextStyle, infoTitleTextStyle, subTextStyle, titleTextStyle } from "../Components/Style";
import { CreatePDF } from "../Components/Media";

//get professor name from url
interface RouteParams {
    professorName: string;
}

//using data from endpoints, fills interface with data
function createProfessor(
    name: string,
    department: string,
    picture: string,
    college: string,
    email: string,
    eid: string,
    courses: string[],
    courseNumbers: string[],
    numberOfPublications: number,
    publications: string[],
    resume: string,
    teachingDays: string,
    teachingTimes: string[],
    classBuildings: string[],
    pdf: string,
    courseNames: string[]

): IProfessor {
    return {
        name,
        department,
        picture,
        college,
        email,
        eid,
        courses,
        courseNumbers,
        numberOfPublications,
        publications,
        resume,
        teachingDays,
        teachingTimes,
        classBuildings,
        pdf,
        courseNames
    };
}

function ProfessorInstancePage() {
    // directs routing to specific prof instance page
    const { professorName } = useParams<RouteParams>();

    const classes = useStyle();


    let errorText: string[] = [];

    let [professor, setProfessor] = React.useState(createProfessor("", "", "", "", "", "", [], [], 0, [], "", "", [], [], "", []));
    const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);
    const [error, setError]: [string, (error: string) => void] = React.useState("");


    useEffect(() => {

        document.body.style.background = 'white';
        document.title = professorName;

        const profName = professorName.replace(" ", "%20");
        //get data from endpoint for a specific professor
        axios.get<IProfessor>(`https://api.bevoscourseguide.me/api/professor/${profName}`).then((response) => {
            let prof: any = response.data
            professor = createProfessor(
                response.data.name,
                response.data.department,
                response.data.picture,
                response.data.college,
                response.data.email,
                response.data.eid,
                response.data.courses,
                prof["course numbers"],
                prof["num pubs"],
                prof["pubs list"],
                response.data.resume,
                prof["teaching days"],
                prof["teaching times"],
                prof["class buildings"],
                prof.pdf,
                prof["course names"]
            );
            setProfessor(professor);
            setLoading(false);
        })
            .catch(ex => {
                const error =
                    ex.response.status === 404
                        ? "Resource Not found"
                        : "An unexpected error has occurred";
                console.log(ex);
                errorText.push(error);
            });


        document.title = professor.name;
    }, []);

    //while data loads into page
    if (loading) {
        return (
            <>
                <Typography variant="h3" align="center" style={{ fontWeight: 600, marginBottom: 20, marginTop: 60 }}>Loading...</Typography>
            </>
        );
    }
    return (
        <Box style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
            <Typography style={titleTextStyle}>{professor.name}</Typography>
            <Typography style={subTextStyle} component={Link} to={{ pathname: `/Departments/${professor.department}` }}>
                DEPARTMENT: {professor.department}
            </Typography>
            {/* Professor image in middle */}
            <Card className={classes.image}>
                <CardMedia
                    component="img"
                    height="100%"
                    width="100%"
                    image={professor.picture}
                    alt="official professor image" />
            </Card>
            <Grid container spacing={5}>
                {/* Right side card with professor personal info */}
                <Grid item xs={4}>
                    <Typography style={infoTitleTextStyle}>
                        Info
                        </Typography>
                    <Typography style={infoTextStyle}>
                        College: {professor.college}
                    </Typography>
                    <Typography style={infoTextStyle}>
                        Email: {professor.email}
                    </Typography>
                    <Typography style={infoTextStyle}>
                        EID: {professor.eid}
                    </Typography>
                    <Typography style={infoTextStyle}>
                        Days Teaching: {professor.teachingDays}
                    </Typography>
                    <Typography style={infoTextStyle}>
                        Teaching Times:
                    </Typography>
                    {professor.teachingTimes.map((prop) => (
                        <Typography style={infoTextStyle}>
                            {prop}
                        </Typography>
                    ))}
                    <Typography style={infoTextStyle}>
                        Teaches In:
                    </Typography>
                    {professor.classBuildings.map((prop) => (
                        <Typography style={infoTextStyle}>
                            {prop}
                        </Typography>
                    ))}
                </Grid>
                <Grid item xs={4} >
                    <Typography style={infoTitleTextStyle}>
                        Classes
                    </Typography>
                    {professor.courses.map((prop, i) => (
                        <Typography style={infoTextStyle} component={Link} to={{ pathname: `/Courses/${professor.courseNames[i]}` }}>
                            {professor.courseNames[i]}
                        </Typography>
                    ))}
                </Grid>
                <Grid item xs={4}>
                    <Typography style={infoTitleTextStyle}>
                        Publications
                    </Typography>
                    <Typography style={infoTextStyle}>
                        Number of Publications: {professor.numberOfPublications}
                    </Typography>
                    <Typography style={infoTextStyle}>
                        Name of Publications:
                </Typography>
                    <Box style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                        <List style={{ maxHeight: 175, width: 300, overflow: "auto", display: "flex", flexDirection: 'column', }}>
                            {professor.publications.map((prop: React.ReactNode) => (
                                <ListItem >
                                    <Typography style={infoTextStyle}>
                                        {prop}
                                    </Typography>
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                </Grid>
            </Grid>
            <Box style={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
                <Typography style={infoTitleTextStyle}>
                    Resume
                </Typography>
                <CreatePDF file={professor.pdf} />
            </Box>
        </Box>
    );
}

export default ProfessorInstancePage;
