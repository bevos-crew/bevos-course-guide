"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _core = require("@material-ui/core");

var _react = _interopRequireWildcard(require("react"));

require("./fonts.css");

var _styles = require("@material-ui/core/styles");

var _Class = _interopRequireDefault(require("@material-ui/icons/Class"));

var _Person = _interopRequireDefault(require("@material-ui/icons/Person"));

var _Apartment = _interopRequireDefault(require("@material-ui/icons/Apartment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const useStyles = (0, _styles.makeStyles)(() => ({
  textFieldStyle: {
    [`& fieldset`]: {
      borderRadius: 50
    },
    backgroundColor: "white",
    borderRadius: 50
  },
  label: {
    fontSize: 28,
    shrink: false,
    margin: 10,
    marginLeft: 20
  },
  input: {
    fontSize: 40,
    color: "black"
  },
  titleStyle: {
    fontFamily: "Bungee Shade",
    fontSize: 100,
    marginTop: "8%",
    color: "white"
  },
  paper: {
    borderRadius: 15
  },
  modelsPaperDiv: {
    margin: 40,
    width: 300,
    height: 300,
    marginTop: 100
  }
})); // Main page

function HomePage() {
  (0, _react.useEffect)(() => {
    document.body.style.backgroundColor = "#bf5700";
    document.title = "Bevo's Course Guide";
  });
  const classes = useStyles();
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    align: "center",
    className: classes.titleStyle
  }, "BEVO'S COURSE GUIDE"), /*#__PURE__*/_react.default.createElement(_core.Box, {
    display: "flex",
    justifyContent: "center",
    marginLeft: "10%",
    marginRight: "10%",
    marginTop: "1%"
  }, /*#__PURE__*/_react.default.createElement(_core.TextField, {
    id: "outlined-basic",
    label: "Search",
    InputLabelProps: {
      className: classes.label
    },
    variant: "outlined",
    fullWidth: true,
    color: "primary",
    size: "medium",
    className: classes.textFieldStyle,
    InputProps: {
      className: classes.input
    }
  })), /*#__PURE__*/_react.default.createElement(_core.Box, {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap"
  }, /*#__PURE__*/_react.default.createElement(ModelPaperLink, {
    Icon: _Class.default,
    name: "Courses",
    link: "/Courses"
  }), /*#__PURE__*/_react.default.createElement(ModelPaperLink, {
    Icon: _Person.default,
    name: "Professors",
    link: "/Professors"
  }), /*#__PURE__*/_react.default.createElement(ModelPaperLink, {
    Icon: _Apartment.default,
    name: "Departments",
    link: "/Departments"
  })));
}

// Card links to model pages
function ModelPaperLink({
  Icon,
  name,
  link
}) {
  const classes = useStyles();
  return /*#__PURE__*/_react.default.createElement("div", {
    className: classes.modelsPaperDiv
  }, /*#__PURE__*/_react.default.createElement(_core.Paper, {
    elevation: 3,
    className: classes.paper,
    style: {
      height: "100%",
      width: "100%"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.CardActionArea, {
    href: link,
    style: {
      height: "100%",
      width: "100%"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.CardContent, {
    style: {
      height: "268",
      width: "268"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      width: "100%",
      height: "100%",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      flexWrap: "nowrap"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Box, {
    width: "200px",
    height: "190px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }, /*#__PURE__*/_react.default.createElement(Icon, {
    style: {
      fill: "#bf5700",
      width: "75%",
      height: "75%"
    }
  })), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h5"
  }, name))))));
}

var _default = HomePage;
exports.default = _default;