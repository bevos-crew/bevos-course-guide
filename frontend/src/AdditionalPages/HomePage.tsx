import { Box, CardActionArea, CardContent, Paper, Typography } from "@material-ui/core";
import React, { useEffect } from "react";
import '../fonts.css';
import ClassIcon from "@material-ui/icons/Class";
import PersonIcon from "@material-ui/icons/Person";
import ApartmentIcon from "@material-ui/icons/Apartment";
import { SearchAutocomplete } from "../Components/SearchAutocomplete";
import { homeStyles } from "../Components/Style";
import { ModelPaperProps } from "../Components/Interfaces";

// Main page
function HomePage() {

    useEffect(() => {
        document.body.style.backgroundColor = "#bf5700";
        document.title = "Bevo's Course Guide";
    }, []);

    const classes = homeStyles();

    return (
        <Box display="flex" alignItems="center" flexDirection="column">
            <Typography align="center" className={classes.titleStyle}>BEVO'S COURSE GUIDE</Typography>
            <Box display="flex" alignItems="center" sx={{ width: 1200 }} marginBottom={-8} flexDirection="column">
                <SearchAutocomplete />
            </Box>
            <Box display="flex" justifyContent="center" flexWrap="wrap">
                <ModelPaperLink
                    Icon={ClassIcon}
                    name="Courses"
                    link="/Courses" />
                <ModelPaperLink
                    Icon={PersonIcon}
                    name="Professors"
                    link="/Professors" />
                <ModelPaperLink
                    Icon={ApartmentIcon}
                    name="Departments"
                    link="/Departments" />
            </Box>
        </Box>
    )
}


// Card links to model pages
function ModelPaperLink({ Icon, name, link }: ModelPaperProps) {

    const classes = homeStyles();

    return (
        <div className={classes.modelsPaperDiv}>
            <Paper elevation={3} className={classes.paper} style={{ height: "100%", width: "100%" }}>
                <CardActionArea href={link} style={{ height: "100%", width: "100%" }}>
                    <CardContent style={{ height: "268", width: "268" }}>
                        <Box sx={{
                            width: "100%",
                            height: "100%",
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            flexWrap: "nowrap",
                        }}>

                            <Box width="200px" height="190px" display="flex" justifyContent="center" alignItems="center">
                                <Icon style={{ fill: "#bf5700", width: "75%", height: "75%" }} />
                            </Box>

                            <Typography variant="h5">{name}</Typography>
                        </Box>
                    </CardContent>
                </CardActionArea>
            </Paper>
        </div>
    );
}

export default HomePage;
