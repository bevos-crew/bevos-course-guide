import json
import pandas as pd
import requests
import sys
import os
import ast
import copy
import re
import time
sys.path.append('../dataScripts/')
from getGoogleImage import getBuildingImage
from getCoordinates import getCoordinates
sys.path.append('../')
from database import db, create_app, Professor2, Course2, Department2, Section2


def print_departments(departments_list: list, index :int):
    if index < len(departments_list):
        #assert(type(departments_list[index].depart_name)        is str)
        #assert(type(departments_list[index].depart_picture)      is str)
        #assert(type(departments_list[index].depart_website)  is str)
        #print(index, departments_list[index].depart_abbrs)
        #assert(type(departments_list[index].depart_abbrs)    is list)
        #assert(type(departments_list[index].depart_additional_links)        is list)
        #assert(type(departments_list[index].depart_parent)       is str)
        #assert(type(departments_list[index].depart_people)     is list)
        try: print("deparment name:         " , departments_list[index].depart_name)
        except: pass
        try: print("department picture:          " , departments_list[index].depart_picture)
        except: pass
        try: print("department website:         " , departments_list[index].depart_website)
        except: pass
        try: print("department abbrs:     " , departments_list[index].depart_abbrs)
        except: pass
        try: print("department additional links:         " , departments_list[index].depart_additional_links)
        except: pass
        try: print("department parent:        " , departments_list[index].depart_parent)
        except: pass
        try: print("department people:   " , departments_list[index].depart_people)
        except: pass
        try: print("department all courses:   " , departments_list[index].depart_all_classes)
        except: pass
        try: print("department all courses nums:   " , departments_list[index].depart_all_class_nums)
        except: pass
        try: print("department all course name num dicts:   " , departments_list[index].depart_all_class_names_nums)
        except: pass
        try: print("department all professors:   " , departments_list[index].depart_all_professors)
        except: pass
        try: print("department address:   " , departments_list[index].depart_address)
        except: pass
        try: print("department long:   " , departments_list[index].depart_lat)
        except: pass
        try: print("department lat:   " , departments_list[index].depart_long)
        except: pass
    else: print("index not in provided departments list")

def get_classes_and_professors(abbr_list: list, df: pd.DataFrame()):
    courses_names_list           = []
    courses_nums_list            = []
    courses_names_nums_dict_list = []
    professors_list              = []
    for abbr in abbr_list:
        df_courses = df.loc[(df['Dept-Abbr'] == abbr)]
        #df_courses = df_courses.drop_duplicates(subset=['Title', 'Instructor'])
        #courses_list += list(set(df_courses['Title'].dropna().tolist()))
        professors_list += list(set(df_courses['Instructor'].dropna().tolist()))

        #get unique courses 
        courses_names_temp  = df_courses['Title'].tolist()
        course_numbers      = df_courses['Course Nbr'].tolist()
        course_abbrs        = df_courses['Dept-Abbr'].tolist()
        # course_nums_temp    = [a + ' ' + cn for a, cn in 
        #                 zip(course_abbrs, course_numbers) if 
        #                 ((a != None) and (cn != None))]
        course_nums_temp    = [a + ' ' + cn if 
                        ((a != None) and (cn != None)) else cn for a, cn in 
                        zip(course_abbrs, course_numbers)]

        course_name_nums_temp = []
        for na, nu in zip(courses_names_temp, course_nums_temp):
            if (na, nu) not in course_name_nums_temp:
                course_name_nums_temp.append((na, nu))
                courses_names_list.append(na)
                courses_nums_list.append(nu)
    courses_names_nums_dict_list = [nu + ' ' + na for na, nu in zip(courses_names_list, courses_nums_list)]

    #print(courses_names_nums_dict_list)  
    return courses_names_list, courses_nums_list, courses_names_nums_dict_list, professors_list

def getLongLat(building: str, df_courses: pd.DataFrame):
    param = 'UT Tower'
    if (not building == 'None') and (not building == ''):
        try: param = re.search("[A-Z]{3}", building).group()
        except: pass
    elif not df_courses.empty:
        try: 
            most_freq_building = df_courses['Building'].mode()
            param = most_freq_building[0]
        except: pass
        print(param)
    if param != 'UT Tower':
        param += ' University of Texas at Austin' #changed param from UT
    ans = getCoordinates(param)
    #print(ans["address"], ans["lat"], ans["lng"])
    return ans["address"], ans["lat"], ans["lng"]

def create_departments_list(dept_dict: dict, df: pd.DataFrame()): 
    all_departments_list = []
    for dept_name, values in dept_dict.items():
        #print(dept_name)
        #print(type(values))
        #print(values.keys())
        #print(values.values())
        #print(values['Links'], values['Abbr'], values['People'])
        name = dept_name
        additional_links = ['None']
        try: additional_links = copy.deepcopy(values['Links'])
        except: pass
        parent = 'None'
        try: parent = values['Parent']
        except: parent = dept_name
        abbrs  = ['None']
        try: abbrs = copy.deepcopy(values['Abbr'])
        except: pass
        #try to get from google books api
        picture = 'None'
        #print(parent + ' building')
        try: picture = getBuildingImage(name + ' building University of Texas at Austin')
        except: pass
        #parse people in values
        items = []
        try: items = values['People'].split('\n')
        except: pass
        #get website
        website = 'none'
        try:
            if '.edu' in items[-1]:
                potential_websites = items[-1].split('|')
                for ws in potential_websites:
                    if ('@' not in ws) and ('.edu' in ws):
                        website = 'www.' + ws.strip()
                        break
        except: pass
        #create list of dicst associated with people from items in 'People'
        department_people = []
        # for c, person in enumerate(items):
        #     if c < len(items) - 1:
        #         person_dict: dict = {}
        #         person_items = person.split(', ')
        #         person_dict['name'] = 'None'
        #         person_dict['title'] = 'None'
        #         person_dict['contact info'] = 'None'
        #         person_dict['office location'] = 'None'
        #         try:
        #             if ('office' not in person_items[0].lower()) and ('@' not in person_items[0]) and (not(any(i.isdigit() for i in person_items[0]))):
        #                 person_dict['name'] = person_items[0]
        #         except: pass
        #         try:
        #             for thing in person_items: 
        #                 if ('@' not in thing) and (not(any(i.isdigit() for i in thing))):
        #                     person_dict['title'] = thing
        #         except: pass
        #         try:
        #             for thing in person_items:
        #                 if '.' in thing and ((any(i.isdigit() for i in thing))):
        #                     person_dict['office location'] = thing
        #         except: pass
        #         try:
        #             for thing in person_items:
        #                 if ('-' in thing) or ('@' in thing) and ((any(i.isdigit() for i in thing))):
        #                     person_dict['contact info'] = thing
        #         except: pass
        #         department_people.append(person_dict)
        # method where we don't parse anything, just return list of people and their info
        try: department_people = items[:-1]
        except: pass
        #get courses and professors lists
        courses_list, courses_nums_list, courses_names_nums_dict_list, professors_list = get_classes_and_professors(abbrs, df)
        #get address, long, lat
        df_courses = pd.DataFrame()
        try: df.loc[(df['Dept-Abbr'] == abbrs[0])]
        except: pass
        head_building_str = ''
        try: head_building_str = values['People']
        except: pass
        address, long, lat = getLongLat(head_building_str, df_courses)
        #Create Department and add to list
        all_departments_list.append(Department2(
            depart_name             = name,
            depart_picture          = picture,
            depart_website          = website,
            depart_abbrs            = abbrs,
            depart_additional_links = additional_links,
            depart_parent           = parent,
            depart_people           = department_people,
            depart_all_classes      = courses_list,
            depart_all_class_nums   = courses_nums_list,
            depart_all_class_names_nums = courses_names_nums_dict_list,
            depart_all_professors   = professors_list,
            depart_address          = address,
            depart_lat              = lat,
            depart_long             = long
        ))
    return all_departments_list
                

def main():
    #OPEN DEPARTMENTS JSON
    start_time = time.time() #start timer
    file = open("../dataFiles/deptsDict.json", "r")
    contents = file.read()
    #keys: dept name, value keys: "Abbr", "People", "Parent", "Links"
    dept_dict = ast.literal_eval(contents)
    file.close()
    json_formatted_str = json.dumps(dept_dict, indent=2)
    #print(json_formatted_str)

    #CREATE DF FROM REPORT
    width = [5, 2, 3, 101, 6, 4, 7, 7, 31, 217, 26, 9, 101, 
            7, 5, 5, 4, 8, 4, 4, 4, 6, 183]
    header = ['Year', 'Semester', 'Dept-Abbr', 'Dept-Name', 'Course Nbr', 'Topic', 
            'Unique', 'Const Sect Nbr', 'Title', 'Crs Desc', 'Instructor', 
            'Instructor EID', 'Instructor Email', 'Days', 'From', 'To', 
            'Building',	'Room', 'Max Enrollment', 'Seats Taken', 'Total X-listings',
            'X-List Pointer', 'X-Listings']
    df = pd.read_fwf('../dataFiles/Current_Semester_Report', widths=width, dtype='str')
    df.columns=header
    df = df.iloc[1:,:]

    #CREATE DEPARTMENTS LIST
    departments_list = create_departments_list(dept_dict, df)
    #print(departments_list)
    #TESTING
    # for i, department in enumerate(departments_list):
    #     print_departments(departments_list, i)

    print("--- %s seconds ---" % (time.time() - start_time))

    #UPLOAD TO DATABASE
    print("Do you want to upload to database? If so, type 'y'. If not, anything else")
    #upload = input()

    upload = 'y'
    #print(os.getcwd())
    os.chdir('../')
    #print(os.getcwd())
    if upload == 'y':
        print('WILL UPLOAD TO DATABASE NOW')
        app = create_app()
        app.app_context().push()
        db.create_all()
        
        print("Do you want to add or delete? if delete, so 'delete', if add, anything else")
        #add_or_delete = input()
        add_or_delete = 'add'
        if add_or_delete == 'delete':
            db.session.query(Department2).delete()
        else:
            db.session.add_all(departments_list)
        try:
            db.session.commit()
        except:
            db.rollback()
    else: pass

if __name__ == "__main__":
    main()

#ADD LIST OF COURSES AND PROFESSORS FOR EACH DEPARTMENT