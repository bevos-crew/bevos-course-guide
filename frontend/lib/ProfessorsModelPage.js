"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _core = require("@material-ui/core");

var _react = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

require("./fonts.css");

var _styles = require("@material-ui/core/styles");

var _ProfessorInstancePage = _interopRequireDefault(require("./ProfessorInstancePage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

//professor attributes
function createData(name, professor_img_url, department, rating, difficulty, num_classes) {
  return {
    name,
    professor_img_url,
    department,
    rating,
    difficulty,
    num_classes
  };
}

const rows = [createData('Glenn Downing', "https://www.cs.utexas.edu/sites/default/files/downing_glenn.jpg", "Computer Science", 4.6, 3.5, 2), createData('Alison Norman', "https://www.cs.utexas.edu/sites/default/files/norman_alison_0.jpg", "Computer Science", 3.6, 4.6, 1), createData('Fares Fraij', "https://www.cs.utexas.edu/sites/default/files/fraij_fares.jpg", "Computer Science", 4.6, 3.5, 2), createData('Michael Scott', "https://www.cs.utexas.edu/sites/default/files/scott_mike.jpg", "Computer Science", 4.4, 3.6, 1), createData('Brendan Bowler', "https://astronomy.utexas.edu/images/cobalt_thumbs/image/3978/c9a61422b7383f8b5c686297f65a9c24.jpg", "Astronomy", 4.5, 3.2, 1), createData('John Markert', "https://cns.utexas.edu/images/cobalt_thumbs/image/1010/e7d3de6aac761fc936dd67fea8a2665c.jpg", "Physics", 4.7, 3.1, 5)]; //Routing between model page and specific professor instance pages

function ProfessorsModelPage() {
  return /*#__PURE__*/_react.default.createElement(_reactRouterDom.Switch, null, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/Professors/:professorName",
    component: _ProfessorInstancePage.default
  }), /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/Professors",
    component: ProfessorCardsGrid
  }));
}

function ProfessorCardsGrid() {
  const theme = (0, _styles.createTheme)({
    typography: {
      fontFamily: 'Bebas Neue',
      fontSize: 70
    }
  }); //styles

  const textStyle = {
    fontFamily: "Bungee Shade",
    fontSize: 70,
    display: "flex",
    justifyContent: "center",
    marginTop: "2%",
    marginBottom: "3%",
    color: "#bf5700"
  };
  const subTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 15,
    display: "flex",
    justifyContent: "center",
    marginLeft: "80%",
    color: "#9E9999",
    maxWidth: "600px"
  };
  const useStyle = (0, _styles.makeStyles)(() => ({
    button: {
      width: "120px",
      variant: "contained",
      background: "#bf5700",
      color: "white"
    },
    card: {
      variant: "outlined",
      width: 250,
      height: 425,
      borderRadius: 15,
      marginTop: "3%",
      marginBottom: "3%",
      background: "white",
      elevation: 6
    }
  }));
  const classes = useStyle();
  const Div = (0, _styles.styled)('div')(({
    theme
  }) => ({ ...theme.typography.button,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(1),
    position: "relative"
  })); //set background as white

  (0, _react.useEffect)(() => {
    document.body.style.background = 'white';
    document.title = "Professors";
  });
  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("h2", {
    style: textStyle
  }, "Professors"), /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      display: "flex",
      justifyContent: "space-around",
      flexWrap: "wrap"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Button, {
    className: classes.button
  }, "Name"), /*#__PURE__*/_react.default.createElement(_core.Button, {
    className: classes.button
  }, "Department"), /*#__PURE__*/_react.default.createElement(_core.Button, {
    className: classes.button
  }, "Ratings"), /*#__PURE__*/_react.default.createElement(_core.Button, {
    className: classes.button
  }, "Difficulty"), /*#__PURE__*/_react.default.createElement(_core.Button, {
    className: classes.button
  }, "Number of Classes")), /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      display: "flex",
      justifyContent: "space-around",
      flexWrap: "wrap"
    }
  }, rows.map(row => /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_core.Card, {
    className: classes.card
  }, /*#__PURE__*/_react.default.createElement(_core.CardActionArea, {
    component: _reactRouterDom.Link,
    to: {
      pathname: `/Professors/${row.name}`
    }
  }, /*#__PURE__*/_react.default.createElement(_styles.ThemeProvider, {
    theme: theme.typography
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, null, /*#__PURE__*/_react.default.createElement(Div, null, row.name))), /*#__PURE__*/_react.default.createElement(_core.CardMedia, {
    component: "img",
    height: "200px",
    width: "100px",
    image: row.professor_img_url,
    alt: row.name
  }), /*#__PURE__*/_react.default.createElement(_core.Typography, null, /*#__PURE__*/_react.default.createElement(Div, null, "department: ", /*#__PURE__*/_react.default.createElement(_reactRouterDom.Link, {
    to: {
      pathname: `/Departments/${row.department}`
    }
  }, row.department)), /*#__PURE__*/_react.default.createElement(Div, null, "rating: ", row.rating), /*#__PURE__*/_react.default.createElement(Div, null, "difficulty: ", row.difficulty), /*#__PURE__*/_react.default.createElement(Div, null, "number of classes: ", row.num_classes))))))), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: subTextStyle
  }, "Num pages: ", rows.length));
}

var _default = ProfessorsModelPage;
exports.default = _default;