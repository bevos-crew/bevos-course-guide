import React, { useEffect, useState, PureComponent } from "react";
import {
    Tooltip,
    ResponsiveContainer,
    RadialBarChart,
    RadialBar,
    Legend,
    Pie,
    PieChart,
    BarChart,
    CartesianGrid,
    XAxis,
    YAxis,
    Bar,
} from "recharts";
import axios from "axios";
import {
    Typography,
    Box
} from "@material-ui/core";

//storage for data from endpoint for each course
interface ICourse {
    name: string,
    number: string,
    professors: string[],
    department: string,
    days: string,
    sections: number,
    numName: string,
  }
  
  //storage for all courses
  interface ICourses {
    courses: any,
    num_pages: number,
    num_instances: number,
  }

  interface ISectionsToCourses {
      name: string;
      value: number;
      fill: string;
  }
  
  //create course data storage by returning interface
  function createSectionsToCourses(
    name: string,
    value: number,
    fill: string,
  ): ISectionsToCourses {
    return {
      name,
      value,
      fill,
    };
  }


const style = {
    top: '50%',
    right: 0,
    transform: 'translate(0, -50%)',
    lineHeight: '24px',
  };


function RenameLater() {
    const [data, setData] = useState(null)
    //get all instances of departments and place them onto cards
    const getCoursesInfo = async () => {
        // const sectionsToNumCourses = new Map<number, number>();
        let course = [];
        const sections: Array<ISectionsToCourses> = [];
        await axios.get(`https://api.bevoscourseguide.me/api/numberofsections`, {
            headers: {
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                course = response.data;
                let sectionsToNumCourses = new Map<string, number>();
                for (var i = 1; i <= 10; i++) {
                    sectionsToNumCourses.set(`${i}`, 0);
                }
                sectionsToNumCourses.set(">=11 sections", 0);
                course.forEach(currCourse => {
                    let sectionsRange = "";
                    let numSections = currCourse.sections;
                    if(numSections >= 1 && numSections <= 10){
                        sectionsRange = `${numSections}`;
                    } else if (numSections >= 11 && numSections <= 80) {
                        sectionsRange = ">=11 sections";
                    }
                    let currCount = sectionsToNumCourses.get(sectionsRange);
                    sectionsToNumCourses.set(sectionsRange, currCount + 1); 
                })
                let c = 0;
                let colors = ["#8884d8", "#83a6ed", "#8dd1e1", "#82ca9d", "#a4de6c", "#d0ed57", "#ffc658", "#FF3F33", "#F933FF", "#9333FF", "#33FF7A"];
                Array.from(sectionsToNumCourses.keys()).forEach(key => {
                    sections.push(createSectionsToCourses(key, sectionsToNumCourses.get(key), colors[c]));
                    c++;
                });
            
            })
            .catch(ex => {
                const error =
                    ex.response.status === 404
                        ? "Resource Not found"
                        : "An unexpected error has occurred";
                console.log(ex);
            });
        return {
            courses: sections,
        }
    }

    useEffect(() => {
        document.body.style.background = 'white';
        document.title = "Departments";

        const fetchData = async () => {
            const CoursesInfo = await getCoursesInfo();
            setData(CoursesInfo.courses);
            console.log(data);
        }
        fetchData();
    },[])
    return ( 
        <RadialBarChart innerRadius="10%" outerRadius="80%" barSize={50} width={800} height={700} data={data}>
            <RadialBar background label={{ position: 'insideStart', fill: '#000000' }} dataKey="value"/>
            <Legend iconSize={10} layout="vertical" verticalAlign="middle" wrapperStyle={style} />
        </RadialBarChart>   
    );
}
export default RenameLater;
