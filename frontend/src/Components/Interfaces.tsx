//container storing information for a single course
export interface ICourse {
    name: string;
    courseNumber: string;
    professors: string[];
    syllabus: string;
    desc: string;
    college: string;
    section: string[];
    department: string,
    pdf: string,
    numName: string
}

//storage for data from endpoint for each course
export interface ICourseCard {
    name: string,
    number: string,
    professors: string[],
    department: string,
    days: string,
    sections: number,
    numName: string,
}

//interface to store data from instance department endpoint
export interface IDepartment {
    name: string;
    picture: string;
    website: string;
    courses: string[];
    people: string[];
    professors: string[];
    abbrs: string[];
    courseNumbers: string[];
    address: string;
    long: string;
    lat: string;
    links: string[];
    parent: string;
}

//storage container for a single department's data
export interface IDepartmentCard {
    name: string,
    picture: string,
    parent: string,
    courses: number,
    professors: number,
    address: string,
}

//storage container for all data from endpoints for a specific professor
export interface IProfessor {
    name: string;
    department: string;
    picture: string;
    college: string;
    email: string;
    eid: string;
    courses: string[];
    courseNumbers: string[];
    numberOfPublications: number;
    publications: string[];
    resume: string;
    teachingDays: string;
    teachingTimes: string[];
    classBuildings: string[];
    pdf: string;
    courseNames: string[];
}

//storage for a single professor instance's data
export interface IProfessorCard {
    name: string,
    picture: string,
    department: string,
    email: string,
    courses: number,
    publications: number,

}

//storage container to store information about each section
export interface ISection {
    id: string;
    professor: string;
    days: string;
    starttime: string;
    endtime: string;
    seats: string;
    title: string;
    thumbnail: string;
    subtitle: string;
    isbn: string;
    listPrice: number;
    retailPrice: number;
    authors: string[];
}

//storage for all courses
export interface ICourses {
    courses: any,
    num_pages: number,
    num_instances: number,
}

//storage to contain multiple instances of departments
export interface IDepartments {
    departments: any,
    num_instances: number,
}

//storage container for multiple professor instances
export interface IProfessors {
    professors: any,
    num_instances: number,
}

//about us pages 
export interface IContributor {
    name: string;
    commits: number;
}

export interface IIssue {
    statistics: IStats;
}

export interface IStats {
    counts: ICounts;
}

export interface ICounts {
    closed: number;
}

//home page
export interface ModelPaperProps {
    Icon: any,
    name: string,
    link: string,
}

//about us interfaces 
export interface ToolsCardInterface {
    img: string;
    name: string;
    desc: string;
    link: string;
    width: string;
    height: string;
}

export interface TeamMemberProps {
    name: string;
    role: string;
    bio: string;
    img: string;
    commits: number;
    issues: number;
    tests: number;
}

export interface GitStatsProps {
    count: number;
    color: string;
    width: string;
    size: number;
}

export interface TotalGitStatsProps {
    commits: number;
    issues: number;
    tests: number;
}

export interface ToolList {
    tools: ToolsCardInterface[];
}

//interfaces for flight right's visuals
export interface IResponse {
    data: IAirline[];
}

export interface IAirportResponse {
    data: IAirport[];
}

export interface IAirline {
    airline_name: string;
    airline_fleet_size: number;
    id: number;
}

//interfaces for airportmap
export interface IMarker {
    markerOffset: number;
    name: string;
    city: string;
    state: string;
    coordinates: number[];
}

export interface Tooltip {
    name: string;
    city: string;
    state: string;
}

export interface IAirport {
    airport_name: string;
    airport_lat: number;
    airport_long: number;
    airport_city: string;
    airport_state: string;
}

//flight times visual's interfaces
export interface IFlightResponse {
    data: IFlight[];
}

export interface IFlight {
    flight_dept_time: string;
    flight_arr_time: string;
}

export interface ITime {
    name: string;
    dept: number;
    arr: number;
}


