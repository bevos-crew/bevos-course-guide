import pandas as pd 
import ast
import pickle
import json

def main():
    width = [5, 2, 3, 101, 6, 4, 7, 7, 31, 217, 26, 9, 101, 
            7, 5, 5, 4, 8, 4, 4, 4, 6, 183]
    header = ['Year', 'Semester', 'Dept-Abbr', 'Dept-Name', 'Course Nbr', 'Topic', 
            'Unique', 'Const Sect Nbr', 'Title', 'Crs Desc', 'Instructor', 
            'Instructor EID', 'Instructor Email', 'Days', 'From', 'To', 
            'Building',	'Room', 'Max Enrollment', 'Seats Taken', 'Total X-listings',
            'X-List Pointer', 'X-Listings']
    df = pd.read_fwf('../dataFiles/Current_Semester_Report', widths=width, dtype='str')
    df.columns=header

    file = open("../dataFiles/deptsDict.json", "r")
    contents = file.read()
    #keys: dept name, value keys: "Abbr", "People", "Parent", "Links"
    dept_dict = ast.literal_eval(contents)
    file.close()

    prof_college_dict = {}

    for index, rec in df.iterrows():
        for dept_name, values in dept_dict.items():
            if rec['Dept-Abbr'] in values['Abbr']:
                try: prof_college_dict[rec['Instructor']] = values['Parent']
                except: prof_college_dict[rec['Instructor']] = 'None'
                break
    
    with open('../dataFiles/profCollegeDict.txt', 'w') as f:
        json.dump(prof_college_dict, f)

if __name__ == "__main__":
    main()