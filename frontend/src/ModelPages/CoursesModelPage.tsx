import React, { useEffect, useState } from "react";
import '../fonts.css'
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, Paper, Box, TablePagination, TableSortLabel, makeStyles, MenuItem, Select, ListItemText, Checkbox, OutlinedInput, FormControl, InputLabel } from '@material-ui/core'
import { Route, Switch, Link } from "react-router-dom";
import CourseInstancePage from "../InstancePages/CourseInstancePage";
import axios from "axios";
import SearchBar from "material-ui-search-bar";
import { ICourseCard, ICourses } from "../Components/Interfaces"
import { titleStyle, courseStyles } from '../Components/Style'
import {getHighlightedText} from '../Components/Repeats'

//create course data storage by returning interface
function createCourse(
  name: string,
  number: string,
  professors: string[],
  department: string,
  days: string,
  sections: number,
  numName: string,
): ICourseCard {
  return {
    name,
    number,
    professors,
    department,
    days,
    sections,
    numName
  };
}

type Order = 'asc' | 'desc';


//create a top cell to describe what data will be in that column
interface HeadCell {
  disablePadding: boolean;
  id: keyof ICourseCard;
  label: string;
  numeric: boolean;
}

//data within a course row, each column is data that is sortable/filterable 
const headCells: readonly HeadCell[] = [
  {
    id: 'name',
    numeric: false,
    disablePadding: false,
    label: 'Courses',
  },
  {
    id: 'professors',
    numeric: false,
    disablePadding: false,
    label: 'Professor',
  },
  {
    id: 'department',
    numeric: false,
    disablePadding: false,
    label: 'Department',
  },
  {
    id: 'sections',
    numeric: true,
    disablePadding: false,
    label: 'Sections',
  },
  {
    id: 'days',
    numeric: false,
    disablePadding: false,
    label: 'Days',
  },
];

//information about sorting of rows
interface EnhancedTableProps {
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof ICourseCard) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

//setting up table heads to include ability to sort
function EnhancedTableHead(props: EnhancedTableProps) {
  const { order, orderBy, rowCount, onRequestSort } =
    props;

  const createSortHandler =
    (property: keyof ICourseCard) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      {headCells.map((headCell) => (
        <TableCell
          key={headCell.id}
          align={headCell.id === 'name' ? 'left' : 'right'}
          padding='normal'
          sortDirection={orderBy === headCell.id ? order : false}
        >
          <TableSortLabel
            active={orderBy === headCell.id}
            direction={orderBy === headCell.id ? order : 'asc'}
            onClick={createSortHandler(headCell.id)}
          >
            {headCell.label}
            {orderBy === headCell.id ? (
              <Box component="span">
              </Box>
            ) : null}
          </TableSortLabel>
        </TableCell>
      ))}
    </TableHead>
  );
}



//function specifying route to instance pages
function CoursesModelPage() {
  return (
    <Switch>
      <Route exact path="/Courses/:courseName" component={CourseInstancePage} />
      <Route exact path="/Courses" component={EnhancedTable} />
    </Switch>

  )
}

//creating table on course model page and adding table and data to it
function EnhancedTable() {
  let errorText: string[] = [];
  const [courses, setCourses] = React.useState([]);
  const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof ICourseCard>('name');
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(25);
  const [count, setCount] = React.useState(0);
  const [totalPages, setTotalPages] = React.useState(0);
  const [search, setSearch]: [string, (search: string) => void] = useState("");
  const [query, setQuery]: [string, (query: string) => void] = useState<string>("");
  const [daysFilter, setDaysFilter]: [string[], (daysFilter: string[]) => void] = useState<string[]>([]);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof ICourseCard,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  //get data from endpoint to fill table
  const getCoursesInfo = async () => {
    let courses = [];
    let stringDaysFilter: string = "";
    ["M", "T", "W", "TH", "F"].forEach((day) => {
      if (daysFilter.indexOf(day) > -1)
        stringDaysFilter += day;
    })
    console.log(stringDaysFilter);
    await axios.get<ICourses>(`https://api.bevoscourseguide.me/api/allcourses?sort_by=${orderBy}.${order}&page=${page + 1}&per_page=${rowsPerPage}&filter=${stringDaysFilter}&query=${query}`, {
      headers: {
        "Content-Type": "application/json"
      },
    })
      .then(response => {
        response.data.courses.forEach(course => {
          courses.push(createCourse(course.name, course.number, course.professors.size > 1 ? course.professors.slice(0, 1) : course.professors, course.college, course.days, course.section.length, course["num name"]))
        });
        setCount(response.data.num_instances);
        setTotalPages(response.data.num_pages);
      })
      .catch(ex => {
        console.log(ex)
        const error =
          ex.response.status === 404
            ? "Resource Not found"
            : "An unexpected error has occurred";
        console.log(ex);
        errorText.push(error);
      });
    return {
      courses: courses,
    }
  }


  useEffect(() => {
    document.body.style.background = 'white';
    document.title = 'Courses';

    const fetchData = async () => {
      const CoursesInfo = await getCoursesInfo();
      setCourses(CoursesInfo.courses);
      setLoading(false);
    }
    setRowsPerPage(rowsPerPage)
    setPage(page);
    setOrder(order);
    setOrderBy(orderBy);
    fetchData();

  }, [page, rowsPerPage, order, orderBy, query, daysFilter]);


  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - courses.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  }

  //if user changes rows per page, is set to change number of rows in table
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  //setting filter as user selects days
  const handleDaysChange = (event) => {
    const {
      target: { value },
    } = event;
    setDaysFilter(
      typeof value === 'string' ? value.split(',') : value
    );
  }

  const classes = courseStyles();

  if (loading) {
    return (
      <>
        <Typography variant="h3" align="center" style={{ fontWeight: 600, marginBottom: 20, marginTop: 60 }}>Loading...</Typography>
      </>
    );
  };

  const days = ["M", "T", "W", "TH", "F"];

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 1;

  return (
    <Box sx={{ width: '100%' }}>
      <Paper className={classes.paper}>
        <Box sx={{ width: "95%", display: "flex", flexDirection: "column", alignItems: "center" }}>
          <Typography style={titleStyle}>Courses</Typography>

          <Box sx={{ width: "80%", display: "flex", flexDirection: "row", alignItems: "center" }}>
            <SearchBar
              value={search}
              className={classes.searchBar}
              placeholder="Search Courses"
              onChange={(newString) => {
                setSearch(newString);
              }}
              onRequestSearch={() => {
                setQuery(search);
              }}
              onCancelSearch={() => {
                setQuery("");
              }}
            />
            <FormControl className={classes.formControl} variant="standard">
              <InputLabel id="daysLabel">&nbsp;&nbsp;&nbsp;&nbsp;Days</InputLabel>
              <Select
                labelId="daysLabel"
                multiple
                input={<OutlinedInput label="Days" />}
                value={daysFilter}
                onChange={handleDaysChange}
                renderValue={(selected: any) => selected.join(', ')}
                MenuProps={{
                  PaperProps: {
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                      width: 250,
                    },
                  },
                  anchorOrigin: {
                    vertical: "bottom",
                    horizontal: "left"
                  },
                  transformOrigin: {
                    vertical: "top",
                    horizontal: "left"
                  },
                  getContentAnchorEl: null
                }}
              >
                {days.map((day) => (
                  <MenuItem key={day} value={day}>
                    <Checkbox checked={daysFilter.indexOf(day) > -1} />
                    <ListItemText primary={day} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>

          <TableContainer component={Paper}>
            <Table
              max-width='700'
              aria-labelledby="tableTitle"
              size='medium'
            >
              <EnhancedTableHead
                order={order}
                orderBy={orderBy}
                onRequestSort={handleRequestSort}
                rowCount={courses.length}
              />
              <TableBody>
                {courses.map((course, index) => {
                  return (
                    <TableRow>
                      <TableCell align="left" component="th" scope="row">
                        <Link to={{ pathname: `/Courses/${course.numName}` }}>
                          {getHighlightedText(course.numName, query)}
                        </Link>
                      </TableCell>
                      {/* <TableCell align="right"> {course.number} </TableCell> */}
                      <TableCell align="right"> <Link to={{ pathname: `/Professors/${course.professors}` }}> {getHighlightedText(course.professors.toString(), query)}  </Link></TableCell>
                      <TableCell align="right"> <Link to={{ pathname: `/Departments/${course.department}` }}> {getHighlightedText(course.department, query)} </Link> </TableCell>
                      <TableCell align="right">{getHighlightedText(course.sections.toString(), query)}</TableCell>
                      <TableCell align="right">{getHighlightedText(course.days, query)}</TableCell>
                    </TableRow>
                  );
                })}
                {emptyRows > 0 && (
                  <TableRow>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, 50, 100]}
              component="div"
              count={count}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </TableContainer>
        </Box>
      </Paper>
    </Box>
  );
}

export default CoursesModelPage;

