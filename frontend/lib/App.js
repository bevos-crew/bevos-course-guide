"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

require("./App.css");

var _HomePage = _interopRequireDefault(require("./HomePage"));

var _reactRouterDom = require("react-router-dom");

var _NavBar = _interopRequireDefault(require("./NavBar"));

var _ProfessorsModelPage = _interopRequireDefault(require("./ProfessorsModelPage"));

var _CoursesModelPage = _interopRequireDefault(require("./CoursesModelPage"));

var _AboutUsPage = _interopRequireDefault(require("./AboutUsPage"));

var _DepartmentModelPage = _interopRequireDefault(require("./DepartmentModelPage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function App() {
  return /*#__PURE__*/_react.default.createElement(_reactRouterDom.BrowserRouter, null, /*#__PURE__*/_react.default.createElement(_NavBar.default, null), /*#__PURE__*/_react.default.createElement(_reactRouterDom.Switch, null, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/"
  }, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Redirect, {
    to: "/Home"
  })), /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/Home"
  }, /*#__PURE__*/_react.default.createElement(_HomePage.default, null)), /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    path: "/Professors"
  }, /*#__PURE__*/_react.default.createElement(_ProfessorsModelPage.default, null)), /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    path: "/Courses"
  }, /*#__PURE__*/_react.default.createElement(_CoursesModelPage.default, null)), /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    path: "/Departments"
  }, /*#__PURE__*/_react.default.createElement(_DepartmentModelPage.default, null)), /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/AboutUs"
  }, /*#__PURE__*/_react.default.createElement(_AboutUsPage.default, null))));
}

var _default = App;
exports.default = _default;