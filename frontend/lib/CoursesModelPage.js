"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

require("./fonts.css");

var _core = require("@material-ui/core");

var _reactRouterDom = require("react-router-dom");

var _CourseInstancePage = _interopRequireDefault(require("./CourseInstancePage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function createData(name, number, professor, department, rating, flags) {
  return {
    name,
    number,
    professor,
    department,
    rating,
    flags
  };
} // array that contains data that we want to show in the table


const rows = [createData('Software Engineering', 'CS373', 'Glenn Downing', 'Computer Science', 4.0, 'writing'), createData('Object Oriented Programming', 'CS371G', 'Glenn Downing', 'Computer Science', 4.0, 'none'), createData('Operating Systems', 'CS439', 'Alison Norman', 'Computer Science', 4.0, 'independent inquiry'), createData('Elements Software Engineer I', 'CS330E', 'Fares Fraij', 'Computer Science', 4.0, 'none'), createData('Algorithms and Complexity', 'CS331', 'Fares Fraij', 'Computer Science', 4.0, 'quantitative reasoning'), createData('Data Structures', 'CS314', 'Michael Scott', 'Computer Science', 4.0, 'quantitative reasoning'), createData('Astronomical Data Analysis', 'AST382D', 'Brendan Bowler', 'Astronomy', 4.0, 'none'), createData('Mechanics', 'PHY301', 'John Markert', 'Physics', 4.0, 'quantitative reasoning'), createData('Introduction to Research-FRI', 'PHY208F', 'John Markert', 'Physics', 4.0, 'none')];

function CoursesModelPage() {
  return /*#__PURE__*/_react.default.createElement(_reactRouterDom.Switch, null, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/Courses/:courseNumber",
    component: _CourseInstancePage.default
  }), /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/Courses",
    component: DisplayTable
  }));
} // makes the background white upon refresh


function DisplayTable() {
  (0, _react.useEffect)(() => {
    document.body.style.background = 'white';
    document.title = 'Courses';
  });
  const textStyle = {
    fontFamily: "Bungee Shade",
    fontSize: 70,
    display: "flex",
    justifyContent: "center",
    marginTop: "2%",
    marginBottom: "3%",
    color: "#bf5700"
  };
  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("h2", {
    style: textStyle
  }, "Courses"), /*#__PURE__*/_react.default.createElement(_core.TableContainer, {
    component: _core.Paper
  }, /*#__PURE__*/_react.default.createElement(_core.Table, {
    "aria-label": "simple table"
  }, /*#__PURE__*/_react.default.createElement(_core.TableHead, null, /*#__PURE__*/_react.default.createElement(_core.TableRow, null, /*#__PURE__*/_react.default.createElement(_core.TableCell, null, "Courses"), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, "Course Number"), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, "Professor"), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, "Department"), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, "Ratings"), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, "Flags"))), /*#__PURE__*/_react.default.createElement(_core.TableBody, null, rows.map(row => /*#__PURE__*/_react.default.createElement(_core.TableRow, null, /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    component: "th",
    scope: "row"
  }, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Link, {
    to: {
      pathname: `/Courses/${row.number}`
    }
  }, row.name)), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, " ", row.number, " "), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, " ", /*#__PURE__*/_react.default.createElement(_reactRouterDom.Link, {
    to: {
      pathname: `/Professors/${row.professor}`
    }
  }, " ", row.professor, "  ")), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, " ", /*#__PURE__*/_react.default.createElement(_reactRouterDom.Link, {
    to: {
      pathname: `/Departments/${row.department}`
    }
  }, " ", row.department, " "), " "), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, row.rating), /*#__PURE__*/_react.default.createElement(_core.TableCell, {
    align: "right"
  }, row.flags)))))));
}

var _default = CoursesModelPage;
exports.default = _default;