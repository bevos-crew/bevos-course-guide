import pandas as pd
import requests
from PyPDF4 import PdfFileReader
import io
import string
import re
import time
import json

df = pd.read_csv('../dataFiles/Current_Semester_Report', sep='\s*\t\s*', header=0, engine="python", dtype={"Course Nbr": object, 'Unique': object})

newCols = {"Dept-Abbr": "Dept", "Dept-Name": "DeptName", "Course Nbr": "CourseNum", "Crs Desc": "Desc"}
df = df.rename(columns=newCols)


def get_isbn(unique):

    course = df[df['Unique'] == unique]
    dept = course['Dept'].to_string(index=False)
    c_num = course['CourseNum'].to_string(index=False)

    params = { "year": 2021, "semester": 9, "department": dept, "course_number": c_num, "unique": unique, "course_type": "In Residence", "search": "Search" }

    try:
        response = requests.get("http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/", params)
    except:
        return ""

    page = response.text

    resDownload = page.find("download/")
    syllabusDownload = page.rfind("download/")

    if syllabusDownload == -1:
        return ""

    resURL = "http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/" + page[resDownload : page.find('\"', resDownload)]

    syllabusURL = "http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/" + page[syllabusDownload : page.find('\"', syllabusDownload)]

    print(syllabusURL)

    try:
        syll = requests.get(syllabusURL)
    except:
        return ""

    syllf = io.BytesIO(syll.content)

    try:
        syllReader = PdfFileReader(syllf)
    except:
        return ""

    pageNum = 0
    isbn = ""
    didFind = False
    try:
        while True:
            contents = syllReader.getPage(pageNum).extractText()
            isbnIndex = contents.find("ISBN")

            if isbnIndex > -1:
                didFind = True
                
                # Check for ISBN-13
                curIndex = isbnIndex + 4

                match = re.search("9[ \d\n\t-]{12,}", contents[curIndex:])
                if match != None:
                    isbn = re.sub('[^0123456789]', '', match.group())
                

                if len(isbn) == 13:
                    break
                elif len(isbn) > 13:
                    isbn = isbn[:13]
                    break
                
                match = re.search("(0|1)[ \d\n\t-]{9,}", contents[curIndex:])
                if match != None:
                    isbn = re.sub('[^0123456789]', '', match.group())

                if len(isbn) == 10:
                    break
                elif len(isbn) > 10:
                    isbn = isbn[:10]
                    break
            
            pageNum += 1

    except: # IndexError:
        pass

    if len(isbn) == 10 or len(isbn) == 13:
        return isbn
    elif didFind:
        print("COULD NOT PARSE ISBN FOR:", unique)
        return ""
    else:
        return ""


get_isbn('52840')
exit()

# Get all ISBNs and write them to a file

try:
    isbnDict = json.load(open("../isbnDict.txt"))
except:
    isbnDict = {}

startT = time.perf_counter()

courseCount = len(isbnDict)
foundISBN = sum(value != "NO ISBN" for value in isbnDict.values())

for uni in df["Unique"].iloc[:]:
    isbn = get_isbn(uni)
    if isbn != "":
        print(isbn)
        isbnDict[uni] = isbn
        foundISBN += 1
    else:
        isbnDict[uni] = "NO ISBN"

    courseCount += 1
    print(str(foundISBN) + "/" + str(courseCount))
    time.sleep(1)

with open("../isbnDict.txt", "w") as isbnFile:
    json.dump(isbnDict, isbnFile)

print("Time:", time.perf_counter() - startT)