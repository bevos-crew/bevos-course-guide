import requests
import re
from bs4 import BeautifulSoup
import pandas as pd
import json

df = pd.read_csv('../dataFiles/Current_Semester_Report', sep='\s*\t\s*', header=0, engine="python", dtype={"Course Nbr": object, 'Unique': object})

newCols = {"Dept-Abbr": "Dept", "Dept-Name": "DeptName", "Course Nbr": "CourseNum", "Crs Desc": "Desc"}
df = df.rename(columns=newCols)

depts = df["Dept"].drop_duplicates()

response = requests.get("https://registrar.utexas.edu/schedules/219/regrulesbydept/all.html#c%20s")

soup = BeautifulSoup(response.content, 'html.parser')

count = 0
deptDict = {}

def stripToAbbr(match):
    abbr = re.sub('[^A-Z ]', '', match)
    return abbr.strip()

def isPeople(text):
    reasons = []
    reasons.extend(re.findall("[A-Z]\d{4}", text))
    reasons.extend(re.findall("[A-Z]{3} \d\.\d+", text))
    reasons.extend(re.findall("\d+-\d+", text))
    return len(reasons) > 0

recentParent = ""

for h in soup.find_all(['h3', 'h4']):
    deptAbbrs = set()
    dept = {}
    # Handle mistake in website
    if "Doreen Lorenzo" in str(h):
        continue
    if "School of Design" in str(h):
        colDepts = str(h.find_next_siblings()[1])
    else:
        colDepts = str(h.find_next_siblings()[0])

    potential = re.findall("[A-Z][A-Z ][A-Z]", colDepts)
    potential.extend(re.findall("[^A-Z][^A-Z]M[^A-Z][^A-Z]", colDepts))
    potential.extend(re.findall("[^A-Z][^A-Z]J[^A-Z][^A-Z]", colDepts))
    potential.extend(re.findall("[^A-Z][A-Z][A-Z][^A-Z]", colDepts))

    for abbr in potential:
        if depts.str.contains(stripToAbbr(abbr)).any():
            deptAbbrs.add(stripToAbbr(abbr))

    dept["Abbr"] = list(deptAbbrs)
    try:
        people = h.find_next_siblings()[1]
        if "rules" in people.text:
            people = h.find_next_siblings()[2]

        if "h4" not in str(people.name) and isPeople(people.text):
            dept["People"] = people.text
        else:
            dept["People"] = ""
    except:
        dept["People"] = ""

    if "h3" in h.name:
        recentParent = h.get_text().strip()
    else:
        dept["Parent"] = recentParent

    links = []
    for tag in h.next_siblings:
        if "h3" in str(tag.name) or "h4" in str(tag.name):
            break
        
        text = str(tag)
        links.extend([match[1:-1] for match in re.findall("\"https://[^\"]+\"", text)])

    dept["Links"] = links

    deptDict[h.get_text().strip()] = dept
    count += len(deptAbbrs)

print(count)

with open("../dataFiles/deptsDict.json", "w") as isbnFile:
    json.dump(deptDict, isbnFile)
