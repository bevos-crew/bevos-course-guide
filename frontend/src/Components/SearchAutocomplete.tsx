import { Autocomplete } from "../../node_modules/@material-ui/lab";
import { TextField } from "../../node_modules/@material-ui/core";
import React, { useEffect, useState } from "../../node_modules/react";
import '../fonts.css';
import { makeStyles } from "../../node_modules/@material-ui/core/styles";
import axios from "../../node_modules/axios";

interface IACProps {
    fontSize?: number;
    borderRadius?: number;
    size?: string;
    initialVal?: string;
    setResults?: (results: IOption[]) => void;
}

interface IData {
    data: IOption[]
}

interface IOption {
    model: string;
    name: string;
    match: string;
}

const debounce = (fn: Function, ms = 300) => {
    let timeoutId: ReturnType<typeof setTimeout>;
    return function (this: any, ...args: any[]) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(() => fn.apply(this, args), ms);
    };
};

let cancelToken;
function SearchAutocomplete({ fontSize = 30, borderRadius = 50, size = "medium", initialVal = "" }: IACProps) {

    const [val, setVal]: [string, (val: string) => void] = useState("");

    const useStyles = makeStyles(() => ({
        textFieldStyle: {
            [`& fieldset`]: {
                borderRadius: borderRadius,
            },
            backgroundColor: "white",
            borderRadius: borderRadius,
            margin: 30,
        },
        label: {
            fontSize: fontSize,
            margin: 6,
            marginLeft: fontSize,
        },
        input: {
            fontSize: fontSize,
            margin: 5,
            marginLeft: 30,
            color: "black",
        },
    }));

    const classes = useStyles();
    const history = require("history").createBrowserHistory({ forceRefresh: true })

    const [options, setOptions]: [IOption[], (options: IOption[]) => void] = useState([]);
    const [inputVal, setInputVal]: [string, (inputVal: string) => void] = useState("");

    useEffect(() => {
        setInputVal(initialVal);
    }, []);

    const getSearchResults = (query: string) => {
        let searchResults = [];

        if (typeof cancelToken != typeof undefined) {
            cancelToken.cancel("Operation canceled due to new request.")
        }

        cancelToken = axios.CancelToken.source();

        axios.get<IData>(`https://api.bevoscourseguide.me/api/search?query=${query}&quantity=10&page=1`,
            { cancelToken: cancelToken.token }
        )
            .then((response) => {
                console.log(response.data)
                searchResults = response.data.data;
                setOptions(searchResults.sort((a, b) => a.model < b.model ? -1 : a.model > b.model ? 1 : 0));
            })
            .catch((ex) => {
                // const error =
                //     ex.response.status === 404
                //     ? "Resource Not found"
                //     : "An unexpected error has occurred";
                console.log(ex);
            });
    };

    const debounceSearchResults = debounce(getSearchResults, 1000);

    return (
        <Autocomplete
            value={val}
            freeSolo
            options={options}
            getOptionLabel={(option: IOption) => option.name ?? ""}
            groupBy={(option: IOption) => option.model}
            filterOptions={(options) => options}
            className={classes.textFieldStyle}
            fullWidth
            color="primary"
            size={size === "small" ? "small" : "medium"}
            classes={{ input: classes.input }}
            //onChange={ (_event, text) => setVal(text ? (text.target ? text.target.value : getOptionLabel(text)) : "") }
            onChange={(_event, text: any) => {
                console.log(text);
                let textString: string;
                //let textString: string = text ? (text.target ? text.target.value : text.name) : ""
                if (!text)
                    textString = ""
                else if (typeof text == "object")
                    textString = text["name"];
                else
                    textString = text

                setVal(textString);
                setInputVal(textString);
                //getSearchResults(textString);
                if (textString)
                    history.push("/Search/" + textString);
            }}
            inputValue={inputVal}
            onInputChange={(_event, value) => {
                setInputVal(value);
                if (value.length % 4 == 1)
                    debounceSearchResults(value);
            }}

            renderInput={params => (
                <TextField id="outlined-basic"
                    {...params}
                    placeholder="Search"
                    variant="outlined"
                    InputProps={{ ...params.InputProps }}
                />
            )}

        />
    )
}

export { SearchAutocomplete };
