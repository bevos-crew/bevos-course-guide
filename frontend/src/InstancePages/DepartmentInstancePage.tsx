import {
    Card, Typography, CardMedia,
    ListItem, ListItemText, Box, List
} from "@material-ui/core";
import React from "react";
import { useParams, Link } from "react-router-dom";
import '../fonts.css';
import axios from "axios";
import { IDepartment } from "../Components/Interfaces"
import { listStyle, imageStyle, titleTextStyle, subsectionTextStyle } from "../Components/Style";
import { DepartmentMap } from "../Components/Media";


export default DepartmentInstancePage;

// RouteParams is used to change from the model page to the instance page
interface RouteParams {
    departmentName: string;
}


//function to create container to store department data
function createDepartment(
    name: string,
    picture: string,
    website: string,
    courses: string[],
    people: string[],
    professors: string[],
    abbrs: string[],
    courseNumbers: string[],
    address: string,
    long: string,
    lat: string,
    links: string[],
    parent: string,
): IDepartment {
    return {
        name,
        picture,
        website,
        courses,
        people,
        professors,
        abbrs,
        courseNumbers,
        address,
        long,
        lat,
        links,
        parent,
    };
}


//function to create single list item of professor with link in a list
function RenderProfessorsList(props: { professor: string }) {
    const { professor } = props;
    return (
        <ListItem>
            <ListItemText >
                <Typography style={listStyle} component={Link} to={{ pathname: `/Professors/${professor}` }}>
                    {professor}
                </Typography>
            </ListItemText>
        </ListItem>
    )
}

//function to create single list item of courses with link in a list
function RenderCourseList(props: { course: string, index: number, courseNumbers: string[] }) {
    return (
        <ListItem>
            <ListItemText  >
                <Typography style={listStyle} component={Link} to={{ pathname: `/Courses/${props.courseNumbers[props.index]} ${props.course}` }}>
                    {props.courseNumbers[props.index] + " " + props.course}
                </Typography>
            </ListItemText>
        </ListItem>
    )
}

/*
Overall page layout of the Department Instance Page including header, image, map
and any subsections with additional information
*/
function DepartmentInstancePage(this: any) {
    const { departmentName } = useParams<RouteParams>();

    const classes = imageStyle();


    let errorText: string[] = [];

    let [department, setDepartment] = React.useState(createDepartment("", "", "", [], [], [], [], [], "", "", "", [], ""));
    const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);
    const [error, setError]: [string, (error: string) => void] = React.useState("");
    const major = departmentName.replace(" ", "%20");
    //get data from endpoint for the department instance
    React.useEffect(() => {
        axios.get<IDepartment>(`https://api.bevoscourseguide.me/api/department/${major}`, {
            headers: {
                "Content-Type": "application/json"
            },
        })
            .then((response) => {
                let depart: any = response.data
                department = createDepartment(response.data.name, response.data.picture, response.data.website, depart["all classes"], response.data.people, depart["all professors"], response.data.abbrs, depart["all class numbers"], response.data.address, response.data.long, response.data.lat, depart["additional links"], response.data.parent);
                setDepartment(department);
                setLoading(false);
            }).catch(ex => {
                const error =
                    ex.response.status === 404
                        ? "Resource Not found"
                        : "An unexpected error has occurred";
                errorText.push(error);
            });
    }, []);

    //waiting for data to upload
    if (loading) {
        return (
            <>
                <Typography variant="h3" align="center" style={{ fontWeight: 600, marginBottom: 20, marginTop: 60 }}>Loading...</Typography>
                <Typography variant="h3" align="center" style={{ fontWeight: 600, marginBottom: 20, marginTop: 60 }}>{error}</Typography>
            </>
        );
    }


    return (

        <Box style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
            <h2 style={titleTextStyle}>{department.name}</h2>
            <Typography align='center' style={listStyle}>
                {department.parent}
            </Typography>
            <Card className={classes.card}>
                <CardMedia component="img"
                    height="100%"
                    width="100%"
                    image={department.picture}
                    alt="University of Texas building">
                </CardMedia>
            </Card>
            <Box alignItems="center" display="flex" flexDirection="column" >
                <Typography align='center' style={subsectionTextStyle}>
                    Information
                </Typography>
                <Typography align='center' style={listStyle} >
                    <a target="_blank" href={"https://" + department.website}>
                        Department Website
                    </a>
                </Typography>
                <Typography align='center' style={listStyle}>
                    Department Abbreviations:
                </Typography>
                {department.abbrs.map((prop: string) => (
                    <Typography style={listStyle}>
                        {prop}
                    </Typography>
                ))}
                <Typography align='center' style={subsectionTextStyle} >
                    Courses:
                </Typography>
                <List style={{ maxHeight: 200, width: 500, overflow: "auto", marginBottom: 40 }}>
                    {department.courses.map((course, index) => (
                        <RenderCourseList course={course} index={index} courseNumbers={department.courseNumbers} />
                    ))}
                </List>
                <Typography align='center' style={subsectionTextStyle}>
                    Professors:
                </Typography>
                <List style={{ maxHeight: 200, width: 500, overflow: "auto", marginBottom: 40 }}>
                    {department.professors.map((professor) => (
                        <RenderProfessorsList professor={professor} />
                    ))}
                </List>
                <Typography align='center' style={subsectionTextStyle}>
                    Department Faculty:
                </Typography>
                {department.people.map((prop) => (
                    <Typography style={listStyle} >
                        {prop}
                    </Typography>
                ))}
                <Typography align='center' style={subsectionTextStyle}>
                    Address:
                </Typography>
                <Typography align='center' style={listStyle}>
                    {department.address}
                </Typography>
                <DepartmentMap department={department} />
                <Typography align='center' style={subsectionTextStyle}>
                    Additional Links:
            </Typography>
                {department.links.map((prop: string) => (
                    <Typography style={listStyle}>
                        <a target="_blank" href={prop}>
                            {prop}
                        </a>
                    </Typography>
                ))}
            </Box>
        </Box>
    );
}