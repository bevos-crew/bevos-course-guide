import json
import pandas as pd
import requests
import sys
import os
import ast
import copy
import time
import pickle
from datetime import datetime
sys.path.append('../dataScripts/')
from getGoogleImage import getProfImage, getProfImage2
from aminerAndReddit import get_aminer_pubs
sys.path.append('../')
from database import db, create_app, Professor2, Course2, Department2, Section2

#IMPORT RELEVANT FILES
resumes_dict = dict()
with open('../dataFiles/resumes_dict.txt', 'rb') as f:
    resumes_dict = pickle.load(f)

file = open("../dataFiles/deptsDict.json", "r")
contents = file.read()
#keys: dept name, value keys: "Abbr", "People", "Parent", "Links"
dept_dict = ast.literal_eval(contents)
file.close()

def print_professors(professors_list: list, index :int):
    if index < len(professors_list):
        #assert(type(professors_list[index].depart_name)        is str)
        #assert(type(professors_list[index].depart_picture)      is str)
        #assert(type(professors_list[index].depart_website)  is str)
        #print(index, professors_list[index].depart_abbrs)
        #assert(type(professors_list[index].depart_abbrs)    is list)
        #assert(type(professors_list[index].depart_additional_links)        is list)
        #assert(type(professors_list[index].depart_parent)       is str)
        #assert(type(professors_list[index].depart_people)     is list)
        try: print("professor name:         " , professors_list[index].prof_name)
        except: pass
        try: print("professor picture:          " , professors_list[index].prof_picture)
        except: pass
        try: print("professor department:         " , professors_list[index].prof_department)
        except: pass
        try: print("professor department abbr:         " , professors_list[index].prof_dept_specific_abbr)
        except: pass
        try: print("professor college:         " , professors_list[index].prof_college)
        except: pass
        try: print("professor resume:     " , professors_list[index].prof_resume)
        except: pass
        try: print("professor num courses:         " , professors_list[index].prof_num_courses)
        except: pass
        try: print("professor courses list:         " , professors_list[index].prof_courses)
        except: pass
        try: print("professor course nums list:         " , professors_list[index].prof_course_nums)
        except: pass
        try: print("professor course name nums list:         " , professors_list[index].prof_course_names_nums)
        except: pass
        try: print("professor number of publications:        " , professors_list[index].prof_num_pubs)
        except: pass
        try: print("professor publications list:   " , professors_list[index].prof_pubs_list)
        except: pass
        try: print("professor email:   " , professors_list[index].prof_email)
        except: pass
        try: print("professor eid:   " , professors_list[index].prof_eid)
        except: pass
        try: print("professor class buildings:   " , professors_list[index].prof_class_buildings)
        except: pass
        try: print("professor teaching days:   " , professors_list[index].prof_teaching_days)
        except: pass
        try: print("professor teaching times:   " , professors_list[index].prof_teaching_times)
        except: pass
    else: print("index not in provided professors list")

def get_resume_url(unique, df):
    course = df[df['Unique'] == unique]
    dept = course['Dept-Abbr']
    c_num = course['Course Nbr']

    params = { "year": 2021, "semester": 9, "department": dept, "course_number": c_num, "unique": unique, "course_type": "In Residence", "search": "Search" }
    #print(params)
    try:
        response = requests.get("http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/", params=params, timeout=10)
        #print('response did get')
    except:
        #print('response did not get')
        return ""

    page = response.text

    resDownload = page.find("download/")
    if resDownload == -1:
        #print('res download failure')
        return ""

    resURL = "http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/" + page[resDownload : page.find('\"', resDownload)]
    return resURL

def get_prof_teaching_times(df: pd.DataFrame()):
    teaching_times = set()
    for i, section in df.iterrows():
        if (not pd.isna(section['From'])) and (not pd.isna(section['To'])):
            start = datetime.strptime(str(section['From']), "%H%M")
            start = start.strftime("%I:%M %p")
            #print("start: " + start)
            end = datetime.strptime(str(section['To']), "%H%M")
            end = end.strftime("%I:%M %p")
            #print("end: " + end)
            teaching_times.add(start + ' - ' + end)
    return list(teaching_times)

def get_department_name(abbr: str):
    #global dept_dict
    if abbr == None:
        return '', ''
    department = 'No Department for: ' + abbr
    college    = 'No College for: ' + abbr
    for dept_name, values in dept_dict.items():
        if abbr in values['Abbr']:
            department = dept_name
            try   : college    = values['Parent']
            except: pass
            return department, college
    return department, college

def create_professors_list(df: pd.DataFrame(), prof_college_dict: dict):
    professors_list      = []
    professor_names_list = []
    for index, rec in df.iterrows():
        if index % 100 == 0:
            print(index)
        if rec['Instructor'] not in professor_names_list:
            professor_names_list.append(rec['Instructor'])
            #name
            name = rec['Instructor']
            #department abbreviation
            dept_specific_abbr = ''
            try: dept_specific_abbr = rec['Dept-Abbr']
            except: pass
            #department - fix by getting from dept dict
            #department = rec['Dept-Name']
            department, college = get_department_name(rec['Dept-Abbr'])
            #college - must get from 
            #college = prof_college_dict.get(name)
            #picture
            picture = 'None'
            #try: picture = getProfImage2(name + ' ' + department)
            try: picture = getProfImage(name + ' ' + department)
            except: pass
            #resume - NOW with dictionary
            resume = 'none'
            #try: resume = get_resume_url(rec['Unique'], df)
            try: resume = resumes_dict.get(rec['Instructor'])
            except: pass
            #courses and num courses, list of course nums, list of dicts with
            #course name -> course num
            df_prof = df.loc[(df['Instructor'] == rec['Instructor'])]
            courses_names_temp = df_prof['Title'].tolist()
            course_numbers = df_prof['Course Nbr'].tolist()
            course_abbrs   = df_prof['Dept-Abbr'].tolist()
            # course_nums_temp = [a + ' ' + cn for a, cn in 
            #                 zip(course_abbrs, course_numbers) if 
            #                 ((a != None) and (cn != None))]
            course_nums_temp    = [a + ' ' + cn if 
                        ((a != None) and (cn != None)) else cn for a, cn in 
                        zip(course_abbrs, course_numbers)]
            courses = []
            course_nums = []
            course_name_nums_temp = []
            for na, nu in zip(courses_names_temp, course_nums_temp):
                if (na, nu) not in course_name_nums_temp:
                    course_name_nums_temp.append((na, nu))
                    courses.append(na)
                    course_nums.append(nu)
            #course_name_num_dicts_list = [{na:nu} for na, nu in zip(courses, course_nums)]
            courses_names_nums_dicts_list = [nu + ' ' + na for na, nu in zip(courses, course_nums)]
            num_courses = len(courses)
            #professor class buildings
            class_buildings = df_prof['Building'].dropna()
            class_rooms = df_prof['Room'].dropna()
            class_building_rooms = list(set([b + ' ' + r for b, r in zip(class_buildings, class_rooms)]))
            #professor teaching days
            teaching_days = ', '.join(list(set(df_prof['Days'].dropna().tolist())))
            #professor teaching times
            teaching_times = get_prof_teaching_times(df_prof)
            #email
            email = rec['Instructor Email']
            # publications
            num_pubs, pubs_list = get_aminer_pubs(name, rec['Unique'])
            #eid
            eid = rec['Instructor EID']

            professors_list.append(Professor2(
                prof_name = name,
                prof_picture = picture,
                prof_department = department,
                prof_dept_specific_abbr = dept_specific_abbr,
                prof_college = college, 
                prof_resume = resume,
                prof_num_courses = num_courses,
                prof_courses = courses,
                prof_course_nums = course_nums,
                prof_course_names_nums = courses_names_nums_dicts_list,
                prof_num_pubs = num_pubs,
                prof_pubs_list = pubs_list,
                prof_email = email,
                prof_eid = eid,
                prof_class_buildings = class_building_rooms,
                prof_teaching_days = teaching_days,
                prof_teaching_times = teaching_times
            ))
    return professors_list

def main():
    #CREATE PROFESSORS LIST
    start_time = time.time() #start timer
    #df = pd.read_csv('../dataFiles/Current_Semester_Report', sep='\s*\t\s*', header=0, engine="python", dtype={"Course Nbr": object, 'Unique': object})
    width = [5, 2, 3, 101, 6, 4, 7, 7, 31, 217, 26, 9, 101, 
            7, 5, 5, 4, 8, 4, 4, 4, 6, 183]
    header = ['Year', 'Semester', 'Dept-Abbr', 'Dept-Name', 'Course Nbr', 'Topic', 
            'Unique', 'Const Sect Nbr', 'Title', 'Crs Desc', 'Instructor', 
            'Instructor EID', 'Instructor Email', 'Days', 'From', 'To', 
            'Building',	'Room', 'Max Enrollment', 'Seats Taken', 'Total X-listings',
            'X-List Pointer', 'X-Listings']
    df = pd.read_fwf('../dataFiles/Current_Semester_Report', widths=width, dtype='str')
    df.columns=header
    df = df.iloc[1:,:]

    prof_college_dict: dict = {}
    with open("../dataFiles/profCollegeDict.txt", "r") as f:
        prof_college_dict = json.load(f)
    #print(prof_college_dict)

    professors_list = create_professors_list(df, prof_college_dict)
    
    #TESTING
    # for c, elem in enumerate(professors_list):
    #     print_professors(professors_list, c)

    print("--- %s seconds ---" % (time.time() - start_time))

    #UPLOAD TO DATABASE
    print("Do you want to upload to database? If so, type 'y'. If not, anything else")
    #upload = input()

    upload = 'y'
    #print(os.getcwd())
    os.chdir('../')
    #print(os.getcwd())
    if upload == 'y':
        print('WILL UPLOAD TO DATABASE NOW')
        app = create_app()
        app.app_context().push()
        db.create_all()
        
        print("Do you want to add or delete? if delete, so 'delete', if add, anything else")
        #add_or_delete = input()
        add_or_delete = 'add'
        if add_or_delete == 'delete':
            db.session.query(Professor2).delete()
        else:
            db.session.add_all(professors_list)
        try:
            db.session.commit()
        except:
            db.rollback()
    else: pass



if __name__ == "__main__":
    main()