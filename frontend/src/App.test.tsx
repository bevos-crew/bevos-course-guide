import React from "react";
import ReactDOM from 'react-dom';
import { Route, Router, MemoryRouter, useParams } from 'react-router-dom';
import { render, screen, waitFor, act } from '@testing-library/react';
import { shallow, configure } from 'enzyme';
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { createMemoryHistory } from "history";

import App from "./App";
import About from "./AdditionalPages/AboutUs/AboutUsPage"
import NavBar from "./Components/NavBar"
import HomePage from "./AdditionalPages/HomePage"
import CourseInstancePage from "./InstancePages/CourseInstancePage"
import CoursesModelPage from "./ModelPages/CoursesModelPage"
import DepartmentInstancePage from "./InstancePages/DepartmentInstancePage"
import DepartmentModelPage from "./ModelPages/DepartmentModelPage"
import ProfessorInstancePage from "./InstancePages/ProfessorInstancePage"
import ProfessorsModelPage from "./ModelPages/ProfessorsModelPage"
import AboutUsPage from "./AdditionalPages/AboutUs/AboutUsPage";
import { exitCode } from "process";

configure({ adapter: new Adapter() });

// jest.mock('react-router-dom', () => ({
//   ...jest.requireActual('react-router-dom'),
//   useParams: jest.fn(),
// }))
const flushPromises = () => new Promise(setImmediate);

jest.setTimeout(5000);

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom') as {}, // use actual for all non-hook parts
  useParams: () => ({
    courseName: 'SOWFTWARE%20ENGINEERING',
    courseNumber: 'CS%20373',
  }),
  useRouteMatch: () => ({ url: 'http://bevoscourseguide.me/Courses/courseNumber/courseName' }),
}));

//OLDER TESTS
// describe('Search Bar Check', () => {
//   it('Has a Search Bar', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with main search bar test');
//     }
//     const linkSearch = screen.getByRole("textbox", { name: /Search/i })
//     expect(linkSearch).toBeInTheDocument();
//   });
// });


describe('Button courses check', () => {
  it('Has a course button', async () => {
    render(<App />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with course button test');
    }
    const linkCourses = screen.getByRole("button", { name: /Courses/i })
    expect(linkCourses).toBeInTheDocument();
  });
});

describe('Button professors check', () => {
  it('Has a professor button', async () => {
    render(<App />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with professor button test');
    }
    const linkProfessors = screen.getByRole("button", { name: /Professors/i })
    expect(linkProfessors).toBeInTheDocument();
  });
});

describe('Button departments check', () => {
  it('Has a department button', async () => {
    render(<App />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with department button test');
    }
    const linkDepartments = screen.getByRole("button", { name: /Departments/i })
    expect(linkDepartments).toBeInTheDocument();
  });
});

describe('Button about us check', () => {
  it('Has a about us button', async () => {
    render(<App />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with about us button test');
    }
    const linkAboutUS = screen.getByRole("button", { name: /About Us/i })
    expect(linkAboutUS).toBeInTheDocument();
  });
});



// describe('Search box Testing', () => {
//   it('Has a Search Box', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with search box test');
//     }
//     const linklinkSearch = screen.getByRole("textbox", { name: /Search/i })
//     expect(linklinkSearch).toBeInTheDocument();
//   });
// });

describe('Page Links Courses', () => {
  it('Has Links to courses model pages', async () => {
    render(<App />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with link to courses test');
    }
    const linklinkCourses = screen.getByRole("link", { name: /Courses/i })
    expect(linklinkCourses).toBeInTheDocument();
  });

});

describe('Page Links Professors', () => {
  it('Has Links to professors model pages', async () => {
    render(<App />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with link to professors test');
    }
    const linklinkProfessors = screen.getByRole("link", { name: /Professors/i })
    expect(linklinkProfessors).toBeInTheDocument();
  });

});

describe('Page Links Departments', () => {
  it('Has Links to departments model pages', async () => {
    render(<App />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with link to departments test');
    }
    const linklinkDepartments = screen.getByRole("link", { name: /Departments/i })
    expect(linklinkDepartments).toBeInTheDocument();
  });
});

describe('Page Heading Testing', () => {
  it('Has Heading', async () => {
    render(<App />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with heading test');
    }
    const headingheadingAboutUs = screen.getByRole("heading", { name: /About Us/i })
    expect(headingheadingAboutUs).toBeInTheDocument();
  });
});

//NEWER TESTS - all adapted from cultural-foodies team/repository (thank you!)
// link: https://gitlab.com/cs373-group-11/cultured-foodies/-/blob/master/frontend/src/tests/frontend.test.tsx
// test App
describe('Render', () => {
  it('renders without crashing', async () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

// test Nav Bar component
describe('<Navigation />', () => {
  it('renders a Navigation Bar', async () => {
    const nav = shallow(<NavBar />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with nav test');
    }
    expect(nav).toMatchSnapshot();
  });
});

// test Views
describe("Render views", () => {
  // test Splash page
  test('Home Page', async () => {
    const component = shallow(<HomePage />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with home page test');
    }
    expect(component).toMatchSnapshot();
    expect(component).not.toBeUndefined;
    expect(component).toHaveLength(1);
  });
  // test About page
  test('About Page', async () => {
    const component = shallow(<AboutUsPage />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with about page test');
    }
    expect(component).toMatchSnapshot();
    expect(component).not.toBeUndefined;
  });
  // tests for model pages
  test('Courses model page', async () => {
    const component = shallow(<CoursesModelPage />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with course model page test');
    }
    expect(component).toMatchSnapshot();
    expect(component).not.toBeUndefined;
    expect(component).toHaveLength(1);
  });
  test('Professors model page', async () => {
    const component = shallow(<ProfessorsModelPage />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with professors model page test');
    }
    expect(component).toMatchSnapshot();
    expect(component).not.toBeUndefined;
    expect(component).toHaveLength(1);
  });
  test('Departments model page', async () => {
    const component = shallow(<DepartmentModelPage />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with departments model page test');
    }
    expect(component).toMatchSnapshot();
    expect(component).not.toBeUndefined;
    expect(component).toHaveLength(1);
  });
});

describe("Course instance Page", () => {
  // tests for instance pages
  test('Course instance page', async () => {
    // const component = shallow(<CourseInstancePage/>);
    // expect(component).toMatchSnapshot();
    // expect(component).not.toBeUndefined;
    // expect(component).toHaveLength(1);

    //jest.spyOn(ReactRouter, 'useParams').mockReturnValue({ courseNumber: 'CS 373', courseName: 'SOFTWARE ENGINEERING'});

    // jest.mock('react-router-dom', () => ({
    //   ...jest.requireActual('react-router-dom') as {}, // use actual for all non-hook parts
    //   useParams: () => ({
    //     courseName: 'SOWFTWARE%20ENGINEERING',
    //     courseNumber: 'CS%20373',
    //   }),
    //   useRouteMatch: () => ({ url: 'http://bevoscourseguide.me/Courses/courseNumber/courseName' }),
    // }));

    const component = render(<CourseInstancePage />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with course isntance page test');
    }
    expect(component).toMatchSnapshot();
    expect(component).not.toBeUndefined;
    //expect(component).toHaveLength(1);

    // const renderWithRouter = (component) => {
    //   const history = createMemoryHistory({
    //     initialEntries: ["Courses/courseNumber/courseName"],
    //     keyLength: 0,
    //   });
    //   const Wrapper = ({ children }) => (
    //     <Router history={history}>
    //       <Route path="Courses/ECE%20380L/10-DATA%20MINING">{children}</Route>
    //     </Router>
    //   );
    //   return {
    //     ...render(component, { wrapper: Wrapper }),
    //     history,
    //   };
    // };

    // const component = renderWithRouter(<CourseInstancePage/>);
    // expect(component).toMatchSnapshot({
    //   location: expect.any(Object),
    //   entries: expect.any(Object),
    //   baseElement: expect.any(baseElement)
    // });
  });
});

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom') as {}, // use actual for all non-hook parts
  useParams: () => ({
    professorName: 'BOYLE,%20C',
  }),
  useRouteMatch: () => ({ url: 'http://bevoscourseguide.me/Professors/professorName' }),
}));
describe("Professor instance page", () => {
  // component = null;
  // beforeAll(async () => {
  //   component = await render(<ProfessorInstancePage/>);
  // });
  test('Professor instance page', async () => {
    const component = render(<ProfessorInstancePage />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with professor instance page test');
    }
    expect(component).toMatchSnapshot();
    expect(component).not.toBeUndefined;
    //expect(component).toHaveLength(1);

    // jest.mock('react-router-dom', () => ({
    //   ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
    //   useParams: () => ({
    //     professorName: 'DOWNING, G',
    //   }),
    //   useRouteMatch: () => ({ url: '/Professors/professorName' }),
    // }));

    // const renderWithRouter = (component) => {
    //   const history = createMemoryHistory({
    //     initialEntries: ["Professors/professorName"],
    //     keyLength: 0,
    //   });
    //   const Wrapper = ({ children }) => (
    //     <Router history={history}>
    //       <Route path="Professors/:AARONSON,%20S">{children}</Route>
    //     </Router>
    //   );
    //   return {
    //     ...render(component, { wrapper: Wrapper}),
    //     history,
    //   };
    // };

    // const component = renderWithRouter(<ProfessorInstancePage/>);
    // expect(component).toMatchSnapshot();

  });
  // afterAll(done => {
  //   // Closing the DB connection allows Jest to exit successfully.
  //   done()
  // });
});

describe("Department Instance page", () => {
  test('Department instance page', async () => {
    // const component = shallow(<DepartmentInstancePage/>);
    // expect(component).toMatchSnapshot();
    // expect(component).not.toBeUndefined;
    // expect(component).toHaveLength(1);

    // jest.mock('react-router', () => ({
    //   ...jest.requireActual('react-router'), // use actual for all non-hook parts
    //   useParams: () => ({
    //     departmentName: 'Applied Learning and Development'
    //   }),
    //   useRouteMatch: () => ({ url: 'localhost:3000/Departments/departmentName' }),
    // }));

    // jest.spyOn(DepartmentInstancePage, 'useParams').mockReturnValue({ departmentName: 'Applied Learning and Development' })
    // const component = render(<DepartmentInstancePage />);
    // //const component = shallow(<DepartmentInstancePage/>);
    // expect(component).toMatchSnapshot();
    // expect(component).not.toBeUndefined;
    // expect(component).toHaveLength(1); 
    const renderWithRouter = (component) => {
      const history = createMemoryHistory({
        initialEntries: ["Departments/departmentName"],
        keyLength: 0,
      });
      const Wrapper = ({ children }) => (
        <Router history={history}>
          <Route path="Departments/Applied%20Learning%20and%20Development">{children}</Route>
        </Router>
      );
      return {
        ...render(component, { wrapper: Wrapper }),
        history,
      };
    };

    const component = renderWithRouter(<DepartmentInstancePage />);
    try {
      await flushPromises();
    } catch (e) {
      console.log('Error with departments instance page test');
    }
    expect(component).toMatchSnapshot();
  });

});

// // test sorting and filtering
describe('Test Sort/Filter Buttons', () => {
  it('Professor Model page sorting and filtering buttons', () => {
    const professorComponent = shallow(<ProfessorInstancePage />);
    expect(professorComponent.find("NAME"));
    expect(professorComponent.find("DEPARTMENT"));
    expect(professorComponent.find("PUBLICATIONS"));
    expect(professorComponent.find("EMAIL"));
    expect(professorComponent.find("NUMBER OF CLASSES"));

    // expect(screen.getByRole("button", { name: /NAME/i })).toBeInTheDocument();
    // expect(screen.getByRole("button", { name: /DEPARTMENT/i })).toBeInTheDocument();
    // expect(screen.getByRole("button", { name: /PUBLICATIONS/i })).toBeInTheDocument();
    // expect(screen.getByRole("button", { name: /EMAIL/i })).toBeInTheDocument();
    // expect(screen.getByRole("button", { name: /COURSES/i })).toBeInTheDocument();
  });

  it('Department Model page sorting and filtering buttons', () => {
    const professorComponent = shallow(<ProfessorInstancePage />);
    expect(professorComponent.find("NAME"));
    expect(professorComponent.find("DEPARTMENT"));
    expect(professorComponent.find("PUBLICATIONS"));
    expect(professorComponent.find("EMAIL"));
    expect(professorComponent.find("NUMBER OF CLASSES"));

    // expect(screen.getByRole("button", { name: /NAME/i })).toBeInTheDocument();
    // expect(screen.getByRole("button", { name: /PARENT/i })).toBeInTheDocument();
    // expect(screen.getByRole("button", { name: /ADDRESS/i })).toBeInTheDocument();
    // expect(screen.getByRole("button", { name: /PROFESSORS/i })).toBeInTheDocument();
    // expect(screen.getByRole("button", { name: /COURSES/i })).toBeInTheDocument();
  });
});

// //test main search page
// describe('Search page', () => {
//   it('Has a ...', () => {
//   });
// });

// //test search bars of model pages 
describe('model page search bars', () => {
  it('Courses model page has search bar', () => {
    const cm = shallow(<CoursesModelPage />);
    expect(cm.find("Search Courses"));
    //expect(screen.getByRole("textbox", { name: /Search/i })).toBeInTheDocument();
  });
  it('Professors model page has search bar', () => {
    const pm = shallow(<ProfessorsModelPage />);
    expect(pm.find("Search Professors"));
    //expect(screen.getByRole("textbox", { name: /Search/i })).toBeInTheDocument();
  });
  it('Departments model page has search bar', () => {
    const dm = shallow(<DepartmentModelPage />);
    expect(dm.find("Search Departments"));
    //expect(screen.getByRole("textbox", { name: /Search/i })).toBeInTheDocument();
  });
});

// //OLDER TESTS
// describe('Search Bar Check', () => {
//   it('Has a Search Bar', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with main search bar test');
//     }
//     const linkSearch = screen.getByRole("textbox", { name: /Search/i })
//     expect(linkSearch).toBeInTheDocument();
//   });
// });


// describe('Button courses check', () => {
//   it('Has a course button', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with course button test');
//     }
//     const linkCourses = screen.getByRole("button", { name: /Courses/i })
//     expect(linkCourses).toBeInTheDocument();
//   });
// });

// describe('Button professors check', () => {
//   it('Has a professor button', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with professor button test');
//     }
//     const linkProfessors = screen.getByRole("button", { name: /Professors/i })
//     expect(linkProfessors).toBeInTheDocument();
//   });
// });

// describe('Button departments check', () => {
//   it('Has a department button', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with department button test');
//     }
//     const linkDepartments = screen.getByRole("button", { name: /Departments/i })
//     expect(linkDepartments).toBeInTheDocument();
//   });
// });

// describe('Button about us check', () => {
//   it('Has a about us button', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with about us button test');
//     }
//     const linkAboutUS = screen.getByRole("button", { name: /About Us/i })
//     expect(linkAboutUS).toBeInTheDocument();
//   });
// });



// describe('Search box Testing', () => {
//   it('Has a Search Box', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with search box test');
//     }
//     const linklinkSearch = screen.getByRole("textbox", { name: /Search/i })
//     expect(linklinkSearch).toBeInTheDocument();
//   });
// });

// describe('Page Links Courses', () => {
//   it('Has Links to courses model pages', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with link to courses test');
//     }
//     const linklinkCourses = screen.getByRole("link", { name: /Courses/i })
//     expect(linklinkCourses).toBeInTheDocument();
//   });

// });

// describe('Page Links Professors', () => {
//   it('Has Links to professors model pages', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with link to professors test');
//     }
//     const linklinkProfessors = screen.getByRole("link", { name: /Professors/i })
//     expect(linklinkProfessors).toBeInTheDocument();
//   });

// });

// describe('Page Links Departments', () => {
//   it('Has Links to departments model pages', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with link to departments test');
//     }
//     const linklinkDepartments = screen.getByRole("link", { name: /Departments/i })
//     expect(linklinkDepartments).toBeInTheDocument();
//   });
// });

// describe('Page Heading Testing', () => {
//   it('Has Heading', async() => {
//     render(<App />);
//     try{
//       await flushPromises();
//     } catch(e){
//       console.log('Error with heading test');
//     }
//     const headingheadingAboutUs = screen.getByRole("heading", { name: /About Us/i })
//     expect(headingheadingAboutUs).toBeInTheDocument();
//   });
// });

// afterAll(done => {
//   done();
// });