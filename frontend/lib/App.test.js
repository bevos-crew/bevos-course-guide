"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _AboutUsPage = _interopRequireDefault(require("../src/AboutUsPage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _enzyme.configure)({
  adapter: new _enzymeAdapterReact.default()
});
describe("Render views", () => {
  test("About", () => {
    const aboutTest = (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_AboutUsPage.default, null));
    expect(aboutTest).toMatchSnapshot();
  });
});