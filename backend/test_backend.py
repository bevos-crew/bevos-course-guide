import unittest
import requests
import time

class RequestTests(unittest.TestCase):

    DEPARTMENT = ['abbrs', 'additional links', 'address', 'all classes',
                  'all professors', 'lat', 'long', 'name', 'parent',
                  'people', 'picture', 'website']

    PROFESSORS = ['class buildings', 'picture', 'courses', 'department',
                  'eid', 'email', 'name', 'num courses', 'num pubs',
                  'pubs list', 'resume', 'teaching days', 'teaching times']

    SECTION = ['book ISBN', 'book authors', 'book listPrice', 'book retailPrice',
               'book subtitle', 'book thumbnail', 'book title', 'days', 'endtime',
               'id', 'professor', 'room', 'seats', 'starttime']
    
    COURSES = ['college', 'days', 'desc', 'hours', 'num name', 'professors',
               'dept general', 'dept specific', 'section', 'syllabus']

    # Check if alldepartments api request has the correct information 
    def test_alldepartments(self):
        for _ in range(10):
            resp = requests.get('https://api.bevoscourseguide.me/api/alldepartments')
            if resp.status_code == 200:
                break
            time.sleep(1)

        self.assertEqual(resp.status_code, 200)
        data = resp.json()

        for department in data['departments']:
            for d in self.DEPARTMENT:
                self.assertIsNotNone(department[d])
        return

    def test_individual_department(self):
        name = 'Department of Computer Science'
        for _ in range(10):
            d_resp = requests.get(f'https://api.bevoscourseguide.me/api/department/{name}')
            if d_resp.status_code == 200:
                break
            time.sleep(1)
        self.assertEqual(d_resp.status_code, 200)

        d_data = d_resp.json()
        for d in self.DEPARTMENT:
            self.assertIsNotNone(d_data[d])
        return

    # Check if allprofessors api request has the correct information 
    def test_allprofessors(self):
        for _ in range(10):
            resp = requests.get('https://api.bevoscourseguide.me/api/allprofessors')
            if resp.status_code == 200:
                break

        self.assertEqual(resp.status_code, 200)
        data = resp.json()
        count = 0
        for professor in data['professors']:
            for d in self.PROFESSORS:
                self.assertIsNotNone(professor[d])
            count += 1
        return

    def test_individual_professor(self):
        name = 'DOWNING, G'
        for _ in range(10):
            p_resp = requests.get(f'https://api.bevoscourseguide.me/api/professor/{name}')
            if p_resp.status_code == 200:
                break
            time.sleep(1)
        self.assertEqual(p_resp.status_code, 200)

        p_data = p_resp.json()
        for p in self.PROFESSORS:
            self.assertIsNotNone(p_data[p])
        return

    def test_individual_section(self):
        name = '00420'
        for _ in range(10):
            s_resp = requests.get(f'https://api.bevoscourseguide.me/api/section/{name}')
            if s_resp.status_code == 200:
                break
            time.sleep(1)
        self.assertEqual(s_resp.status_code, 200)

        s_data = s_resp.json()
        for s in self.SECTION:
            self.assertIsNotNone(s_data[s])
        return

    # Check if allcourses api request has the correct information 
    def test_allcourses(self):
        for _ in range(10):
            resp = requests.get('https://api.bevoscourseguide.me/api/allcourses')
            if resp.status_code == 200:
                break
            time.sleep(1)

        self.assertEqual(resp.status_code, 200)
        data = resp.json()

        for course in data['courses']:
            for c in self.COURSES:
                self.assertIsNotNone(course[c])
        return

    def test_individual_course(self):
        number = 'ARI 310K'
        name   = 'DESIGN I'

        for _ in range(10):
            c_resp = requests.get(f'https://api.bevoscourseguide.me/api/course/{number} {name}')
            if c_resp.status_code == 200:
                break
            time.sleep(1)
        self.assertEqual(c_resp.status_code, 200)

        c_data = c_resp.json()
        for c in self.COURSES:
            self.assertIsNotNone(c_data[c])
        return

    def test_pagination_department(self):
        per_page = 10
        query = f'?sort_by=name&page=1&per_page={per_page}&query='
        for _ in range(10):
            resp = requests.get(f'https://api.bevoscourseguide.me/api/alldepartments{query}')
            if resp.status_code == 200:
                break
            time.sleep(1)

        self.assertEqual(resp.status_code, 200)
        data = resp.json()

        self.assertEqual(len(data['departments']), per_page)
        return

    def test_pagination_professor(self):
        per_page = 10
        query = f'?sort_by=publications.desc&page=2&per_page={per_page}&filter='
        for _ in range(10):
            resp = requests.get(f'https://api.bevoscourseguide.me/api/allprofessors{query}')
            if resp.status_code == 200:
                break
            time.sleep(1)

        self.assertEqual(resp.status_code, 200)
        data = resp.json()

        self.assertEqual(len(data['professors']), per_page)
        return

    def test_pagination_course(self):
        per_page = 10
        query = f'?sort_by=name.desc&page=1&per_page={per_page}&filter=MWF'
        for _ in range(10):
            resp = requests.get(f'https://api.bevoscourseguide.me/api/allcourses{query}')
            if resp.status_code == 200:
                break
            time.sleep(1)

        self.assertEqual(resp.status_code, 200)
        data = resp.json()

        self.assertEqual(len(data['courses']), per_page)
        return

    def test_search_pagination(self):
        per_page = 12
        query = f'?query=neuro&quantity={per_page}&page=1'
        for _ in range(10):
            resp = requests.get(f'https://api.bevoscourseguide.me/api/search{query}')
            if resp.status_code == 200:
                break
            time.sleep(1)

        self.assertEqual(resp.status_code, 200)
        data = resp.json()

        self.assertEqual(len(data['data']), per_page)

    def test_search(self):
        per_page = 12
        name = 'downing'
        query = f'?query={name}&quantity={per_page}&page=1'
        for _ in range(10):
            resp = requests.get(f'https://api.bevoscourseguide.me/api/search{query}')
            if resp.status_code == 200:
                break
            time.sleep(1)

        self.assertEqual(resp.status_code, 200)
        data = resp.json()

        for c in data['data']:
            self.assertTrue(name.upper() in c['match'].upper())


if __name__ == '__main__':
    unittest.main()