import React, { useEffect, useState, PureComponent } from "react";
import {
    Bar,
    BarChart,
    CartesianGrid,
    Legend,
    XAxis,
    YAxis,
    Tooltip,
    Radar,
    RadarChart,
    PolarGrid,
    PolarAngleAxis,
    PolarRadiusAxis,
    Label,
    ResponsiveContainer,
    RadialBarChart,
    RadialBar,
} from "recharts";
import axios from "axios";
import {
    Typography,
    Box
} from "@material-ui/core";




//storage container for a single department's data
interface IDepartment {
    depart_name: string,
    num_professors: number,
}

//create interface of department to store data for a single department
function createDepartment(
    depart_name: string,
    num_professors: number,
): IDepartment {
    return {
        depart_name,
        num_professors,
    };
}



function NumProfessorsRadarChart() {
    const [data, setData] = useState(null)
    //get all instances of departments and place them onto cards
    const getDepartmentsInfo = async () => {
        let depart = [];
        let departments: Array<IDepartment> = [];
        await axios.get(`https://api.bevoscourseguide.me/api/departmentdistribution`, {
            headers: {
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                console.log(response);
                depart = response.data;
                depart.forEach(department => {
                    if (department.num_professors > 50) {
                        departments.push(createDepartment(department.depart_name.replace("Department of ", "").replace("School of ", "").replace("College of ", ""), department.num_professors));
                    }
                });
            })
            .catch(ex => {
                const error =
                    ex.response.status === 404
                        ? "Resource Not found"
                        : "An unexpected error has occurred";
                console.log(ex);
            });
        return {
            departments: departments,
        }
    }

    useEffect(() => {
        document.body.style.background = 'white';
        document.title = "Departments";

        const fetchData = async () => {
            const DepartmentsInfo = await getDepartmentsInfo();
            setData(DepartmentsInfo.departments);
        }
        fetchData();
    }, [])
    return (
        <Box display="flex" flexDirection="column" alignItems="center">
            <ResponsiveContainer width={1400} height={700}>
                <RadarChart
                    outerRadius={300}
                    innerRadius={75}
                    data={data}
                    cx="50%"
                    cy="50%"
                >
                    <PolarGrid />
                    <PolarAngleAxis dataKey="depart_name" />
                    <PolarRadiusAxis angle={30} />
                    <Radar
                        name="Professors"
                        dataKey="num_professors"
                        stroke="#82ca9d"
                        fill="#82ca9d"
                        fillOpacity={0.3}
                    />
                    <Legend verticalAlign="top" />
                </RadarChart>
            </ResponsiveContainer>
        </Box>
    );
}
export default NumProfessorsRadarChart;
