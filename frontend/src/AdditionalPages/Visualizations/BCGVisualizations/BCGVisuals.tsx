import React, { useEffect, useState } from "react";

import {
    Box, Typography,
} from "@material-ui/core";

import NumProfessorsRadarChart from "./NumProfessorsRadarChart";
import DistributionofCoursesByTheNumberofTheirSections from "./DistributionofCoursesByTheNumberofTheirSections";
import NumCoursesPerCNSDepartment from "./NumCoursesPerCNSDepartment";

function BCGVisuals() {

    return (
        <Box display="flex" flexDirection="column" alignItems="center" width={1000}>
            
            <Typography variant="h5">Number of Professors Per Department Where There Are More Than 50 Professors</Typography>
            <NumProfessorsRadarChart />
            <Typography variant="h5">Distribution of Courses By The Number of Their Sections </Typography>
            <DistributionofCoursesByTheNumberofTheirSections />
            <Typography variant="h5">Number of Courses Per CNS Department</Typography>
            <NumCoursesPerCNSDepartment />
        </Box>
    );
}

export default BCGVisuals;
