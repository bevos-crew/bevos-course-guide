import React from "react";
import { useStyle } from "./Style";
import { Button, Menu, ListSubheader, MenuItem, Typography } from "@material-ui/core";

//highlight what user searches in search bar
export function getHighlightedText(text: string, highlight: string, ) {
    // Split on highlight term and include term into parts, ignore case
    const parts: string[] = text.split(new RegExp(`(${highlight})`, 'gi'));
    return <span> {parts.map((part, i) =>
        <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? { backgroundColor: "yellow", padding: "0.1em 0.0em" } : {}}>
            {part}
        </span>)
    } </span>;
}

//render sorting options menu for the sorting only buttons
export function RenderMenu(props: {
    button: string, alpha: Boolean, setOrderSorting: (ascending: number) => void, onRequestSort: (event: React.MouseEvent<unknown>, property) => void
}) {
    const { onRequestSort, setOrderSorting } = props;
    const [openMenu, setOpenMenu] = React.useState(null);
    const open = Boolean(openMenu);
    const handleClick = (click) => {
        setOpenMenu(click.currentTarget);
    };
    const handleClose = () => {
        setOpenMenu(null);
    };

    const createSortHandler =
        (property, i: number) => (event: React.MouseEvent<unknown>) => {
            onRequestSort(event, property);
            setOrderSorting(i);
        };

    const sort = [
        ["A-Z", "Z-A"],
        ["Increasing", "Decreasing"]
    ]
    const index = props.alpha === true ? 0 : 1;
    let category = props.button.toLowerCase();
    const classes = useStyle();
    return (
        < div >
            <Button className={classes.button} onClick={handleClick} id={props.button} aria-controls={"menu" + props.button}
                aria-haspopup="true" aria-expanded={open ? 'true' : undefined} >
                <Typography>
                    {props.button}
                </Typography>
            </Button>
            <Menu open={open} onClick={handleClose} id={"menu" + props.button} transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }} anchorEl={openMenu} aria-labelledby={props.button} style={{ maxHeight: 400, width: 350 }}>
                <ListSubheader>Sort</ListSubheader>
                {sort[index].map((sorting, i) =>
                    <MenuItem onClick={createSortHandler(category, i)}>
                        {sorting}
                    </MenuItem >
                )}
            </Menu >
        </div >
    )
}

//render button and menu for sorting and filtering button
export function RenderFiltering(props: { list: string[], category: string, onRequestFilter: (event: React.MouseEvent<unknown>, property: string) => void, setOrderSorting: (ascending: number) => void, onRequestSort: (event: React.MouseEvent<unknown>, property) => void }) {
    const [openMenu, setOpenMenu] = React.useState(null);
    const open = Boolean(openMenu);
    const handleClick = (click) => {
        setOpenMenu(click.currentTarget);
    };
    const handleClose = () => {
        setOpenMenu(null);
    };

    const sort = [
        "A-Z", "Z-A"
    ]
    const createFilterHandler =
        (property: string) => (event: React.MouseEvent<unknown>) => {
            props.onRequestFilter(event, property);
        };

    const createSortHandler =
        (property, i: number) => (event: React.MouseEvent<unknown>) => {
            props.onRequestSort(event, property);
            props.setOrderSorting(i);
        };

    const classes = useStyle();

    return (
        < div >
            <Button className={classes.button} onClick={handleClick} id={"Filter"} aria-controls={"menu filter"}
                aria-haspopup="true" aria-expanded={open ? 'true' : undefined} >
                <Typography>
                    {props.category}
                </Typography>
            </Button>
            <Menu open={open} onClick={handleClose} id={"menu filter"} transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }} anchorEl={openMenu} aria-labelledby={"Filter"} style={{ maxHeight: 400, width: 350 }}>
                {sort.map((sorting, item) =>
                    <div>
                        {item === 0 ? <ListSubheader>Sort</ListSubheader> : null}
                        <MenuItem onClick={createSortHandler(props.category.toLowerCase(), item)}>
                            {sorting}
                        </MenuItem >
                    </div>
                )}
                {props.list.map((filt, item) =>
                    <div>
                        {item === 0 ? <ListSubheader>Filter</ListSubheader> : null}
                        <MenuItem onClick={createFilterHandler(filt)}>
                            {filt}
                        </MenuItem >
                    </div>
                )}
            </Menu >
        </div >
    )
}