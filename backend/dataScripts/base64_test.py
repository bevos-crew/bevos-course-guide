from base64 import b64encode
import requests

def get_as_base64(url):
    return b64encode(requests.get(url).content).decode('utf-8')

#print(get_as_base64("http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/download/11305452"))
exp = get_as_base64("http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/download/11305452")

with open('../dataFiles/b64url_exp', 'w') as f:
    f.write(exp)