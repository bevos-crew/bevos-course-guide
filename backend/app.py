from flask import Flask, jsonify, request

from database import create_app, Professor2, Course2, Department2, Section2, Rating
from database import ProfessorSchema2, CourseSchema2, DepartmentSchema2, SectionSchema2
import requests
from base64 import b64encode
from sqlalchemy import desc, asc, func
import math
import random

(app, db) = create_app()

app.app_context().push()
app.debug = True

professor_schema2  = ProfessorSchema2()
course_schema2     = CourseSchema2()
department_schema2 = DepartmentSchema2()
section_schema2    = SectionSchema2()

#department_dict = {}
#f = open("dept.txt", "r")
#for line in f.readlines():
#    split = line.split(": ")
#    department_dict[split[0]] = split[1].strip()

course_dict = {}
f = open("courses.txt", "r")
for line in f.readlines():
    split = line.split('": "')
    course_dict[split[0]] = split[1].strip()

section_dict = []
f = open("num_sections_per_course.txt", "r")
for line in f.readlines():
    split = line.split('": ')
    section_dict.append({
        "course": split[0],
        "sections": int(split[1].strip())
    })

"""
    Default api to check if the API is up and running
"""
@app.route('/api')
def default():
    return jsonify({"hello fren": "this is the default api"})

"""
    Reformats the keys such that all of the keys do not have underscores
    for the frontend
"""
def reformat_keys(data: dict):
    new_data = {}
    for k in data.keys():
        splitted = k.split("_")
        temp = splitted[1]
        if len(splitted) > 2:
            temp = f"{temp} {splitted[2]}"
        new_data[temp] = data[k]

    return new_data

"""
    Used to get the information for the pdf information for the professor
    resumes
"""
def get_as_base64(url):
    return b64encode(requests.get(url).content).decode('utf-8')

"""
    Extracted common queries for the "all" endpoints
"""
def extract_pagination():
    sort_by  = request.args['sort_by']  if 'sort_by'  in request.args else 'name'
    page     = request.args['page']     if 'page'     in request.args else '1'
    per_page = request.args['per_page'] if 'per_page' in request.args else '10'

    sort_by  = sort_by.split('.')
    page     = int(page)
    per_page = min(50, int(per_page))

    return sort_by, page, per_page

"""
    Returns all of the section. 
"""
#@app.route('/api/allsections')
#def all_sections():
#    section = Section2.query.all()
#    return_val = []
#
#    for s in section:
#        data = section_schema2.dump(s)
#        new_data = reformat_keys(data)
#        return_val.append(new_data)
#
#    return jsonify({"section": return_val})

"""
    Returns one section given its section number
"""
@app.route('/api/section/<string:section_unique>')
def section(section_unique: str):
    section = Section2.query.get(section_unique)
    data = section_schema2.dump(section)
    new_data = reformat_keys(data)
    return jsonify(new_data)

"""
    Returns every course mapped to the number of sections
"""
@app.route('/api/numberofsections')
def number_of_sections():
    return jsonify(section_dict)

"""
    Returns a paginated portion of all courses
"""
@app.route('/api/allcourses')
def all_course():
    query = request.args['query'] if 'query' in request.args else ''
    query = query.upper()

    sort_by, page, per_page = extract_pagination()

    order = desc if len(sort_by) > 1 and sort_by[1] == 'desc' else asc
    days = request.args['filter'] if 'filter' in request.args else ''

    # filter out the courses that do not contain a particular query
    course_filter = Course2.query.filter(
        func.upper(Course2.course_num_name).contains(query) |
        func.upper(Course2.course_desc).contains(query) |
        func.upper(Course2.course_college).contains(query)
    )
    if days != '':
        # filter out the courses that do not contain a particular query
        # and are not under a specific day set
        course_filter = Course2.query.filter(
            (Course2.course_days == days) & (
                func.upper(Course2.course_num_name).contains(query) |
                func.upper(Course2.course_desc).contains(query) |
                func.upper(Course2.course_college).contains(query)
            )
        )

    # Order the rows by name, num professors, department, num sections, or days
    if sort_by[0] == 'name':
        filtered = order(Course2.course_num_name)
    elif sort_by[0] == 'professors':
        filtered = order(func.cardinality(Course2.course_professors))
    elif sort_by[0] == 'department':
        filtered = order(Course2.course_college)
    elif sort_by[0] == 'sections':
        filtered = order(func.cardinality(Course2.course_section))
    elif sort_by[0] == 'days':
        filtered = order(Course2.course_days)

    # Query to database
    courses = course_filter.order_by(filtered).paginate(page=page, per_page=per_page, error_out=False).items
    num_rows = course_filter.count()

    return_val = []
    
    # Reformat json keys
    for c in courses:
        data = course_schema2.dump(c)
        new_data = reformat_keys(data)
        return_val.append(new_data)

    num_pages = int(math.ceil(num_rows / per_page))
    return jsonify({"courses" : return_val, 'num_instances': num_rows, 'num_pages': num_pages})

"""
    Endpoint for a single instance of a course
"""
@app.route('/api/course/<string:number_name>')
def course(number_name: str):
    #MODIFICATION DUE TO MERGING OF PRIMARY KEYS FOR COURSES
    course = Course2.query.get(number_name)
    data = course_schema2.dump(course)
    new_data = reformat_keys(data)
    
    new_data['pdf'] = get_as_base64(new_data['syllabus'])

    return jsonify(new_data)

"""
    Endpoint to add a rating value (1 through 5) to a given course
"""
@app.route('/api/rating/<string:number_name>/<int:new_rating>')
def rating(number_name: str, new_rating: int):
    # Set minimum/maximum for rating integer values
    if new_rating < 1:
        new_rating = 1
    if new_rating > 5:
        new_rating = 5

    course = db.session.query(Course2).get(number_name)
    rating = db.session.query(Rating).get(number_name)

    rating.course_rating_amount += 1

    # Compute iterative mean
    first = (rating.course_rating_amount - 1) / (rating.course_rating_amount) * course.course_rating
    second = (1 / rating.course_rating_amount) * new_rating

    course.course_rating = first + second
    rating.course_rating = first + second

    # Commit the new values for ratings
    db.session.commit()
    
    return jsonify({'Rating': 'added'})

"""
    Reduced course information for customer visualization
"""
@app.route('/api/coursereduced')
def course_reduced():
    count = Course2.query.count()
    new_data = [None] * count

    per_page = 50
    num_pages = int(math.ceil(count / per_page))

    i = 0
    for page in range(1, num_pages + 1):
        # compute each page of course information and iteratively get all the
        # reduced information
        course = db.session.query(
            Course2.course_dept_general_name,
            Course2.course_dept_specific_abbr,
            Course2.course_professors,
            Course2.course_section,
            Course2.course_num_name,
            Course2.course_desc,
        ).paginate(page=page, per_page=per_page, error_out=False).items

        for c in course:
            new_data[i] = course_schema2.dump(c)
            new_data[i]['num_section'] = len(new_data[i]['course_section'])
            del new_data[i]['course_section']
            i += 1

        del course

    return jsonify(new_data)


"""
    Department distribution for the number of professors for our visualization
"""
@app.route('/api/departmentdistribution')
def department_distribution():
    count = Department2.query.count()
    new_data = [None] * count

    per_page = 10
    num_pages = int(math.ceil(count / per_page))

    i = 0
    for page in range(1, num_pages + 1):
        # compute each page of department information and iteratively get all the
        # reduced information
        department = db.session.query(Department2.depart_name, Department2.depart_all_professors).paginate(page=page, per_page=per_page, error_out=False).items

        for d in department:
            new_data[i] = department_schema2.dump(d)
            new_data[i]['num_professors'] = len(new_data[i]['depart_all_professors'])
            del new_data[i]['depart_all_professors']
            i += 1

        del department

    return jsonify(new_data)

"""
    Returns a paginated portion of all departments
"""
@app.route('/api/alldepartments')
def all_departments():
    query = request.args['query'] if 'query' in request.args else ''
    sort_by, page, per_page = extract_pagination()

    order = desc if len(sort_by) > 1 and sort_by[1] == 'desc' else asc
    parent = request.args['filter'] if 'filter' in request.args else ''
    query = query.upper()

    # filter out the departments that do not contain a particular query
    depart_filter = Department2.query.filter(
        func.upper(Department2.depart_name).contains(query) |
        func.upper(Department2.depart_parent).contains(query) |
        func.upper(Department2.depart_address).contains(query)
    )

    # filter out the departments that do not contain a particular query
    # and are not under a specific parent college
    if parent != '':
        depart_filter = Department2.query.filter(
            (Department2.depart_parent == parent) & (
                func.upper(Department2.depart_name).contains(query) |
                func.upper(Department2.depart_parent).contains(query) |
                func.upper(Department2.depart_address).contains(query)
            )
        )

    # Order the rows by name, address, num professors, num classes, or parent college
    if sort_by[0] == 'name':
        filtered = order(Department2.depart_name)
    elif sort_by[0] == 'address':
        filtered = order(Department2.depart_address)
    elif sort_by[0] == 'professors':
        filtered = order(func.cardinality(Department2.depart_all_professors))
    elif sort_by[0] == 'courses':
        filtered = order(func.cardinality(Department2.depart_all_classes))
    elif sort_by[0] == 'parent':
        filtered = order(Department2.depart_parent)

    # Query to database
    departments = depart_filter.order_by(filtered).paginate(page=page, per_page=per_page, error_out=False).items
    num_rows = depart_filter.count()
    return_val = []

    # Reformat json keys
    for d in departments:
        data = department_schema2.dump(d)
        new_data = reformat_keys(data)

        new_data["all class numbers"] = []
        for name in new_data['all classes']:
            number = course_dict[name]
            new_data["all class numbers"].append(number)

        return_val.append(new_data)

    num_pages = int(math.ceil(num_rows / per_page))
    return jsonify({'departments': return_val, 'num_instances': num_rows, 'num_pages': num_pages})

"""
    Endpoint for a single instance of a department
"""
@app.route('/api/department/<name>')
def department(name: str):
    department = Department2.query.get(name)

    data = department_schema2.dump(department)
    new_data = reformat_keys(data)

    new_data["all class numbers"] = []
    for name in new_data['all classes']:
        number = course_dict[name]
        new_data["all class numbers"].append(number)

    return jsonify(new_data)



@app.route('/api/allprofessors')
def all_professors():
    query = request.args['query'] if 'query' in request.args else ''
    query = query.upper()

    sort_by, page, per_page = extract_pagination()

    order = desc if len(sort_by) > 1 and sort_by[1] == 'desc' else asc
    department = request.args['filter'] if 'filter' in request.args else ''

    # filter out the professors that do not contain a particular query
    prof_filter = Professor2.query.filter(
        func.upper(Professor2.prof_name).contains(query) |
        func.upper(Professor2.prof_department).contains(query) |
        func.upper(Professor2.prof_college).contains(query) |
        func.upper(Professor2.prof_email).contains(query) |
        func.upper(Professor2.prof_eid).contains(query)
    )
    if department != '':
        # filter out the professors that do not contain a particular query
        # and are not under a specific department
        prof_filter = Professor2.query.filter(
            (Professor2.prof_department == department) & (
                func.upper(Professor2.prof_name).contains(query) |
                func.upper(Professor2.prof_department).contains(query) |
                func.upper(Professor2.prof_college).contains(query) |
                func.upper(Professor2.prof_email).contains(query) |
                func.upper(Professor2.prof_eid).contains(query)
            )
        )

    # Order the rows by name, publications, email, courses, department
    if sort_by[0] == 'name':
        filtered = order(Professor2.prof_name)
    elif sort_by[0] == 'publications':
        filtered = order(Professor2.prof_num_pubs)
    elif sort_by[0] == 'email':
        filtered = order(Professor2.prof_email)
    elif sort_by[0] == 'courses':
        filtered = order(Professor2.prof_num_courses)
    elif sort_by[0] == 'department':
        filtered = order(Professor2.prof_department)

    # Query database
    professors = prof_filter.order_by(filtered).paginate(page=page, per_page=per_page, error_out=False).items
    num_rows = prof_filter.count()
    return_val = []

    # Reformat json keys
    for p in professors:
        data = professor_schema2.dump(p)
        new_data = reformat_keys(data)

        new_data['course numbers'] = []
        for name in new_data['courses']:
            number = course_dict[name]
            new_data["course numbers"].append(number)

        return_val.append(new_data)

    num_pages = int(math.ceil(num_rows / per_page))
    return jsonify({"professors": return_val, 'num_instances': num_rows, 'num_pages': num_pages})

"""
    Endpoint for a single instance of a professor
"""
@app.route('/api/professor/<string:name>')
def professor(name: str):
    professor = Professor2.query.get(name)
    data = professor_schema2.dump(professor)
    new_data = reformat_keys(data)

    new_data['course numbers'] = []
    for name in new_data['courses']:
        number = course_dict[name]
        new_data["course numbers"].append(number)

    new_data['pdf'] = get_as_base64(new_data['resume'])

    return jsonify(new_data)

@app.route('/api/search')
def search():
    # Parse query parameters
    query    = request.args['query']    if 'query'    in request.args else ''
    quantity = request.args['quantity'] if 'quantity' in request.args else '9'
    page     = request.args['page']     if 'page'     in request.args else '1'
    page = int(page)
    quantity = int(quantity)

    per_page = quantity // 3
    query = query.upper()

    # Create filters for the string query
    depart_filter = Department2.query.filter(
        func.upper(Department2.depart_name).contains(query) |
        func.upper(Department2.depart_parent).contains(query) |
        func.upper(Department2.depart_address).contains(query)
    )
    prof_filter = Professor2.query.filter(
        func.upper(Professor2.prof_name).contains(query) |
        func.upper(Professor2.prof_department).contains(query) |
        func.upper(Professor2.prof_college).contains(query) |
        func.upper(Professor2.prof_email).contains(query) |
        func.upper(Professor2.prof_eid).contains(query)
    )
    course_filter = Course2.query.filter(
        func.upper(Course2.course_num_name).contains(query) |
        func.upper(Course2.course_desc).contains(query) |
        func.upper(Course2.course_college).contains(query)
    )

    total_departs = depart_filter.count()
    total_prof    = prof_filter.count()
    total_course  = course_filter.count()

    maximum   = max(max(total_departs, total_course), total_prof)
    instances = total_departs + total_course + total_prof
    num_pages = int(math.ceil(instances / quantity))


    # sort the totals from least -> greatest
    sorting = [(total_departs, 'd'), (total_prof, 'p'), (total_course, 'c')]
    sorting = sorted(sorting, key=lambda a: a[0])
    #print(sorting)

    # figure out how many paginations per model
    so_far = (page - 1) * per_page

    offsets    = [so_far, so_far, so_far]
    iterations = [per_page, per_page, per_page]

    # first one is about to exhaust values
    if sorting[0][0] - so_far < per_page and sorting[0][0] - so_far > 0:
        iterations[0] = sorting[0][0] - so_far
        iterations[2] += per_page - (sorting[0][0] % per_page)
    # first one has exhausted values
    elif sorting[0][0] - so_far <= 0:
        offsets[2] += per_page - (sorting[0][0] % per_page)
        iterations[0] = 0

        # compute how many more we have to go through
        off = per_page // 2
        iterations[1] += off
        iterations[2] += off

        # compute how many additional components we've already sent
        already = (so_far - sorting[0][0]) // 4
        offsets[1] += already * off
        offsets[2] += already * off

    quant = quantity // 2
    # second one is about to exhaust all values
    if sorting[1][0] - offsets[1] < quant and sorting[1][0] - offsets[1] > 0:
        iterations[1] = sorting[1][0] - offsets[1]
        iterations[2] += quant - iterations[1]
    # second one has exhausted values
    elif sorting[1][0] - offsets[1] <= 0:
        # compute how many more we have to go through
        offsets[2] += quant - ((sorting[1][0] - offsets[1]) % quant)
        iterations = [0, 0, quantity]

        already = (offsets[1] - sorting[1][0]) // quant
        offsets[2] += already * quant

    if sorting[0][0] == 0 and sorting[1][0] == 0:
        iterations = [0, 0, 12]
        offsets = [0, 0, quantity * (page - 1)]

    # iterate and get the data from the database
    for i in range(0, 3):
        (total, model) = sorting[i]

        if model == 'd':
            department_results = [None] * iterations[i]
        if model == 'p':
            professor_results  = [None] * iterations[i]
        if model == 'c':
            course_results     = [None] * iterations[i]

        for j in range(iterations[i]):
            if model == 'd':
                temp = depart_filter.paginate(page=offsets[i] + j + 1, per_page=1, error_out=False).items

                if len(temp) > 0:
                    #print(f'd offsets: {offsets[i] + j + 1}')
                    department_results[j] = temp[0]

            if model == 'p':
                temp = prof_filter.paginate(page=offsets[i] + j + 1, per_page=1, error_out=False).items

                if len(temp) > 0:
                    #print(f'p offsets: {offsets[i] + j + 1}')
                    professor_results[j] = temp[0]

            if model == 'c':
                temp = course_filter.paginate(page=offsets[i] + j + 1, per_page=1, error_out=False).items
                if len(temp) > 0:
                    #print(f'c offsets: {offsets[i] + j + 1}')
                    course_results[j] = temp[0]


    len_depart = len(department_results)
    len_course = len(course_results)
    len_prof   = len(professor_results)

    data = {
        'data': [],
        'num_pages': num_pages,
        'num_instances': instances
    }

    if len_depart + len_course + len_prof == 0:
        data = {'data': [], 'num_pages': 0, 'num_instances': 0}
        return jsonify(data)

    # Post processing to proper formatting to what frontend team wants
    for c in course_results:
        if c == None:
            continue
        c = course_schema2.dump(c)
        course_data = {'model': 'Course', 'name': c['course_num_name'], 'match': ''}

        for k in c:
            if query in str(c[k]).upper():
                course_data['match'] = str(c[k])
                break

        data['data'].append(course_data)

    for d in department_results:
        if d == None:
            continue
        d = department_schema2.dump(d)
        depart_data = {'model': 'Department', 'name': d['depart_name'], 'match': ''}

        for k in d:
            if query in str(d[k]).upper():
                depart_data['match'] = str(d[k])
                break

        data['data'].append(depart_data)

    for p in professor_results:
        if p == None:
            continue
        p = professor_schema2.dump(p)
        prof_data = {'model': 'Professor', 'name': p['prof_name'], 'match': ''}

        for k in p:
            if query in str(p[k]).upper():
                prof_data['match'] = str(p[k])
                break

        data['data'].append(prof_data)

    return jsonify(data)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=True)