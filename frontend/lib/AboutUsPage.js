"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _core = require("@material-ui/core");

var _styles = require("@material-ui/styles");

var _react = _interopRequireWildcard(require("react"));

import _commit from "../src/icons/commit.svg"
//var _commit = require("../src/icons/commit.svg");
jest.mock("../src/icons/commit.svg")

import _issue from "../src/icons/issue.svg"
//var _issue = require("../src/icons/issue.svg");
jest.mock("../src/icons/issue.svg")

import _test from "../src/icons/test.svg"
//var _test = require("../src/icons/test.svg");
jest.mock("../src/icons/test.svg")

var _axios = _interopRequireDefault(require("axios"));

import _AdeetPhoto from "../src/images/AdeetPhoto.jpg"
//var _AdeetPhoto = _interopRequireDefault(require("../src/images/AdeetPhoto.jpg"));
jest.mock("../src/images/AdeetPhoto.jpg")

import _RachelPhoto from "../src/images/RachelPhoto.jpg"
//var _RachelPhoto = _interopRequireDefault(require("../src/images/RachelPhoto.jpg"));
jest.mock("../src/images/RachelPhoto.jpg")

import _KatelynPhoto from "../src/images/KatelynPhoto.jpg"
//var _KatelynPhoto = _interopRequireDefault(require("../src/images/KatelynPhoto.jpg"));
jest.mock("../src/images/KatelynPhoto.jpg")

import _LaurenPhoto from "../src/images/LaurenPhoto.jpg"
//var _LaurenPhoto = _interopRequireDefault(require("../src/images/LaurenPhoto.jpg"));
jest.mock("../src/images/LaurenPhoto.jpg")

import _SahilPhoto from "../src/images/SahilPhoto.jpg"
//var _SahilPhoto = _interopRequireDefault(require("../src/images/SahilPhoto.png"));
jest.mock("../src/images/SahilPhoto.jpg")

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// Retrieves gitlab info asynchronously
const getGitlabInfo = async () => {
  let commits = new Map();
  let issues = new Map();
  let errorText = [];
  await _axios.default.get("https://gitlab.com/api/v4/projects/29855940/repository/contributors", {
    headers: {
      "Content-Type": "application/json"
    }
  }).then(response => {
    let commitCount = 0;
    response.data.forEach(contributor => {
      commits.set(contributor.name, (commits.get(contributor.name) ?? 0) + contributor.commits);
      commitCount += contributor.commits;
    });
    commits.set("TOTAL", commitCount);
  }).catch(ex => {
    const error = ex.response.status === 404 ? "Resource Not found" : "An unexpected error has occurred";
    console.log(ex);
    errorText.push(error);
  });
  const gitLabMembers = ["N-Krypt", "sahiljain11", "lwarhola", "rachelli0603", "kba488", "TOTAL"];

  for (const member of gitLabMembers) {
    let params = null;

    if (member === "TOTAL") {
      params = {};
    } else {
      params = {
        "assignee_username": member
      };
    }

    await _axios.default.get("https://gitlab.com/api/v4/projects/29855940/issues_statistics", {
      headers: {
        "Content-Type": "application/json"
      },
      params: params
    }).then(response => {
      console.log(response.request);
      console.log(response.data);
      let issueCount = response.data.statistics.counts.closed;
      issues.set(member, issueCount);
    }).catch(ex => {
      const error = ex.response.status === 404 ? "Resource Not found" : "An unexpected error has occurred";
      console.log(ex);
      errorText.push(error);
    });
  }

  return {
    commits: commits,
    issues: issues,
    errorText: errorText
  };
};

const useStyles = (0, _styles.makeStyles)(() => ({
  memberPaperDiv: {
    margin: 20,
    width: 300,
    height: 475
  },
  nameStyle: {
    fontSize: 35,
    fontWeight: 500
  },
  roleStyle: {
    fontSize: 22,
    fontWeight: 100,
    marginTop: -20,
    marginBottom: -10
  },
  gitStatsPaperDiv: {
    margin: 20,
    width: 300,
    height: 200
  },
  toolsPaperDiv: {
    margin: 20,
    width: 300,
    height: 300
  },
  paper: {
    borderRadius: 15
  },
  cardMediaStyle: {
    height: 150,
    objectFit: "cover",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  }
}));
// Default data if gitlab doesn't get anything
const defaultCommits = new Map();
const defaultIssues = new Map(); // Main page

function AboutUsPage() {
  const [commits, setCommits] = _react.default.useState(defaultCommits);

  const [loading, setLoading] = _react.default.useState(true);

  const [error, setError] = _react.default.useState("");

  const [issues, setIssues] = _react.default.useState(defaultIssues); // Gets gitlab data and uses it to update the state


  (0, _react.useEffect)(() => {
    document.body.style.background = 'white';
    document.title = 'About Us';

    const fetchData = async () => {
      const gitLabInfo = await getGitlabInfo();
      setCommits(gitLabInfo.commits);
      setIssues(gitLabInfo.issues);
      setError(gitLabInfo.errorText[0] ?? "");
      setLoading(false);
    };

    fetchData();
  }, []);
  const pageStyle = {
    width: "80%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "10%",
    marginRight: "10%"
  }; // Defines all team members and their individual data

  let teamMembers = [{
    name: "Adeet Parikh",
    role: "Front-end",
    bio: "I am a third year CS major at UT Austin. I grew up in Lancaster, PA, and I spend my free time playing video games, hanging out with friends, and watching football.",
    img: _AdeetPhoto.default,
    commits: commits.get("Adeet Parikh") ?? 0,
    issues: issues.get("N-Krypt") ?? 0,
    tests: 0
  }, {
    name: "Sahil Jain",
    role: "Back-end | Phase 1 Leader",
    bio: "I am a third year CS and physics major at UT Austin. Outside of class, you can usually find me playing videogames, reading, or messing around with robotics.",
    img: _SahilPhoto.default,
    commits: (commits.get("Sahil Jain") ?? 0) + (commits.get("sahiljain11") ?? 0),
    issues: issues.get("sahiljain11") ?? 0,
    tests: 0
  }, {
    name: "Lauren Warhola",
    role: "Front-end",
    bio: "I am a junior Computer Science major at UT Austin. In my free time, I love to read, paint, take photos, and hike.",
    img: _LaurenPhoto.default,
    commits: (commits.get("Lauren M Warhola") ?? 0) + (commits.get("Lauren Warhola") ?? 0),
    issues: issues.get("lwarhola") ?? 0,
    tests: 0
  }, {
    name: "Rachel Li",
    role: "Front-end",
    bio: "I am a 4th year Computer Science student at the University of Texas at Austin. In my free time I enjoy crocheting, night walks, and reading crime fiction.",
    img: _RachelPhoto.default,
    commits: (commits.get("rachelli0603") ?? 0) + (commits.get("Rachel Li") ?? 0),
    issues: issues.get("rachelli0603") ?? 0,
    tests: 0
  }, {
    name: "Katelyn Ashok",
    role: "Front-end",
    bio: "I am a senior, but third year CS major at UT Austin. In my free time I enjoy traveling, eating spicy food, reading mystery novels, and petting cats.",
    img: _KatelynPhoto.default,
    commits: commits.get("Katelyn Ashok") ?? 0,
    issues: issues.get("kba488") ?? 0,
    tests: 0
  }]; // Show loading until gitlab api calls are finished

  if (loading) {
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
      variant: "h3",
      align: "center",
      style: {
        fontWeight: 600,
        marginBottom: 20,
        marginTop: 60
      }
    }, "Loading..."), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      variant: "h3",
      align: "center",
      style: {
        fontWeight: 600,
        marginBottom: 20,
        marginTop: 60
      }
    }, error));
  }

  return /*#__PURE__*/_react.default.createElement("div", {
    style: pageStyle
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h3",
    align: "center",
    style: {
      fontWeight: 600,
      marginBottom: 20,
      marginTop: 60
    }
  }, "About Us"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "body1",
    align: "center",
    style: {
      maxWidth: 1000,
      fontSize: 20
    }
  }, "We are a group of upperclassmen CS students at UT Austin. Having gone through the stressful and confusing registration process, we wanted to create a website that consolidated the information on each course that the university offers. New students at UT can save time and use this information to pursue their interests in all of the classes that the university offers. Former students can use this resource to put down their ratings for classes they have taken at UT, so this can create a positive cycle of students helping other students."), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h3",
    align: "center",
    style: {
      fontWeight: 600,
      marginBottom: 20,
      marginTop: 60
    }
  }, "The Team"), /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      display: "flex",
      justifyContent: "center",
      flexWrap: "wrap",
      maxWidth: 1200
    }
  }, teamMembers.sort((a, b) => a.commits < b.commits ? 1 : a.commits === b.commits ? a.issues < b.issues ? 1 : -1 : -1).map(member => /*#__PURE__*/_react.default.createElement(TeamMember, {
    name: member.name,
    role: member.role,
    bio: member.bio,
    img: member.img,
    commits: member.commits,
    issues: member.issues,
    tests: member.tests
  }))), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h3",
    align: "center",
    style: {
      fontWeight: 600,
      marginBottom: 20,
      marginTop: 40
    }
  }, "Total Repo Stats"), /*#__PURE__*/_react.default.createElement(TotalGitStats, {
    commits: commits.get("TOTAL") ?? 0,
    issues: issues.get("TOTAL") ?? 0,
    tests: 0
  }), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h3",
    align: "center",
    style: {
      fontWeight: 600,
      marginBottom: 20,
      marginTop: 40
    }
  }, "Tools Used"), /*#__PURE__*/_react.default.createElement(ToolsDisplay, null), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h3",
    align: "center",
    style: {
      fontWeight: 600,
      marginBottom: 20,
      marginTop: 40
    }
  }, "Integrating Data"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "body1",
    align: "center",
    style: {
      maxWidth: 1000,
      fontSize: 20
    }
  }, "Our website will allow students to see the ratings for courses, and see which were most liked by students. Prospective students can look at average ratings for professors and classes in each department to aid in choosing their major. Students can also quickly gauge the difficulty of a schedule they are considering before registering for classes."));
}

// Single team member card with parameters
function TeamMember({
  name,
  role,
  bio,
  img,
  commits,
  issues,
  tests
}) {
  const classes = useStyles();
  return /*#__PURE__*/_react.default.createElement("div", {
    className: classes.memberPaperDiv
  }, /*#__PURE__*/_react.default.createElement(_core.Paper, {
    elevation: 3,
    className: classes.paper
  }, /*#__PURE__*/_react.default.createElement(_core.CardMedia, {
    image: img,
    className: classes.cardMediaStyle
  }), /*#__PURE__*/_react.default.createElement(_core.CardContent, null, /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      display: "flex",
      flexDirection: "column",
      alignItems: "stretch",
      justifyContent: "space-between",
      height: "293px",
      width: "268px"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h4",
    align: "center",
    className: classes.nameStyle
  }, name), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h5",
    align: "center",
    className: classes.roleStyle
  }, role), /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      height: 90,
      display: "flex",
      alignItems: "center"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "body2",
    align: "center",
    color: "textSecondary"
  }, bio)), /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-around",
      flexWrap: "nowrap",
      borderTop: "solid 1px #A9A9A9",
      paddingTop: "10px",
      width: "100%"
    }
  }, /*#__PURE__*/_react.default.createElement(GitCommits, {
    count: commits,
    color: "#A9A9A9",
    width: "33%",
    size: 12
  }), /*#__PURE__*/_react.default.createElement(GitIssues, {
    count: issues,
    color: "#A9A9A9",
    width: "33%",
    size: 12
  }), /*#__PURE__*/_react.default.createElement(GitTests, {
    count: tests,
    color: "#A9A9A9",
    width: "33%",
    size: 12
  }))))));
}

// Individual stat card
function GitCommits({
  count,
  color,
  width,
  size
}) {
  return /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      width: width,
      height: "100%",
      display: "flex",
      flexDirection: "column",
      flexWrap: "nowrap",
      alignItems: "center",
      flexGrow: 0
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h6",
    style: {
      fontSize: size,
      color: color
    }
  }, "Commits"), /*#__PURE__*/_react.default.createElement(_commit.ReactComponent, {
    fill: color,
    style: {
      height: size * 2,
      width: size * 2
    }
  }), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h6",
    style: {
      fontSize: size + 8,
      color: color
    }
  }, count));
} // Individual stat card


function GitIssues({
  count,
  color,
  width,
  size
}) {
  return /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      width: width,
      height: "100%",
      display: "flex",
      flexDirection: "column",
      flexWrap: "nowrap",
      alignItems: "center",
      flexGrow: 0
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h6",
    style: {
      fontSize: size,
      color: color
    }
  }, "Issues"), /*#__PURE__*/_react.default.createElement(_issue.ReactComponent, {
    fill: color,
    style: {
      height: size * 2,
      width: size * 2
    }
  }), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h6",
    style: {
      fontSize: size + 8,
      color: color
    }
  }, count));
} // Individual stat card


function GitTests({
  count,
  color,
  width,
  size
}) {
  return /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      width: width,
      height: "100%",
      display: "flex",
      flexDirection: "column",
      flexWrap: "nowrap",
      alignItems: "center",
      flexGrow: 0
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h6",
    style: {
      fontSize: size,
      color: color
    }
  }, "Tests"), /*#__PURE__*/_react.default.createElement(_test.ReactComponent, {
    fill: color,
    style: {
      height: size * 2,
      width: size * 2
    }
  }), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h6",
    style: {
      fontSize: size + 8,
      color: color
    }
  }, count));
}

// Layout of total stat cards
function TotalGitStats({
  commits,
  issues,
  tests
}) {
  const classes = useStyles();
  return /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      width: "100%",
      height: "100%",
      display: "flex",
      justifyContent: "center",
      flexWrap: "wrap"
    }
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: classes.gitStatsPaperDiv
  }, /*#__PURE__*/_react.default.createElement(_core.Paper, {
    elevation: 3,
    className: classes.paper
  }, /*#__PURE__*/_react.default.createElement(_core.CardContent, null, /*#__PURE__*/_react.default.createElement(GitCommits, {
    count: commits,
    color: "black",
    width: "100%",
    size: 20
  })))), /*#__PURE__*/_react.default.createElement("div", {
    className: classes.gitStatsPaperDiv
  }, /*#__PURE__*/_react.default.createElement(_core.Paper, {
    elevation: 3,
    className: classes.paper
  }, /*#__PURE__*/_react.default.createElement(_core.CardContent, null, /*#__PURE__*/_react.default.createElement(GitIssues, {
    count: issues,
    color: "black",
    width: "100%",
    size: 20
  })))), /*#__PURE__*/_react.default.createElement("div", {
    className: classes.gitStatsPaperDiv
  }, /*#__PURE__*/_react.default.createElement(_core.Paper, {
    elevation: 3,
    className: classes.paper
  }, /*#__PURE__*/_react.default.createElement(_core.CardContent, null, /*#__PURE__*/_react.default.createElement(GitTests, {
    count: tests,
    color: "black",
    width: "100%",
    size: 20
  })))));
} // Info for different tools


let tools = [{
  img: "https://about.gitlab.com/images/press/logo/jpg/gitlab-icon-rgb.jpg",
  name: "GitLab",
  desc: "Git repository and DevOps",
  link: "https://gitlab.com/bevos-crew/bevos-course-guide",
  width: "100%",
  height: "100%"
}, {
  img: "https://res.cloudinary.com/postman/image/upload/t_team_logo/v1629869194/team/2893aede23f01bfcbd2319326bc96a6ed0524eba759745ed6d73405a3a8b67a8",
  name: "Postman",
  desc: "API Testing and Documentation",
  link: "https://documenter.getpostman.com/view/5971933/UUy4ckKn",
  width: "75%",
  height: "75%"
}, {
  img: "https://www.docker.com/sites/default/files/d8/styles/role_icon/public/2019-07/Moby-logo.png?itok=sYH_JEaJ",
  name: "Docker",
  desc: "Containerization for standardized runtime environments",
  link: "https://www.docker.com",
  width: "100%",
  height: "72%"
}, {
  img: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1280px-React-icon.svg.png",
  name: "React",
  desc: "Typescript library for front-end development",
  link: "https://reactjs.org",
  width: "140%",
  height: "100%"
}, {
  img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPwAAADICAMAAAD7nnzuAAAApVBMVEX///8AgcsAsP8Arv8Afcr6/v8Af8uX2f+XxOYAsf8AmeYAe8kAhc0Ag8zZ7PcArP+62/Dz+v2Vx+eBvONSpNkAtP/R6PXu+v+u1O1gq9zm8vrY8//1+/1tst+96f+f3/9szv/I4vN80/8nkNFeyf88mtUyvv+y5f/G7P9NoNeezepNxP8juf/i9v8ajtA1ltN6ueFpwO+H1v8SqvITneQUouiKwuWVY4CpAAALTUlEQVR4nOWdaWPaOBCGfchiUwMBknA1JKU0F0kgze7y/3/aSkCDjXXPSDbs+xkRPUXT0TFH9ON7dKrqzjLY8Ch9a42wZhNU/cEqB8D3BwsaJUn6+AT6F6xH44ecUvd58+Exg2f69Yw4rRCaTyiNY2f43fA9fHrx8x11cn41vWoTNndX+D/D9/AM/+bzVEy/P9jP3Q3+MPwLnun1CX2ePtR72S5ZV/jC8CJ8mp6A6Q8n+dfcHeBLw4vwfO033PSZtRbmbg1/NLwMz/Df7hvs9pbrvDh3W/jj4cfwDfb6We8hp7EzvGB4FZ65vUbueLuz9vHcbeBFwwXwjTR9Zq15Ze7m8OLhQni+9u9vvcLY6Xb5UP3ZzOFlwyXwDP/1zjORuebXRDx5M3jpcCk8M/1fzTD97qQjmbsRvGK4HJ67vc/61362WRDZ3A3glcNV8Ely+Vaz6d+O1xX/ZAGvGa6GT5KL1zp3vMPrWDV3LfxSPVwHz71+XW5vOmsrlqwJ/F+a8Tr4renXcdgdDVaaqYeA3+54wwAXVDx51grPDruBTX/4InPN4eG3O95wa3/0oV/xIeG56Qe64u4vV6J9eK3wTI93/r0+O3lq/FNN8AF2vGwzarbig8N7P+xOP3SevU747WHX10VPNl4bL/h64Dn+nRf8efWaqXnwbO17MH3hNVMD4T0875jsZZsCz73+Ex5+f7ww2Ms2B54JbcerO7g2ET5NUK64uzPqsuJrhmeC73iZsTv+6gjwFATPTR/i9rLe2uj05gWexBMYfMJN3xU/mzsbOxyexi+9CAzvvOP9io+oAz5vD/oRHJ6fdxxMfzQQPiCFgSerj92M4fAJP+zarX2rgys2PI2vh/sPoMCndvFcw4nbpgYDnpKH8dcHUOCtTP84PiIoPFlsjR0X3jioIxurHpA8wxM66RY/gAZvFNTBjB3k2UHwNJ4Myx9AhNcHdXRn8idT3/CUrMfHd5CY8Bz/U276043NBR0yfL7a9CsfwIVneruv/hGuvvUtFSI8bc+6gg+gwyfi5535NY57c4GnOd/LBoEXHXaHM3x0Y3hm7JIPeICvPO/0QQdXIDxpX01lH/ACXwrqyDTxEV7hSUdo7H7hk68MhvkEYxvvBp/Tl57qM/7g04TteKdXMcC9af7VtPCrgfp1UUPw+JYC8N9+O11J70VWaxj8UGrsRvBp6/uvFID/zX3F03wy/Es9HJBgZAYfRXevzvQX35zRCXfNuhtI//DRqOW69p3h6WrAfWUD4KPo/eeFE74jPIk/dv6pEfBR9PwrGDw7ec73f7Uh8FH29HgZAp7mi8PJsynwDP/e2vTt4UnJNTcHPoq+25q+LTyNy5vRJsFz07fy+nbwlFzPy3+uWfDc9H3B0+o1U8Pgmdtrma99C3gSb6qb0cbB23h9Y3ja+RCdPBsIH2WmO15DeEoexCfPJsJz0zdye2bwZDGWUDQTnq39zxs9vgk8bQuMveHwzOvr3Z4enpLjV5TTgOeHXSD8Lj7iNOGj0b167WvgSXupvmvxCz+CwbMvUJq+El51pxwAvj9YQeH5jvfCBb76ZBoUPuu9aEPRDOCZ13+U/fhSeErWyjtl3/DdCb9URoDnpv9mB09XS/FrZhj4P0kNKPDM7f1IRL++GJ4Q1SuKd/hsvN5/LRI89/oCfBF8IRiqDvisd3g8Q4OP+oLDbhWe0sXYaMV7gi8lNeDB8yvuY7dXgSftgc69FYUNP9qUoj4x4dmG/0cZ/wheEh8hFy58JTYEF555/cfihr8ET/P13PKnQoWvJjVgw0fRU8HrF+HpWnZwlQsRvjupJjXgwxcPuwd4g72sQGjw4tgQD/Dc6+/vuf7Ak87Eztj3QoLPxgthbIgX+Ch7fi3A01hyS6UVCnwmzWDyA8/W2fZ5ZwufG+5lBcKAVyTo+oLnh923lMGT1ZV7FhIcfjpQ5HP4g2em/3rxjZrvZQUCw6uTGnzCR9Hdb+UtlVZXsLCUrrQqVgj4CHTPNL1qK9kNQtHU4z3DA3Q71kZyec6orA0+6z3oo3XPFN4sK+Es4acbs7DNM4Q3z0o4P/i5WWmoc4QfzixitM8Lvm9XO+Oc4Jlnt8tKOCN4+6yEs4HvftjXzjgTeLfaGecB75hhew7w8nrMZw8PSKc/dXiLoo9VESi8PKwiAHxmVOFVJhpD4dU5BH7hQRm2NJemyBrDR7eqHAKf8LBCOeXiF67w0rAKz/DLNQCdUpP3XhN4RQ6BN/jeCyC5mJLJXP8nTOGjW0kOgSd4WO0MQVYCCF4UVuENfrQBZdgqApVd4cXpQx7gb2HGbhPcYQEfRdVoQnz4OaQ0FM0fbJ6ArODLYRU+4CFFH1XFL8SyhD/OIcCF728WkP/nzI19L1v4I6+PCj+2r2lcQKcz6/dee/hSDgEePKzoI89KsH/0dIEv5BCgwY8sC5iXlbeXLhEOTvCHHAIk+P4GUvTRwrOX5Qi/TZhPkeChB1ezvaxAzvA7r48BDyr6yPMNnf8yAJ7nEFyC4acfoJrGKydj3wsCz90eNCwFtJclRgdXuWDwwKCaKLp3rhVjmIKkmnpP99QNY9Oqlfy9ckQnNlkJAnUn2lthLEqJWunFzb+amCqhyK5QjrOM7siwKCVq8RjUi39s8a2zEo5kGNyBRSkRh2f0bO1b/JfP3JurZ99pbrivQIKUqbUPPb+wMH2XrISiusZP3ViUErW+0g5MTZ90XLISDuoPzCsqY1FK1CqknNwYmD4BGrtdcAcWpUSt4s0IW/sd5WRo/GCbglTW8NrqVhiLUqJWOdNMbfr5agnqCWfb9yssPMf/V7b1ICuYsU/tgzuwKCU6hudr/x/R2qcElJXAgzvsz45IkDJV4ZPkpmr6lAIOrlyGHT3rh9+u/TI7fC/rdGzGopRICH/k9kj7A+bZl66vX1iUEkngC/i0AzN2SGMULEqJpPBbr39U9NFFoPdeLEqJFPDbHe9aU49ZI2AxaSxKiVTwDD/5hLDfjkGvX50ZGqZYaviE9z5y/u45pDEKpcclJ/GlhU90DVBkGrp09DywmwZ3QKSHT9Ibt75foNcvQH6vuQzgt72P7ObSHy8gK749A+0rjGUEnyS2fb8g772ykpP4MoVPU2PT784IZMWvl16BizKFT0y7fQI7egKPzXaygDfp++WalbATobAnIFvZwMt6Hx0E6oJEqbrkJL7s4HV9v0DuTdS4y68s4RVeH9bRk4Tx7GVZwzO9Cky/tuCO0PBpWvH6sOCO/MEudrFO+Eq3T1hHT2qQldAk+O2O9+s7YMEdxiUnmwOfJJePT9tv6MHCNsN6diz43WEXdEtFyRr2BATUnUEpdKnSm98g97bahHdvJVk3QCkJEs6k7NIXSnevl674gL5fspacwXWcweAf3jYrwaeMuiDgwbuVnPQnRd4iNjyN7bMSfEvbBQEH3i0rwbv692+2bc/s4QElJz1L3QUBAd4+BSmknkWl0LHgTTNs65NVt08beFBWQihJuyDA4OlK0xilIXr/abr2jeG/OnqegL4rGqA4wNNOnQdXa90i9v2i+QqWlRBeykodNvCksZ5dpa9S6BD4ZhxcXfT8qml7pu37VenoeUKSVeowhA8SXuFR75+qta/u+yXq6HliOm6AYggPTUFqiLJn6dqX9/2iwcIrfEva7VMCz/MNT9rYy5Lcc4nhw4ZXhJB53y9oVkIj9Vy95xL0/YqB+YZN1aiy4632/QJmJTRZo5/Kvl/QrISmix12pX2/OtfDM3FvMmWSvl80N+roeeoqHHYP8PWFV4TWe6XvV+zW9+s0tff6f/p+naNnV2h32OXwlB1c/ycr/qBRi5n+Nx44ebaeXSV22P0WKi+ggXoG9v3S6T8XJhWgat+isAAAAABJRU5ErkJggg==",
  name: "MaterialUI",
  desc: "React Components Library",
  link: "https://mui.com",
  width: "80%",
  height: "64%"
}, {
  img: "https://www.jdrf.org/wp-content/uploads/2020/12/AWS-logo-2.jpg",
  name: "AWS",
  desc: "Cloud service to host website",
  link: "https://aws.amazon.com",
  width: "146%",
  height: "110%"
}, {
  img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAvVBMVEX/////UAD/jET/iTv/TQD/PwD/SwD/SAD/RgD/QgD/iT7/j0b/RAD/gy7/PAD/hzj/+vf/9PD/+/j/uab/7uj/gSr/qnz/5Nz/o4n/hjT/qJD/v57/dkv/upX/6+H/3tT/sIX/glz/j2//iGX/sp3/18v/m3//Yyr/ybr/WBX/bC3/lXf/0MP/ZjD/cEH/tY7/wbH/yKz/l1n/1cD/nmP/fDz/lFP/czH/XSD/o2//hF//eU//rH//tqL/zbS/yN1xAAAGBElEQVR4nO2ca1fiPBSFKbaFcikUEcRR1FEURLzMOF7m9v9/1ltfRAo02WnaI641+/mspCdPT3KSpi2VCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEII+VcZ3ve+7aRTfzj53i6omdH+oeuFjeez3W5Bv2jI+GHQqpVV1GqDQa+IKzp3gorvxLh+wzvtF/CLhvRn4aRcVwY4j7J5lLeZkRO4zhI/vCji4k3YDeN+rfwAIZZbj1GuZvbDZHyvNCYfc6ueh/MufUIh1lrDHM1MPWcD3/+IEO/CRXMwxPLA/oL2UwKM89EtaghT01+27KMAy7VH22auw7QA4+Q4LDKYVC79ZXMTKLH1xbIZdz0HF3i7hYazyTh58xjcp62OVTPnDUWAcaMFR7TO5Wrf4gjvbVrp+IrwYgJZiePV/DeQuGMj8UCt0HFnhUeV5Hk9PUQkdqrqAB0nzDfN6hkH6x0qkokH2giDsUBkCzYUOq6AxHZFF6BTPZAIbc7dukIZiftahU5FsDydpE1Shc+JndRqJhHhVCa6UqpCI4mDbBKBQqeyLxSfQqHjFywxAgE61XOpAFMVxhKvcIhZymWk0AmupSJMVxiHiALMJDFK78cEodTyYlfVtIHEpnkmQoW+2OrCUVX7Bqsoc4lYodiEr1RYbCZeIIXus1CAGoVGmWi4LYUVelIK/+iaNpA4MJN4oS/Y4qYuhQIs6Vs2yEQjiREoZwQVatbcRUqcbk1hR5OEBUrs4iyUmu2BwtfFPgxxgBeuUKEvtr5HLcd921M/xpizAyVGih3ED1Co2zb5H3dWGjaRxCaS+BVmoZRC3c7Xsm9f8krcYhbqt00WfYslgkyECv0boQDbKMC3vsUSb3XNdGEWhiOhCHG1P+/b4R6SuKeTeLY9hWiYeU+PXBKHuJzZnsLFCJcrE3+i4UxsXQi3TRJ9i+dEpcQ+zsJjoQhNs/CVHJmIFf4SChBX+8kRDkqsnaQ3gxV6UgrhmntlhLPOxF/bU4jrjJURzlLiMb5TpA7TYIWrI1wXZmIz7fACVngqFCCu9tdHuN82Eo9xM1IK8YJtfZIyWGJsSjzcmkKDan9jhMMSf6//ywiXM1IKcbW/OcJ1s0u8gQrPhALs4hEuZZLKnIkjnIVSZ71sFJpI3Fu9YKiwsj2F6XVGxky8xs3kOQCoAy/Y0usMg0xMXvIl2qmsfBUKEC/YVJPUCZTYW/4xViiWhVihapKK8BJjKXEGFUodTBjmqDMySMQKAymFeMGmrjO6A+NMxFkopbCfq87AEl/mfzjGzUidYjuFCn9q/ts4E6HCqtQBKINtE+0kdbtjlIlYYbA9hfpqPzLLxM2TgB+lEK+5UZ2BJb4ozyAlaEgpxGtuVCpGJoWN6gzSUqHUETaDNTecpAwyESsMpI4/QYUGpSLOxPLVJ1ZoUmdAieUy7EgphXjBZlLtY4l1IFHsuDPeNjGr9nNLDOzeRsFghWalYhsOp3qJje0p9Ayr/aN8Ev3PrjDOxFYeiQ2pw85YoXmpmEtiRSjA0hlUaF4q5slEMYUl/Mg3Q6l4BO/TsurwuCMVIKy5M1X7betMDP5IRXiHTl5kq/YNJKZmhZxCeP4p44LNNhMFX6NEBxO8jKXiF6tMFFSIHGau9u0yUfJN2F19Hmav9m0kuhOJ0N4YaVelFgu2tsUSI7gTiOz9grSzRcOiVMwuUVSh/pm6VbWfPRNFFer3v+yq/awS5d77eUO9AWa5YOvA+rv+lLxxxF4aWTBW7dJYT1J/8WAzSdwoUgdll5wqHhyG1ufIH9CDmvKPpcSG/CdoOk7qYOPZb+21ayjE5X0aSt+jr0STlBC9PM/xIhzi1bzNUPrjLHPas42vQ4T5toWiRzSg1p/i5PDFR5l3DrxkMrrBc+7zq0dNoLFem4Q3H/hRtu60GlR89/VLaVXvuYgpuHvSGmhu1lpr8Cj2Gno6nfH0Jh5zLk/Pizo21/l++/BYS/9O37fevdS5IEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQj4//wFur3yNQ7KoqgAAAABJRU5ErkJggg==",
  name: "Namecheap",
  desc: "Domain Name Provider",
  link: "https://www.namecheap.com",
  width: "100%",
  height: "100%"
}, {
  img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAb1BMVEX///9yidppgththdmsuOjS2fJngNjq7flshNlwh9pngdj7/P6jsebe4/ZkftfAye33+P2AlN2ZqeO5w+uxvOn4+f11jNuotefv8fp9kt3O1fGMnuDa3/TI0O/k6PfX3fSTpOJdedaHmt+2wOqXpuIXpP81AAAHaklEQVR4nO2d64KyKhRAFRoCNM1LZlrWNL3/M57Kmilzo80I+Hn2+ktFK+S+IcdBEARBEARBEARBEARBEARBEARBEJss90k6M0ua7JfG9MKIEU5NwwmLwpUBP29GuHDtIDifaRfcutySXg13F3oFk8Cq34Ug1Ck4Y7b9zrBUn2AibdtdYdpKsRpDCV6Qc02GG1ttaBMR6REMiW2zb+ROi2Fm2+uBjQ7B/TiamRqmo1f0x1ILL9C1BkPbUs9oeEzz8bQzF8jwE43FmKqhli5xP64ylMM3NR/jMiQfaIiGaGgbNERDNLQPGqIhGtoHDdEQDe2DhmiIhu8gBIUTORW/W2geiaHgjGeln4I7HiJJ40hIovgNRmxImfB31XXh9gsw4NUl1cuL2Ya9Gf5g3VDILL1+fccrYg4WUTarbh8/TyL2TklaNqTBaVu/a+FTcimcc2UkRLJvJOHnKngp6M1Xfpdcs/5xHlYNKZ/VmwqrMGP0rHaujFGchsVinudLz/NWy3m1LxK/zBjhggfl/pbHKhV9HS0aCubXfvmaSkJImRZzMJIp36clkYRF9z1db8b61Ud7hrKst0xyP2Bsk257fPI23QSBe3fMT722gGwZCl5cX+ytgyAK+28PzZPNZ3b/zh+qDtSyIS/rB3QXkPTd3a9q/Rnd3uOdumujHUNxqL/r5lj8JgcvFPe4vLizFO0YsmsZpKffb14W91rcWRetGIrYuQSf5p0fpaKodz79rkK0Ysgu45OBQlznXQFmNgxvtXAgDh3dog1Due/+kP4sOgrRhuHAES4dYYIWDPnAQZ+huk+0YEi9gTNUP6bmDak/dIbqDsO84fDBguqIZPOG2eAZOptRGfKvwTN0vlRtjXFDpiG2PFc9pqYN9YSWHxVdomlDngyen6PuEk0b6nhIz8NvxRzKtKGW0Hlla2rYkGs6iaRoTQ0bagjYvaLo9E0/pYPndgNuTM0aXpcvtBCDimYNOXwKaVXM4vUOWrq5pqoWdnZgRTRrCPcVCSdUUM7i1qlVQupUH5x4wf2F4XoIvae8f0EqWn6E71SegcdDwCyNGoLV8PDzFiFeJA4/T6DIoFIEK6JRQwKsX+we23p6UqVyaP4Mnnk0aiir9nc8v6pZWZ/39gOguQHPIhk15O2P2Pa5u26MexqnGaGhuzeKMgQGpY0hlzg+paaN1AOQbQRURJOG0BpUs5EQylRo7A41NSYNoYam+d3YU+qpkQqt80BzRJOG0LD7ZTXwObVnGUKHAk0aMqAZbDT0onxKTXrWQ2gn0aShAN7QmPo0WstmKrgnYL8M4UWo52ZQ5qrUANx3BGLiTBo2ByvfPG2QvVyds3+8B4bAiwTARqJBQwrf+vNw1wt9Len1Tw2jx5Z33wC2LwwaqnbVvhXJsWXcs76nSoUgtFZj0lAVWbLPGOdEyvYx2TWVS6hDrQEmwQYNiTqwq9olIbxOVYWq1CtAh2jQEJpZDAUwuzBp+Lf4mU6AuBOTT6nmm/CAHSiThs1WcvE35W3j85bWDfnLKw9/uOIoL5sN18p6S/Nq6GyiX67ye+nnS9/j2e/xW15bfpa/cFx9yaAlrqo9W5P1sK238AN23L0XYJOnlLktq6qV9ae0fe66I5wHcZ8o7ytecQzaV8a9Eaxi0EPby5cnJoR0133C+bZrQQR3W19ajsDQ5e1r3tWRCZdKdkhUo5688KmkgrjtQ9cDtDVjdt+CAMv6i4PkwhVcumVaVC+P4HyfxBnhQvCf8xYNQEHju9wl0KrkuzpiRJynEMw9nvz0KwnDMJnF5Yaw22W20RdQyMsIDm0zHk8D3gz7GGwgBKXn6RIhT6cOG0tUP+zpiOJpwGtTyx6HQ3j7/Xn+yCL3XJ615DnrdZSItdTCsOPgjJ1Y/aiZa9HzSteXTeRd1pWbpTMz0n06DNT/ztqn/dPFWo71VNAZLqP04zYnXvb1e9iYmhe+K/ucJbV5/pAS5h5m4a5QhRY24XERzuKI9D4ObPscsLj0Cm8dXRb8vYPrtg31g4ZoiIb2QUM0REP7oCEaoqF90BAN0dA+aIiG/0vDrbH/KOm1HqXBUHVkdVDErM+rNBgCUR/DQ9d5j4VIDYbgyYfByZw868xLh2Fq6s8rWeUsO//ETodh59VNQ3GJefcU279XdBiCYRFDU2/TlMZv4Om+umkw6nDHg7Jp02LoxIZqIqmD23xV/6TH0Otu4gZB3M6Kqf7XVY+hMzfVJ97yS4zeOHClIkZKkd0DbELw74d1GTrzzERd/DkjBcY6aDN0PL/nNbh/4eG86RZQ1Gd4flIP7969/T4P4fFVe2SUTkPHyZOS8oerq4fn8yGGKG+dTuk1vLCca+XxEEfrIFW/oUnaBqnTMmwbpE7N0ImbY43JGT6ex5yooROyqRs2hjdTNHQWj4PiSRo684e+f5qG576fTtzwoWOcrKETy6kbOimbuuFt3j9lQ+dDTt3Qqc4z8GkbXnZuJm7orKJg4obnjvFXf2XzT6HlbmYEQRAEQRAEQRAEQRAEQRAEQRAEUfEfzEGGJCUotwoAAAAASUVORK5CYII=",
  name: "Discord",
  desc: "Communication Platform",
  link: "https://discord.com",
  width: "100%",
  height: "100%"
}];

function ToolsDisplay() {
  return /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      width: "100%",
      height: "100%",
      display: "flex",
      justifyContent: "center",
      flexWrap: "wrap",
      maxWidth: "1200px"
    }
  }, tools.map(tool => /*#__PURE__*/_react.default.createElement(ToolsCard, {
    img: tool.img,
    name: tool.name,
    desc: tool.desc,
    link: tool.link,
    width: tool.width,
    height: tool.height
  })));
}

function ToolsCard({
  img,
  name,
  desc,
  link,
  width,
  height
}) {
  const classes = useStyles();
  return /*#__PURE__*/_react.default.createElement("div", {
    className: classes.toolsPaperDiv
  }, /*#__PURE__*/_react.default.createElement(_core.Paper, {
    elevation: 3,
    className: classes.paper,
    style: {
      height: "100%",
      width: "100%"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.CardActionArea, {
    href: link,
    style: {
      height: "100%",
      width: "100%"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.CardContent, {
    style: {
      height: "268",
      width: "268"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      width: "100%",
      height: "100%",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      flexWrap: "nowrap"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Box, {
    width: "200px",
    height: "190px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: img,
    alt: `${name} Logo`,
    style: {
      width: width,
      height: height
    }
  })), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h5"
  }, name), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "body1",
    align: "center"
  }, desc))))));
}

var _default = AboutUsPage;
exports.default = _default;