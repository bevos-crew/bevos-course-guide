from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, String, Float, Integer, PickleType, Text, create_engine
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.ext.mutable import MutableList
from sqlalchemy.types import TypeDecorator, JSON
import sqlalchemy 
import json

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)
    db.init_app(app)

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    f = open("../credentials.txt", "r")
    app.config['SQLALCHEMY_DATABASE_URI'] = f.readline()

    return app

class Test_Table(db.Model):
    rand_num = Column(Integer, primary_key=True)
    test_list = Column(MutableList.as_mutable(ARRAY(JSON)))

    def __init__(self,
                rand_num: 0,
                test_list: []):
                self.test_list = test_list
                self.rand_num = rand_num

app = create_app()
app.app_context().push()
app.debug = True

f = open("../credentials.txt", "r")
engine = create_engine(f.readline())
Test_Table.__table__.drop(engine)
Test_Table.__table__.create(engine)

data = [Test_Table(0, [{'Software Engineering': 'CS 373'}, {'Data Mining': 'CS 363D'}]),
        Test_Table(1, [{'Networks': 'CS 356'}, {'Operating Systems': 'CS 439'}])]
db.session.add_all(data)
db.session.commit()
