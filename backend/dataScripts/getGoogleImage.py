import requests
from requests.auth import HTTPBasicAuth
import time
import json
import os
import pandas as pd
from dotenv import load_dotenv
from PIL import Image
from io import BytesIO

load_dotenv()

def getProfImage(query):
    headers = {'Accept': 'application/json'}
    auth = HTTPBasicAuth('API_KEY', os.getenv('GOOGLENEWKEY'))
    #params = {"key": os.getenv('GOOGLENEWKEY'), "cx": "5c31e7602d76c3674", "searchType": "image", "imgType": "face", "q": query}
    params = {"key": os.getenv('GOOGLENEWKEY'), "cx": "5c31e7602d76c3674", "searchType": "image", "imgType": "photo", "q": query}


    response = requests.get('https://customsearch.googleapis.com/customsearch/v1/siterestrict', headers=headers, auth=auth, params=params)
    #print(response.url)

    jsonResp = json.loads(response.content)
    try:
        imgUrl = jsonResp["items"][0]["image"]["thumbnailLink"]
    except:
        print('image not found: ', query)
        imgUrl = "https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png"

    return imgUrl

def getProfImage2(query):
    headers = {'Accept': 'application/json'}
    auth = HTTPBasicAuth('API_KEY', 'AIzaSyBYnW5yeWE9xyDTlsTXqKx1GdCuieoakOA')
    #params = {"key": 'os.getenv('GOOGLENEWKEY')', "cx": "5c31e7602d76c3674", "searchType": "image", "imgType": "face", "q": query}
    params = {"key": 'AIzaSyBYnW5yeWE9xyDTlsTXqKx1GdCuieoakOA', "cx": "c1b88fdcc6779ddc5", "searchType": "image", "imgType": "photo", "q": query}

    response = requests.get('https://customsearch.googleapis.com/customsearch/v1/siterestrict', headers=headers, auth=auth, params=params)
    #print(response.url)

    jsonResp = json.loads(response.content)
    try:
        imgUrl = jsonResp["items"][0]["image"]["thumbnailLink"]
    except:
        print('image not found: ', query)
        imgUrl = "https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png"

    return imgUrl


def getBuildingImage(query):
    headers = {'Accept': 'application/json'}
    auth = HTTPBasicAuth('API_KEY', os.getenv('GOOGLENEWKEY'))
    params = {"key": os.getenv('GOOGLENEWKEY'), "cx": "5c31e7602d76c3674", "searchType": "image", "imgType": "photo", "q": query}

    response = requests.get('https://customsearch.googleapis.com/customsearch/v1/siterestrict', headers=headers, auth=auth, params=params)
    #print(response.url)

    jsonResp = json.loads(response.content)
    imgUrl = ''
    try:
        imgUrl = jsonResp["items"][0]["image"]["thumbnailLink"]#
    except:
        imgUrl = "https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png"

    return imgUrl



#imgUrl = getBuildingImage("Department of Computer Science Building University of Texas at Austin")
#img = Image.open(BytesIO(requests.get(imgUrl).content))
#img.show()

#imgUrl = getProfImage2("Scott Aaronson")
#imgUrl = getProfImage2("GASKINS, A School of Architecture")
#imgUrl = getProfImage2("NORMAN, A Department of Computer Science")

#img = Image.open(BytesIO(requests.get(imgUrl).content))
#img.show()

