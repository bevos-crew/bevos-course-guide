import {
    ComposableMap,
    Geographies,
    Geography,
    Marker
} from "react-simple-maps";

import React, { useEffect, useState } from "react";
import axios from "axios";
import { Box, CircularProgress, Typography } from "@material-ui/core";
import { IMarker, IAirportResponse, Tooltip, IAirport } from "../../../Components/Interfaces";

// const geoUrl = "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json"

const geoUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json"

const getAirportsInfo = async () => {
    let markers: IMarker[] = []
    await axios.get<IAirportResponse>(
        "https://api.flightright.me/airports?limit=117"
    )
        .then((response) => {
            console.log(response)
            response.data.data.forEach((airport: IAirport) => {
                let name = airport.airport_name; //airport.airport_runway_number > 7 ? airport.airport_name: ""
                let marker: IMarker = { markerOffset: -30, name: name, city: airport.airport_city, state: airport.airport_state, coordinates: [airport.airport_long, airport.airport_lat] };
                markers.push(marker);
            })
        })
        .catch((ex) => {
            console.log(ex);
            const error =
                ex.response.status === 404
                    ? "Resource Not found"
                    : "An unexpected error has occurred";
        });

    console.log(markers)

    markers = markers.filter((elem, index, self) => self.findIndex((t) => { return (t.name === elem.name) }) == index)

    return markers;
}

function AirportMap() {

    const [markers, setMarkers]: [IMarker[], (markers: IMarker[]) => void] = useState([]);
    const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);
    const [tooltip, setTooltip]: [Tooltip, (tooltip: Tooltip) => void] = React.useState<Tooltip>({ name: "", city: "", state: "" });

    useEffect(() => {

        const fetchData = async () => {
            const markersResponse = await getAirportsInfo();
            setMarkers(markersResponse);
            setLoading(false);
        };

        fetchData();

    }, []);

    if (loading) {
        return (
            <CircularProgress />
        );
    }

    return (
        <>
            <Box sx={{ height: 0 }}>
                {tooltip.name != "" &&
                    <>
                        <Typography variant="h6" align="center">{tooltip.name}</Typography>
                        <Typography variant="h6" align="center">{`${tooltip.city}, ${tooltip.state}`}</Typography>
                    </>
                }
                {tooltip.name === "" &&
                    <Typography variant="h6" align="center">Hover on an airport to see its location</Typography>
                }
            </Box>
            <ComposableMap
                projection="geoAlbersUsa"
                projectionConfig={{
                    scale: 800
                }}
            >
                <Geographies geography={geoUrl}>
                    {({ geographies }) =>
                        geographies
                            .map(geo => (
                                <Geography
                                    key={geo.rsmKey}
                                    geography={geo}
                                    fill="#EAEAEC"
                                    stroke="#D6D6DA"
                                />
                            ))
                    }
                </Geographies>
                {markers.map(({ name, city, state, coordinates, markerOffset }) => (
                    <Marker key={name} coordinates={coordinates}
                        onMouseEnter={() => {
                            setTooltip({ name: name, city: city, state: state });
                        }}
                        onMouseLeave={() => {
                            setTooltip({ name: "", city: "", state: "" });
                        }}
                    >
                        <g
                            fill="none"
                            stroke="#FF5533"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            transform="translate(-12, -24)"
                        >
                            <circle cx="12" cy="10" r="3" />
                            <path d="M12 21.7C17.3 17 20 13 20 10a8 8 0 1 0-16 0c0 3 2.7 6.9 8 11.7z" />
                        </g>
                        <text
                            textAnchor="middle"
                            y={markerOffset}
                            style={{ fontFamily: "system-ui", fill: "#5D5A6D" }}
                        >
                            {""}
                        </text>
                    </Marker>
                ))}
            </ComposableMap>
        </>
    );
}

export default AirportMap;