import React from "react";

import {
    Box, Typography,
} from "@material-ui/core";

import FleetSizePie from "./FleetSizePie";
import AirportMap from "./AirportMap";
import FlightTimes from "./FlightTimes";

function FRVisuals() {

    return (
        <Box display="flex" flexDirection="column" alignItems="center" width={1000}>

            <Typography variant="h5">Fleet Size of Every Airline</Typography>
            <FleetSizePie />
            <Typography variant="h5">Number of Flights Departing and Arriving During Each Hour</Typography>
            <FlightTimes />
            <Box height={50}></Box>
            <Typography variant="h5">Location of Every Airport</Typography>
            <AirportMap />
        </Box>
    );
}

export default FRVisuals;