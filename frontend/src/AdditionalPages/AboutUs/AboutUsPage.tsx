import {
  Box,
  CardContent,
  CardActionArea,
  Paper,
  Typography,
  CardMedia,
  LinearProgress,
  Grid,
} from "@material-ui/core";
import { useEffect } from "react";
import { ReactComponent as CommitIcon } from "./icons/commit.svg";
import { ReactComponent as IssuesIcon } from "./icons/issue.svg";
import { ReactComponent as TestIcon } from "./icons/test.svg";
import axios from "axios";
import React from "react";
import adeetPhoto from "./images/AdeetPhoto.jpg";
import rachelPhoto from "./images/RachelPhoto.jpg";
import katelynPhoto from "./images/KatelynPhoto.jpg";
import laurenPhoto from "./images/LaurenPhoto.jpg";
import sahilPhoto from "./images/SahilPhoto.png";
import '../../fonts.css'
import { aboutUsStyles, aboutUsSectionStyle, titleStyle, aboutUsPageStyle } from "../../Components/Style";
import { IIssue, IContributor, GitStatsProps, TeamMemberProps, TotalGitStatsProps, ToolList, ToolsCardInterface } from "../../Components/Interfaces";

// Retrieves gitlab info asynchronously
const getGitlabInfo = async (setLoadPercent: (loadPercent: number) => void) => {
  let commits = new Map<string, number>();
  let issues = new Map<string, number>();
  let errorText: string[] = [];

  await axios
    .get<IContributor[]>(
      "https://gitlab.com/api/v4/projects/29855940/repository/contributors",
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then((response) => {
      let commitCount = 0;
      response.data.forEach((contributor) => {
        commits.set(
          contributor.name,
          (commits.get(contributor.name) ?? 0) + contributor.commits
        );
        commitCount += contributor.commits;
      });
      commits.set("TOTAL", commitCount);
    })
    .catch((ex) => {
      const error =
        ex.response.status === 404
          ? "Resource Not found"
          : "An unexpected error has occurred";
      console.log(ex);
      errorText.push(error);
    });

  setLoadPercent(50);

  const gitLabMembers: string[] = [
    "N-Krypt",
    "sahiljain11",
    "lwarhola",
    "rachelli0603",
    "kba488",
    "TOTAL",
  ];

  for (const member of gitLabMembers) {
    let params = null;
    if (member === "TOTAL") {
      params = {};
    } else {
      params = {
        assignee_username: member,
      };
    }

    await axios
      .get<IIssue>(
        "https://gitlab.com/api/v4/projects/29855940/issues_statistics",
        {
          headers: {
            "Content-Type": "application/json",
          },
          params: params,
        }
      )
      .then((response) => {
        console.log(response.request);
        console.log(response.data);
        let issueCount = response.data.statistics.counts.closed;
        issues.set(member, issueCount);
      })
      .catch((ex) => {
        const error =
          ex.response.status === 404
            ? "Resource Not found"
            : "An unexpected error has occurred";
        console.log(ex);
        errorText.push(error);
      });

    setLoadPercent(100);
  }

  return {
    commits: commits,
    issues: issues,
    errorText: errorText,
  };
};

// Default data if gitlab doesn't get anything
const defaultCommits: Map<string, number> = new Map<string, number>();
const defaultIssues: Map<string, number> = new Map<string, number>();

// Main page
function AboutUsPage() {
  const [commits, setCommits]: [
    Map<string, number>,
    (commits: Map<string, number>) => void
  ] = React.useState(defaultCommits);
  const [loading, setLoading]: [boolean, (loading: boolean) => void] =
    React.useState<boolean>(true);
  const [error, setError]: [string, (error: string) => void] =
    React.useState("");
  const [loadPercent, setLoadPercent]: [number, (loadPercent: number) => void] =
    React.useState<number>(0);


  const [issues, setIssues]: [
    Map<string, number>,
    (issues: Map<string, number>) => void
  ] = React.useState(defaultIssues);

  // Gets gitlab data and uses it to update the state
  useEffect(() => {
    document.body.style.background = "white";
    document.title = "About Us";

    const fetchData = async () => {
      const gitLabInfo = await getGitlabInfo(setLoadPercent);
      setCommits(gitLabInfo.commits);
      setIssues(gitLabInfo.issues);
      setError(gitLabInfo.errorText[0] ?? "");
      setLoading(false);
    };

    fetchData();
  }, []);


  // Defines all team members and their individual data
  let teamMembers: TeamMemberProps[] = [
    {
      name: "Adeet Parikh",
      role: "Full Stack | Phase II Leader",
      bio: "I am a third year CS major at UT Austin. I grew up in Lancaster, PA, and I spend my free time playing video games, hanging out with friends, and watching football.",
      img: adeetPhoto,
      commits: commits.get("Adeet Parikh") ?? 0,
      issues: issues.get("N-Krypt") ?? 0,
      tests: 18,
    },
    {
      name: "Sahil Jain",
      role: "Back-end | Phase I Leader",
      bio: "I am a third year CS and physics major at UT Austin. Outside of class, you can usually find me playing videogames, reading, or messing around with robotics.",
      img: sahilPhoto,
      commits:
        (commits.get("Sahil Jain") ?? 0) + (commits.get("sahiljain11") ?? 0),
      issues: issues.get("sahiljain11") ?? 0,
      tests: 12,
    },
    {
      name: "Lauren Warhola",
      role: "Front-end | Phase III Leader",
      bio: "I am a junior Computer Science major at UT Austin. In my free time, I love to read, paint, take photos, and hike.",
      img: laurenPhoto,
      commits:
        (commits.get("Lauren M Warhola") ?? 0) +
        (commits.get("Lauren Warhola") ?? 0),
      issues: issues.get("lwarhola") ?? 0,
      tests: 5,
    },
    {
      name: "Rachel Li",
      role: "Front-end",
      bio: "I am a 4th year Computer Science student at the University of Texas at Austin. In my free time I enjoy crocheting, night walks, and reading crime fiction.",
      img: rachelPhoto,
      commits:
        (commits.get("rachelli0603") ?? 0) + (commits.get("Rachel Li") ?? 0),
      issues: issues.get("rachelli0603") ?? 0,
      tests: 5,
    },
    {
      name: "Katelyn Ashok",
      role: "Full Stack | Phase IV Leader",
      bio: "I am a senior, but third year CS major at UT Austin. In my free time I enjoy traveling, eating spicy food, reading mystery novels, and petting cats.",
      img: katelynPhoto,
      commits: commits.get("Katelyn Ashok") ?? 0,
      issues: issues.get("kba488") ?? 0,
      tests: 31,
    },
  ];

  return (
    <div style={aboutUsPageStyle}>
      <Typography
        variant="h3"
        align="center"
        style={titleStyle}
      >
        About Us
      </Typography>
      <Typography
        variant="body1"
        align="center"
        style={{ maxWidth: 1000, fontSize: 20 }}
      >
        We are a group of upperclassmen CS students at UT Austin. Having gone
        through the stressful and confusing registration process, we wanted to
        create a website that consolidated the information on each course that
        the university offers. New students at UT can save time and use this
        information to pursue their interests in all of the classes that the
        university offers. Former students can use this resource to put down
        their ratings for classes they have taken at UT, so this can create a
        positive cycle of students helping other students.
      </Typography>
      <Typography
        variant="h3"
        align="center"
        style={aboutUsSectionStyle}
      >
        The Team
      </Typography>
      {loading ? (
        <Grid spacing={1} container>
          <Grid xs item>
            <LinearProgress variant="determinate" style={{ margin: 0, height: 25 }} value={loadPercent} />
          </Grid>
        </Grid>
      ) : (
          <>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                flexWrap: "wrap",
                maxWidth: 1200,
              }}
            >
              {/* sort by commits > issues for no reason */}
              {teamMembers
                .sort((a, b) =>
                  a.commits < b.commits
                    ? 1
                    : a.commits === b.commits
                      ? a.issues < b.issues
                        ? 1
                        : -1
                      : -1
                )
                .map((member) => (
                  <TeamMember
                    name={member.name}
                    role={member.role}
                    bio={member.bio}
                    img={member.img}
                    commits={member.commits}
                    issues={member.issues}
                    tests={member.tests}
                  />
                ))}
            </Box>
            <Typography
              variant="h3"
              align="center"
              style={aboutUsSectionStyle}
            >
              Total Repo Stats
        </Typography>

            {/* Display total git stat cards */}
            <TotalGitStats
              commits={commits.get("TOTAL") ?? 0}
              issues={issues.get("TOTAL") ?? 0}
              tests={66}
            />
          </>
        )}

      <Typography
        variant="h3"
        align="center"
        style={aboutUsSectionStyle}
      >
        Tools Used
      </Typography>
      <ToolsDisplay tools={tools} />
      <Typography
        variant="h3"
        align="center"
        style={aboutUsSectionStyle}
      >
        Repo and API Docs
      </Typography>
      <ToolsDisplay tools={repoDocs} />
      <Typography
        variant="h3"
        align="center"
        style={aboutUsSectionStyle}
      >
        APIs and Additional Data Sources
      </Typography>
      <ToolsDisplay tools={apis} />
      <Typography
        variant="h3"
        align="center"
        style={aboutUsSectionStyle}
      >
        Integrating Data
      </Typography>
      <Typography
        variant="body1"
        align="center"
        style={{ maxWidth: 1000, fontSize: 20 }}
      >
        Our website will allow students to see the ratings for courses, and see
        which were most liked by students. Prospective students can look at
        average ratings for professors and classes in each department to aid in
        choosing their major. Students can also quickly gauge the difficulty of
        a schedule they are considering before registering for classes.
      </Typography>
    </div>
  );
}


// Single team member card with parameters

function TeamMember({
  name,
  role,
  bio,
  img,
  commits,
  issues,
  tests,
}: TeamMemberProps) {
  const classes = aboutUsStyles();

  return (
    <div className={classes.memberPaperDiv}>
      <Paper elevation={3} className={classes.paper}>
        <CardMedia image={img} className={classes.cardMediaStyle} />
        <CardContent>
          {/* Height is 443 px */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "stretch",
              justifyContent: "space-between",
              height: "293px",
              width: "268px",
            }}
          >
            <Typography
              variant="h4"
              align="center"
              className={classes.nameStyle}
            >
              {name}
            </Typography>
            <Typography
              variant="h5"
              align="center"
              className={classes.roleStyle}
            >
              {role}
            </Typography>
            <Box sx={{ height: 90, display: "flex", alignItems: "center" }}>
              <Typography variant="body2" align="center" color="textSecondary">
                {bio}
              </Typography>
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-around",
                flexWrap: "nowrap",
                borderTop: "solid 1px #A9A9A9",
                paddingTop: "10px",
                width: "100%",
              }}
            >
              <GitCommits
                count={commits}
                color="#A9A9A9"
                width="33%"
                size={12}
              />
              <GitIssues count={issues} color="#A9A9A9" width="33%" size={12} />
              <GitTests count={tests} color="#A9A9A9" width="33%" size={12} />
            </Box>
          </Box>
        </CardContent>
      </Paper>
    </div>
  );
}

// Individual stat card
function GitCommits({ count, color, width, size }: GitStatsProps) {
  return (
    <Box
      sx={{
        width: width,
        height: "100%",
        display: "flex",
        flexDirection: "column",
        flexWrap: "nowrap",
        alignItems: "center",
        flexGrow: 0,
      }}
    >
      <Typography variant="h6" style={{ fontSize: size, color: color }}>
        Commits
      </Typography>
      <CommitIcon fill={color} style={{ height: size * 2, width: size * 2 }} />
      <Typography variant="h6" style={{ fontSize: size + 8, color: color }}>
        {count}
      </Typography>
    </Box>
  );
}

// Individual stat card
function GitIssues({ count, color, width, size }: GitStatsProps) {
  return (
    <Box
      sx={{
        width: width,
        height: "100%",
        display: "flex",
        flexDirection: "column",
        flexWrap: "nowrap",
        alignItems: "center",
        flexGrow: 0,
      }}
    >
      <Typography variant="h6" style={{ fontSize: size, color: color }}>
        Issues
      </Typography>
      <IssuesIcon fill={color} style={{ height: size * 2, width: size * 2 }} />
      <Typography variant="h6" style={{ fontSize: size + 8, color: color }}>
        {count}
      </Typography>
    </Box>
  );
}

// Individual stat card
function GitTests({ count, color, width, size }: GitStatsProps) {
  return (
    <Box
      sx={{
        width: width,
        height: "100%",
        display: "flex",
        flexDirection: "column",
        flexWrap: "nowrap",
        alignItems: "center",
        flexGrow: 0,
      }}
    >
      <Typography variant="h6" style={{ fontSize: size, color: color }}>
        Tests
      </Typography>
      <TestIcon fill={color} style={{ height: size * 2, width: size * 2 }} />
      <Typography variant="h6" style={{ fontSize: size + 8, color: color }}>
        {count}
      </Typography>
    </Box>
  );
}

// Layout of total stat cards
function TotalGitStats({ commits, issues, tests }: TotalGitStatsProps) {
  const classes = aboutUsStyles();

  return (
    <Box
      sx={{
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        flexWrap: "wrap",
      }}
    >
      <div className={classes.gitStatsPaperDiv}>
        <Paper elevation={3} className={classes.paper}>
          <CardContent>
            <GitCommits count={commits} color="black" width="100%" size={20} />
          </CardContent>
        </Paper>
      </div>
      <div className={classes.gitStatsPaperDiv}>
        <Paper elevation={3} className={classes.paper}>
          <CardContent>
            <GitIssues count={issues} color="black" width="100%" size={20} />
          </CardContent>
        </Paper>
      </div>
      <div className={classes.gitStatsPaperDiv}>
        <Paper elevation={3} className={classes.paper}>
          <CardContent>
            <GitTests count={tests} color="black" width="100%" size={20} />
          </CardContent>
        </Paper>
      </div>
    </Box>
  );
}

// Info for different tools
let tools: ToolsCardInterface[] = [
  {
    img: "https://www.docker.com/sites/default/files/d8/styles/role_icon/public/2019-07/Moby-logo.png?itok=sYH_JEaJ",
    name: "Docker",
    desc: "Containerization for standardized runtime environments",
    link: "https://www.docker.com",
    width: "100%",
    height: "72%",
  },
  {
    img: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1280px-React-icon.svg.png",
    name: "React",
    desc: "Typescript library for front-end development",
    link: "https://reactjs.org",
    width: "140%",
    height: "100%",
  },
  {
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPwAAADICAMAAAD7nnzuAAAApVBMVEX///8AgcsAsP8Arv8Afcr6/v8Af8uX2f+XxOYAsf8AmeYAe8kAhc0Ag8zZ7PcArP+62/Dz+v2Vx+eBvONSpNkAtP/R6PXu+v+u1O1gq9zm8vrY8//1+/1tst+96f+f3/9szv/I4vN80/8nkNFeyf88mtUyvv+y5f/G7P9NoNeezepNxP8juf/i9v8ajtA1ltN6ueFpwO+H1v8SqvITneQUouiKwuWVY4CpAAALTUlEQVR4nOWdaWPaOBCGfchiUwMBknA1JKU0F0kgze7y/3/aSkCDjXXPSDbs+xkRPUXT0TFH9ON7dKrqzjLY8Ch9a42wZhNU/cEqB8D3BwsaJUn6+AT6F6xH44ecUvd58+Exg2f69Yw4rRCaTyiNY2f43fA9fHrx8x11cn41vWoTNndX+D/D9/AM/+bzVEy/P9jP3Q3+MPwLnun1CX2ePtR72S5ZV/jC8CJ8mp6A6Q8n+dfcHeBLw4vwfO033PSZtRbmbg1/NLwMz/Df7hvs9pbrvDh3W/jj4cfwDfb6We8hp7EzvGB4FZ65vUbueLuz9vHcbeBFwwXwjTR9Zq15Ze7m8OLhQni+9u9vvcLY6Xb5UP3ZzOFlwyXwDP/1zjORuebXRDx5M3jpcCk8M/1fzTD97qQjmbsRvGK4HJ67vc/61362WRDZ3A3glcNV8Ely+Vaz6d+O1xX/ZAGvGa6GT5KL1zp3vMPrWDV3LfxSPVwHz71+XW5vOmsrlqwJ/F+a8Tr4renXcdgdDVaaqYeA3+54wwAXVDx51grPDruBTX/4InPN4eG3O95wa3/0oV/xIeG56Qe64u4vV6J9eK3wTI93/r0+O3lq/FNN8AF2vGwzarbig8N7P+xOP3SevU747WHX10VPNl4bL/h64Dn+nRf8efWaqXnwbO17MH3hNVMD4T0875jsZZsCz73+Ex5+f7ww2Ms2B54JbcerO7g2ET5NUK64uzPqsuJrhmeC73iZsTv+6gjwFATPTR/i9rLe2uj05gWexBMYfMJN3xU/mzsbOxyexi+9CAzvvOP9io+oAz5vD/oRHJ6fdxxMfzQQPiCFgSerj92M4fAJP+zarX2rgys2PI2vh/sPoMCndvFcw4nbpgYDnpKH8dcHUOCtTP84PiIoPFlsjR0X3jioIxurHpA8wxM66RY/gAZvFNTBjB3k2UHwNJ4Myx9AhNcHdXRn8idT3/CUrMfHd5CY8Bz/U276043NBR0yfL7a9CsfwIVneruv/hGuvvUtFSI8bc+6gg+gwyfi5535NY57c4GnOd/LBoEXHXaHM3x0Y3hm7JIPeICvPO/0QQdXIDxpX01lH/ACXwrqyDTxEV7hSUdo7H7hk68MhvkEYxvvBp/Tl57qM/7g04TteKdXMcC9af7VtPCrgfp1UUPw+JYC8N9+O11J70VWaxj8UGrsRvBp6/uvFID/zX3F03wy/Es9HJBgZAYfRXevzvQX35zRCXfNuhtI//DRqOW69p3h6WrAfWUD4KPo/eeFE74jPIk/dv6pEfBR9PwrGDw7ec73f7Uh8FH29HgZAp7mi8PJsynwDP/e2vTt4UnJNTcHPoq+25q+LTyNy5vRJsFz07fy+nbwlFzPy3+uWfDc9H3B0+o1U8Pgmdtrma99C3gSb6qb0cbB23h9Y3ja+RCdPBsIH2WmO15DeEoexCfPJsJz0zdye2bwZDGWUDQTnq39zxs9vgk8bQuMveHwzOvr3Z4enpLjV5TTgOeHXSD8Lj7iNOGj0b167WvgSXupvmvxCz+CwbMvUJq+El51pxwAvj9YQeH5jvfCBb76ZBoUPuu9aEPRDOCZ13+U/fhSeErWyjtl3/DdCb9URoDnpv9mB09XS/FrZhj4P0kNKPDM7f1IRL++GJ4Q1SuKd/hsvN5/LRI89/oCfBF8IRiqDvisd3g8Q4OP+oLDbhWe0sXYaMV7gi8lNeDB8yvuY7dXgSftgc69FYUNP9qUoj4x4dmG/0cZ/wheEh8hFy58JTYEF555/cfihr8ET/P13PKnQoWvJjVgw0fRU8HrF+HpWnZwlQsRvjupJjXgwxcPuwd4g72sQGjw4tgQD/Dc6+/vuf7Ak87Eztj3QoLPxgthbIgX+Ch7fi3A01hyS6UVCnwmzWDyA8/W2fZ5ZwufG+5lBcKAVyTo+oLnh923lMGT1ZV7FhIcfjpQ5HP4g2em/3rxjZrvZQUCw6uTGnzCR9Hdb+UtlVZXsLCUrrQqVgj4CHTPNL1qK9kNQtHU4z3DA3Q71kZyec6orA0+6z3oo3XPFN4sK+Es4acbs7DNM4Q3z0o4P/i5WWmoc4QfzixitM8Lvm9XO+Oc4Jlnt8tKOCN4+6yEs4HvftjXzjgTeLfaGecB75hhew7w8nrMZw8PSKc/dXiLoo9VESi8PKwiAHxmVOFVJhpD4dU5BH7hQRm2NJemyBrDR7eqHAKf8LBCOeXiF67w0rAKz/DLNQCdUpP3XhN4RQ6BN/jeCyC5mJLJXP8nTOGjW0kOgSd4WO0MQVYCCF4UVuENfrQBZdgqApVd4cXpQx7gb2HGbhPcYQEfRdVoQnz4OaQ0FM0fbJ6ArODLYRU+4CFFH1XFL8SyhD/OIcCF728WkP/nzI19L1v4I6+PCj+2r2lcQKcz6/dee/hSDgEePKzoI89KsH/0dIEv5BCgwY8sC5iXlbeXLhEOTvCHHAIk+P4GUvTRwrOX5Qi/TZhPkeChB1ezvaxAzvA7r48BDyr6yPMNnf8yAJ7nEFyC4acfoJrGKydj3wsCz90eNCwFtJclRgdXuWDwwKCaKLp3rhVjmIKkmnpP99QNY9Oqlfy9ckQnNlkJAnUn2lthLEqJWunFzb+amCqhyK5QjrOM7siwKCVq8RjUi39s8a2zEo5kGNyBRSkRh2f0bO1b/JfP3JurZ99pbrivQIKUqbUPPb+wMH2XrISiusZP3ViUErW+0g5MTZ90XLISDuoPzCsqY1FK1CqknNwYmD4BGrtdcAcWpUSt4s0IW/sd5WRo/GCbglTW8NrqVhiLUqJWOdNMbfr5agnqCWfb9yssPMf/V7b1ICuYsU/tgzuwKCU6hudr/x/R2qcElJXAgzvsz45IkDJV4ZPkpmr6lAIOrlyGHT3rh9+u/TI7fC/rdGzGopRICH/k9kj7A+bZl66vX1iUEkngC/i0AzN2SGMULEqJpPBbr39U9NFFoPdeLEqJFPDbHe9aU49ZI2AxaSxKiVTwDD/5hLDfjkGvX50ZGqZYaviE9z5y/u45pDEKpcclJ/GlhU90DVBkGrp09DywmwZ3QKSHT9Ibt75foNcvQH6vuQzgt72P7ObSHy8gK749A+0rjGUEnyS2fb8g772ykpP4MoVPU2PT784IZMWvl16BizKFT0y7fQI7egKPzXaygDfp++WalbATobAnIFvZwMt6Hx0E6oJEqbrkJL7s4HV9v0DuTdS4y68s4RVeH9bRk4Tx7GVZwzO9Cky/tuCO0PBpWvH6sOCO/MEudrFO+Eq3T1hHT2qQldAk+O2O9+s7YMEdxiUnmwOfJJePT9tv6MHCNsN6diz43WEXdEtFyRr2BATUnUEpdKnSm98g97bahHdvJVk3QCkJEs6k7NIXSnevl674gL5fspacwXWcweAf3jYrwaeMuiDgwbuVnPQnRd4iNjyN7bMSfEvbBQEH3i0rwbv692+2bc/s4QElJz1L3QUBAd4+BSmknkWl0LHgTTNs65NVt08beFBWQihJuyDA4OlK0xilIXr/abr2jeG/OnqegL4rGqA4wNNOnQdXa90i9v2i+QqWlRBeykodNvCksZ5dpa9S6BD4ZhxcXfT8qml7pu37VenoeUKSVeowhA8SXuFR75+qta/u+yXq6HliOm6AYggPTUFqiLJn6dqX9/2iwcIrfEva7VMCz/MNT9rYy5Lcc4nhw4ZXhJB53y9oVkIj9Vy95xL0/YqB+YZN1aiy4632/QJmJTRZo5/Kvl/QrISmix12pX2/OtfDM3FvMmWSvl80N+roeeoqHHYP8PWFV4TWe6XvV+zW9+s0tff6f/p+naNnV2h32OXwlB1c/ycr/qBRi5n+Nx44ebaeXSV22P0WKi+ggXoG9v3S6T8XJhWgat+isAAAAABJRU5ErkJggg==",
    name: "MaterialUI",
    desc: "React Components Library",
    link: "https://mui.com",
    width: "80%",
    height: "64%",
  },
  {
    img: "https://www.jdrf.org/wp-content/uploads/2020/12/AWS-logo-2.jpg",
    name: "AWS",
    desc: "Cloud service to host website",
    link: "https://aws.amazon.com",
    width: "146%",
    height: "110%",
  },
  {
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAvVBMVEX/////UAD/jET/iTv/TQD/PwD/SwD/SAD/RgD/QgD/iT7/j0b/RAD/gy7/PAD/hzj/+vf/9PD/+/j/uab/7uj/gSr/qnz/5Nz/o4n/hjT/qJD/v57/dkv/upX/6+H/3tT/sIX/glz/j2//iGX/sp3/18v/m3//Yyr/ybr/WBX/bC3/lXf/0MP/ZjD/cEH/tY7/wbH/yKz/l1n/1cD/nmP/fDz/lFP/czH/XSD/o2//hF//eU//rH//tqL/zbS/yN1xAAAGBElEQVR4nO2ca1fiPBSFKbaFcikUEcRR1FEURLzMOF7m9v9/1ltfRAo02WnaI641+/mspCdPT3KSpi2VCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEII+VcZ3ve+7aRTfzj53i6omdH+oeuFjeez3W5Bv2jI+GHQqpVV1GqDQa+IKzp3gorvxLh+wzvtF/CLhvRn4aRcVwY4j7J5lLeZkRO4zhI/vCji4k3YDeN+rfwAIZZbj1GuZvbDZHyvNCYfc6ueh/MufUIh1lrDHM1MPWcD3/+IEO/CRXMwxPLA/oL2UwKM89EtaghT01+27KMAy7VH22auw7QA4+Q4LDKYVC79ZXMTKLH1xbIZdz0HF3i7hYazyTh58xjcp62OVTPnDUWAcaMFR7TO5Wrf4gjvbVrp+IrwYgJZiePV/DeQuGMj8UCt0HFnhUeV5Hk9PUQkdqrqAB0nzDfN6hkH6x0qkokH2giDsUBkCzYUOq6AxHZFF6BTPZAIbc7dukIZiftahU5FsDydpE1Shc+JndRqJhHhVCa6UqpCI4mDbBKBQqeyLxSfQqHjFywxAgE61XOpAFMVxhKvcIhZymWk0AmupSJMVxiHiALMJDFK78cEodTyYlfVtIHEpnkmQoW+2OrCUVX7Bqsoc4lYodiEr1RYbCZeIIXus1CAGoVGmWi4LYUVelIK/+iaNpA4MJN4oS/Y4qYuhQIs6Vs2yEQjiREoZwQVatbcRUqcbk1hR5OEBUrs4iyUmu2BwtfFPgxxgBeuUKEvtr5HLcd921M/xpizAyVGih3ED1Co2zb5H3dWGjaRxCaS+BVmoZRC3c7Xsm9f8krcYhbqt00WfYslgkyECv0boQDbKMC3vsUSb3XNdGEWhiOhCHG1P+/b4R6SuKeTeLY9hWiYeU+PXBKHuJzZnsLFCJcrE3+i4UxsXQi3TRJ9i+dEpcQ+zsJjoQhNs/CVHJmIFf4SChBX+8kRDkqsnaQ3gxV6UgrhmntlhLPOxF/bU4jrjJURzlLiMb5TpA7TYIWrI1wXZmIz7fACVngqFCCu9tdHuN82Eo9xM1IK8YJtfZIyWGJsSjzcmkKDan9jhMMSf6//ywiXM1IKcbW/OcJ1s0u8gQrPhALs4hEuZZLKnIkjnIVSZ71sFJpI3Fu9YKiwsj2F6XVGxky8xs3kOQCoAy/Y0usMg0xMXvIl2qmsfBUKEC/YVJPUCZTYW/4xViiWhVihapKK8BJjKXEGFUodTBjmqDMySMQKAymFeMGmrjO6A+NMxFkopbCfq87AEl/mfzjGzUidYjuFCn9q/ts4E6HCqtQBKINtE+0kdbtjlIlYYbA9hfpqPzLLxM2TgB+lEK+5UZ2BJb4ozyAlaEgpxGtuVCpGJoWN6gzSUqHUETaDNTecpAwyESsMpI4/QYUGpSLOxPLVJ1ZoUmdAieUy7EgphXjBZlLtY4l1IFHsuDPeNjGr9nNLDOzeRsFghWalYhsOp3qJje0p9Ayr/aN8Ev3PrjDOxFYeiQ2pw85YoXmpmEtiRSjA0hlUaF4q5slEMYUl/Mg3Q6l4BO/TsurwuCMVIKy5M1X7betMDP5IRXiHTl5kq/YNJKZmhZxCeP4p44LNNhMFX6NEBxO8jKXiF6tMFFSIHGau9u0yUfJN2F19Hmav9m0kuhOJ0N4YaVelFgu2tsUSI7gTiOz9grSzRcOiVMwuUVSh/pm6VbWfPRNFFer3v+yq/awS5d77eUO9AWa5YOvA+rv+lLxxxF4aWTBW7dJYT1J/8WAzSdwoUgdll5wqHhyG1ufIH9CDmvKPpcSG/CdoOk7qYOPZb+21ayjE5X0aSt+jr0STlBC9PM/xIhzi1bzNUPrjLHPas42vQ4T5toWiRzSg1p/i5PDFR5l3DrxkMrrBc+7zq0dNoLFem4Q3H/hRtu60GlR89/VLaVXvuYgpuHvSGmhu1lpr8Cj2Gno6nfH0Jh5zLk/Pizo21/l++/BYS/9O37fevdS5IEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQj4//wFur3yNQ7KoqgAAAABJRU5ErkJggg==",
    name: "Namecheap",
    desc: "Domain Name Provider",
    link: "https://www.namecheap.com",
    width: "100%",
    height: "100%",
  },
  {
    img: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMQEBUQEhAVFhUVGBcWGRUXFxcVGhcVFRUWGhcXGBYZHCggGBolGxUVITEhJS0rLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGislICYtLS01LSsrLy0uLS0tLS0tLS0rLS03LS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EADoQAAECAgULAwMCBgMBAAAAAAEAAgMRBBQWUZIFEiExUlNhkaHR0gYVQRNi4VSiY4GjseLwInHB8f/EABkBAQADAQEAAAAAAAAAAAAAAAABAwQCBf/EADQRAAIBAgIHBgYBBQEAAAAAAAABAgMRBCEFEhUxUVKRExRhgaHRMkFTceHwsSNiksHxIv/aAAwDAQACEQMRAD8A+4oiIAiIgCIo4sQNBc4gACZJ1ADWSgJEWlPqei704H+K8tPRd6cD/Fdar4FXb0uZdUbtFpLT0XenA/xS09F3pwP8U1XwHb0+ZdUbtFpLUUXfHA/xS09F3pwP8U1XwHb0+ZdUbtFpLUUXe/sf2S1FF3v7H9k1XwHb0uZdUbtFpLUUXfHA/wAUtRRd7+x/ZNV8B29LmXVG7RaS1FF3v7H9ktRRd6cD+yar4Dt6XMuqN2io0DKkKPP6b5kaxIg/9yI1K450hMrm1iyMlJXRkihrLb+hSsNv6FCSZFDWG39ClYbf0KAmRQ1ht/QpWG39CgJkWDIgdqKzQBERAEREAREQBERAEREAVLK1FMaC+GDIuEgeM5ifDQrqijRQxpc4gNAmSdAAGslEQ1rK3E4E+l6Tuxjb3Sy9J3Qxt7rpD6uou8dgd2S19F3hwOXXeVxRXsWfJPp+Dm7L0ndDG3ull6Tuhjb3XSWvou8OBytZOy5ApBLYT5uAnmkFpleJjSixF8k0cz0O4LWlGaXirf6ORsvSd0Mbe6WXpO6GNvdd/EeGguJkBpJVH3qDtHCVxUxkafxyS+7S/krjoyEvh1n09jjrL0ndDG3ull6Tuhjb3XY+9wdo4SnvcHaOEqvaNH6keq9zvZPhL98jjrL0ndDG3ull6Tuhjb3XY+9wdo4SnvcHaOEptGj9SPVe42T4S/fI46y9J3Qxt7pZek7oY2912PvcHaOEp73B2jhKbRo/Uj1XuNk+Ev3yNP6XyHFgRDFiSH/EtDQZkzI0mWiWhdNHZnCSo+9wdo4SnvcHaOErmWOoSd3Uj/kvc00sHOnHVjFmdXdd1C9q7ruoUfvUHaOEqxRacyLPMdOWsaj1UwxVKb1YzTfg0zqVKcVdxa8iOruu6hKu67qFcc4ATKirLb+ivOCCruu6hKu67qFPWW39ErLb+iZg8o8EtMyrCwhxA7Us1ACIiAIiIAiIgCIiAIiIAqGV6H9eA+EDIuEgbiDMT4TCvqOLFDGlzjIATJPwAoaurMmMnFqS3rPofOj6OpOwzGF5Y+lbLcYXWWro227CV7aujbTsJVfdVwZt2/LjD98zkrHUrZbjC23pj01FgxxFiloDQZAGZJIlpl8SJW2tXRtp2Er21dG2nYSuo4azvZldTTbqQcHKKTyy/wCmzp9H+rDcwGRP9wZ/+LQezRtkYgrlq6NtOwlLV0badhKoxGjqdeSlNO+7L9ZmpaQjTVoyiU/Zo2yMQT2WNsjEFctXRtp2Eq3k3LEGkEthu0jTIggyv061nehqK5uq9i6Ok3J2Tiaj2WNsjEE9ljbIxBdJSI7YbS9xk0aSVqbU0fadhKLQ9F7tbqvYuWLrPcl0KPssbZGIJ7LG2RiCu2po97sJS1NHvdhKnYtL+7qvY67zX5fQpeyxtkYgnssbZGIK7amj3uwlXcnZVhR5iG6ZGsEEGV+nWEehqK363VexDxVdK7XoaX2aNsjEFfyPk58Nxe+Q0SABnrI0nktnSqS2Ewve6TRrK1Fq6Ntuwld0dF0qc1ON7rx/4Z62PdtSbSv5G6jQ84SVWruu6rzJuVYVIn9N0yNYIIP/AHI/CuucAJlb81kZ4yUldMp1d13VKu67qp6y3ilZbxTMk8o0Et0lWFhDiB2pZqCQiIgCIiAIiIAiIgCIiAKllSh/XgvhTlnCU7iNI/lMK6o4jw0FxMgNJKXtmQ1dWZwp9JUi5uL8JZOk3NxfhdUctwr3YU97hXuwqraVH6keqKdk/wBsjlLJ0n7cX4SydJ+3F+F2NEylDinNaTO4iXJWY8ZsNpe4ya0TJPwFfTxKqK8GmvArlo+nHKV15nC2TpP24vwlk6T9uL8LoD6to97sKuZNy1BpDi1jjnATkRIyvF6s15r5ehTHD4aTspepydk6T9uL8La+nPT8SBF+rELRIEAAzmT8nguocQBMqKst4rl1JNWL4YOlCSkr5eJXyzQvrwXQwZEyIPxMGYnw0LkrK0i5mL8Lta03ilZbxURqSirI3QqygrI4qytIuZi/CWVpFzMX4XcQ4odqUVLpTILDEe7Na3Wf91lddtIsVebdkvQ4yytIuZi/C2/pvIb4EQxIhE80tDQZ6yCST/JZWzo38TD+V5bOi/xMP5VbxKatdF8qGLkraj6Gwy9k40iCYbTJ0w4T1THweGkrlLKUi5mL8LeWzo38TD+V5bOi/wATD+VzHEKKsmjFV0RVqPWlTlc89MZCfR3uiRCJluaGgz0Egkk/yC6GPDzhJc/bOi/xMP5WwyVlqFSp/TcZt1giRlfxCdqpveSsDUoQzg0vFE1VdwXtVdwVt7gBMqKst4rq7ODyjQS3SVYWEOKHalmoAREQBERAEREAREQBERAFXpsD6jHMnKY18fhWFFHihjS9xk1omTcAuZRUk4v5kptO6Oe9iifbzPZPYon28z2Vg+q4F0TCO68tZAuiYR3WHYtHhLr+Db3jEcPQzyXkp0OIHvI0TkJz0kS/9V/K1D+vBdCnLOAkeIII/lMLW2sgXRMI7payBdEwjutmHwioR1YJ77metGpV+NfKxoj6RpF7MR8VsvT3p2JAi/ViOboBADSTpIlMmQ0SVu1kC6JhHdXMm5ahUhxawkOAnJwlMXhaZOds0Yo6PjBqWq8uLNhFZnCSq1Z3BW3OkJlRVlvFVl5DVXcEqzuCmrTeKVpvFMweUeCWmZVPL+TazAdCDs06CCdUxp08FsIcYO1LyPHaxpc4yAXEmrO+47pylCSlHej5/Ymk3wsR8UsVSb4eN3iuw99hXP5DunvsK5/Id1g7bCfUXVHrd+xnIun5OPsVSb4eN3iliqTfDxu8V2HvsK5/Id099hXP5DunbYT6i6od+xnIun5OPsTSb4WI+K3npb06+ivdEiObMtzAGkkSJBJJIGn/AIhbT32Fc/kO6s0OnsizzSZj4OgqylVw0pJQmm/uU4jGYqdNxmrJ78ixHh5wkqtVdwVt7wBMqOst4rceYKPCLdanUcKKHalIoAREQBERAEREAREQBERAFUylRPrQnQ5yzhKdx1g8wFbUFJjthsc9xk1omTcAl7Zkq98jjj6Sj7UPE7xXlk498PE7xWzPraj7EXC3yVzJPqODSH/TZnNdKYDgBMDXIgkLpYq+SaN06eKgnKUHZeBoLJx74eJ3ilk498PE7xXaRooY0ucZAaSVrvfYVz+Q7ripjoUnackvuVQnWn8Kv5HOWTj3w8TvFbPIPp58GL9WI5ugEANJOkiUySB8TWw9+h3P5DuvffodzuQ7qp6SotW7SPUmUcQ1bVNlFZnCSq1V3BeUPKTIpzWzB1yIlNT0ukthMMR5k1ukld06kai1oO68DLOLh8WRDVncEqzuC1NsIGzEwt8l7bCBsRMLfJW6suBn7xR5kbqjwS0zKwyjRfqwyycjoIPELUWwgbETC3yS2EDYiYW+S5nS14uMlk8jqOKpRaakjD2GLezmeyewxb2cz2WdsIGxEwt8kthA2ImFvkvP2NQ4S6s1bVXNHoYewxb2cz2T2GLezmeyzthA2ImFvklsIGzEwt8k2PQ4S6kbVXNEw9hi3s5nsr2SsmmE4vcRMiQAumD8/wDSzyVlqFSZhhII0lrhIyv0EghbF7wBMrqno2jSmpJO64s6eMlVhk1Z8DyPDzhJVqs7gpay24pWW3Fb8yk9gQc3SVOo4UUO1KRQAiIgCIiAIiIAiIgCIiAKllWhiPBfBJlnCU7jrB46QFdUVIjthsL3GTWiZPBQ1fIlScXrL5HBH0NH3kLm7wWz9O+lX0eMIsR7Tmg5obM6SCJkkD4J0K2fWMDYicm+S8tjB2InJnkuVhknezLqmm5VIuLmrPgjeU2B9WG5k5T+eIMx/ZaT2GJtM5nslsYOxE5M8ktjB3cTkzyVVfR9Ou1Kad/DIz0tIxpq0ZIewxNpnM9k9hibTOZ7K7krL8KkuzGhzXSnJwAmBrlIlXqZSmwYbojzJrRMn/flZnonDLen1NVPH1KmcGn9ka/JeSnQ357nDQCABP5vmrWWKD9eC6FOU5SOuRBBGi7QtMfXNH3cbCzzS3NH3cbCzzV9BUaEdWD8SyrgsVVvrwfD5GusdH3kPm7xSx0feQ+bvFbG3NH3cbCzzXluaPu42FnmtPelxMWwp8j6mvsdH3kPm7xSx0feQ+bvFb/I3qCDSnFjM5rgJ5rgAZXiRI+QtjTKU2CwveZNH+gDirFVb3GeejYwlqSi0+Fzj7HR95D5u8UsdH3kPm7xW2tfB3cXk3yS18HdxeTPJWf1eBOzI8r6mpsdH3kPm7xSx0feQ+bvFba18HdxeTPJX8lZahUkkMmHDTmuABleJEqG6i3oh6Ngldp9Sh6cyC6jPdEe8FxGaA2cgCQSSSBp0Bb+NDzhJeveAJlRVoXFVNtu7LadONOOrHcRVU3he1U3hSVoXFK0Lil2diBBzdJVhRwowdqUigBERAEREAREQBERAEREAVTKVEEWE6ETIOEp3HWDzAVtQ0mO2GwvcZNaJkoiGrqxxp9Gxt5D/d2SxsbeM5u8VtT6uhbuJ+3yVrJfqCFSH/TAc10pgOlplrkQTpVzdRb0UPRsEruL6mgsdG3jObvFLGxt4zm7xXZx4wY0ucZAaVrPfmbD+ndZquNhSdpySJho2E/hi35lHIHpt0CL9WI9pIBADZ/IlMk8J6Ft8sUAUiC+CTLOlI65EEEGXzpAVb35my/kO6e/M2X8h3VEtIYeW+aNlHCVaNtSNrO+/wCZy59DR95C5u8F5YePvIXN/iup9+Zsv5DunvzNl/Id1R3jB8/qz0++Y7guiOWsPH3kLm/xSw8feQub/FdT78zZfyHdPfmbL+Q7p3jB8/qx3zHcF0RrvTXpp1FeYsR7XOkWgNnITlMkmVy3eV6AKRCMOciZEHXIjVoVX35my/kO6e/M2X8h3VsMbhobpox1Y4mrPXks/L5GishG3kPm7xXlkI28h83eK6ag5SZGJAmCNMjdwkrr3gCZW2niu0jrQaaK5VasXaWT+xxlkI28h83eK23p/IJo7zEe8FxGaA2cgCQTpOs6AtxWxcUrYuK7dSTVmcSrTkrMkjQ84SVeqm8KeFGDtSyiPDRMqsqK1VN4Sqm8KSti4pWhceinMCBBzdJVhRwowdqUigBERAEREAREQBERAEREAVXKNFEaE6ETIOEp3fIPMBWlDSaQ2Gxz3GTWiZKLeSr/ACOSPpCLvWfuV/InpwwYoiPeCWzkGz1kSmSeBOheH1fD3T/2915bCHun8291e+1atb+DQ3Wasb6lwBEYWEyn83SMx/ZaWz79tvVR2wh7p/NqWwh7p/NvdYa+j4V2pTjn97E03Wpq0UZ2fftt69ks+/bb17LC2EPdP5tV3JWX4dIdmBrmulMAy0ga5EfKoeh8OlfVfVnbxGISu/4Ktn37bevZLPv229ey6FzpCZUFbFxXOysNy+rOO+VeK6Gls+/bb17JZ9+23r2W6rYuKkhRg7QmysNyvqx3yrxXQ0Nn37bevZLPv229ey6Fz5CZUFbFxTZWG5X1Y75V8OhSyXkswnF7nAmUhLUtnFh5wkoa2Liva2LitdGhCjDUgrIoqVJTetIjqpvCVU3hZ1sXFe1sXFXZlZ7BgZpmSs40POElFWxcUrYuKWZJhVTeEqpvCkrYuK8rYuKZkGcCDm/KmUUKMHKVQSEREAREQBERAEREAREQBVqfRhFhuhu0BwlO6485KyoKXSGwmOiPMmtEyiF7ZnKH0c/fN5FLHxN6zkVZtmzcu5heWzZuXcwr71f2xztKHOuhXsfE3rORSx8Tes5FbbJPqKHSH/TzXNdIkTkQZaxMfMltqTGENpe7UFxKrKOcnYup4qVTODucnY+JvWcitjkT079CJ9V7w4gEAASAnrMzwU3v7d27mFZoOVWxXZsi06xPTNZI6So1HqKaz8C6ca6i9ZZeRfiMzhJV6qbwrLnSEyoK2LloRlMKqbwpIMDNMyV5WuCVrgmYJorM4SVaqm8LOtcFnCjB2hNwIaqbwlVN4Vl780TKhrXBMwYVU3hKqbwpoUYO0LN780TKXBWqpvCVU3hZ1sXJWuCZgwqpvCVU3hZ1rgla4JmDKDBzdM1OooUYOUqgBERAEREAREQBERAEREAVWn0URYbobtThKY+LjzkrSrU2ltgw3RHnQ0TP8vgcUvYaut/5te+XU5Y+jH/EduA915Yx+/bhPdZH1234gOxjsvLds/Tuxjso70uPod7Bn9N/5fkv5E9NVeJ9V0TOInmgCQExIk6dOgnmt5SoAiMLDqP/ANXKW7Z+ndjHZLds/Tuxjsq51YTTUnf5GilovEUlaELea9zZWfdvByPdWsnZJ+k7OLpkapCUp/K0du2fp3Yx2WxyJ6oZSYn0swsdIkTIIMtem9ZKWDwkZqUVmvFv+S+tSxig3NZfPdu8jfRGZwkq9VN6sufITKgrXBeijzTGqm9Kqb1lWuHVK1w6pmDGqm9SQYGaZzWNa4dUrXDqmYJorM4SVeqm9ZVrh1WcKMHaJJuB5BgZpnNSRWZwkj35omVDWuHVQDGqm9Kqb1lWuHVK1w6qcwY1U3pVTesq1w6pWuHVMwZQYObpmp1FCjBylUAIiIAiIgCIiAIiIAiIgCqZRobY0J0J05OEtGsXEcQZFW0QlNp3Rw59CH4pAwf5LywZ/Uf0/wDJdyiq7GHA3bTxXN6R9jh7Bn9R/T/ySwZ/Uf0/8l3CJ2MOA2niub0j7HD2DP6j+n/ktnkD0sKNE+q6JnuAIbJuaBPWdZmZaP5rpUUqlBO6RxUx+IqRcZSyfgv9IwiMzhJQVX7uitIrDGVar93RKr93RWkU3BVqv3dEqv3dFaRLgq1X7uizgwM0znNTooBhFh5wkoKr93RWkQFWq/d0Sq/d0VpFNwVar93RKr93RWkS4IYEHN+ZqZEUAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgP/2Q==",
    name: "Prettier",
    desc: "Typescript Code Formatter",
    link: "https://prettier.io",
    width: "90%",
    height: "80%",
  },
  {
    img: "https://warehouse-camo.ingress.cmh1.psfhosted.org/d3a1a77162e3cd8c3d2089f27899b6eee71af013/68747470733a2f2f7261772e67697468756275736572636f6e74656e742e636f6d2f7073662f626c61636b2f6d61696e2f646f63732f5f7374617469632f6c6f676f322d726561646d652e706e67",
    name: "Black",
    desc: "Python Code Formatter",
    link: "https://black.readthedocs.io/en/stable/",
    width: "240%",
    height: "100%",
  },
  {
    img: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png",
    name: "PostgreSQL",
    desc: "Relational Database Management",
    link: "https://www.postgresql.org",
    width: "80%",
    height: "80%",
  },
  {
    img: "https://pbs.twimg.com/profile_images/476392134489014273/q5uAkmy7_400x400.png",
    name: "SQL Alchemy",
    desc: "Python Database Toolkit",
    link: "https://www.postgresql.org",
    width: "95%",
    height: "100%",
  },
  {
    img: "https://programmer.help/images/blog/cd9c7b39766d44f1f0143b92b5ef1180.jpg",
    name: "Flask",
    desc: "API Development Framework",
    link: "https://flask.palletsprojects.com/en/2.0.x/",
    width: "80%",
    height: "8x0%",
  },
  {
    img: "https://reverbc.gallerycdn.vsassets.io/extensions/reverbc/vscode-pytest/0.1.1/1617123275355/Microsoft.VisualStudio.Services.Icons.Default",
    name: "Pytest",
    desc: "Python unit testing",
    link: "https://docs.pytest.org/en/6.2.x/",
    width: "80%",
    height: "80%",
  },
  {
    img: "https://www.radview.com/wp-content/uploads/2021/02/selenium_logo_square_green.png",
    name: "Selenium",
    desc: "Front-end unit testing",
    link: "https://www.selenium.dev",
    width: "80%",
    height: "80%",
  },
  {
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANYAAADrCAMAAAAi2ZhvAAAAflBMVEX///+ZQk+SLj7t4uOWOkiYP0yTMUGUNUSRKzyVN0aXPUqRKjuTM0KVN0X07O3q29348vPgys3BlJqtbXanYGr8+fm9jJOjWGPTtLiyd3+oY225hYzk1NaeTFjKpKnbwsXRsLTXu7/IoKWdSVW0e4OranOhU1+MFSyJBiWPIjWIlMr0AAASsElEQVR4nO1daZequhIVJGEQJxScUHFo3+v//wefdrdtdlIZsAF9a5291v1yjw2pVKVSM73eP+gw6b8R8sbI6v/Hfxv8tzmyenPmvQmSc3NU9arhq8m5g+8bJGsyeDU5dwQNymCvt34TKQyalMFebxm+mqBv8GmjZI34qwn6RtwoVb3eR/Jqim4INg2TtUxfTdINDcvgVRdGrybphqZl8D2kMCgaJ+vwBlLoNy2DVyn04Q1D3oUByNG6YY1T1evNRCn0Jy28gUBfvFhakEFJCuNdC28gkAXiXpYtvGEiksUuLbyBgCcabc3rwRtWIIX9Vt4hoRQPdJC18o49SOGxlXdIKFqXwV4vF29ktm3lHRJEA5udWnoJSGHUgRROQQ+2I4MvkMINyOC4pbfkcRcyIUBUg8xr7TWoC9vavV+ADMZtyaAshe295wcgg1GLuyheI+3rQt7V285d7d8NKINtaqhuNO4Pzp3owS+Ib2rFT3ggF52Slm3Qbm6SL+w7k8FOpRDPccs2jfiuVm/kHPygtrXupgOb+gv7Tr3WzqSwY7sa7MJadtq0joZBL2hRc5H18bxnt17V+DFETsL2Iyfoh9eIBZVRnUABxrlG9ddZFxA1Sdz/bp3USAxgPKh9GZSkkDtLYRnVOfkYvaueWmg9PCmFtxi+O7sg4j/oQAZRF3quunDs11kg5KrZ/Oml1gFGWh2j/d8qwJVdkNMNq6eXWgdjkEK3hY5/riHuxq61KIOOf/NnnERdyJ3+5K6v3c4iZKrZ+k+LdQdIoVPe88HgyCXVUoEMLv+6XkfUl8KHgedkRkIVyGdHOSdJChN7XctYMPC4fZUj0R5MPppYsROOoo53kELREnKIw0GFVdqVDOLuO0hhHyqm7OxasFo/bw5b8cWh7dfgOtnZhTI4a2jJLqglhX1Mpnu+5TDuRD2YHppbtRWwUlvVGMRaPHsU6WUy2OtdQAqN299X6m/MNYGwZZ3KoCyFxopMmVk2du1AD3YpgxILjPWzKrMs7AVBsJ3DprF1lUKVWebYBNwGSZ3wRxNAUdFLIcUso2VSQ7xbAEqhflMpZl33odL+AYjBsGMZlErjB7rXj+iSbH2AEczoRove3YCXpk5YNiSzDOwCGTQId1sYwbZqpFBbucx0kTiQwai11esBxkBIGwM6ZmkNczSiu5fBq/sA4kJemyN9ga8miZTVdHmaB2gD2shBZjEw5DnJLlbTQW0B4JpTMQo4fld3v7Dm/J4JajWNyuZAABmeP0Jtz4m/qB/8aR6g5oiQg8SsjXQ3U4nTU/vFnnZgkFKRQmTWLU6NDqVqGZVvIINSSFnR2Njz9b1IZJdSa1E8EwZvHLBwRQqRWd85IClYI7ML/qJG6qxhfBhK49HAuN+skGSU2fV0orNhYIIXpbCAfNE9vzrG04Vy1kXBsQugQQNTANi78TCDkF2YPe2o2NMOWORAlEJk1uA3u4rsArXQafmbESiF1eMfJuTJuuFDy67OCnSswIS8IIUZebJuKMH8Fdn1JnrwhhWdNgRyvQC8MWTXIy/8PjIoFVw9pDDTnKwbSjxdv8LWYamiFTnowvvW53Dryq4zREGSX8ntrODYBWeqgAKZJZcqT4Fd0Q+7uip6dwOUnv5I4QSoUj3MBcWuDguOHZCD8fqtr4/Q0agucUqcrryzslI3nJVSshz6/6lwALLr6weNFxxP+uOyHD89C2Uq6sLhLbiOzBoQ8gQ0fJ+9Jlv5RvvNnEU+59z3+Xa2e0ai0QdmH9kUIjF07GYrsWua4V28fz5Zl+8WPA0ez2fJ0A82NSk7rKUuaxbj/6APP7IrCbgUT0z57DknclREMTHaI4jm7gHi/BiklvEgumzixTZWJOFe/cqFSSHvz2O7+daRsCqINc94INJYrAf7/A2WnmqG4Je+Noh8e160ctAf44tDk3+iLcPaOkyBYf68RnFavvItzwsSq2TvfJfpNDpmuU4+SEJnhvU9E6t+9imylGhbd+Z7VYY8vec2tGfgWCteOm2z55uyFfncbe6TqXb44Di0x3cK1UydtvmK1EDXws7vbxhyv5nzMxzoKj8dH2Z63Nx1RZ73qTukR9ftvfLLas6PAjeJ/n6cRqw3dSaPcdoO2tcZbRPZbrCLOlCGBWGapnFC0Dsgt3pJ7HMShFdQD2EedVnI1V33hXCeUkbCwGwjFvI+szRcZ8vD/rBbeepdxhLCNOsrFym7mjrZ8ord6qQ+hMyUKrcWC9P15jAty/1xEYXKvxqd5VLiPIvW+8dejouBbDVQS5on8kPm4kOySN46IkWQSVqQ+ZfqsYOjisk2WWryVKQ9SueSGZpnsvIfKNfpUmJWupAfcoyRcDU+K1fWBCf5NYeTxPVPvbkhrYi6ccfSVa324kjHh3rI5IJcV2owpMoaTthq+RkXa0j6w8sY3cuTr3FJsgRVqcNDeiu0jKSqV8wGeT4tYDvUKlrPEq2wUPezGWbnJVsVsqAe13lnG3iXlFfG/L9faZ6BdGnzQhBA0NugkiuELVdoo9AXwBdmROTmBxCMMSkDVCwp7VXAVWHSLHigMWIHEYfQcPvnoDZAgkBq9J5LT9pgTR2oWCNlvgeOMNgG9Jj7YAnwE6EsEkJVkcmhGkMpAa00xBCxJdsMUijuNEReLSWMoszDCRVl0JLNB5VJltuL6WtbC6W2CBg4bglRgqwJpWaQt9NYjHfAeSCDPaWw0bYWypEuNS0mH4cW/w40Q/Ix+8FqruMiBfHHZNOdWMljbeMFARI6E0X1bjwVN4AeZ8kdUCdDrVTEThAPUsWLWXTr+A6NsE1A49oeUtndX+29d4cosWQEQZAfexU2JOMeF0bf9hLtknRk2SJcYvqM1AiCXNkTmOJBFErqxIXaK5JGdrKsvUSiUiYvFIEsez4Cru6HhheptRe52Ge7OtReiDcyVTW5fp6s3wPwArIEVUhySzhbdiEc09wCIbSWBzsIob1iUOAWOTxIuLDtKgNyVg9NLlYU27uBS3uY3dqcJQ6kIXvgxbCctZRDvC6EvhhQ8NaddhhKblXwoHspbojdodbreK1RQGLUwJq1XqlRLhnWbn3x7iPPjigStqdBdZl4QX3UaLTNHWKk1tkKYDxR0UIwdS2Tu8A+EPUmWB8WNQamLuMPiHRZksQjq6kLdp7FMYFkhljKCI6Jpb9PFFj2MS5/UcMxAbNySP5E1BlmdkGvDxo44EYaPRMIc4H8wJkzxmsh0qqx1qCCKjV4FXjf4NPOrk7/BCtORB2F0mk6XRBp1QnH1jFEg0FS9IFBCr2B3j+GFeHWwIA+L9RbKxmEr3RmMcpWquP+B2qwGJ+GOURtieYKqyQNlcbavEyvUpoeSMD4Lo/RiiWfY/hzKNmPkhMV0bpnBgyRjTmpsTCiTdQjmpT6azKTAraV+pPxVrptZPMml+5YKljdl4LVvnwqnghWG9y7iZQQ4XJqYVLIqQWZWURq4SIJopKfUN0/ObsVBPJNuwyk7TVpzEpaEhvMhOKislCSU1QH40JJBK0Pj4eMMy5nk4iBrnIiyOOn6qEsR9VJtpLN95u8JC+5pe2qw355pNJ2ZDRa7Y9maTrPquWhOp6JtB15Dahpu9i/bHa3/OHm4itpO0uodUTUeHwlWcOYskuHpPKt6ARpGA6pJGtCxiTpJOswvC6Ezm6bY637Oh+colcka28zWEB7C4eaX3uxGMU16gbIzPENOZFY14Eqkqy7kC9YjOLCtaaCxdoHTU6udBls6qweXZZIheuHtJjWDLnRJV9vmmdobusfumrJITPHbB2pCk5GDzqXiz7JZ3hmwamiOp9gM/bS7NzI8q0VhXZh9me2AMxUvnRNMEmhbPtonsAry4KuT7LsdGAYifGLycxYLpfY+nnvqFwqNqOVQ82lXHUiERVt3GqjpxctYcyf4ZhR/aqYVZqDwdylIexo2B8Wf67cC9n3hGXytY7FVMp3a29k/JIbC1PZLAgTt4rxqU6LXc0N/7KrV8VebhI/ALcz9lnxtTFiQk0f6oKqiuCcL88nUCGuUzblcq404jxNOfcHp3P1RMdBXmYfYeRfn5FenxF8HO8CI0qh1jfBovpvxwymXrgNvOzlWHTi+ftJWU730/JPPRSj8XR/2E/HIqvFyldtXInoWHHbDwlrKS7QXvM95Dg0BSdkOxhMRHRrxFnh3cdYe9M6IJmkyVaTzXsiB90G9RSSEjQEsf4OkHVSm2Fh2H0xYrrPaQB1JqkL3urHHWB5pIWJBQV3/2VbkyzZ8A7anTg6t5GFrcC/P6lJlkwVXWHcHD5sZ2RHf+1kYeWyiI0cO7HUPP8ZIrdSank4kfmXcDFDZ503PJNNJkNFYTOw7Tpod8EnE0ORluz9ZCEHMbSB5sYgvo06I5B+Ecqw3MsMx4FstPPWm57h3iLCGTjoQZh4BnVApqTyUvEgwvZHnGElhaqdIIQhxkjB9jeYuhslojLsYIgqlk2r/w4WBoSPnTzQ/kVpl4m7GJENdXVqJQhkGDHWD+kLTfC04or7Oexk8LeYTiMUGg7dAz0Oh4tk1+RDrYeJOxnji96xmmUFGcRxsTmsmagkr7gatki7GU4MEzNVRQhjOeTKa6mJQDKTx3OidIl3NN4MEitq/BMETb6scYgK80TvdHQeEEGdqKMhHTidRt1KCEwpI4vBafaSxe+/5xnZSz6oWibnDjg6hOkkMlMt9JLCSMEPXZNjSmV8krCrCUx7ECO1iAbUO2HPrlEnBKfr4ewXSrb0C/Glo4+lSCeLUO9wtIjP9Mh5U5buVpquf2OXdbPILKNfsNyJqlRSUtRkzvUWP65ap+aOEmIUVDZSjC7ROXOXeQhXYredfBf9CzkGnKn8H9RfkRVRVIpaYVVXev0LWJRAhT5hlqWmyMaeoo5PXc7i/DC3aN4ATstQE/a3pKjZoNM5WVg0RX9QR1SEegdYjv8BuNxd3CrkejL6m3hi+bchzK5PvQ/j7r7Tc0Vf7qWm6zFF08mUgd3R5ytOj52OxN7JmXJNqsS5R25PJDwDv+j0uy/9tXIYNJ2GYhzbXA7d30rJkLBjokYb1bmLNEv2agRuM0ECGD/tOhW/8SZSTTZt1A6Grtk0Wrn4lkQWD9adfsBhclhTdmio6/PJzZ6zgv2Ws8A/Zd0ZSlfpr9YRObdMH9+CPh6nEXmHbdHdVNt8vCy20ZA2SmN9OxWS1Zm35ID+dLe5xH6onftjigRNXMPRnSEflYdsdYl8rqfoa7GmSNA7cSsf73fnBfN5GlCjigDMPGdsUlNltIS8rDangU+PXiIQeGaljZrwJZNdJ/vNglskTmJVdLZdmTBWuvvB3eURh3G6IGT2O/PkbDw1j7JIfGrOlhEBzxysGzG8aRsH0SjGBdUzYCXqc+Ok187WCE0bGO1Ofk3R826xvLhwVNYQT+vmu9y96YxoqbDSFA8WS2fjGnquWqy7+sVk5xHJIwtJQcovuzrXD3Sht39xjc/qyDorSdHpfKhpKUAVQNufsJ7O62gJlsQpH5yKwzObDQV3bX5gMq88Ncus41DI/eCyOh7Gzx4L/M5Do4SImNhnPX/RM0z9QbzeVNPR38456Iy2PtY10mSOgKL0yqDNbv80gwAdfGKyT4UhgKTYT9fZ4emx8xTgBS18GHm0MeuJgEfzXdn41QLJ/CYm/gP6m4GJqICzYtrKbYk96+aPQ9dF/2ziVJLyczs03QBkqc3Ez6Ovy8bewGK+PrRp1ahfz2sE49VAbyQl/vbYcogBPnVKp4vqo5z5eqLi6MmvJdQCfEbJc2g7s6Jc6znFUtY2o74hzUv/c6dBudZzivGTu3vxR0ChjcfCP23m1Z7VEpX4Nb6t8WdgmYNUsVUPh4W+ozEYzLoNAs1wf5n3HL/yncFID6JN13FI+fvXjGwCsD0k8zVJgBtR3DUM0SSULs3a5SPjs6830oPoFUT1iA/YhJc6Md49mVb7Qcy7zcYKUCuA2MAlznjDOEsMfm/gZ68iqkdWAMXWGbC9W8jvYjJnr+L3QqKuOBKVMmlgznuPluuBKZIUvE78fnGmJhWEwVmjFPPyuDCHMeNXKQrEBzmBIeB8tSxh0/NyWVx8bk4LxPErz5QIpbPsB0nII299LrJjlhWr+SmypqOYTXw7hdraI6w0CIL4+l9iH2nA0qR6H6J62oqtWmD+ttOiNRdMa8f9JSTR/FVfZTdhQvWNOCPgq1d/WlSHncFdN4Kl4VtodA1Ga7ePpkmM8i+d+b1PYnpxTW78IOFJ8a7SJ+JwcicsSQer/Zsz6hd7txxbkKazVsOYjWNcKN9DldgU+/H58CYmUh1MC0bXvt3SUfHHU5/VfQ/0l5tL6PP0ZjjdEMfp1UKcF4f/X5LuyPvTw64oNptNUWTVvpz8Xx2mf/iHf/gHA/4HxHQG8/mlsNQAAAAASUVORK5CYII=",
    name: "Jest Typescript",
    desc: "Front-end unit testing",
    link: "https://jestjs.io",
    width: "78%",
    height: "90%",
  },
  {
    img: "https://funthon.files.wordpress.com/2017/05/bs.png",
    name: "BeautifulSoup",
    desc: "HTML Parser for web scraping",
    link: "https://www.crummy.com/software/BeautifulSoup/bs4/doc/",
    width: "120%",
    height: "60%",
  },
  {
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAb1BMVEX///9yidppgththdmsuOjS2fJngNjq7flshNlwh9pngdj7/P6jsebe4/ZkftfAye33+P2AlN2ZqeO5w+uxvOn4+f11jNuotefv8fp9kt3O1fGMnuDa3/TI0O/k6PfX3fSTpOJdedaHmt+2wOqXpuIXpP81AAAHaklEQVR4nO2d64KyKhRAFRoCNM1LZlrWNL3/M57Kmilzo80I+Hn2+ktFK+S+IcdBEARBEARBEARBEARBEARBEARBEJss90k6M0ua7JfG9MKIEU5NwwmLwpUBP29GuHDtIDifaRfcutySXg13F3oFk8Cq34Ug1Ck4Y7b9zrBUn2AibdtdYdpKsRpDCV6Qc02GG1ttaBMR6REMiW2zb+ROi2Fm2+uBjQ7B/TiamRqmo1f0x1ILL9C1BkPbUs9oeEzz8bQzF8jwE43FmKqhli5xP64ylMM3NR/jMiQfaIiGaGgbNERDNLQPGqIhGtoHDdEQDe2DhmiIhu8gBIUTORW/W2geiaHgjGeln4I7HiJJ40hIovgNRmxImfB31XXh9gsw4NUl1cuL2Ya9Gf5g3VDILL1+fccrYg4WUTarbh8/TyL2TklaNqTBaVu/a+FTcimcc2UkRLJvJOHnKngp6M1Xfpdcs/5xHlYNKZ/VmwqrMGP0rHaujFGchsVinudLz/NWy3m1LxK/zBjhggfl/pbHKhV9HS0aCubXfvmaSkJImRZzMJIp36clkYRF9z1db8b61Ud7hrKst0xyP2Bsk257fPI23QSBe3fMT722gGwZCl5cX+ytgyAK+28PzZPNZ3b/zh+qDtSyIS/rB3QXkPTd3a9q/Rnd3uOdumujHUNxqL/r5lj8JgcvFPe4vLizFO0YsmsZpKffb14W91rcWRetGIrYuQSf5p0fpaKodz79rkK0Ysgu45OBQlznXQFmNgxvtXAgDh3dog1Due/+kP4sOgrRhuHAES4dYYIWDPnAQZ+huk+0YEi9gTNUP6bmDak/dIbqDsO84fDBguqIZPOG2eAZOptRGfKvwTN0vlRtjXFDpiG2PFc9pqYN9YSWHxVdomlDngyen6PuEk0b6nhIz8NvxRzKtKGW0Hlla2rYkGs6iaRoTQ0bagjYvaLo9E0/pYPndgNuTM0aXpcvtBCDimYNOXwKaVXM4vUOWrq5pqoWdnZgRTRrCPcVCSdUUM7i1qlVQupUH5x4wf2F4XoIvae8f0EqWn6E71SegcdDwCyNGoLV8PDzFiFeJA4/T6DIoFIEK6JRQwKsX+we23p6UqVyaP4Mnnk0aiir9nc8v6pZWZ/39gOguQHPIhk15O2P2Pa5u26MexqnGaGhuzeKMgQGpY0hlzg+paaN1AOQbQRURJOG0BpUs5EQylRo7A41NSYNoYam+d3YU+qpkQqt80BzRJOG0LD7ZTXwObVnGUKHAk0aMqAZbDT0onxKTXrWQ2gn0aShAN7QmPo0WstmKrgnYL8M4UWo52ZQ5qrUANx3BGLiTBo2ByvfPG2QvVyds3+8B4bAiwTARqJBQwrf+vNw1wt9Len1Tw2jx5Z33wC2LwwaqnbVvhXJsWXcs76nSoUgtFZj0lAVWbLPGOdEyvYx2TWVS6hDrQEmwQYNiTqwq9olIbxOVYWq1CtAh2jQEJpZDAUwuzBp+Lf4mU6AuBOTT6nmm/CAHSiThs1WcvE35W3j85bWDfnLKw9/uOIoL5sN18p6S/Nq6GyiX67ye+nnS9/j2e/xW15bfpa/cFx9yaAlrqo9W5P1sK238AN23L0XYJOnlLktq6qV9ae0fe66I5wHcZ8o7ytecQzaV8a9Eaxi0EPby5cnJoR0133C+bZrQQR3W19ajsDQ5e1r3tWRCZdKdkhUo5688KmkgrjtQ9cDtDVjdt+CAMv6i4PkwhVcumVaVC+P4HyfxBnhQvCf8xYNQEHju9wl0KrkuzpiRJynEMw9nvz0KwnDMJnF5Yaw22W20RdQyMsIDm0zHk8D3gz7GGwgBKXn6RIhT6cOG0tUP+zpiOJpwGtTyx6HQ3j7/Xn+yCL3XJ615DnrdZSItdTCsOPgjJ1Y/aiZa9HzSteXTeRd1pWbpTMz0n06DNT/ztqn/dPFWo71VNAZLqP04zYnXvb1e9iYmhe+K/ucJbV5/pAS5h5m4a5QhRY24XERzuKI9D4ObPscsLj0Cm8dXRb8vYPrtg31g4ZoiIb2QUM0REP7oCEaoqF90BAN0dA+aIiG/0vDrbH/KOm1HqXBUHVkdVDErM+rNBgCUR/DQ9d5j4VIDYbgyYfByZw868xLh2Fq6s8rWeUsO//ETodh59VNQ3GJefcU279XdBiCYRFDU2/TlMZv4Om+umkw6nDHg7Jp02LoxIZqIqmD23xV/6TH0Otu4gZB3M6Kqf7XVY+hMzfVJ97yS4zeOHClIkZKkd0DbELw74d1GTrzzERd/DkjBcY6aDN0PL/nNbh/4eG86RZQ1Gd4flIP7969/T4P4fFVe2SUTkPHyZOS8oerq4fn8yGGKG+dTuk1vLCca+XxEEfrIFW/oUnaBqnTMmwbpE7N0ImbY43JGT6ex5yooROyqRs2hjdTNHQWj4PiSRo684e+f5qG576fTtzwoWOcrKETy6kbOimbuuFt3j9lQ+dDTt3Qqc4z8GkbXnZuJm7orKJg4obnjvFXf2XzT6HlbmYEQRAEQRAEQRAEQRAEQRAEQRAEUfEfzEGGJCUotwoAAAAASUVORK5CYII=",
    name: "Discord",
    desc: "Communication Platform",
    link: "https://discord.com",
    width: "100%",
    height: "100%",
  },
];

let repoDocs: ToolsCardInterface[] = [
  {
    img: "https://about.gitlab.com/images/press/logo/jpg/gitlab-icon-rgb.jpg",
    name: "GitLab",
    desc: "Git repository and DevOps",
    link: "https://gitlab.com/bevos-crew/bevos-course-guide",
    width: "100%",
    height: "100%",
  },
  {
    img: "https://res.cloudinary.com/postman/image/upload/t_team_logo/v1629869194/team/2893aede23f01bfcbd2319326bc96a6ed0524eba759745ed6d73405a3a8b67a8",
    name: "Postman",
    desc: "API Testing and Documentation",
    link: "https://documenter.getpostman.com/view/5971933/UUy4ckKn",
    width: "75%",
    height: "75%",
  },
];

let apis: ToolsCardInterface[] = [
  {
    img: "https://media-exp1.licdn.com/dms/image/C560BAQHVNU4q60vtBw/company-logo_200_200/0/1519887653068?e=2159024400&v=beta&t=JMmVWgoFrV6teFwMMungkt7gnEcL6tectE6ek4fbqB0",
    name: "Aminer",
    desc: "API for Professor Publication Data",
    link: "https://www.aminer.org",
    width: "75%",
    height: "80%",
  },
  {
    img: "https://thumbs.dreamstime.com/b/creative-open-book-logo-concept-204039596.jpg",
    name: "Google Books",
    desc: "API for Textbook Information",
    link: "https://developers.google.com/books",
    width: "100%",
    height: "100%",
  },
  {
    img: "https://lh3.googleusercontent.com/Fz89BLtrgrv6TjhJOeQug0uwgBtJqKtqzTF8Lk_FoyPQtPZnL65NWqMupqBEZ8ezl5Hn_plffgM1IpKCuRzG=w160-h160",
    name: "Google Custom Search",
    desc: "API for Images",
    link: "https://developers.google.com/custom-search/v1/overview",
    width: "90%",
    height: "90%",
  },
  {
    img: "https://images.g2crowd.com/uploads/product/image/social_landscape/social_landscape_8d58e1295b8ad54a323f7f5ac55e5280/google-maps-api.png",
    name: "Google Maps",
    desc: "API for Frontend Map Display",
    link: "https://developers.google.com/maps",
    width: "180%",
    height: "100%",
  },
  {
    img: "https://lh3.googleusercontent.com/EMzbsH0qJXweaoeOxPia96kx0u2h9b-QoEggREKjjsPwBdEn4cO7zBTai1cywpw4TvZrOgJTRPMc4GNdykXN-w=w160-h160",
    name: "Google Geocoding",
    desc: "API for Address and Coordinates",
    link: "https://developers.google.com/maps/documentation/geocoding/overview",
    width: "100%",
    height: "100%",
  },
  {
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN0AAADkCAMAAAArb9FNAAAAmVBMVEX////TXhPRUwDSWQDRVQDQUADSWwXdiWDSWgDQTgDTXA7dh1vglW/PSAD//fvUXgDZe0735dz78u3swbH57OXORADy08XkpIb139TquqTkpYz99/PkooPwzb378OrafEvZdj/24dfrvKbosZjWbC7VZR/im3rz2MvmrJHvybjfj2jhmHbXbjLVZyTZeETcglXNOwDXbzrdhVQInoL8AAAXrklEQVR4nN2d6YKiuBaAIQsSFLpVQC3EDffdef+Hu9lQVo2WKH3PjxnLsi0+kpw9QdM+JrPP/amPS/9iN759DVVJZ3ueLPE4+7az7H3jat4sESEzrYtQBs+3CDg537mkt8lK94aUYYZ09NftBIHk6TS8RYigNf3u1f1ORs3zuM1eUDrdxARAYzG/jJtbZK81DesW6X77El+XnrfwxStGx8WyLMMwcbNDFx6mP5PWdy/xZdmYdhS/7sZ0XOCWvcfpdLTvfO8SX5b28dwK4h82CytJZzbZmy7hPxjA/95VvibO0ttfNcbqsF/CJJ1xop8Y2vJNi6y/eanPy8xGm/h1ewJDrZemO2oDMnQ6WP4MTsG9b6uX9OdeL7ZknQa1dv4BJOF0a77YdRKqRodW+6tXrC7UNWmM5OtgSRVLf2KDY2rdWTplW00SyBb5NzzRyF6sbq+H1OQRsu2kdaa119wmNlLjif8B09D+Qwbx6y7eBsHOtk/T5CTkclgSaKE0HpqP7n1zLcSJ9YO/GLvaGtsXPpIZOrrQEBqc0ngG+ldMw/RCDcIA2QupOrN0CFG9k6H7V0zDqGH5Wqjb8DZL03Swx4Y4S0dNw9/am4ZgCTbaam+D9S3AGaTpzC1zv/J0FonKv7cWsgaR5v61yfI2DJ0WyGBA+qE8nXWo+dDNUE8Ltra9da9vOWtCDlk8HcynzRzd/otX/lh8veVoQ2KPE1HpzCRUbw5wFs9C2XfqTdff07AtAvY+odnDBUB8Le1gFsW0cnho+a1rfyTuadLWurqtb27v9SfY0s0Te7nKzE1j3ywYPTCu5coLWoeV5s8TRoDyNglfWYQZ9AydpdP3ojwe1OvnTztrPdTaY5skjAD1oAngdDw8DdN0kBvu3GKk2CD8FkWJDPSZNtpSI5BIIqyB3XKnF4YH2Nhl6ERA/ie/9OhQ18pj2SwiGosTu3EzAtrMACabYgHTJZjRdUGKBHOtWking+Z3QIrEpSqRKspx//ZWOKcofMS0MR08zDi79mGRpOMfL6arV7gwM4GeMALTI2YXbRzZD00zputp+wTLnZnJ/i1aFf6lL0hkA2Ny/cndeoSbNkF3YnRyyh6SdPzyy+iowznI/6HviBOZc/kyGNokctcgRQcknZ6k45qxlI7ekt1XWIrEkRY8QqDBbMIPNWQGL4uU0vF/cYdOR8d6GfaNAaibz+jaWCQtJZ00FE+NHTWJf75Gkhd/z5UJH5QR+T2dReqTiGiPPeFWAVbVYXSQl1tL6R7MTAuA2uQARy1v4S/B9bI5HS+FMDok3bMUHS9tldIZ+qAulcugh012oxswnnK/patRoDeAyDqwF0yXJOiG7D1GB+UHTXW6xXdQcrLRmQMpHCumVrgTwjivdJYpPumo0+lmTealZbKrQT/s9RBJ95LRoSudHLtn6EBNSpYRj1DlVGqDBB1PODA6Oc2eocNu8V/7uEznTFfaIk+0Rjy0SdMdxAefoYP1qZmssYWH4uUI84CA03E/+EU6HdQHb6pfw+kDydHFAcRTdDqoY3psTpg+KKZL5vwe0ul2vZxoLocrnbh+Rid7qIJkBuzx2A2/xlAqjoXZLed03JOkdKbs73uGDuFJTWxCUhwdXOn49Tdfo+vVK6sp7ZMDb3Q8gDkaOpRRtjqdcFFrI8FC9loGyIyjV0E3oXTL+HeqdCI3Xx9pyyRPAIwrHc8L/V/QaTKODsgfRjfFcT6W0qHruP6zdDJXGxDumPSL6EZAlU6k02okcuzcBB1Xe4zuR/yugC5fA+JiXb6BcEdk7riQTlb6C+hICd3kCwR3xJF0U8Jve4qO9TGwiZumo7Y+KKWrT+JBiIyl+wk6XumYGJipTC8qoNuuymZmXRIPGVkRrhASdDa3FRCfnE6CzmDx4C7XCFFzOp9wl5nTcUdxL6Ys1KHZvcUIxnjFOslKayS1pbuNHZ+sc2EqYKqDw8ItrT1p5NogrgLv/IkvSkh43ZTSWTan2wuXMcVh6et+azLV1iXLrsZ0PCDo47i+eBCGOUWHus6W2cd/jq4r6chB1oZHooMhRQfkBpKCho74E1+5+IcyIDzc6Y8z2dY0naz2DUrpyKevW00knZ/dj5aemTKg+Ofo1h53mUMvnW0dpLWjLMZmG24TdDVJtGekJ4YlPCd7Frp6xuMyFiyA7x5Kc0akRh0dCYnv+fQ2duGckF764qG+Yv1ypUNXV7qcrCY22XXS6w4OtY3lHcI7666GCbG8TMdEdNmmtwE1D948vKdVcL1SYlTWufagdtMmJ3Gdaa1CJtwK/kt0Jtmkfh5tiX2Mm8eetAi1owtIqvWwsyT25daSkbbmcpTLLQKu225fthPyqgycHiCLZBdpmk5Sh2Xxneh5rJOs8PWinAgQPb0ZObPuRCvKppyun/v+b4rDBgKKxryBQVC2Qy8TyYkWt3K6WqXapwezO0AwGg6ZY2KDKOdIZeNUNHHu0emgPvna/iTYA0Msp1VL7xWUFbN0JnOzy9cdxa8PnhYyjxHd2fefpGOfJWxh+XfodFKXMy78CxaL5eo/bXal8Z2lj3UDTvi/u0MHI6cWTR3+fsiSWxASbB9nDJD6zchItx/GdJb133+6vj3y396js06QnGrgbfZddu1kqLVnDWTjcTS30cWycCO5/OS+eWvyn072Omzxob07M+kNq0eD5gzqUHph/TkkoKdtWe8bSjRYcjprPqFsoYZ0aHINVLCVJCmoBh2a4cI+QpFpcCIT0AWjzXhYauHjdemwfOZ8rxPmnDmINc7uRNbznljWtwdvdTlv3T7h5dW1bVvUjA/AXF6ecd3oCa3DfEHEchN1LbTotx/Q0Xvw1bYOt+k1qTvf50eNBOdVl3XFHXzNinMKQBerz1xY5Cj8tFFLLkLQyO17zck37UJoi6hn6jGP2aE3emgf6EvHiOmQaPeijudYsHWW52186Y/hvuSSiQUlM89ax4sjlr4IDOL6hyh3DJB9Em5xZ+nRcLY8n5Kfm9+o5G08Hsr5NrNIjr8FxPMWy6sOceKKCHVJnAH0mlPJdm60hVZRli9kpV1igQMdDZ/VHJfAM5azsLs+nGXrxoYsh3xtoTUdN68hAu1g6ImNzE/RWYtPh3qOvjxaTJ+5mrMj9iTW2z6Y0P+294u+JIA93dtm2J6kY7bjs6nb5kGbWLqFKM8fwH3nYDplujHQd07LY7Fdh+dnLbvlSjZ7d3WrnqOjM+CjO2YiutrmFmuZXRO0H1GtcfAM3fvbZlBYuNKczroINqdnDxPp12fpLEYX7D7jdK7OK6rhWdvFELM9nCEZs6npDDxqHXpSg3NDjbjqcX7IMBXxPU130Jw1wB+xDR1quVdi3kFA/+Lu6uu6tkudqwQd23hB2bLR7NN045B6BWJfd9Uy32qBbHBG1NqNJ7c1v/6h/opQItxBxh2nh9e5SP1ZOurVWGIIK5cWtc9j4UrBo6ZN6NS85h5DCu6JkeTBjTmA6wJ99zSdFFD5Tu0Z1Sg9gPi91B3tRGODznXG+IyORS1+Q0RupDDpGtwL6+4JqThan3r96dhGEau8kbbWY9td29cwpbvU2syfDjzpJ8NCPdd5lc7InTD6VnFgo+nBiO81A5EW8g5v/5pzjtbaxmZj15MbDYv3HXfIvf2g9wRXGs6OETHpFa+JwfzjwOau8eA6dq0N/RVj7XC6sj5EZzt/FQ9U6La4x0WXqnhv3J5bONRaIrTsXcfu2NcaNl8bbA+9OIaj4Fu28FW4qgMi6i7ScNXFVD13RDOR1ojTmA59A4i9dqJHChQkONsNDM0XlaZesdFzj9zhb0E00IYybL7EDKuj5mPEzZtYeGZOC0ybBELS6L6MVzIf3ikOsnBHk5X7FY7plpG2Q2ILiOiRyo5d/8TZptrDhEq5oMqN3hpR3bziOsNp2fHxrY4dBEAWf0RVNd3ttRoTE2IeNPyCruqybNcErDY8ZFAbgnahdLWiFsWWt5ZXVVOd6YwNkZ2wx+6rFo8KqdIhay+gv7fwVDtMtdFwfbuRgd1xcNyd32WXn3AM/QtlA9cw6Bd0uNpOFodNO6g51PSk/ONmxDSl3PPDa1fxXmUtPGID4d7NcXFfnpmWV22gvkGIzblRpoWyu9B8tslc0IkDqETGJ5xjE8FUqPCyL2ZVezh4e+4NVoAa1f489f6UetcsiSnN7ZQPDvMsNpQNoCgdBr1MZ1a7n3LQY9OOjtAqtZnFpU7ZiVlwcRK0VIpgtFlgA+g5d/NVusQRPNXQaWzaUd0xTaquDt5oS6pJZ8gUvqU46NpaABNYBZ6v8yIdqvgpC4yuTeBSC/DtzbZN3WdMZ2WbyKnTkcd443lhtfnF+NW6VJz745MMQDr/bkcRbrwVgzNJX/uRx3E4RLBtSr7mNTpZ1KxKnLXN/rdDBts1Iixzp7kYaT2sA+aDDUSI4lCjYchWtyIp2yHzEK/K6Hxg2vzq+wT7rAAUtV1/yxyxBpCVtg3hbCYwyaS0srg6ei+Hr5X1IHVNXntk0oALh1UQ5ocTXVd9HZnyMOTRlLVRIUhOpZGKPznP/fKe4Qd0FZm7cJ5oIfpBcB57HqMWtsDl2v62hoyt9Bb7F953WroD6Ct0/l6yuS15XJgBItdxOmGDQPNaIA3WBPEQp0TCw1lM2JfpKggQ+uxgTKbqO9szX16UzgQYmwYGCJGx/JPBEFG2bekFbOaeLC63FaquhWKM311NaDc9zB8IMNp5vCDgUjrghluLYEwOQzlSnSFBCOxKXfiN7sni8vRUsmVSQUxcZmRektHWtnkhIGh5oiAQRJyO/a7vx7eys2OjuCxlmy08eQRl/4Th62kjOjm3bzN6naVHhowgWNqxl7/uxXRXcZd03FBRb5+QrhkXYPssQCfL/S/wkPGmzFGP2C02HA5lu94xY52hc1uYzsl8NSSWgRkXYFcnTNka7m9ic1aNfUubR//Ml1HQw8l2Ui9N194SBEzxAZcs/cy8oW6LJ/MNq2OcWGmUbppUEzB/g88yZQrD6WVaZVN07S2mbPGpdD0EcTqhGiGvJRajzwN0Afr3VZ0Zi1GSxH9SqHXOfk+Cbtqk46bLEMcZzVhUB8eJf4xteQwxS6xAHJ9J/Gs6qlze8Bia9Z/8PcIxXb9Jx21xC3HWhFURUdzaxdikovEn2IB4eLVVb6DTIfzludLTAjY6GTldh2o/hOfpvzCbY0RkB0dPegA8sWKkNeo76ESb4C/EKbYsAaWzJhjiSz4M8JdTwUbiknK4p2wZjfoWOtYmWEn6bwhpqHUsDXE6Qy8+uaLLxg1mkkbvotNxBYee+xfqTo5LTepoZyOpRLsHYPB21Iy8i+79zyfbXAgizdIQx915hlSiM8ZmFJ1hWle6kOmN8hDH3XqxgZgtKJtVfD7r2+je6lJvFpStxZRiWOQstLfngzQQM52y6WVl7rfRvbGO3j3g2NnQEL5EmUCORkoHucoHFjCTlrAiutxeqteFjga4VnHWSLdQak/5tHmWOUBnQFdbebKvjnR0NAD4kc5Gd8t9fHRbU9OTJ3NgrGnfxIf7C6JWdGw0ALoZZCeiq0o3J/HPq/F5HO8rNJGJ949UWY3onAiCOMS5ymZCgLQKq2McdjtrxlaeyHw73a/jvICxFR28PRVDuZqcZU+3s2ZJo4lK4FwTumCNAF6U613/cu3p/qFsuNyDqR9d0AMAH8oVezg/t+KIANz1zqqhg7842rbD2Pblym+z8GRPd4ex4ZN6nvhddC+fS9wZYnCn0sHYZH6PfpIloJ+pX7yL7sUAz11igGWIE227Obera3iyk7uzpGy48VwG/E105kvPDXJ35LorSRsBCAhMTdCZbsv2jNGOFQ5az2ao3uVnosvTFS93S8ctXkT9HWHNiMnWuhmKUwqjHUtkPs32PjrdINunkkftFh23xEQbDSYEgVv7RgSINOw8SRv3Rt3EiR4/kP1tdGz/qbrD0m4SALaZC3aj2FFxIhwnN90tgQgssyWZUQ/YH6XTLXBQM0R9ypYfjKs4EYn32bY52zDL1t8SpCs84e2ddCw51nhc9+qfCM4PxlWCtR23nrQblK2gKCJ62RVq27yx+I3y8LFWqzHBuLyKE/Ts+EyRKR+3oqKIaCFSqP6+m+5BS6p/JLhw34dkG3rxI4WnJwJByUdF655C9ffddHfx/AnBKJ+Xi6UzvKYU6MqEoPSjojtMoafk7XQ62JfNmK5HrPKJ29l5cc6Zzl5YluaK6SxEHlug99NRy1cyfN07R4eMWnFKQVv9pWzmvfXrA4uGuQrmtQI6VtQrVGflyXh3e5ZdCqyMA++luZiE3t1cUcV0T54sQNnih0lSrVPElhkmXzWtXw0dVS4HVb+z3Tg3pfYLWcEnn+YKD68WKaqiUx2+dvMct9WEF8aWBXEGC1y8bSX4gkW4CXq8P71/um4OD/eUbZ9joxGrVeiVjIb24/R3hXQP96f3x+c4qtnMMSQF6UnXZl+Ut9t91iTwWLNUSafr955vv7p4sbO5OeCSNJe02xlt6+zZs1AVvOhq6VAp3WpyjndFdClbfCZKVsSpNlm7PeL9XwpFtYrpSuy6f4i7FLSuDu70WYrdFHbmXelFfz5GUKEL917s+c8WlK28wMo3XEOSTdpIL/rzMUKG7qeA7bYaZyZAuHlPsYcAgXyY+70YISX5RGB3QWLveLAAiDxI4XW9dUGY264nXdeM+6WcAaLjlktz9TPOc7vQpHwxAkrRpRLwM/N6LFlkgYL0ZDi2lTL2YjVOvhIBJcRKbDwaAF2aXycCdDll+2OdgY6NQjW0+S9Hh3B5W/TH6HTLXl3ZwpiN6pKC3t8Jb23LhXVOtCDZIy433lCttaliOnns78COPchgzdLKw4JJNb8+Gjoh7pAgK/c8qbZqXrhyOk41l26Fs2a6pEgNag4/zDPrXfG2APjyfrHP0Anp9AgqS3PJzWTZE44b5j9Cx8qmoOAc3fjXRM+2plzpXu6PrJxOZhFGdAEBozzbxw9GM3OpWZ4qry+deMRzZ0nHDd59knjfK2p4ODK614vyVdPpiJ3cuQflnWoxXWEajO+WgEV0o6GC4a+cjp/cuX/EViZs/xwqSDG0t0BlK0T1dPTyNOWcVpCZnAsDoWHOn2R5QaVDND5Bp3xUSecHZ9QKKUw5L1iBqzzuryXdtEXVaoalOCfN93uqVHprQ+eOMR0RpORj6UVOW63pNjwTpEQnTqtVoWv9cpeTilgqdPIsEZUdfcJpU0j4fYKOH1SqSGf8a3SKxx/xM3zU6JhLqkNbob+iejqs1iLEz18qnpmbzGKkdDQ2V+n0+wCdWsMhGzsDz3N0ziD3oDPXzqUtvkenVsiLWFdpLnfuDgGyjOyb5Q0hH6dTWf0aO1m+oPOyS9gx+Uo691t0aiXusCjHxY96+8VJqx+g+8UeGkH3+hHqH7AIv9iq8JM76q12dAq+fJks+fFTrz+xpzbWvPzqXk+JfYDuN838/5m/+4IP0P3ikNSxwVyugiIgEwWnrXq6J9rd242MCzIxSsshm4XCXtTq6ZQPX/ZPxM6MxwHnkypMgkgHKifLfoBOSaE7szk2cx7loNDlcpbsEHCVo4eqpys7zTktXZs/8UzJgeyXtOh8hU7JkRIPvlR7qLNsYlE4V+kDdLpKRPoMnWhiqQmdUq7kGTrxSDSVBNMH8ipKeS5xNLLaQ3lqRZfNLxdKxKMBpEQnHiOpMiU+QKf0HOqIbZsnxW5NNsvAHgFqAZXl/ImxU+m9iBBCue0+guWQdcNnqOys02/QqSRWfkBhB74z0AHIhlADr/xAmY/TAZWNXcV3oMf6hnPPB9gon9H2CbqXj15xkGrB4It0Lx8KFJi/uzsfoXv53oseWqWZ/TW61583KjuEa00HX04bie7uX5yy+omxezltpNwh/EW6wnYTJeEn6BvZ7qonpPf6Q2WU6VRzWv48M0o01IE4XzrhslGZER+gM9VOuJgdcNYj9UnJoRWs7KVyzz5AZyhkW4MfAKhXknHGNsWHH7hs65PSav4AnaWQNvJ5qkStn0PT2TUrJfA/QTd5fBkhMQzDVIriNc2A9MM537pIhsCoWszF48vwTw0manRN9tGTin834B+tWP4HAc7BIVV4YIYAAAAASUVORK5CYII=",
    name: "UT Syllabus Page",
    desc: "Scraped for Professor CVs, Syllabi, and Professor Names",
    link: "https://utdirect.utexas.edu/apps/student/coursedocs/nlogon/",
    width: "75%",
    height: "80%",
  },
  {
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH4AAACiCAMAAACwAYnuAAAAM1BMVEX////AWifQg13v1snfrJPEZDX79fHnwa7Ib0LMeVDYmHjz4Nb36uTUjmvry7vcoobjt6D5YCDRAAAMx0lEQVR4nO1c18LlqgpesaWX93/aHQQUW8rf5lwcbmYmMX40AdE1n88DclrrzZy0qpPGrkHwUk0w7hyvn0zcpnOCc55zPttCu6dBqdWYQ+v5GeYCmNO3IC9Y8Zy4Gmpv9l8BrTOygnmQj/WvUEsa1fH5V9ie1EP4gX16A6fWS2nA2b/oeX08U+kF/OAB+68vIYeLZ73gpYD3mI9XynPygWPNY4aAP/Va0enP06z7qYQf/gKaSJfw6g/h3b+F/3wZ3p1ra/Jp53TS43SWGR3qXGv76T1fhjc3X8z6OIHylTMO2QN7rpx7H34Bv1+k2Auypz761pwsxgP4QuLndDtnhL/lFGlUkDX9i7lfQcodVX3GuDNrprZ4Ad/0GkWqnE7czKa6K1121lCkjNfw00v4+tsavPyqCW++Ds/CqQjvZ1Lr8pmDesxL+NbIAI+m1TJQAPz4CetIw1hwjuWn4E9DDgSvCCKD7zJ4gvUAZ3VXjwA6zHENb8LMAf70QSgCICczvK80ACqDv3SaBN7ewu8+npWzJE8gJq8Qbl7CNxyYZ6mUx1X4Gt8VWl7CZ7BU7zH8TMZ4DP95CH/uO0YJr01IAGzgD7uF+vTr6RY0EJ5M1V1FDX6vD+O54c/xRGShBPwS4emt2gm+nUVz+PZIhveDwoJVwb80J0UllvMdvPoavFdvUCjDIy3BNO/hj1v4c9XlhkzhmQvd96/hGx5KrjfW31bhkdj1GvDTM3h2JjnxhtuoOcIbX+kZufY4sDTgzWt4DGizTJGcjaLLzScbKPI7+Mb6jPCaQXP4XsLrAPoO/m6YgB9xr7vEmkLjvtY+h9fP4Mn1EB5SeaKlsqRxWO8RfNv1MvhGwmOME75mnVZFtRB8e+Fl8Bd7HMCQIdmB60++9PQxZvILwXfT5GfX8MsreJznxC33OQlZcPz5AXyAfQSvTmkvcXMu1D68gW8nvF/Z5eTwFwNz+NP/DbaRdt+zGbHd1Bd7nAezvoEfwLBp5VoUOad7TLGT9Bi+nfA+ve+FtsqWOs3Y1LrY7qfw3+w/v6fp38Kb/yX4V5b9efi/Rqeg/7Sl/Ev0f/jvUNHX+034UfmjKTxUkGUtHyTASdobhq7gW2CPHPsZQxH+a2Dz44BRZei7635tdiPvuPkJeNet/xJ++3K4voV/otbpohccaauweAPv1tsO/0lnXXOvfWft9hLeQQl/e7C1PbIbVBaFji7hHW4gnkzclZKlhDuKHP8SfvUP7Z1b2SdM7oiQhZLtAh7eTfeC4RQX20NPZ4hZbdEguap2zi/G0/euNh9A1CG5ZhLO7Oa+UD/CG1RgWuv1/oG5PVyjev5a+9rrVuXiM7wq4VcPvN3Bc/vmWvvaA+vc+hfw1mtK38GTT91on6YZsq3M3oRf0BeOO3hIW/e+r/F0esqGIexBf5r0gw75u4xos/9uuNP+jMLknsRSGwE/T72T6roMu159y1GYLie0DnnSbPY2/Pl3Oy2orsJZcgK5VzwLv1ygq9eiN2U/BlV1CbzikcAAL5XLGwUzOR2MvhwYlvGoRPjjaKcFfOwk6DJQZLQTrI5TNgiC2CeeQntLuRR+YHiM9h20CKJMrf6KodnbL1l8nnYgeES1ocXFMHoOfM4fPVIwNvncSD4jH9VXltzxmDiBdbTMBTyfgFLQV7ASHeH3wDYvPV2TEV+6yptOEd8zZI84I0Q0HeVRQXn+IcYGZwN6XFNuzRGsonemtE0IcPCK8Q2qeonwJhi2Jz3Cutj8m9kyg2RCyjDDVrvIBFeU2AxD8ERMyZOXSEWAGPQ4bXpHWYgPcEH21ejU7BWtGLvw+5i9SWXbTHoc+OsxqNwIO9JiP9Vu/cob0pBCSWasVkGorfNDoRKQ8JxjMDjvEawZRUMFYTbWKBx8ZulkLEnTm820G6kn1mR9uoGuNU62I4NozBkHZ39FbfXRHXWX1WEOjVZWzqSYNPxinmHn8g44M19BZGlj7/UTggyohgzHZL6Nn1FtqouxhvWitY2fSYOP0fXJgsPAkpTwn3koHJAWtqqc8xkODEp+tEZ3J9dntc0hwPRV6VlUmb1dRSEED9OGHm8wDWKgBXG5hulCgNGf0vZI8YSBaMwfBHj1CSkjOgwFSvTSePpEpFeG36sFl6O3kfauetK5dfF8fTAueR6LJOl7SEuPtc5QLSX6LlfKFoXJ+ez9/FPCrfQ89j0yXW8MNM99pdd31VS+BlstaneMU2vKr7B+qNqb+3CPJa0vd8mMfzX2EPiWwZ+L1mTCb0GKYWMJCjMtDux6QMhbJissFnwLBZamYD+BmWHMXMR4zoSchFe+JZStu9mufraB6vGAmDnbIo2fZs/dDUWO8Y75cWGX0dkDZ0yLM4ggx5Je0dVC21FZ8n4ZGH1jHkY3SStF3a9bMq+apS/hMBhgl55ZNFsQOL/OhuyMrNrTDBoPqVyiGKF7Ah9nrgJsrn1cvBNZ9kzCB1ePi9QEEK5D/BpebhRHe2DM2tSrRNkDmX0WNymF9sEag59zoBfBiomreZJf014YjG5Bg6ZPWI1dWI5i0QmE9qFygK97cMqDRNSCf2kp+aSn8cA/PF8+ifdtjBUze/SDoH2smyas2FeSBnXoCk1xreCYcc8npqJBVGifT7znKN3RsauGOdHQNO1CttQl1qfkCBYLlExYhyhRnwJRSVVm9kSjWDNrcj9E30tNp9pXQhfTgmkRxgnjo+7XotqjPM3/9DsGgt+wSB6jWbo8RkQ1xX9NfAMp7E1Y97UjV5c0+VTI19QDCPXpUeqetxaVmsMzLeChRdbYUIKM7KL4RXDTotLIGxfoUcHDnJHebAP8lpTxGZ2FPk+Ayg9LUgVXoXifl6o6N4nrV0uPhOtNle6woJ3nXb0iUNJxF+pCJysbAoN0vsCUT9h+4Rmf2aebNrMG/s+NsV944EJb+gE5XlmYkUtkC8onbAg7p3Pki61BUCsaH3aGsgaYEheXX9lEfA13RZQy5IO4fXxywuB3epbz9jSpAa49pCC1XWLKWPj1BjGVbAauZKcaOv2eHd1UVYxsdwlnyeeadn23zXveF+9p2WIS4evdyilhDXS/7QNNdgpjoXy5a/GDeJMv7ymOr4eoV0n4ugzhIpOnHg2hQQo7H3wb7vqAZ7E0zvoVZqG2n8I6o8qr1YZLxOeGhGcZen3Kh5Fr8aEvuXg2/cYD/EjwTPO3HIjEJ+4OdoQ+aMXdeR9uS8jtfJQHiw3I8pxoty0+ZZSR8dFkIw64ajPPaDAM9QOjC29quT2JT8ncxX+pJXzovCKuGqj0eg8wh42q7++Ez6sG7NZMm9Nss8aOV3wOnw4IMx/+L7SSadFdHg5S0OClwb2qmA8A3jW8b6F2Au1fu9TRqEC+Pp2iDB38O+Zdz5RvNxxd5dh82QdL8LIBqpbGxA1aE/VD3o3hyy5e+TBkSEMHljqzjzfht37dMIVMy6q/i5o8TtZT/nYeePXoG21lb4daerAtHmC9rovO7ripTKomkYPaoqKijDyVPyjgjZbhwFFo2LD6buFZ/XKO5dgVX9bcfN07CPm97PsOdkfF+J9eSzVz+rtNWJ+o/qAoLX8PZ8n7x2Agh5s473ayt2+DeZZ8xktiXnmNLJ2cFKX3+KjL3aODdU3adWd4bueXNqkTn13w8vIXhY3ZcRoo/3tU+UTMUfMOV6mF6zdxTxW3xI9v4/Ba26I6vCgLL6oVmbQu/EH5pBs8z7NwTf7m+RUbDn7s/ntQJe8/Fkx/W+i9TdJgvjDh0jwzxROabYLvrYcBjAwJ4vsDfodanaWEPtGQsNxcafy8okHhcFzgW9+Wc7z5wJYpJiEn2jW96iroL6/39BX8c5YVvcrPrwleBd/eza4Cd14PPMvrX3cHfPK/JPkAHQF+GbNXinyG7V6G0HsK+0OSZDEhlQ1+LQ1cVtrwCP2CGHbfQY+8xxw9H3DjrZ9F6cI/M3Af53+hYcINKbbIF9EFftnMoBW4EnyliODF82V0cXRb6Sg4NkDtYCm6zjfQ5SSVmMV5tjjCEWb/FrrArzU1phZA7HSOr1dcNlNYb7kBIka+8TriN9+++y16tgmM9wt7qIIzkXXfxPkWuVhrChgPAmkARJW+JzrtFz95eEOxZywVoEecns9z8B9R9Ie9mAckJBovJz3EwB/8TzxkVG/39WRReHPX7i0JA9h6i2GZxJAnNe0rkoc3QxmEnMyI6gcVHwBkGavyhrpMxw9u2H6FDttgoJe7ylbH+fu0JN0yYsAZCf5boiMlCgAfWNIq6PdER1qSjUyXVWC/KjrSlhd9v+rwJYnT20QRX/2lwGuaKzfo9m/n1hfUZxZQv+xyOSUW+Du9R4pBwPyl3iPh9YX1T/y9Soe9zv+39B+6Bl8w7m+FIgAAAABJRU5ErkJggg==",
    name: "UT Registration Rules",
    desc: "Scraped for Department Information",
    link: "https://registrar.utexas.edu/schedules/219/regrulesbydept/all.html#c%20s",
    width: "70%",
    height: "90%",
  },
];

function ToolsDisplay({ tools }: ToolList) {
  return (
    <Box
      sx={{
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        flexWrap: "wrap",
        maxWidth: "1200px",
      }}
    >
      {tools.map((tool) => (
        <ToolsCard
          img={tool.img}
          name={tool.name}
          desc={tool.desc}
          link={tool.link}
          width={tool.width}
          height={tool.height}
        />
      ))}
    </Box>
  );
}

function ToolsCard({
  img,
  name,
  desc,
  link,
  width,
  height,
}: ToolsCardInterface) {
  const classes = aboutUsStyles();

  return (
    <div className={classes.toolsPaperDiv}>
      <Paper
        elevation={3}
        className={classes.paper}
        style={{ height: "100%", width: "100%" }}
      >
        <CardActionArea href={link} style={{ height: "100%", width: "100%" }}>
          <CardContent style={{ height: "268", width: "268" }}>
            <Box
              sx={{
                width: "100%",
                height: "100%",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                flexWrap: "nowrap",
              }}
            >
              <Box
                width="200px"
                height="190px"
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <img
                  src={img}
                  alt={`${name} Logo`}
                  style={{ width: width, height: height }}
                />
              </Box>

              <Typography variant="h5">{name}</Typography>
              <Typography variant="body1" align="center">
                {desc}
              </Typography>
            </Box>
          </CardContent>
        </CardActionArea>
      </Paper>
    </div>
  );
}

export default AboutUsPage;
