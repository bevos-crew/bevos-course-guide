import React from "react";
import "./App.css";
import HomePage from "./AdditionalPages/HomePage";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import NavBar from "./Components/NavBar";
import ProfessorsModelPage from "./ModelPages/ProfessorsModelPage";
import CoursesModelPage from "./ModelPages/CoursesModelPage";
import AboutUsPage from "./AdditionalPages/AboutUs/AboutUsPage";
import DepartmentChosen from "./ModelPages/DepartmentModelPage";
import SearchResultsPage from "./AdditionalPages/SearchResultsPage";
import Visualizations from "./AdditionalPages/Visualizations/Visuals";

function App() {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route exact path="/">
          <Redirect to="/Home" />
        </Route>
        <Route exact path="/Home">
          <HomePage />
        </Route>
        <Route path="/Professors">
          <ProfessorsModelPage />
        </Route>
        <Route path="/Courses">
          <CoursesModelPage />
        </Route>
        <Route path="/Departments">
          <DepartmentChosen />
        </Route>
        <Route exact path="/Visualizations">
          <Visualizations />
        </Route>
        <Route exact path="/AboutUs">
          <AboutUsPage />
        </Route>
        <Route exact path="/Search/:query">
            <SearchResultsPage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
