import * as React from '../../node_modules/react';
import { TablePagination } from '../../node_modules/@material-ui/core'

// Code From https://mui.com/components/pagination/
interface pageData {
    from: any,
    to: any,
    count: any,
    page: any
}

function defaultLabelDisplayedRows(a: pageData) {
    var total_pages = Math.floor(a.count / (a.to - a.from + 1))
    if (a.count % (a.to - a.from + 1) > 0) {
        total_pages += 1
    }
    return `${a.from}-${a.to} of ${a.count !== -1 ? a.count : `more than ${a.to}`} | Page ${a.page + 1} of ${a.to === a.count ? a.page + 1 : total_pages}`;
}

function Pagination(props: any) {
    const { page, setPage, itemsPerPage, setItemsPerPage, count, pageOptions } = props;

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setItemsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <TablePagination
            count={count}
            page={page}
            onPageChange={handleChangePage}
            rowsPerPage={itemsPerPage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            labelRowsPerPage={"Items Per Page"}
            rowsPerPageOptions={pageOptions}
            labelDisplayedRows={defaultLabelDisplayedRows}
        />
    );
}

export default Pagination
