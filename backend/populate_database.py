from flask import Flask, jsonify
from sqlalchemy import create_engine
from database import db, create_app, Course, Department, Professor, Section, Professor2, Course2, Department2, Section2

app = create_app()

app.app_context().push()
app.debug = True

if __name__ == "__main__":
    #engine = create_engine('postgresql://scott:tiger@localhost:5432/mydatabase')
    f = open("credentials.txt", "r")
    engine = create_engine(f.readline())
    #Course.__table__.drop()
    #Section.__table__.drop()
    #Professor.__table__.drop()
    #Department.__table__.drop()
    #db.create_all()
    #Course2.__table__.drop(engine)
    Course2.__table__.create(engine)
    Section2.__table__.create(engine)
    Department2.__table__.create(engine)
    Professor2.__table__.create(engine)
    #Course2.__table__.drop(engine)
    #Section2.__table__.drop(engine)
    #Professor2.__table__.drop(engine)
    #Department2.__table__.drop(engine)

    # professor_list = []
    # for _ in range(5):
    #     new_prof = Professor(prof_name="whatever data here")
    #     #professor_list.append(new_prof)

    # db.session.add_all(professor_list)
    # db.session.commit()
