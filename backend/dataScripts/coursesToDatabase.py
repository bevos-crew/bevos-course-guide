import pandas as pd
import requests
import time
import json

df = pd.read_csv('../dataFiles/Current_Semester_Report', sep='\s*\t\s*', header=0, engine="python", dtype={"Course Nbr": object, 'Unique': object})

newCols = {"Dept-Abbr": "Dept", "Dept-Name": "DeptName", "Course Nbr": "CourseNum", "Crs Desc": "Desc"}
df = df.rename(columns=newCols)

def load_unique(courseNumber):
    courseDict = {}
    