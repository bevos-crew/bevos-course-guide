"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

var _styles = require("@material-ui/styles");

var _Home = _interopRequireDefault(require("@material-ui/icons/Home"));

var _reactRouterDom = require("react-router-dom");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const useStyles = (0, _styles.makeStyles)(() => ({
  textButtonStyle: {
    fontSize: 20,
    fontWeight: 500
  }
}));

function NavBar() {
  const classes = useStyles();
  const iconStyle = {
    margin: 0,
    padding: 0
  };
  const toolBarStyle = {
    justifyContent: "space-between",
    marginLeft: 10,
    marginRight: 200
  };
  return /*#__PURE__*/_react.default.createElement(_core.AppBar, {
    position: "static",
    color: "secondary"
  }, /*#__PURE__*/_react.default.createElement(_core.Toolbar, {
    style: toolBarStyle
  }, /*#__PURE__*/_react.default.createElement(_core.IconButton, {
    edge: "start",
    color: "primary",
    "aria-label": "menu",
    style: iconStyle,
    component: _reactRouterDom.Link,
    to: "/Home"
  }, /*#__PURE__*/_react.default.createElement(_Home.default, null)), /*#__PURE__*/_react.default.createElement(_core.Button, {
    component: _reactRouterDom.Link,
    to: "/Courses"
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h5",
    color: "primary",
    className: classes.textButtonStyle
  }, "Courses")), /*#__PURE__*/_react.default.createElement(_core.Button, {
    component: _reactRouterDom.Link,
    to: "/Professors"
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h5",
    color: "primary",
    className: classes.textButtonStyle
  }, "Professors")), /*#__PURE__*/_react.default.createElement(_core.Button, {
    color: "primary",
    component: _reactRouterDom.Link,
    to: "/Departments"
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h5",
    color: "primary",
    className: classes.textButtonStyle
  }, "Departments")), /*#__PURE__*/_react.default.createElement(_core.Button, {
    color: "primary",
    component: _reactRouterDom.Link,
    to: "/AboutUs"
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h5",
    color: "primary",
    className: classes.textButtonStyle
  }, "About Us"))));
}

var _default = NavBar;
exports.default = _default;