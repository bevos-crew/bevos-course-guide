import { Card, Box, CardActionArea, CardContent, Typography, CardMedia, Menu, MenuItem, ListSubheader } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Route, Switch, Link } from "react-router-dom";
import '../fonts.css';
import DepartmentInstancePage from "../InstancePages/DepartmentInstancePage";
import Pagination from "../Components/Pagination"
import axios from "axios";
import SearchBar from "material-ui-search-bar";
import { IDepartments, IDepartmentCard } from "../Components/Interfaces"
import { titleStyle, textStyle, useStyle, cardStyle } from '../Components/Style'
import { getHighlightedText, RenderMenu, RenderFiltering } from "../Components/Repeats";


//create interface of department to store data for a single department
function createDepartment(
    name: string,
    picture: string,
    parent: string,
    courses: number,
    professors: number,
    address: string,
): IDepartmentCard {
    return {
        name,
        picture,
        parent,
        courses,
        professors,
        address,
    };
}

function DepartmentChosen() {

    //route to a specific department or the general model page
    return (
        <Switch>
            <Route exact path="/Departments/:departmentName" component={DepartmentInstancePage} />
            <Route exact path="/Departments" component={DepartmentModelPage} />
        </Switch>
    )
}


type Order = 'asc' | 'desc';

//get data for all department instances from the endpoint and place data onto the page
function DepartmentModelPage() {
    let errorText: string[] = [];
    let [page, setPage] = useState(0);
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [count, setCount] = useState(10);
    const [departments, setDepartments] = React.useState([]);
    const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);
    const [order, setOrder] = React.useState<Order>('asc');
    const [search, setSearch]: [string, (search: string) => void] = useState("");
    const [query, setQuery]: [string, (query: string) => void] = useState<string>("");
    const [parentFilter, setParentFilter]: [string, (parentFilter: string) => void] = useState<string>("");
    const [orderBy, setOrderBy] = React.useState<keyof IDepartmentCard>('name');


    //set what attribute instances will be sorted by
    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof IDepartmentCard,
    ) => {
        setOrderBy(property);
    };

    //set parent being filtered by
    const handleRequestFilter = (
        event: React.MouseEvent<unknown>,
        property: string,
    ) => {
        setParentFilter(parentFilter === property ? "" : property);
    };

    //set order of sorting
    const setOrderSorting = (
        ascending: number,
    ) => {
        setOrder(ascending === 0 ? 'asc' : 'desc');
    }

    //get all instances of departments and place them onto cards
    const getDepartmentsInfo = async () => {
        let departments = [];
        await axios.get<IDepartments>(`https://api.bevoscourseguide.me/api/alldepartments?sort_by=${orderBy}.${order}&filter=${parentFilter}&page=${page + 1}&per_page=${itemsPerPage}&query=${query}`, {
            headers: {
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                console.log(response);
                response.data.departments.forEach(department => {
                    departments.push(createDepartment(department.name, department.picture, department.parent, department["all classes"].length, department["all professors"].length, department.address))
                });
                setCount(response.data.num_instances);
            })
            .catch(ex => {
                const error =
                    ex.response.status === 404
                        ? "Resource Not found"
                        : "An unexpected error has occurred";
                console.log(ex);
                errorText.push(error);
            });
        return {
            departments: departments,
        }
    }

    //add specific formatting to the url and overall page
    useEffect(() => {
        document.body.style.background = 'white';
        document.title = "Departments";

        const fetchData = async () => {
            const DepartmentsInfo = await getDepartmentsInfo();
            setDepartments(DepartmentsInfo.departments);
            setLoading(false);
            setOrder(order);
            setOrderBy(orderBy);
        }

        setCount(count);
        setItemsPerPage(itemsPerPage);
        setPage(page);

        fetchData();
    }, [page, itemsPerPage, order, orderBy, parentFilter, query]);

    const classes = useStyle();
    const buttons = [
        "Name",
        "Address",
        "Professors",
        "Courses",
    ]

    // Show loading until gitlab api calls are finished
    if (loading) {
        return (
            <>
                <Typography variant="h3" align="center" style={{ fontWeight: 600, marginBottom: 20, marginTop: 60 }}>Loading...</Typography>
            </>
        );
    }

    const options = [
        "Cockrell School of Engineering", "College of Education", "College of Fine Arts",
        "College of Liberal Arts", "College of Natural Sciences", "College of Pharmacy", "Dell Medical School",
        "Graduate School", "Jackson School of Geosciences", "LBJ School of Public Affairs", "Moody College of Communication",
        "Red McCombs School of Business", "School of Architecture", "School of Information", "School of Law",
        "School of Nursing", "School of Undergraduate Studies", "Steve Hicks School of Social Work",
    ]

    const alphabetical = [
        true,
        true,
        false,
        false
    ]


    return (
        <div>
            <h2 style={titleStyle}>Departments</h2>
            <Box sx={{ display: "flex", justifyContent: "space-around", flexWrap: "wrap" }}>
                <SearchBar
                    value={search}
                    placeholder="Search Departments"
                    onChange={(newString) => {
                        setSearch(newString);
                    }}
                    onRequestSearch={() => {
                        setQuery(search);
                    }}
                    onCancelSearch={() => {
                        setQuery("");
                    }}
                />
                {buttons.map((row, index) => (
                    <div>
                        <RenderMenu button={row} alpha={alphabetical[index]} setOrderSorting={setOrderSorting} onRequestSort={handleRequestSort} ></RenderMenu>
                    </div>
                ))}
                <RenderFiltering list={options} onRequestFilter={handleRequestFilter} setOrderSorting={setOrderSorting} onRequestSort={handleRequestSort} category={"School"}></RenderFiltering>

            </Box>
            <Box sx={{ display: "flex", justifyContent: "center", flexWrap: "wrap" }}>
                {departments.map((department) => (
                    <Card className={classes.departCard}>
                        <CardActionArea component={Link} to={{ pathname: `/Departments/${department.name}` }}>
                            <Box style={{ display: "flex", flexDirection: "column", justifyContent: "center" }}>
                                <Typography align="center" style={textStyle}>
                                    <div>{getHighlightedText(department.name, query)}</div>
                                </Typography>
                                <CardMedia
                                    component="img"
                                    height="300px"
                                    width="100px"
                                    image={department.picture}
                                />
                                <CardContent>
                                    <Typography style={cardStyle}>
                                        Parent School: <span>&nbsp;</span>{getHighlightedText(department.parent, query)}
                                    </Typography>
                                    <Typography style={cardStyle}>
                                        Number of Courses: <span>&nbsp;</span>{getHighlightedText(department.courses.toString(), query)}
                                    </Typography>
                                    <Typography style={cardStyle}>
                                        Number of Professors: <span>&nbsp;</span>{getHighlightedText(department.professors.toString(), query)}
                                    </Typography>
                                    <Typography style={cardStyle}>
                                        Address: <span>&nbsp;</span>{getHighlightedText(department.address, query)}
                                    </Typography>
                                </CardContent>
                            </Box>
                        </CardActionArea>
                    </Card>
                ))}
            </Box>
            <Box sx={{ flexGrow: 1 }} mr={20}>
                <Pagination page={page} setPage={setPage} itemsPerPage={itemsPerPage} setItemsPerPage={setItemsPerPage} count={count} pageOptions={[5, 10, 25, 50, 100]} />
            </Box>
        </div >
    );


}

export default DepartmentChosen;