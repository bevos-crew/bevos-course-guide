import React, { useEffect, useState } from "react";

import {
    CircularProgress
  } from "@material-ui/core";
import axios from "axios";

import {
    PieChart,
    Pie,
    Sector,
    ResponsiveContainer,
} from "recharts";
import { IResponse, IAirline } from "../../../Components/Interfaces";

const getAirlinesInfo = async () => {
    let fleets: any[] = []
    await axios.get<IResponse>(
      "https://api.flightright.me/airlines?limit=192"
    )
    .then((response) => {
        console.log(response)
        response.data.data.forEach((airline: IAirline) => {
            let airlineBar = { name: airline.airline_name, fleet_size: airline.airline_fleet_size, id: airline.id }
            fleets.push(airlineBar);
        })
    })
    .catch((ex) => {
        console.log(ex);
        const error =
            ex.response.status === 404
            ? "Resource Not found"
            : "An unexpected error has occurred";
    });

    fleets.sort((a, b) => b.fleet_size - a.fleet_size)
    return fleets;
}

const renderActiveShape = (props) => {
    const RADIAN = Math.PI / 180;
    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';
  
    return (
      <g>
        <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
          {payload.name}
        </text>
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius}
          outerRadius={outerRadius}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
        <Sector
          cx={cx}
          cy={cy}
          startAngle={startAngle}
          endAngle={endAngle}
          innerRadius={outerRadius + 6}
          outerRadius={outerRadius + 10}
          fill={fill}
        />
        <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
        <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`Fleet Size ${value}`}</text>
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
          {`(Proportion ${(percent * 100).toFixed(2)}%)`}
        </text>
      </g>
    );
};

function FleetSizePie() {

    const [fleets, setFleets]: [any[], (fleets: any[]) => void] = useState<number[]>([]);
    const [activeIndex, setActiveIndex]: [number, (activeIndex: number) => void] = useState(0);
    const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);

    const onPieEnter = (_, index) => {
        setActiveIndex(index);
    }

    useEffect(() => {

        let cancel = false;

        const fetchData = async () => {
            if (cancel) {
                return;
            }
            const fleetsResponse = await getAirlinesInfo();
            setFleets(fleetsResponse)
            setLoading(false)
        };

        fetchData();

        return (() => {
            setFleets([]);
            cancel = true;
        });
        
    }, []);

    if (loading) {
        return (
            <CircularProgress />
        );
    }    

    return (
            
        <ResponsiveContainer width="90%" height={600}>
            <PieChart width={600} height={500}>
                <Pie
                    activeIndex={activeIndex}
                    activeShape={renderActiveShape}
                    data={fleets}
                    cx="50%"
                    cy="50%"
                    innerRadius={160}
                    outerRadius={220}
                    fill="#8884d8"
                    dataKey="fleet_size"
                    onMouseEnter={onPieEnter}
                    onClick={() => window.location.href=`https://www.flightright.me/airlines/view/${fleets[activeIndex].id}`}
                />
            </PieChart>
        </ResponsiveContainer>
    );
}

export default FleetSizePie;