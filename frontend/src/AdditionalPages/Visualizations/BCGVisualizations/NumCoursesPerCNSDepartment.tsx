import React, { useEffect, useState, PureComponent } from "react";
import {
    Bar,
    BarChart,
    CartesianGrid,
    Legend,
    XAxis,
    YAxis,
    Tooltip,
    Radar,
    RadarChart,
    PolarGrid,
    PolarAngleAxis,
    PolarRadiusAxis,
    Label,
    ResponsiveContainer,
    RadialBarChart,
    RadialBar,
} from "recharts";
import axios from "axios";
import {
    Typography,
    Box
} from "@material-ui/core";


//storage container for a single department's data
interface IDepartment {
    name: string,
    courses: number,
}

//storage to contain multiple instances of departments
interface IDepartments {
    departments: any,
}


//create interface of department to store data for a single department
function createDepartment(
    name: string,
    courses: number,
): IDepartment {
    return {
        name,
        courses,
    };
}



function NumCoursesPerCNSDepartment() {
    const [data, setData] = useState(null)
    //get all instances of departments and place them onto cards
    const getDepartmentsInfo = async () => {
        let depart = [];
        let departments: Array<IDepartment> = [];
        await axios.get<IDepartments>(`https://api.bevoscourseguide.me/api/alldepartments?sort_by=courses.desc&page=1&per_page=125&filter=College of Natural Sciences`, {
            headers: {
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                depart = response.data.departments;
                depart.forEach(department => {
                    if (department.parent.includes('College of Natural Sciences') && !department.name.includes("College of Natural Science")) {
                        departments.push(createDepartment(department.name.replace("Department of ", "").replace("School of ", ""), department["all class"].length));
                    }
                });
            })
            .catch(ex => {
                const error =
                    ex.response.status === 404
                        ? "Resource Not found"
                        : "An unexpected error has occurred";
                console.log(ex);
            });
        return {
            departments: departments,
        }
    }

    useEffect(() => {
        document.body.style.background = 'white';
        document.title = "Departments";

        const fetchData = async () => {
            const DepartmentsInfo = await getDepartmentsInfo();
            setData(DepartmentsInfo.departments);
        }
        fetchData();
    }, [])
    return (
        <BarChart
          width={800}
          height={600}
          data={data}
          layout="horizontal"
          margin={{ bottom: 150, left: 50, right: 50}}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis
            interval={0}
            type="category"
            dataKey="name"
            angle={-30}
            textAnchor="end"
            label={{
              value: "Department Name",
              position: "insideBottom",
              offset: -110,
            }}
          />
          <YAxis
            type="number"
            label={{
              value: "Number of Classes",
              angle: -90,
              position: "insideLeft",
              offset: 15,
            }}
          />
          <Tooltip />
          <Bar dataKey="courses" fill="#FF9933" />
        </BarChart>
    );
}
export default NumCoursesPerCNSDepartment;
