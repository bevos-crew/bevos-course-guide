import pandas as pd
import requests
import io
import string
import re
import time
import json

df = pd.read_csv('../dataFiles/Current_Semester_Report', sep='\s*\t\s*', header=0, engine="python", dtype={"Course Nbr": object, 'Unique': object})

newCols = {"Dept-Abbr": "Dept", "Dept-Name": "DeptName", "Course Nbr": "CourseNum", "Crs Desc": "Desc"}
df = df.rename(columns=newCols)

def get_prof(unique):

    course = df[df['Unique'] == unique]
    dept = course['Dept'].to_string(index=False)
    c_num = course['CourseNum'].to_string(index=False)

    prof = course['Instructor'].to_string(index=False)

    params = { "year": 2021, "semester": 9, "department": dept, "course_number": c_num, "unique": unique, "course_type": "In Residence", "search": "Search" }

    try:
        response = requests.get("http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/", params)
    except:
        return ""

    page = response.text

    prof_reg = prof + "[a-zA-Z-]+"

    match = re.search(prof_reg, page, re.IGNORECASE)
    
    if match == None:
        return ""

    prof_name = re.sub('[^[a-zA-Z-,]]', '', match.group())

    names = prof_name.split(', ')

    if len(names) < 2:
        return ""

    full_name = names[1] + ' ' + names[0]

    return full_name


# Get all prof names and write them to a file

try:
    profsDict = json.load(open("../dataFiles/profNameDict.txt"))
except:
    profsDict = {}

startT = time.perf_counter()

courseCount = len(profsDict)
foundName = sum(value != "NO NAME FOUND" for value in profsDict.values())

print(foundName)
exit()

for uni in df["Unique"].iloc[:]:
    name = get_prof(uni)
    if name == "":
        profsDict[uni] = "NO NAME FOUND"
    else:
        profsDict[uni] = name

    courseCount += 1
    print(courseCount, uni, name)
    time.sleep(0.1)

with open("../profNameDict.txt", "w") as profsFile:
    json.dump(profsDict, profsFile)

print("Time:", time.perf_counter() - startT)