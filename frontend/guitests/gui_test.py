import pytest
from selenium import webdriver
from selenium.webdriver import Remote
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

driver = None
wait = None

# SWITCH TO MAIN SITE
url = "https://dev.bevoscourseguide.me"
api_url = "https://api.bevoscourseguide.me/api/"

def setup_module(module):
    global driver, wait
    chrome_path = r'/usr/local/bin/chromedriver'

    chrome_options = Options()
    ser = Service(chrome_path)
    chrome_options.add_argument('--headless') #comment out of running locally
    chrome_options.add_argument('--no-sandbox')
    #driver = webdriver.Chrome(service=ser, options=chrome_options)
    #driver = webdriver.Chrome(executable_path=chrome_path, options=chrome_options)
    driver = Remote("http://selenium__standalone-chrome:4444/wd/hub", desired_capabilities=chrome_options.to_capabilities())
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    return driver

def teardown_module(module):
    driver.close()

def test_title():
    assert driver.title == "Bevo's Course Guide"

def test_nav_bar_links():
    nav_bar = driver.find_elements(By.XPATH, "/html/body/div/header/div/a")
    # Navigate to courses page
    nav_bar[1].click()
    assert driver.title == "Courses"
    assert driver.current_url == url + "/Courses"
    time.sleep(1)
    # Navigate to Home
    nav_bar[0].click()
    assert driver.title == "Bevo's Course Guide"
    assert driver.current_url == url + "/Home"
    # Navigate to Professors
    time.sleep(1)
    nav_bar[2].click()
    assert driver.title == "Professors"
    assert driver.current_url == url + "/Professors"
    # Navigate to Departments
    time.sleep(1)
    nav_bar[3].click()
    assert driver.title == "Departments"
    assert driver.current_url == url + "/Departments"
    # Navigate to Visualizations
    # nav_bar[4].click()
    # assert driver.title == "Visualizations"
    # assert driver.current_url == url + "/Visualizations"
    # Navigate to About Us
    nav_bar[5].click()
    assert driver.title == "About Us"
    assert driver.current_url == url + "/AboutUs"
    
def test_home_page_links():
    driver.get(url)
    buttons = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, "jss7")))
    buttons[0].click()
    assert driver.title == "Courses"
    assert driver.current_url == url + "/Courses"
    driver.get(url)
    buttons = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, "jss7")))
    buttons[1].click()
    assert driver.title == "Professors"
    assert driver.current_url == url + "/Professors"
    driver.get(url)
    buttons = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, "jss7")))
    buttons[2].click()
    assert driver.title == "Departments"
    assert driver.current_url == url + "/Departments"

def test_about_us_headers():
    driver.get(url + "/AboutUs")
    time.sleep(3)
    headers = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, "h3")))
    headers = map(lambda header: header.text, headers)
    expected = ["About Us", "The Team", "Total Repo Stats", "Tools Used", "Repo and API Docs", "APIs and Additional Data Sources", "Integrating Data"]
    assert list(headers) == expected

def test_about_us_repo_stats():
    driver.get(url + "/AboutUs")
    stats = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, "jss6")))
    commits = stats[0].find_elements(By.TAG_NAME, "h6")
    assert int(commits[1].text) > 0
    issues = stats[1].find_elements(By.TAG_NAME, "h6")
    assert int(issues[1].text) > 0
    tests = stats[2].find_elements(By.TAG_NAME, "h6")
    assert int(tests[1].text) >= 0

def test_tools_used_links():
    driver.get(url + "/AboutUs")
    tools = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, "jss7")))
    assert len(tools) == 24
    # tools[0].click()
    # assert driver.current_url == "https://www.docker.com/"
    # driver.back()
    # time.sleep(3)
    # tools = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, "jss7")))
    # tools[15].click()
    # assert driver.current_url == "https://gitlab.com/bevos-crew/bevos-course-guide"
    # driver.back()
    # time.sleep(3)
    # tools = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, "jss7")))
    # tools[19].click()
    # assert driver.current_url == "https://developers.google.com/custom-search/v1/overview"
    # driver.back()

def test_courses_model_page():
    driver.get(url + "/Courses")
    time.sleep(15)
    rows = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, "tr")))
    rows[0].find_element(By.TAG_NAME, 'th').click()
    print(driver.current_url)
    assert driver.current_url == url + "/Courses/AAS%20302%20IMMIGRATION%20AND%20ETHNICITY"
    
def test_course_instance_page():
    driver.get(url + "/Courses/AAS%20302%20IMMIGRATION%20AND%20ETHNICITY")
    time.sleep(15)
    links = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, "a")))
    # for link in links:
    #     #print(link)
    #     print(link.get_attribute('href'))
    links[6].click()
    #assert driver.current_url == url + "/Departments/Slavic%20and%20Eurasian%20Languages"
    assert driver.current_url == url + "/Departments/College%20of%20Liberal%20Arts"

def test_professors_model_page():
    driver.get(url + "/Professors")
    time.sleep(15)
    links = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, "a")))
    # for link in links:
    #     print(link.get_attribute('href'))
    links[6].click()
    print(driver.current_url)
    assert driver.current_url == url + "/Professors/AARONS,%20C"


def test_departments_model_page():
    driver.get(url + "/Departments")
    time.sleep(15)
    links = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, "a")))
    # for link in links:
    #     print(link.get_attribute('href'))
    links[6].click()
    #assert driver.current_url == url + "/Departments/Applied%20Learning%20and%20Development"
    #assert driver.current_url == url + "/Departments/School%20of%20Architecture"
    assert driver.current_url == url + "/Departments/Am%C3%A9rico%20Paredes%20Center%20for%20Cultural%20Studies"

def test_main_search(): #STILL NEED TO DO
    driver.get(url)
    time.sleep(10)
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div/div/div/input').click()
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div/div/div/input').send_keys("downing")
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div/div/div/input').send_keys(Keys.RETURN)
    assert url + "/Search/downing" in driver.current_url

def test_course_model_filter():
    driver.get(url + "/Courses")
    time.sleep(15)

    initial_course = driver.find_element_by_xpath('/html/body/div/div/div/div/div[2]/table/tbody/tr[2]/th')
    driver.find_element_by_xpath('/html/body/div/div/div/div/div[1]/div[2]/div/div').click()
    driver.find_element_by_xpath('/html/body/div[2]/div[3]/ul/li[1]/div/span').click()
    new_course = driver.find_element_by_xpath('/html/body/div/div/div/div/div[2]/table/tbody/tr[1]/th')
    assert initial_course != new_course

def test_course_model_sort():
    # Sort by 
    driver.get(url + "/Courses")
    time.sleep(15)
    #initial_course = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/div/div/div[2]/table/tbody/tr[1]')))
    initial_course = driver.find_element_by_xpath('/html/body/div/div/div/div/div[2]/table/tbody/tr[2]/th')
    driver.find_element_by_xpath('/html/body/div/div/div/div/div[2]/table/thead/th[1]/span').click()
    #new_course = driver.find_element_by_xpath('//*[@id="root"]/div/div/div/div[2]/table/tbody/tr[1]')
    new_course = driver.find_element_by_xpath('/html/body/div/div/div/div/div[2]/table/tbody/tr[1]/th')
    assert initial_course != new_course

def test_course_model_search():
    # Search From courses model page
    driver.get(url + "/Courses")
    initial_url = driver.current_url
    time.sleep(10)
    driver.find_element_by_xpath('/html/body/div/div/div/div/div[1]/div[1]/div/div/input').click()
    driver.find_element_by_xpath('/html/body/div/div/div/div/div[1]/div[1]/div/div/input').send_keys("computer")
    driver.find_element_by_xpath('/html/body/div/div/div/div/div[1]/div[1]/div/div/input').send_keys(Keys.RETURN)
    print(driver.current_url) 
    time.sleep(10)
    assert initial_url == driver.current_url


def test_professor_model_sort():
    # Sort by Name
    driver.get(url + "/Professors")
    time.sleep(10)
    initial_professor = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[1]/a/img')
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div[2]/div/button/span[1]').click()
    driver.find_element_by_xpath('/html/body/div[2]/div[3]/ul/li[3]').click()
    time.sleep(10)
    new_professor = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[1]/a/img')
    
    assert initial_professor == new_professor

def test_professor_model_search():
    # Search From professor model page
    driver.get(url + "/Professors")
    initial_url = driver.current_url
    time.sleep(10)
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div[1]/div/div/input').click()
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div[1]/div/div/input').send_keys("downing")
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div[1]/div/div/input').send_keys(Keys.RETURN)
    print(driver.current_url)
    time.sleep(10)
    assert initial_url == driver.current_url

def test_department_model_filter():
    driver.get(url + "/Departments")
    time.sleep(15)
    
    initial_department = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[1]/a/div')
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div[6]/button/span[1]/p').click()
    driver.find_element_by_xpath('/html/body/div[2]/div[3]/ul/div[3]/li[2]').click()
    new_department = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[1]/a/div')
    assert initial_department == new_department

def test_department_model_sort():
    driver.get(url + "/Departments")
    time.sleep(10)
    initial_department = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[1]/a/div/p/div/span')
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div[2]/div/button/span[1]').click()
    driver.find_element_by_xpath('/html/body/div[2]/div[3]/ul/li[3]').click()
    new_department = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[1]/a/div/p/div')
    assert initial_department != new_department
    
def test_department_model_search():
    # Search From departments model page
    driver.get(url + "/Departments")
    initial_url = driver.current_url
    time.sleep(10)
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div[1]/div/div/input').click()
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div[1]/div/div/input').send_keys("science")
    driver.find_element_by_xpath('/html/body/div/div/div[1]/div[1]/div/div/input').send_keys(Keys.RETURN)
    print(driver.current_url) 
    time.sleep(10)
    assert initial_url == driver.current_url