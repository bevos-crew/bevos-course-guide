import pandas as pd
import requests
import ast
import pickle

def get_syllabus_url(unique, df):

    course = df[df['Unique'] == unique]
    dept = course['Dept-Abbr']
    c_num = course['Course Nbr']

    params = { "year": 2021, "semester": 9, "department": dept, "course_number": c_num, "unique": unique, "course_type": "In Residence", "search": "Search" }
    #print(params)
    try:
        response = requests.get("http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/", params=params, timeout=10)
        #print(response.content)
    except:
        #print('response did not get')
        return ""

    page = response.text
    syllabusDownload = page.rfind("download/")

    if syllabusDownload == -1:
        #print("syllabus download failed")
        return ""

    syllabusURL = "http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/" + page[syllabusDownload : page.find('\"', syllabusDownload)]
    return syllabusURL

def get_resume_url(unique, df):
    course = df[df['Unique'] == unique]
    dept = course['Dept-Abbr']
    c_num = course['Course Nbr']

    params = { "year": 2021, "semester": 9, "department": dept, "course_number": c_num, "unique": unique, "course_type": "In Residence", "search": "Search" }
    #print(params)
    try:
        response = requests.get("http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/", params=params, timeout=10)
        #print('response did get')
    except:
        #print('response did not get')
        return ""

    page = response.text
    resDownload = page.find("download/")
    if resDownload == -1:
        #print('res download failure')
        return ""

    resURL = "http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/" + page[resDownload : page.find('\"', resDownload)]
    return resURL

def create_resume_and_syllabi_dict(df: pd.DataFrame()):
    resume_dict  = dict()
    syllabi_dict = dict()

    course_names_nbr_list = []
    professor_names_list  = []
    for index, rec in df.iterrows():
        if index % 100 == 0:
            print(index)
        #SYLLABUS
        if (rec['Title'], rec['Course Nbr']) not in course_names_nbr_list:
            course_names_nbr_list.append((rec['Title'], rec['Course Nbr']))
            course_syllabus = get_syllabus_url(rec['Unique'], df)
            syllabi_dict[(rec['Title'], rec['Course Nbr'])] = course_syllabus
        #RESUME
        if rec['Instructor'] not in professor_names_list:
            professor_names_list.append(rec['Instructor'])
            resume = get_resume_url(rec['Unique'], df)
            resume_dict[rec['Instructor']] = resume

    return resume_dict, syllabi_dict

def main():
    width = [5, 2, 3, 101, 6, 4, 7, 7, 31, 217, 26, 9, 101, 
            7, 5, 5, 4, 8, 4, 4, 4, 6, 183]
    header = ['Year', 'Semester', 'Dept-Abbr', 'Dept-Name', 'Course Nbr', 'Topic', 
            'Unique', 'Const Sect Nbr', 'Title', 'Crs Desc', 'Instructor', 
            'Instructor EID', 'Instructor Email', 'Days', 'From', 'To', 
            'Building',	'Room', 'Max Enrollment', 'Seats Taken', 'Total X-listings',
            'X-List Pointer', 'X-Listings']
    df = pd.read_fwf('../dataFiles/Current_Semester_Report', widths=width, dtype='str')
    df.columns=header
    df = df.iloc[1:,:]

    # resume_dict, syllabi_dict = create_resume_and_syllabi_dict(df)

    # with open('../dataFiles/syllabi_dict.txt', 'wb') as f:
    #     pickle.dump(syllabi_dict, f, protocol=pickle.HIGHEST_PROTOCOL)
    # with open('../dataFiles/resumes_dict.txt', 'wb') as f:
    #     pickle.dump(resume_dict, f, protocol=pickle.HIGHEST_PROTOCOL)

    with open('../dataFiles/syllabi_dict.txt', 'rb') as f:
        sy_dict = pickle.load(f)
        print("length of syllabus dict: ", len(sy_dict))
    with open('../dataFiles/resumes_dict.txt', 'rb') as f:
        re_dict = pickle.load(f)
        print('length of resumes dict: ', len(re_dict))

    course_names_nbr_list = []
    professor_names_list  = []
    for index, rec in df.iterrows():
        if index % 100 == 0:
            print(index)
        #SYLLABUS
        if (rec['Title'], rec['Course Nbr']) not in course_names_nbr_list:
            course_names_nbr_list.append((rec['Title'], rec['Course Nbr']))
        #RESUME
        if rec['Instructor'] not in professor_names_list:
            professor_names_list.append(rec['Instructor'])

    print('num of unique courses: ', len(course_names_nbr_list))
    print('num of unique professors: ', len(professor_names_list))


if __name__ == "__main__":
    main()