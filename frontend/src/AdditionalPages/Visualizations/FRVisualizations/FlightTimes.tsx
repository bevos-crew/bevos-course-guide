import React, { useEffect, useState } from "react";

import {
    CircularProgress
  } from "@material-ui/core";
import axios from "axios";

import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import { ITime, IFlight, IFlightResponse } from "../../../Components/Interfaces";


const getFlightTimes = async () => {
    let times: ITime[] = [];

    for(let i = 0; i < 24; i++) {
        if(i < 10) {
            times.push({name: "0" + i + ":00", dept: 0, arr: 0})
        } else {
            times.push({name: i + ":00", dept: 0, arr: 0})
        }
    }

    await axios.get<IFlightResponse>(
      "https://api.flightright.me/flights?limit=2163"
    )
    .then((response) => {
        console.log(response)
        response.data.data.forEach((flight: IFlight) => {
            let dept = flight.flight_dept_time.substring(11, 13);
            let arr = flight.flight_arr_time.substring(11, 13);

            times[parseInt(dept)].dept++;
            times[parseInt(arr)].arr++;
        });
    })
    .catch((ex) => {
        console.log(ex);
        const error =
            ex.response.status === 404
            ? "Resource Not found"
            : "An unexpected error has occurred";
    });

    console.log(times);
    return times;
}

function FlightTimes() {
    const demoUrl = 'https://codesandbox.io/s/stacked-area-chart-ix341';

    const [times, setTimes]: [ITime[], (markers: ITime[]) => void] = useState([]);
    const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);

    useEffect(() => {

        const fetchData = async () => {
            const markersResponse = await getFlightTimes();
            setTimes(markersResponse)
            setLoading(false)
        };

        fetchData();
        
    }, []);

    if (loading) {
        return (
            <CircularProgress />
        );
    }    


    return (
        <ResponsiveContainer width={800} height={300}>
            <AreaChart
                width={500}
                height={300}
                data={times}
                margin={{
                    top: 10,
                    right: 30,
                    left: 0,
                    bottom: 0,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Area type="monotone" dataKey="arr" stackId="1" stroke="#8884d8" fill="#8884d8" />
                <Area type="monotone" dataKey="dept" stackId="1" stroke="#82ca9d" fill="#82ca9d" />
                {/* <Area type="monotone" dataKey="amt" stackId="1" stroke="#ffc658" fill="#ffc658" /> */}
            </AreaChart>
        </ResponsiveContainer>
    );
}

export default FlightTimes;