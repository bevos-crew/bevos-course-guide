#from  database import db, create_app, Professor, Course, Department
import pandas as pd
from datetime import datetime, time
#from sqlalchemy import SQLAlchemyError
from sqlalchemy.ext.mutable import MutableList
import requests
import sys
import time
import os
import ast
import re
import pickle
sys.path.insert(1, '../dataScripts/')
from getGoogleBooksInfo import getBookInfo
sys.path.append('../')
from database import db, create_app, Professor2, Course2, Department2, Section2

#IMPORT RELEVANT DICTIONARIES
syllabi_dict = dict()
with open('../dataFiles/syllabi_dict.txt', 'rb') as f:
    syllabi_dict = pickle.load(f)

ISBN_dict = dict()
file = open("../dataFiles/isbnDict.txt", "r")
contents = file.read()
ISBN_dict = ast.literal_eval(contents)
file.close()

file = open("../dataFiles/deptsDict.json", "r")
contents = file.read()
#keys: dept name, value keys: "Abbr", "People", "Parent", "Links"
dept_dict = ast.literal_eval(contents)
file.close()


def print_courses(courses_list: list, index :int):
    if index < len(courses_list):
        #assert(type(courses_list[index].course_name)        is str or pd.isna(courses_list[index].course_name))
        #assert(type(courses_list[index].course_number)      is str or pd.isna(courses_list[index].course_number))
        assert(type(courses_list[index].course_professors)  is MutableList or pd.isna(courses_list[index].course_professors))
        assert(type(courses_list[index].course_syllabus)    is str or pd.isna(courses_list[index].course_syllabus))
        assert(type(courses_list[index].course_desc)        is str or pd.isna(courses_list[index].course_desc))
        assert(type(courses_list[index].course_hours)       is MutableList or pd.isna(courses_list[index].course_hours))
        assert(type(courses_list[index].course_college)     is str or pd.isna(courses_list[index].course_college))
        assert(type(courses_list[index].course_section)     is MutableList or pd.isna(courses_list[index].course_section))
        try: print("course num + name:         " , courses_list[index].course_num_name)
        except: pass
        try: print("course prof:         " , courses_list[index].course_professors)
        except: pass
        try: print("course syllabus:     " , courses_list[index].course_syllabus)
        except: pass
        try: print("course desc:         " , courses_list[index].course_desc)
        except: pass
        try: print("course hours:        " , courses_list[index].course_hours)
        except: pass
        try: print("course days:         " , courses_list[index].course_days)
        except: pass
        try: print("course college:      " , courses_list[index].course_college)
        except: pass
        try: print("course dept_general_name:      " , courses_list[index].course_dept_general_name)
        except: pass
        try: print("course dept_specific_abbr:      " , courses_list[index].course_dept_specific_abbr)
        except: pass
        try: print("course section list: " , courses_list[index].course_section)
        except: pass
        try: print("course rating: " , courses_list[index].course_rating)
        except: pass
    else: print("index not in provided courses list")

def print_sections(sec_list: list, index: int):
    if index < len(sec_list):
        assert(type(sec_list[index].sec_id)        is str or pd.isna(sec_list[index].sec_id))
        assert(type(sec_list[index].sec_days)      is str or pd.isna(sec_list[index].sec_days))
        assert(type(sec_list[index].sec_room)      is str or pd.isna(sec_list[index].sec_room))
        assert(type(sec_list[index].sec_starttime) is str or pd.isna(sec_list[index].sec_starttime))
        assert(type(sec_list[index].sec_endtime)   is str or pd.isna(sec_list[index].sec_endtime))
        assert(type(sec_list[index].sec_seats)     is int or pd.isna(sec_list[index].sec_seats))
        assert(type(sec_list[index].sec_professor) is str or pd.isna(sec_list[index].sec_professor))
        #print(print(sec_list[index].sec_book_ISBN))
        assert(type(sec_list[index].sec_book_ISBN) is str or pd.isna(sec_list[index].sec_book_ISBN))
        assert(type(sec_list[index].sec_book_title)is str or pd.isna(sec_list[index].sec_book_title))
        assert(type(sec_list[index].sec_book_subtitle) is str or pd.isna(sec_list[index].sec_book_subtitle))
        #assert(type(sec_list[index].sec_book_authors) is list or pd.isna(sec_list[index].sec_book_authors))
        assert(type(sec_list[index].sec_book_thumbnail) is str or pd.isna(sec_list[index].sec_book_thumbnail))
        #print(sec_list[index].sec_book_listPrice)
        #print(type(sec_list[index].sec_book_listPrice))
        #assert(type(sec_list[index].sec_book_listPrice) is str or pd.isna(sec_list[index].sec_book_listPrice))
        #assert(type(sec_list[index].sec_book_retailPrice) is str or pd.isna(sec_list[index].sec_book_retailPrice))
        try: print("section id:        " , sec_list[index].sec_id)
        except: pass
        try: print("section days:      " , sec_list[index].sec_days)
        except: pass
        try: print("section room:      " , sec_list[index].sec_room)
        except: pass
        try: print("section starttime: " , sec_list[index].sec_starttime)
        except: pass
        try: print("section endtime:   " , sec_list[index].sec_endtime)
        except: pass
        try: print("section seats:     " , sec_list[index].sec_seats)
        except: pass
        try: print("section professor: " , sec_list[index].sec_professor)
        except: pass
        try: print("section dept_general_name: " , sec_list[index].sec_dept_general_name)
        except: pass
        try: print("section course_name: " , sec_list[index].sec_course_name)
        except: pass
        try: print("section course_num: " , sec_list[index].sec_course_num)
        except: pass
        try: print("section book ISBN:        " , sec_list[index].sec_book_ISBN)
        except: pass
        try: print("section book title:      " , sec_list[index].sec_book_title)
        except: pass
        try: print("section book subtitle:      " , sec_list[index].sec_book_subtitle)
        except: pass
        try: print("section book authors: " , sec_list[index].sec_book_authors)
        except: pass
        try: print("section book thumbnail:   " , sec_list[index].sec_book_thumbnail)
        except: pass
        try: print("section book list price:     " , sec_list[index].sec_book_listPrice)
        except: pass
        try: print("section book retail price: " , sec_list[index].sec_book_retailPrice)
        except: pass

#adapted from get_isbn() func. in Adeet's readSyllabusISBN.py
def get_syllabus_url(unique, df):

    course = df[df['Unique'] == unique]
    dept = course['Dept-Abbr']
    c_num = course['Course Nbr']

    params = { "year": 2021, "semester": 9, "department": dept, "course_number": c_num, "unique": unique, "course_type": "In Residence", "search": "Search" }
    #print(params)
    try:
        response = requests.get("http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/", params=params, timeout=10)
        #print(response.content)
    except:
        #print('response did not get')
        return ""

    page = response.text
    syllabusDownload = page.rfind("download/")

    if syllabusDownload == -1:
        #print("syllabus download failed")
        return ""

    syllabusURL = "http://utdirect.utexas.edu/apps/student/coursedocs/nlogon/" + page[syllabusDownload : page.find('\"', syllabusDownload)]
    return syllabusURL

def google_books(sec_unique: str, crs_rec: pd.DataFrame):
    #global ISBN_dict
    ISBN = ISBN_dict.get(sec_unique)
    param = ISBN
    if ISBN == "NO ISBN":
        try: param = crs_rec['Title'] + " " + crs_rec['Crs Desc']
        except: param = crs_rec['Title']
    book_response_dict = getBookInfo(param)
    return book_response_dict

def get_department_name(abbr: str):
    #global dept_dict

    department = 'No Department for: ' + abbr
    college    = 'No College for: ' + abbr
    for dept_name, values in dept_dict.items():
        if abbr in values['Abbr']:
            department = dept_name
            try   : college    = values['Parent']
            except: pass
            return department, college
    return department, college

def create_courses_list(df: pd.DataFrame()):
    courses_list      = []
    #course_names_list = []
    course_names_nbr_list = []
    all_sections_list = []
    for index, rec in df.iterrows():
        if index % 100 == 0:
            print(index)
        #if rec['Title'] not in course_names_list:
        if (rec['Title'], rec['Course Nbr']) not in course_names_nbr_list:
            course_names_nbr_list.append((rec['Title'], rec['Course Nbr']))
            #course_names_list.append(rec['Title'])
            #course_nbr_list.append(rec['Course Nbr'])
            course_abbr_num = ''
            hours = set()
            professors = set()
            days = '' #JUST ADDED
            sections = []
            section_ids = []
            #get course syllabus - NOW WITH DICT
            #course_syllabus = get_syllabus_url(rec['Unique'], df)
            course_syllabus = ''
            try: course_syllabus = syllabi_dict.get((rec['Title'], rec['Course Nbr']))
            except: pass
            #try appending department abbreviation to course num
            try: course_abbr_num = rec['Dept-Abbr'] + ' ' + rec['Course Nbr']
            except: course_abbr_num = rec['Course Nbr']
            #try appending abbr course + name
            course_num_name = course_abbr_num
            try: course_num_name += (' ' + rec['Title'])
            except: pass
            #get department general name and college
            dept_general_name, college = get_department_name(rec['Dept-Abbr'])
            #create all sections for course and add to sections list
            section_numbers = df.loc[(df['Course Nbr'] == rec['Course Nbr']) & 
                        (df['Title'] == rec['Title'])] #.loc[:,'Const Sect Nbr'].tolist()
            for i, section in section_numbers.iterrows():
                #add section days
                if (section['Days'] != None) and (not pd.isna(section['Days'])):
                    days += section['Days']
                #try to append hours after converting to 12hr
                start = ''
                end = ''
                if (not pd.isna(section['From'])) and (not pd.isna(section['To'])):
                    start = datetime.strptime(str(section['From']), "%H%M")
                    start = start.strftime("%I:%M %p")
                    #print("start: " + start)
                    end = datetime.strptime(str(section['To']), "%H%M")
                    end = end.strftime("%I:%M %p")
                    #print("end: " + end)
                    hours.add(start + ' - ' + end)
                    #print(hours)
                #try to append professor if unique
                if section['Instructor'] != None: professors.add(section['Instructor'])
                #try to cast num seats to int
                seats = 0
                try: seats = int(section['Max Enrollment']) 
                except: pass
                #try to concat building and room
                section_room = ''
                try: section_room = section['Building'] + ' ' + section['Room']
                except: pass
                #try to concat section id, starttime, endtime to str
                unique      = section['Unique']
                start_time  = start #section['From']
                end_time    = end #section['To']
                try: unique = str(unique)
                except: pass
                try: start_time = str(start_time)
                except: pass
                try: end_time = str(end_time)
                except: pass
                #get google books info
                book_response_dict = google_books(unique, rec)
                ISBN_identifier    = "0"
                try: ISBN_identifier = book_response_dict['ISBN']["identifier"]
                except: pass
                list_price         = 'None'
                try: list_price = book_response_dict['listPrice']['amount']
                except: pass
                retail_price       = 'None'
                try: retail_price = book_response_dict['retailPrice']['amount']
                except: pass
                book_title         = "No Title"
                try: book_title = book_response_dict['title']
                except: pass
                book_subtitle      = "No Subtitle"
                try: book_subtitle = book_response_dict['subtitle']
                except: pass
                book_authors       = ["No Author"]
                try: book_authors = book_response_dict['authors']
                except: pass
                book_thumbnail     = "No Thumbnail"
                try: book_thumbnail = book_response_dict['thumbnail']
                except: pass
                all_sections_list.append(Section2(sec_id = unique, 
                                        sec_days             = section['Days'], 
                                        sec_room             = section_room,
                                        sec_starttime        = start_time, 
                                        sec_endtime          = end_time, 
                                        sec_seats            = seats,
                                        sec_professor        = section['Instructor'],
                                        sec_dept_general_name= dept_general_name,
                                        sec_course_name      = rec['Title'],
                                        sec_course_num       = course_abbr_num,
                                        sec_book_ISBN        = ISBN_identifier,
                                        sec_book_title       = book_title,
                                        sec_book_subtitle    = book_subtitle,
                                        sec_book_authors     = book_authors, 
                                        sec_book_thumbnail   = book_thumbnail,
                                        sec_book_listPrice   = list_price,
                                        sec_book_retailPrice = retail_price))
                section_ids.append(unique) #append just section_ids
            #put all professors and hours into a string - might need to change professors to array
            #professors = ', '.join(professors)
            professors = list(professors)
            hours      = list(hours)
            #get unique days
            list_days       = []
            list_days += re.findall(r'M', days) 
            list_days += re.findall(r'T(?!H)+', days) 
            list_days += re.findall(r'W', days)
            list_days += re.findall(r'TH', days)
            list_days += re.findall(r'F', days)
            unique_days = ''.join(list(dict.fromkeys((list_days))))
            #create course object and add to list
            course = Course2(course_num_name=course_num_name,
                            course_professors=professors, 
                            course_syllabus=course_syllabus,
                            course_desc=rec['Crs Desc'], 
                            course_hours=hours, 
                            course_days=unique_days,
                            course_college=college, 
                            course_dept_general_name=dept_general_name,
                            course_dept_specific_abbr=rec['Dept-Abbr'],
                            course_section=section_ids,
                            course_rating=-1)
            courses_list.append(course)

    return courses_list, all_sections_list

def main():
    #CREATE COURSES LIST
    start_time = time.time() #start timer
    #df = pd.read_csv('../dataFiles/Current_Semester_Report', sep='\s*\t\s*', header=0, engine="python", dtype={"Course Nbr": object, 'Unique': object})
    width = [5, 2, 3, 101, 6, 4, 7, 7, 31, 217, 26, 9, 101, 
            7, 5, 5, 4, 8, 4, 4, 4, 6, 183]
    header = ['Year', 'Semester', 'Dept-Abbr', 'Dept-Name', 'Course Nbr', 'Topic', 
            'Unique', 'Const Sect Nbr', 'Title', 'Crs Desc', 'Instructor', 
            'Instructor EID', 'Instructor Email', 'Days', 'From', 'To', 
            'Building',	'Room', 'Max Enrollment', 'Seats Taken', 'Total X-listings',
            'X-List Pointer', 'X-Listings']
    df = pd.read_fwf('../dataFiles/Current_Semester_Report', widths=width, dtype='str')
    df.columns=header
    #df['Year']           = df['Year'].astype('Int64')
    #df['Semester']       = df['Semester'].astype('Int64')
    #df['Unique']         = df['Unique'].astype('Int64')
    #df['Const Sect Nbr'] = df['Const Sect Nbr'].astype('Int64')
    #df['From']           = df['From'].astype('Int64')
    #df['To']             = df['To'].astype('Int64')
    #df['Max Enrollment'] = df['Max Enrollment'].astype('Int64')
    #df['Seats Taken']    = df['Seats Taken'].astype('Int64')

    # with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    #     print(df.iloc[8398:8410,:].columns)
    #     print(df.iloc[8398:8410,:])
    df = df.iloc[1:,:]
    courses_list, all_sections_list = create_courses_list(df)
    
    #TESTING
    #print_courses(courses_list, 0)
    #print_sections(all_sections_list, 0)
    #print_course_and_sections(courses_list, 1)
    #print_course_and_sections(courses_list, -1)
    # for c, elem in enumerate(courses_list):
    #     print_courses(courses_list, c)
    # for c, elem in enumerate(all_sections_list):
    #     print_sections(all_sections_list, c)

    print("--- %s seconds ---" % (time.time() - start_time))

    #UPLOAD TO DATABASE
    print("Do you want to upload to database? If so, type 'y'. If not, anything else")
    #upload = input()

    upload = 'y'
    #print(os.getcwd())
    os.chdir('../')
    #print(os.getcwd())
    if upload == 'y':
        print('WILL UPLOAD TO DATABASE NOW')
        app = create_app()
        app.app_context().push()
        db.create_all()
        
        print("Do you want to add or delete? if delete, so 'delete', if add, anything else")
        #add_or_delete = input()
        add_or_delete = 'add'
        if add_or_delete == 'delete':
            #db.session.query(Section2).delete()
            db.session.query(Course2).delete()
        else:
            #name_number_list = [(c.course_name, c.course_number) for c in db.session.query(Course)]
            #print(name_number_list[:10])
            db.session.add_all(courses_list)
            db.session.add_all(all_sections_list)
        try:
            db.session.commit()
        except:
            db.rollback()
    else: pass



if __name__ == "__main__":
    main()

