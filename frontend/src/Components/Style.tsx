import { createTheme, makeStyles } from "@material-ui/core/styles";

export const titleStyle = {
    fontFamily: "Bungee Shade",
    fontSize: 70,
    fontWeight: 600,
    display: "flex",
    justifyContent: "center",
    marginTop: "2%",
    color: "#bf5700",
};

export const textStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 30,
    display: "flex",
    marginTop: "1%",
    marginBottom: "1%",
    color: "#bf5700",
    alignSelf: "center",
};

//create font for ProfessorModelPage
export const theme = createTheme({
    typography: {
        fontFamily:
            'Bebas Neue',
        fontSize: 9,
    },
});

export const useStyle = makeStyles(() => ({
    button: {
        width: "120px",
        variant: "contained",
        background: "#bf5700",
        color: "white",
    },
    card: {
        variant: "outlined",
        width: 275,
        height: 475,
        borderRadius: 15,
        marginTop: "3%",
        marginBottom: "3%",
        background: "white",
        elevation: 6,
        margin: 20,
    },
    departCard: {
        variant: "outlined",
        width: 400,
        height: 575,
        borderRadius: 15,
        marginTop: "3%",
        marginBottom: "3%",
        background: "white",
        elevation: 3,
        margin: 20,
    },
    // styles used for image for professors instance
    image: {
        width: 400,
        height: 500,
        marginTop: "2%",
        marginBottom: "5%",
        elevation: 0,
        display: "flex",
    }
}));

export const cardStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 18,
    display: "flex",
    justifyContent: "left",
    marginTop: "1%",
    marginBottom: "1%",
};

export const courseStyles = makeStyles(() => ({
    paper: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    searchBar: {
        width: "70%",
        margin: "2%"
    },
    formControl: {
        width: "20%",
        m: 1,
    }

}));

//section paper style
export const paperTitleStyle = {
    align: "center",
    fontSize: 21,
    fontFamily: "Bebas Neue",
    justifyContent: "center",
}

//section description
export const textStyleForDescription = {
    fontSize: 21,
    align: "center",
    alignContent: "center",
    fontFamily: "Bebas Neue",
    display: "flex",
    justifyContent: "center",
};

export const pageStyle = {
    width: "100%",
    display: "flex",
    align: "center",
    flexDirection: "column",
    elevation: 10,
    background: "#fffaf5",
    textAlign: "center",

} as const;

export const listStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 26,
    display: "flex",
    justifyContent: "center",
    color: "black",
};

//used to fix size and location of department building image
export const imageStyle = makeStyles(() => ({
    card: {
        variant: "outlined",
        width: 700,
        height: 500,
        marginBottom: "3%",
        background: "white",
        align: "center",
    }
}));

//Used for Instance title font formatting
export const titleTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 70,
    display: "flex",
    justifyContent: "center",
    marginTop: "2%",
    marginBottom: "0%",
    color: "#bf5700",
};

//subheader font formatting
export const subsectionTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 40,
    display: "flex",
    justifyContent: "center",
    marginTop: "0%",
    color: "#bf5700",
};

//for professors instance page
export const subTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 25,
    display: "flex",
    justifyContent: "center",
    marginTop: "0%",
    marginBottom: "2%",
    marginLeft: "30%",
    marginRight: "30%",
    color: "#9E9999",
    maxWidth: "600px",
};
export const infoTitleTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 40,
    display: "flex",
    justifyContent: "center",
    color: "#9E9999",
    maxWidth: "600px",

};
export const infoTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 25,
    display: "flex",
    justifyContent: "center",
    color: "#bf5700",
    minWidth: "200px",
    maxWidth: "600px",
};

export const aboutUsStyles = makeStyles(() => ({
    memberPaperDiv: {
        margin: 20,
        width: 300,
        height: 475,
    },
    nameStyle: {
        fontSize: 35,
        fontWeight: 500,
    },
    roleStyle: {
        fontSize: 21,
        fontWeight: 100,
        marginTop: -20,
        marginBottom: -10,
    },
    gitStatsPaperDiv: {
        margin: 20,
        width: 300,
        height: 200,
    },
    toolsPaperDiv: {
        margin: 20,
        width: 300,
        height: 300,
    },
    paper: {
        borderRadius: 15,
    },
    cardMediaStyle: {
        height: 150,
        objectFit: "cover",
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
}));

export const aboutUsSectionStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 60,
    display: "flex",
    justifyContent: "center",
    marginTop: "4%",
}

//about us page
export const aboutUsPageStyle = {
    width: "80%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "10%",
    marginRight: "10%",
} as const;

export const homeStyles = makeStyles(() => ({
    textFieldStyle: {
        [`& fieldset`]: {
            borderRadius: 50,
        },
        backgroundColor: "white",
        borderRadius: 50,
    },
    label: {
        fontSize: 30,
        margin: 6,
        marginLeft: 30,
    },
    input: {
        fontSize: 30,
        margin: 5,
        marginLeft: 30,
        color: "black",
    },
    titleStyle: {
        fontFamily: "Bungee Shade",
        fontSize: 90,
        marginTop: "5%",
        color: "white",
    },
    paper: {
        borderRadius: 15,
    },
    modelsPaperDiv: {
        margin: 40,
        width: 300,
        height: 300,
        marginTop: 100,
    },
}));

export const visualButtonStyle = makeStyles(() => ({
    leftButton: {
        borderTopRightRadius: "0",
        borderBottomRightRadius: "0",
        width: 215,
    },
    rightButton: {
        borderTopLeftRadius: "0",
        borderBottomLeftRadius: "0",
        width: 215,
    },
}));