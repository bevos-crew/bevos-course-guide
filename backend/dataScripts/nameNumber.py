import pandas as pd
from datetime import datetime, time
#from sqlalchemy import SQLAlchemyError
from sqlalchemy.ext.mutable import MutableList
import requests
import sys
import time
import os
import ast
import json

def main():
    width = [5, 2, 3, 101, 6, 4, 7, 7, 31, 217, 26, 9, 101, 
                7, 5, 5, 4, 8, 4, 4, 4, 6, 183]
    header = ['Year', 'Semester', 'Dept-Abbr', 'Dept-Name', 'Course Nbr', 'Topic', 
            'Unique', 'Const Sect Nbr', 'Title', 'Crs Desc', 'Instructor', 
            'Instructor EID', 'Instructor Email', 'Days', 'From', 'To', 
            'Building',	'Room', 'Max Enrollment', 'Seats Taken', 'Total X-listings',
            'X-List Pointer', 'X-Listings']
    df = pd.read_fwf('../dataFiles/Current_Semester_Report', widths=width, dtype='str')
    df.columns=header

    name_number_dict = {}

    for index, rec in df.iterrows():
        value = ''
        try: value = rec['Dept-Abbr'] + ' ' + rec['Course Nbr'] 
        except: pass
        name_number_dict[rec['Title']] = value

    with open('../dataFiles/names_and_nums_of_courses.txt', 'w') as f:
        json.dump(name_number_dict, f)

if __name__ == "__main__":
    main()
