import requests
from requests.auth import HTTPBasicAuth
import time
import json
import os
import pandas as pd
from dotenv import load_dotenv

load_dotenv()

df = pd.read_csv('../dataFiles/Current_Semester_Report', sep='\s*\t\s*', header=0, engine="python", dtype={"Course Nbr": object, 'Unique': object})

newCols = {"Dept-Abbr": "Dept", "Dept-Name": "DeptName", "Course Nbr": "CourseNum", "Crs Desc": "Desc"}
df = df.rename(columns=newCols)

def getBookInfo(unique):
    query = unique
    #query = "Computer Science Software Engineering"
    headers = {'Accept': 'application/json'}
    auth = HTTPBasicAuth('API_KEY', os.getenv('GOOGLEBOOKSKEY2'))
    params = {"q": query}

    response = requests.get('https://www.googleapis.com/books/v1/volumes', headers=headers, auth=auth, params=params, timeout=100)

    jsonResp = json.loads(response.content)
    #print(json.dumps(jsonResp['items'][0], indent=4, sort_keys=True))
    try:
        bookInfo = jsonResp['items'][0]

        bookResponse = {}

        try: bookResponse['ISBN'] = bookInfo['volumeInfo'].get('industryIdentifiers')[0] or {"identifier": "0", "type": "No Type"}
        except: bookResponse['ISBN'] = {"identifier": "0", "type": "No Type"}
        try: bookResponse['title'] = bookInfo['volumeInfo'].get('title') or "No Title"
        except: bookResponse['title'] = "No Title"
        try: bookResponse['subtitle'] = bookInfo['volumeInfo'].get('subtitle') or "No Subtitle"
        except: bookResponse['subtitle'] = "No Subtitle"
        try: bookResponse['authors'] = bookInfo['volumeInfo'].get('authors') or ["No Author"]
        except: bookResponse['authors'] = ["No Author"]
        try: bookResponse['thumbnail'] = bookInfo['volumeInfo'].get('imageLinks').get('thumbnail') or "No Thumbnail"
        except: bookResponse['thumbnail'] = "No Thumbnail"
        try:
            if bookInfo.get('saleInfo'):
                bookResponse['listPrice'] = bookInfo['saleInfo'].get('listPrice') or {"amount":"None","currencyCode":"None"}
                bookResponse['retailPrice'] = bookInfo['saleInfo'].get('retailPrice') or {"amount":"None","currencyCode":"None"}
            else:
                bookResponse['listPrice'] = {"amount":"None","currencyCode":"None"}
                bookResponse['retailPrice'] = {"amount":"None","currencyCode":"None"}
        except: 
             bookResponse['listPrice'] = {"amount":"None","currencyCode":"None"}
             bookResponse['retailPrice'] = {"amount":"None","currencyCode":"None"}

        if bookResponse['listPrice'] == {'None'}:
            bookResponse['listPrice'] = {"amount":"None","currencyCode":"None"}
        if bookResponse['retailPrice'] == {'None'}:
            bookResponse['retailPrice'] = {"amount":"None","currencyCode":"None"}
        return bookResponse
    except: 
        return {}
        print('no books info')

#print(json.dumps(getBookInfo("Computer Science Software Engineering"), indent=4, sort_keys=True))