"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _core = require("@material-ui/core");

var _react = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

require("./fonts.css");

var _styles = require("@material-ui/core/styles");

var _DepartmentInstancePage = _interopRequireDefault(require("./DepartmentInstancePage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function createData(major, college_img_url, minors, courses, professors, students, college) {
  return {
    major,
    college_img_url,
    minors,
    courses,
    professors,
    students,
    college
  };
}

function DepartmentChosen() {
  //add specific formatting to the url and overall page
  (0, _react.useEffect)(() => {
    document.body.style.background = 'white';
    document.title = "Departments";
  }); //route to a specific department or the general model page

  return /*#__PURE__*/_react.default.createElement(_reactRouterDom.Switch, null, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/Departments/:departmentName",
    component: _DepartmentInstancePage.default
  }), /*#__PURE__*/_react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/Departments",
    component: DepartmentModelPage
  }));
}

function DepartmentModelPage() {
  const titleStyle = {
    fontFamily: "Bungee Shade",
    fontSize: 70,
    display: "flex",
    justifyContent: "center",
    marginTop: "2%",
    marginBottom: "3%",
    color: "#bf5700"
  };
  const textStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 18,
    display: "flex",
    justifyContent: "center",
    marginTop: "1%",
    marginBottom: "1%",
    color: "#bf5700"
  };
  const useStyle = (0, _styles.makeStyles)(() => ({
    button: {
      width: "120px",
      variant: "contained",
      background: "#bf5700",
      color: "white"
    },
    card: {
      variant: "outlined",
      width: 400,
      height: 525,
      borderRadius: 15,
      marginTop: "3%",
      marginBottom: "3%",
      background: "white",
      elevation: 3
    }
  }));
  const classes = useStyle();
  const buttons = ["Professors", "College", "Students", "Minors", "Courses"];
  const department = [createData("Astronomy", "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBQUFBcUFBUYFxcXFxoYFxsXGhcXGxcXGBcYGBccGhsbICwkHB0pIBwYJTYlKS4wMzMzGiI5PjkxPSwyMzABCwsLEA4QHRISHjIpICkwMjI4NDQ0MjI1MjIzMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/AABEIAK4BIgMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAADAQIEBQYAB//EAEAQAAIBAwMCBAUBBwMBBgcAAAECEQADIQQSMQVBEyJRYQYycYGRsRQjQqHB0fBSYuFyFTNTgrLxBxaSk8LS4v/EABoBAAIDAQEAAAAAAAAAAAAAAAECAAMEBQb/xAAsEQACAgEEAQMCBgMBAAAAAAAAAQIRAwQSITFBE1FhIqEycYGRscEFQtEU/9oADAMBAAIRAxEAPwAirTwtcBTwK6RzxQKcBXAUsVAHRSxSgU4CoAaop22nRTopkiDYrgKdFO20SDIrop+2kioBsbFJFOIpQtMhbGRXRRClJtoiNgSKQiikU0iigMERTXNOd+AOT3gkVlurfEp3eDox4tw83B5gvAlezcjPArHn1Si9seX9kbMGklP6pcR+7Nb+yt4a3MQZ3R/DDEA/T3oLLUY9R1GjSzdYNcsG2Ld5QJKuGaXHqc5HeOatLlu26LessHtuJBXgT/kQciqdLrHJ7cn6P+mXarRKK3Y+vK/shba7bRYrorpnL7BFaQiilaQrUDYKK6nmuIpaCmMriKeBSGiSwZFIVohWkqABlabFEIrooBAlaTbRStNIqETBxXU6uqDEpaeBSKKcBVJccBT1FIBRFFQh0UoFLSgVAHBacFpyilAprIJFKBRAtdtog5BRXRRIrgtFIRjYpKLSFadIVsFTCKKVoVxwPr2HrQk4xVvokU5Ol2NfAntULqGtt2kL3G2IPy3sB3Pt+lCvdStm6LG4NcYMSoAYIApJL+nYepnsKpvifTaVWt3dQ5OxGCoOXMgjaswAMjGOJOK5mfVub2w4Xv5Z1NPo1Bb58v28ItOm64ai2t0BkVmbaDHCsQGb14mMjP3rLajqqI729Ba3PcLM7xMbjMLPIE4PA9DWr6HqvEsW7mwWwSxC4hVDGP5AVltR1hLbNb0KB7jOSz8qC5khfXP2x3rDDt8G+T4Rqr+q1OnSy5tm7YNoJft4J3gszuv+6CPr9pFh8LdOto9y7prm/SX0BVJkJcB88qR5TBiMcQRgVBv9Q1OmSy5tm7YNoLeSJYOCxZl7bvY8+0SLD4Z0FsXX1OluBtPeQHaDEXA0ncnZgIHrj80StRZbF26JfUNH4fmXK/8Ap/4qMbR2q5GG/kQSIP4/nWiuJIIiZFVFlJ0v0b8Z/wD6rZpdfklKMZdXTMOq0GNRlKPfZBim7aLFJFd44AErSRRyKaVqEBRSEUXbTStQZMYaGVoxWkIoBBRSRRSKaRUICimkUUimxQY1A4pKJFdS2wkgClFIKetVl49RTqQClAqAYoFPApBT4qAFVafFItEAoogkV0U6K4CmFbGxSxThS04rYMikNFpjCohWQ9VqgikkgACSxIAH3NV3Ttfbvr4iGUDspMQX2gSQScDJ/HpyD4mOn8I/tLwhcMAOWKtO1RyZ4P17cgnR9Ta/Z1d7S27e1vI8ABASo3yIyIJ+veuHqc0st31fCO5psEcaTXbXJVdD/ZE1TpYG94d3eSyqCwhAx55yR+tN+I20lu4t66N9zYFW2oBZiCSGI7CI5/nxRvhjV2GuXE09uLaruLkbd7lohR6c/wBqD8Q3NHaveLcAuXSqgW0ALEqMM3piOfTg1R/t5L3+Eu+k6kvZt3HUJuBYr2UeaASfaJNZO/1izYYpo08S4WYl+UUuSTBxInGIHua2PTbxuWrbsm3eklRmJB8vv6Vl9T1DSaQ7bSi7dGAqRCnjLRC/aTUh21QZPhM0Vzq93TpZNy2bunuWgLoC+ZXli7LgBsASvPER3nfD3Sra3zqdLcDWLtvIUmPEBwSs4IGPUZkdyEddFq3a8e3Nm7b3O6yxt3JMyvOyIyMj3mpfSOki1qRf09ydPdQkhGG1mjyEgYJiQGH0NVSrbXX8Msj37mliq/pC/u2Ho7D+QqyIqu6OIFwelw/zn+1Zl5L2RNVoyCSuVwY7gGePUY/n+IZWrPrS3wgbTwbiHeVbi4qyCh9J3TPYgVXW9Ql1FuIjW90hkcFSjqYYe4nuMV3v8dq5TqEv3/pnB/yGkjC5x4Xlf8GRSEUTbSkV1mchASK6KJtpsUAgyKaRRYpCKDHTAkUhFGIpu2gEGRTdtF20hWoMA2muo+2uocEEWnrTFp61UXIIKIopi09agbFinqKQCngVBRVFEApgFPAqEFrgK4inKKdCMTbXRTq6nQrGU1qe1Mc4NSTqLYIq5JGY+JtVbti2Lls3XLFraAGCwUiWbgATPf6VYJqvDsb9RtQ+H+85KqTyBEkx7TxVd8QdRdHtpasm5d2syM07EmFJwZY8CMc1ZazVJbsE3jgoEfBySvmgKPr9K81LpHpUqbKr4Z11u6Li2rRt27YQKzCC5O7MdhA/n24qL106Oxfe9c81xgpFtQCxKryQPX1MferToGtF1LhW34SKwCAxLDbJYgcfzqr6ppdFp7t29eZSzsGVIG75QCAoO4y2ZwM5po/iYH+E0GjYXLVtmXaLlsEpzAZMr29Y4rMNp9HokHikFxkIBJOP9P8AUxWqQC5bWJQPbxGCoZRERwRPb0rJdO+Fktr4mqcAxucluD3Jdv6fmlg6uwyTpcGutda0wSzZvg2xdTdbdhCKWY+QvEK2O+DH2L+k9GuaXVjw3P7NdDsyggoHgbTB+UnORgx9qevTtNqbSWWZd5tlkByWtywB2n5lzyM5zFB6LpdTo79uxv3adwwUN5thAJAR+QOPI3A4qttbWk/0Y6bs11V/Tf8AvLw/3D9WqxAqu0WL10eoB/T+9Z0XWJ1vS+Jb2C4bTSCjqYKurKyx9SIjvMd6rNM91ki/bCXUuMrFY23MKwcDlZnI4q26xZt3LTrdgWyrByeAu0kk/gGqrRae7btm3cuC7bUqbVzlihDYLfxxjPOck81v0Eksi/Mw66O7FL8hdtdtp8UhFekPMpAytN20aKaRUCDimlaLFJFBjgSlJFGIpu2hYyBlaQLRts12yKRyGUQUV1SPDrqTcW7GQFp60xaepoEQQURaEDT1NRBJCilFMBolEDFAp60xTRFNQA6urq6mTFaOrq6up0JIQmhPxRGoVyq9RLbib+GPp1eVL5Rmeqa2+b3hWLYDKqzcaGKoxJ8ij3ByfxVp1bWW7dtvFMKxAIySY9AKrrmt1FzUG3bQIlu4q3H+Zio2sY/hUFT3zmp3WtXYt29t9wFJDAEncxXgKo8x+1ef9lR6L3djOj6wXbZZbfhqH2qDyRAMkdueJPHNU2q6Hprdy5fvsArOzSxIxPEt5ifZf51edI1SXbSvbQopYwGABw22YH0rOL8MtcuNcvuWO9oLnedm7H+0Y/8AapF/U+aA1wai6p2EWyAQh2SJAgDbInI4rH2+hXtSQ+pdn7hX8qr9EH6xn1rYaq4baMwUtsRjtWJMRx2rFXOsazUwLS+EjZG2Xcj/AKogfj70YXzRJ1STNzd+H1v2bWYe0IRlJRljujg4PNJ0fq+pt310mpXxCfkuYV1gT514b03LHPFRrg1du3YuWHYbbe24HG+2xBE7x8wb/cCPqan9M+ILV65bt37XhXwf3cjcjGM+HcA7/wCkwaqadPi1/A6q1RqKrbGNQ/un/wCtWRqt41I90/oT/Ss6LSVrFBQgiR3HMgkAiO+Caz/SdEltGaxdNzSuqtbWdwttu8+3uvaV9e0mtJeOD7Z/BBqg6dpLLPcu6Z/KQ6XkBI/eCI3oeGGcnOBkitGmltmn8r+TPqI7oNfDCxXEU4CuAr1Z5VcjIppWjbaQihY9A9tNiixXRQbCkC20kUaK7ZNVtl0Ygxbnij2LW7y965bZ9hVpoNPuIDbSfUHzCqMmTauTXhxWyB+yP6V1a3wR/gFJWT/1mr04fJ5mDTgaalKBW6zm0FBp6GhKaeDQCSFNPBoCmiqaKFYQGiA0EGnBqNgDBqTdTAaWaZMVsITTVppNdViK2KTQ7polA1FzaCYJgEwIkwCYEkCaza2VYmvyNOhjeZfFme0fUdRevgLbVLSswc/OzFQRBI8qZHufep/W/B8NTeZUVG3SxC5ggDOe/bNQei9VuX7ki2LdraSCTudiSImMJyTGakda6TavNbdwsouSQJzBGTxH0rhtVJXwd27TJWgdHsq1n5WBKEgwctBjnn+VY/8AYNZqz++dgpJ8g/dpAP8Ap+Zx3zPNbBStu0PDBdUQlQpBLACcEwD/ACrKr13U3WXw0W2jEGT+8crOcRAx9aaN22hZVSs12pdFDFyqqFMliFABxkk45rMXPiTTWv3enQ3CP4bShUEerRED1zWk1ttbiXLZ4ZCuc88cfas22msaeQwLHb6bV4nAHHHM0sK/2DOT8GpsdfNi3aNyy7W3Qs7W4c22JEgpgsuTkZxxUzTWNJqWTUWLiko4J8MiJjh05Ro7YNY3WdfB06BUAdGMNugwCWAC9x6z7cmpPQ9Gbr29RbgMjDxCh2NsHIO0jeuRjP8AKq2lV9BjO3R6PNV94xqLfuP6MKBqPiDT23ZLj7WUxBHJILDj2H8xWf1vxbbN1WRCVUja0wXO6CNpEqeeR78EGqYxZc5xXk2t7Kn6H9KoLGk09zUG7aeLtosl1ASrEGR5hyUJyJlT24q3satXTeCIiTxAxJn6f0rz3qnWbLXUuW1IdArFwx8xx8rLB2kQT2OQRzVmLhleVqja7aWKpNB8SW7jKhgXGMss/KpEqTHBzwf5VeqwPBFenhkUlaPMyxuLoaRSRT4pDT7gJDYpKdSGlsZIQCiW2A5oRNNmklyWwltdkxXTuKkWdURkeVR2GCfvVYopyPOKqcEa4ZuejQjVN/t/JrqovHNdWT0l7o0737FHprRY4GfSjvpGX5gR3qw+GnRXO9oxjiPyc1a/EGqt7IUgn2j9YNGWeayVXBTHFHbbMqaQGudhPB/IppIramY5qmFVqcHAEnAHP0qM7kAkCT2HGe0nsKgXfN83759rMttcW5UgEEwZaSBJk+wqrLmWP8yzFglkfHROOvZ8WQD/AL2kJHqo5f7Y96lJ064IPi+IQOLiLtn3CkVDTTFrircucP4iIpIEW1/ijnzZz6LEVG6smjuXm36k2rqAISr+GR5hcGeCSO4PE8VzMueeR1dL4Olh08IK6t/JbE3VaDaJEfPbZefTY5x+fvXPq1X522f9alQPq6ytRP2bUiyi2NQrsC777vnV1afDWRuhR5cjkD3p+k1OshhqLdsgBiDbJhyI2gqxOTn6UIaicFxIeenxTfMSaupUjBBHqpDD8qTTkcHg1Tvr7bOFuWntuYUEjuYAiDPeJjsakafVWwfDW9LDEO8tI/6s/itmHWyupow5tDGrg+S0Vqia7VLbRrjTtVSTALGI9BzR7e6Buie8SBPtNV3VNVaS23jMoRvKdxI3bgcCMkmDxVmunuhFLyyrQQ2zlfhEXonUzd3Kto2raqNm4oC0zwi4UY9e9RviLopv3F85KbRKs7lQY7IP8zUvpWtt3VY21Kqp2/J4c47AgH0yaq+qDWvce3bbaoiDbTOZiXcmPqIrmpfVxwdRv6fcvdyWrYDMqolsKSYVQoAXvxVJp+uaZXS1ZBcFlQeEhCLuIAJaACBPvVvrtOLlt7ZOGTbPPOJ96haDo9u3tgMdsEEkDK8YA/rQjVNsk7tJB+s6h0tubchts7oDBYySR9PrWF1OqZ2/es8x/FPH+307H09q3uu1CW0LOCV8ogKXJJPlAABPMV5/1tkJbw0liYIMyOTkEzPrP4FCKvgTINLoQADuxAkbfWYP4wT24o+m6vcth7dtoD4JwHAxw3pGDFZ53KAD1H0mo76g4zVqx2VKVGnGoZ2V7kseWyqzBGZIMyJ7fion7QLZ3Ad5Bkzz3IH0zVTcvOYIxE4H9v60M6loEk/fM/5xTemDcejdF6rut3fF1JQFQQkRvlYg+WIj74GazF5txIUgKuF8SVaIgHjjHP8AzVNa1Ejbj6n7d+3A/FJc1pjaZMdyew+n61I4qGc7VFnptb4ZZmYMSPKJbBHE4HbcIHOPSK9B+A7yujuLj3G8slwBt+bA7k+piDtBrzvS9J8Sw15JdlYLg8MWChSDEEg7gcjtzWn/APh91G8twWnQi20kNtIyAAB6ASO3cn3rXi4krMmVXF0ek0k100hNbmzDRxNdSbqaTQsNCtSUhNIWqDI5mioWu6lbtqdzEE8HaT+go2o1C213OYA71518WddD3PDtXDdGAcCAwP8ACAPQ8/WsmqlLbS8mzSRi5W/Ban4ujHitjHHp966sbt1QwC0DiA8R2jFJXN9CPv8Ac6fqS9vseoq8U43T69oNCrq7W04fqSofupS1DrqIjdhfDVwQy7l9CAcniQfvTGfaJYi2BD7UktgFrgJHzeY9h6+tIWAXL7ZOD3O3MDvwHH0NCQj+C3MkHc2JW55ngc4EY+lcjUz3ZGdnTQ2Y1+5M6eApO1NoUYY8kv5mxHrz7/Sq3U9Q3GNR092BJAZVS4CCdsk4iQZOcA1Yolzw2G4C4xfaYwnOyVjMeWfXNRtMmtV1Fy5ae3I3naVfbtzAECd34FZlVu/5aNPNEXq17QeItu67W3tKqKVNxQqwHABUQP7VI0doC2/g6xn3Miq9x1u+GVMsonuR2PFA1PUNSGYNohcSTtK3EJK7vLKkHtBoOuuacW0W7pXCOXcoqfI4Gzc4SIJDf5FNXS/uwNolWm1iuBdezct92UOrYGIBO3kU3p1x2cC5pfCYCd4a26yYHzL3Mn/6TVRpG0gW41p2tgKVc7nATxOCN2Nwj3irDoplvLqPGBIAHlO0liRLDOQQM+lWUitvyaXd39Kq9dpUuoVuE7QysYIGVbcJJBxirhtOArSwwpODP5A/vWb6x01L/hlyAEJwV3SI4jjmOav1OVTcVHpGXTYnDc32ydohbVP3ZVhJkqwaSMHM84qrv9cbeEWy079pa46KBkZCruJORgxWh+G+jKUKgwqjBgCfsDVXqlsJfKN4RubpEkM5bEEAkkHjgCsMMsZTaXLRraaSE6w1zw28IsHxG0BiRORBB5HtVRp+l3PFt3H3nbBPiOzZzJCk45jgVYdT6illNzhyDgbBJwKp9B8SrcvJaW2wDz5mIEQhbgDPHr3q6KdOhZdl31Zh4Vzd8u0TGe9eb6u4ck/yM1veualktsVaMwcTgg+oxmM15rqrzMSfTHbtz9qOONsrydiXW3iWOdsfT0qMpA/5/wDakZ6aQa1JUUsebhmmlpporqIAivFOVJn1+3fjmhClCk8VCE6zqXthkDCNwY5kPsnbHIPJradG+NWVDvt7nBywgSgzkzDMJ4j1rEWdIzHa3lIDRvO2SoOATyZgUXTWXDrbBkMYiYEmRmJ/MelNGTXQk4prk9r6X1JL9tbikZzAIMexjvUuap+h6RrNva7TOQMeUcxjnnmrHea2RfHJhlV8Bi1JuqJc1SL8zqv/AFED9ajp1O2z+GtxCYnDKfbtU3Im1+xYs9NL0HdQjfG8JPmKlgIPAMHPH2qN0RJvodrnPhtDbcczGMzntXjfVbyi8zBSBMgZBGTHPaI/Nbz46usLQAuKizJGd7YMbQCJEwPY15zuuXWLGWYCSTkwsT9cfpWXI7dG3D9MSd/2ge1sx2878du9dVfPvXVT6aNXqnsddXV1dM451dXUxz/n0zSSlti2PCO6SR11yCAqCQANx9d21oH/AEkn7j1oNy4Y89zaPOuCF+fCZkZAj7mot1wT57v2BAw4CAED3BImcn2pNM1rcAoJLd4/8KFyTBx9K4j55Z3F7EjqIsi2tq5c8McrDBD5MmCR2GTQ9BaRWa5b1Luuwrt3q6KWMhoBieYqJ1TqemFzZetlyBglAwAfEA+45oTajRiwWgLauttbDgsU+mcbeR2mgo8eQuSvwSRp9YsbNXuAHD20Odscie+aLrNVqww8IWmAQBt+8MXht0RgDK/zqm0mm0DOvhMdxaQA7+YjzcMM/wBqLqTbuO7DVOrSZVLiiAoAPlkRG39aauevsC+O/uS7upvG2TcsK7bgNisCGWJLEt74ipvQiCwItC0d+4rhZ2QZxzgRVVe01w2bbpqHC72HiFVuB2OdskxIFWHSyVXzt4jBGloCZOFIXgdhTPjkrU1J0ndF9qNSSCNwyvHcz5Z5wJQ/c1lut3NQXK27vhqABELk9zugsKuWvGCSu3P1MAs0H7mqjqpZfMVaM9jS3ymGnTLTQNcWyq7yXNsBjJO4wCTn6mqS1oE8dGZTuDE84nJOI9at9NqRuZIINsKCWwDIPB9RGfqKjXmh90cbv0NIkk2/I7tpWx3VrYC/cEzn5kUk5qssXktsGMACZgAdoHH+YqZr9QXRScE27ZIBmD4a4n81mH1JkptkAETJGe3FPESRY6/r9u5bZQDMiO4j+/8AesZqLhLMT3/lVjdSFDE424An7Z4nINVvzTifpV2NJCTvyAU07eZmn3rJU5H+TFCUVbZTQ5kyc9pn6iabFcwjH/FK1sgAkGDMHsY5iiAVB2iafeuKflWI9O/4oNT7WguHAQnEgj1gf3FRugoDpw7soEsZwDJq0sJeRlYOisogFtpIiT/EPUE/moyW7lhlZTDAnKngqcf0NWvT9G14s9wqM7mJE7twnB9TmqpTp/BcoRcfN/0Srl/WbkS7qmXxENxNhksvmg4G3lWESDjinanVhLoAu3NwClZlz5hg7t3ofyK74g1Nq2Lf70X3CbWXYV8EAyqSwg5ZzA4zVSvWlUgqmR6lREHAEKZFRSlJWTGoRlyuPJe9T0Atnzv52MlYUOC3mG8FsSM0DSvaQkl3DCNjKMBpyTgyIAxIqA3Wxd+eN8AKcmCMDcSBiPSmXLzzG+2Cex3Hn/zVWoyXbpl05Y03tXBsLXWbjCN4nbJO1YEAEkkmIyO1Wdo3C9p3KtEyV52sIOPQH0rPaBw6rKwMCQSYGQ31Bx+TW/8Ah+zZCruDYb0MbiocrMYbbmJo+pKL7bF9FSi2kv2KD4m6Wblp1S2GeAEn03AnuBWd6R8Oummvo6kPcGAInCmBJHEntXs/V7Vk2pWP4Y2xkRA+0Ad+wrOHoz7PEUeTJ/Q8AnPNa1JPlmCSceF0eDHSOMG08jB8rcj/AMtdXsuz2rqgd3wU11/PbUHA3G4eeFgKTPJJB7/LQrOouOrFJB3lV3Jc5JUKYB+QSZbHb0zk9PY1cFfETmNxeM+5Ig/erXQ/D3Umg271rBnN4Z+oODWaWeMO5JfqXrFauuPyLdOrW0TdefZPygq6sYGTtYSMho9ooWm6wl5GcBtimMjJI83H4/NVHxD0bqNwq197T+GNqhLlnyiZgAETn1qVoHWzbW2GWQTuz3809/ZaHrb4upX+oY4dsk6J63FXC22MT2/8PzD+ZIHuTU1NXuAXwhb2KJYTuuFvMZn0iKqk1rHvak7Jz/8Ac/i7dven3dfstsXe2G820Tg5O2c/SapcbL01Y271W+hI/ZbjAFgCrcgHBiO80/XdQKbJsXH3LuYIu/YScg45/tVe3U9QGEtpwvkLS2RgboG7sZiad1Dq9y224eF4RjazEywgEkAHOSeKbbykkiblXYfSa1GZj+zshVWbc1tVMDBgjM/0qmudV0u6DpwAfmJUKxbPMc8n8mjD4hLW3LMiNICRuO6MyfT0ql1OpLh2Z7UkbjGC3tjlvb3z3qyMWVTkvBdt1PTXEW3c3bBkJLlAfWFPPvWg6bdtqNqEeVVhQcBQR/aPvXmJIAI8rEgQfNjIOMj3GR3PsasejXxafcWK7gMhlGAchgQZnEccfhnj+RFNLhI32pvHZtzLXNrc5zsP6VE/bXt71VzF0BTJJ2g7cocbD5Rn6+tVXT+qreuW7YaC9w7d8gLORLH3n8/apfxr0u9pIW4yMpgh0MyI9OR96olxJRfb6HtNUwH/AM0W0uOp3MrNO4ZyeZB5HvUzXdVC2Jt7Y27kDAEETBhSMfrisE5k8/bjmpV3qTNb8LaIUQCMEEGcR9/rNXemvAu+Rd9Q64AQhA3ALMLAEjdA9sjj0qp1GqlgQCO59+8A0PrFom+4APzRgHMAD+lXNzoqJoU1Acl3um3sKwF2K0mQSDMrx70Goxr5GW5vkpdb/pg4nH1OPr3qPYRgwkEDPHrGOKl2cMWbJAjPc9op9jR3bgaFM7lWAD8zGFgev/NMmkqGlG3ZX3mJmZxzz2oKNWz6B0azctXjfZ1chVtgB4U7kLlyuDjAB+tZfUaBkP3+/r+lSOSLbj7FUou7ITHNFmQoJx/Wm3EIJHcelJbtljtHNWi+S00fS7s7gogZyJGIP9RWw1Wq8Q+J4aW2aQQiwBC7RBI3dpM/8APRG229szB//EUK7ewf+v8AU5/WsjlufJcoV0U/VGthlEAGZJyOV9YkivSfhW/ozpGRkm8wJVmCgtJ8sEDy5M5APmB715b1ixdtspuCBcG9DA8yyQsEe3r60/pfUTbM5mGzzJ5H0+UUZwbjaFbLj4w6DdsHxLibA8ERtAyO5AnAX65rH+HPBmt51jr1zWIguOCFwDMSVdgZ7nyx7TNU/WrVjT3w2mueLbADAuoBLQCykex+nNNCVcCtGYuWypg03dWo+LOtDW3EdLCWyLYkW17KSCxjjg/TFZYir021yIy26f1S8NqK8RO2e/cLPuQP8NazQdbfbbvbl2lmtuoZZ3bTBCTMbWIDH6etefKYyKtrOguXQbqbYVS7jcqlYMMYJz2OJOaryY1Ls0YM0oOl5PQF63f27k3EQDEA+XggA1a6f42vrbKPx5hLABgs7R8p49PtWO6J1Nbdu6j2wwdXAHIQmCIP2x9azQ1RW6CDids+3FU45SbcX0i7PjxqKad2b8/ENz/TbPv4hzS1m7Wu0+0br9wNA3AWVIDRmDOc11WbpGTZH2LF7dzYbhtWwAJnxLY5XfwGndt4HNA0PWGZiBvUAZ2u4/rQ9CjeY7pBdoDZAj2NGs2CoZvL5ixiAeD7iq249NFiseuta5eW2AzDbudmdvKBzjuJjv3qNrfDWFaPQcnv6ipXR9OzC5cJWSSuBGPKaHqulszgyvlyPtn0pOE6Lq4KrUrbZHCORcD7QAhKkcMS849vKZ9apdJoTcuLbJC7n2ljwM8mr4dKKAkOZE4xHH0oKdOZTu3AmCe/oatU9vBU8bbsi6lFbeNssbrMrhm/7uCAoTiDgzzgUN9NdvYQFvDUeUEYHAgf53qxtaAmCzSJEjPHel1KIQ2xdpOJmOZB49poLJzyB4zLkGpmt1T33Ny825oCzCqYQBVEAAcD0ov/AGcdwEjn3/tSr0/aZeDJ7f1kVdvQnpsrHGcUpJNaS3obUNBMqjMQbaESFnBYk/pQtJpkYbkdhAkgoojIGIbPJ9OBQ9RUH0nZV3djeGEUrCgOSSd7yZIHYRAomt6heuqEdmcKccmMbf0A/A9Ku7NuAWBkKAYOAeew+nrU421t2t8BoUEhgSO4wN3tSPMr6H9BvyZCzobrnCOR6hGI49hUvpmnK3kR0JcsqhDAO5mgYJyePzWqa5a2YVxPdSqmcCflNRtMYuWzucsLqmZWDLKROO1J6+61RZHDtaaYX4kQLqfEdNihoItkGWAiZ7yQT+ag6/qDXLaWwWKozEKR8oPoZMnJ9PvVl1m2XuFgY3MWjICmeBHbNRP2FhyR+WP9qpjVL4Gm3J2ytt2g3IxU5dU6BjbIU+UrAO4Mu6CpBG055zzUpdH7/wDq/vQb3SkKlt7j6H/nipGS3clbTA9P6uyk3Hcu5YHe5LcgYJ5MAH6R+a3Xa4EQFnyxPBH0xmpdnpyqpZxuBzgwfwQfU96sbXRrBVW2nzKDknuO+au+ndYFGXRlunrbJY3DjET3z/arFLNmJBUMIjsuBJIxg1DsaPaZYKwMwJOIMelGtWCWLE+UR5cn1iJ44q2fPkSKrwWelv2wpDOPmBEHmPpRGfdvKgxMyRH+mKrhcPbFPs3GAPmwecCqUhydfPibQQW2YWYxIjGfQVX6XpxNyTgSeZGDiBjJzUlXb1ottWPMdvWmjcemK1YDTrftr4dplLIxkFUeSBbYQXUwMjApvU/2p4LpaaVK+RbSxJBnAENj5h2JHc1YC2R6ZPvQmJ7QPpin3LsG1lENHdCAbf8AUCJHHIPPqTUa8rE7nBHY7RGff+9aUg+v8zQtn+SaZTFcH0U2kso3zoVBkbxuLboMeWc5jIH2o+nsPEt5QGnORhQPwRj81eaK0HLA8KMc5zGc0zqGlITkGT7jjI7mKWWVdFkcdckG2F2rggG5Pl5xghvv78VJtdOsbTufbvZoUCYZcoBiQsMeD2zOKj6fTvkSMZGTg+vFODm5GFH88expb9ixNRb4s59Kkny9z/p/vXUTaPVvzXVLZX+h/9k=", 0, 72, 35, 159, "Natural Sciences"), createData("Computer Science", "https://www.cs.utexas.edu/sites/default/files/styles/1200_wide/public/legacy_files/news/images/teasers/CapCampThumbnail_0.jpg?itok=Z64Fu52d", 3, 104, 117, 2006, "Natural Sciences"), createData("Physics", "https://lh5.googleusercontent.com/p/AF1QipM4p2_UaZ2BBBiVUBmYslbTYIjWWi_STFIDcmNl=w408-h544-k-no", 2, 120, 61, 614, "Natural Sciences")];
  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("h2", {
    style: titleStyle
  }, "Departments"), /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      display: "flex",
      justifyContent: "space-around",
      flexWrap: "wrap"
    }
  }, buttons.map(row => /*#__PURE__*/_react.default.createElement(_core.Button, {
    className: classes.button
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, null, /*#__PURE__*/_react.default.createElement("div", null, row))))), /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      display: "flex",
      justifyContent: "space-around",
      flexWrap: "wrap"
    }
  }, department.map(row => /*#__PURE__*/_react.default.createElement(_core.Card, {
    className: classes.card
  }, /*#__PURE__*/_react.default.createElement(_core.CardActionArea, {
    component: _reactRouterDom.Link,
    to: {
      pathname: `/Departments/${row.major}`
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: textStyle
  }, /*#__PURE__*/_react.default.createElement("div", null, row.major)), /*#__PURE__*/_react.default.createElement(_core.CardMedia, {
    component: "img",
    height: "300px",
    width: "100px",
    image: row.college_img_url
  }), /*#__PURE__*/_react.default.createElement(_core.CardContent, null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    align: "center",
    style: textStyle
  }, "College: ", row.college), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    align: "center",
    style: textStyle
  }, "Courses: ", row.courses), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    align: "center",
    style: textStyle
  }, "Number of Minors/Certificates: ", row.minors), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    align: "center",
    style: textStyle
  }, "Professors: ", row.professors), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    align: "center",
    style: textStyle
  }, "Number of Students:", row.students)))))));
}

var _default = DepartmentChosen;
exports.default = _default;