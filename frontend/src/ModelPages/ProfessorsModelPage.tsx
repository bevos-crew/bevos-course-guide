import {
    Card,
    Box,
    CardActionArea,
    Typography,
    CardMedia,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Route, Switch, Link } from "react-router-dom";
import "../fonts.css";
import Pagination from "../Components/Pagination"
import {
    styled,
    ThemeProvider,
} from "@material-ui/core/styles";
import ProfessorInstancePage from "../InstancePages/ProfessorInstancePage";
import axios from "axios";
import SearchBar from "material-ui-search-bar";
import { IDepartments, IProfessorCard, IProfessors } from "../Components/Interfaces"
import { titleStyle, theme, useStyle } from '../Components/Style'
import { getHighlightedText, RenderMenu, RenderFiltering } from "../Components/Repeats";


//create interface of a single professor from parameters
function createProfessor(
    name: string,
    picture: string,
    department: string,
    email: string,
    courses: number,
    publications: number,
): IProfessorCard {
    return {
        name,
        picture,
        department,
        email,
        courses,
        publications
    };
}

type Order = 'asc' | 'desc';


//Routing between model page and specific professor instance pages
function ProfessorsModelPage() {
    return (
        <Switch>
            <Route
                exact
                path="/Professors/:professorName"
                component={ProfessorInstancePage}
            />
            <Route exact path="/Professors" component={ProfessorCardsGrid} />
        </Switch>
    );
}

//card organization for professors
function ProfessorCardsGrid() {
    let errorText: string[] = [];
    let [page, setPage] = useState(0);
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [count, setCount] = useState(10);
    const [professors, setProfessors] = React.useState([]);
    const [departments, setDepartments] = React.useState([]);
    const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);
    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof IProfessorCard>('name');
    const [search, setSearch]: [string, (search: string) => void] = useState("");
    const [query, setQuery]: [string, (query: string) => void] = useState<string>("");
    const [deptFilter, setDeptFilter]: [string, (deptFilter: string) => void] = useState<string>("");

    const classes = useStyle();

    const Div = styled('div')(({ theme }) => ({
        ...theme.typography.button,
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(1),
        position: "relative",
        variant: "h3",
        textOverflow: 'ellipsis',
        overflow: 'hidden',
    }));

    //set what the cards will be sorted by
    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof IProfessorCard,
    ) => {
        setOrderBy(property);
    };

    //decision for ascending or descending in order
    const setOrderSorting = (
        ascending: number,
    ) => {
        setOrder(ascending === 0 ? 'asc' : 'desc');
    }

    //set filter for specifying professor's departments
    const handleRequestFilter = (
        event: React.MouseEvent<unknown>,
        property: string,
    ) => {
        setDeptFilter(deptFilter === property ? "" : property);
    };

    //get professor data from api endpoints
    const getProfessorsInfo = async () => {
        let professors = [];
        await axios.get<IProfessors>(`https://api.bevoscourseguide.me/api/allprofessors?sort_by=${orderBy}.${order}&page=${page + 1}&per_page=${itemsPerPage}&filter=${deptFilter}&query=${query}`, {
            headers: {
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                setCount(response.data.num_instances);
                response.data.professors.forEach(professor => {
                    if (professor.name !== "NaN") {
                        professors.push(createProfessor(professor.name, professor.picture, professor.department, professor.email, professor.courses.length, professor["num pubs"]))
                    }
                });
            })
            .catch(ex => {
                const error =
                    ex.response.status === 404
                        ? "Resource Not found"
                        : "An unexpected error has occurred";
                errorText.push(error);
            });

        return {
            professors: professors,
        }
    }

    //call endpoint to collect name of all majors to plug into filter menu
    const getFilterInfo = async () => {
        let departments = [];
        console.log(orderBy);
        await axios.get<IDepartments>(`https://api.bevoscourseguide.me/api/alldepartments?sort_by=name&page=1&per_page=313&filter=`, {
            headers: {
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                response.data.departments.forEach(department => {
                    if (department.name !== "NaN") {
                        departments.push(department.name.replace("Department of ", ""))
                    }
                });
            })
            .catch(ex => {
                const error =
                    ex.response.status === 404
                        ? "Resource Not found"
                        : "An unexpected error has occurred";
                errorText.push(error);
            });

        return {
            departments: departments,
        }
    }


    //set background as white and fetch data and set it to be used on page
    useEffect(() => {
        document.body.style.background = 'white';
        document.title = "Professors";

        const fetchData = async () => {
            const ProfessorsInfo = await getProfessorsInfo();
            setProfessors(ProfessorsInfo.professors);
            setLoading(false);
            setOrder(order);
            setOrderBy(orderBy);
            const FilterInfo = await getFilterInfo();
            setDepartments(FilterInfo.departments);

        }

        setCount(count);
        setItemsPerPage(itemsPerPage);
        setPage(page);
        fetchData();
    }, [page, itemsPerPage, count, order, orderBy, query, deptFilter]);

    // Show loading until gitlab api calls are finished
    if (loading) {
        return (
            <>
                <Typography variant="h3" align="center">Loading...</Typography>
            </>
        );
    }

    //placing all of the data onto the page
    const buttons = [
        "Name",
        "Publications",
        "Email",
        "Courses"
    ]

    const alphabetical = [
        true,
        false,
        true,
        false
    ]

    return (
        <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
            <h2 style={titleStyle}>Professors</h2>
            <Box sx={{ display: "flex", justifyContent: "space-between", flexWrap: "wrap", width: "80%" }}>
                <SearchBar
                    value={search}
                    placeholder="Search Professors"
                    onChange={(newString) => {
                        setSearch(newString);
                    }}
                    onRequestSearch={() => {
                        setQuery(search);
                    }}
                    onCancelSearch={() => {
                        setQuery("");
                    }}
                />
                {buttons.map((row, index) => (
                    <div>
                        <RenderMenu button={row} alpha={alphabetical[index]} setOrderSorting={setOrderSorting} onRequestSort={handleRequestSort}></RenderMenu>
                    </div>
                ))}
                <RenderFiltering list={departments} onRequestFilter={handleRequestFilter} setOrderSorting={setOrderSorting} onRequestSort={handleRequestSort} category={"Department"}></RenderFiltering>
            </Box>
            {/* Professor Card Instance with filterable attributes and image */}
            <Box sx={{ display: "flex", justifyContent: "center", flexWrap: "wrap", maxWidth: 1400 }}>
                {professors.map((professor) => (
                    <Card className={classes.card} >
                        <CardActionArea component={Link} to={{ pathname: `/Professors/${professor.name}` }}>
                            <ThemeProvider theme={theme.typography}>
                                <Typography>
                                    <Div>{getHighlightedText(professor.name, query)}</Div>
                                </Typography>
                            </ThemeProvider>
                            <CardMedia
                                component="img"
                                height="200px"
                                width="100px"
                                image={professor.picture}
                                alt={professor.name}
                            />
                            <Typography style={{ wordWrap: 'break-word' }}>
                                <Div>department: <Link to={{ pathname: `/Departments/${professor.department}` }}>{getHighlightedText(professor.department, query)}</Link></Div>
                                <Div>publications: {getHighlightedText(professor.publications.toString(), query)}</Div>
                                <Div>classes: {getHighlightedText(professor.courses.toString(), query)}</Div>
                                <Div>email: {getHighlightedText(professor.email, query)} </Div>
                            </Typography>
                        </CardActionArea>
                    </Card>
                ))}
            </Box>
            <Box sx={{ flexGrow: 1 }} mr={20}>
                <Pagination page={page} setPage={setPage} itemsPerPage={itemsPerPage} setItemsPerPage={setItemsPerPage} count={count} pageOptions={[5, 10, 25, 50, 100]} />
            </Box>
        </Box>
    );
}

export default ProfessorsModelPage;

