"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.searchCourses = exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

require("./fonts.css");

var _reactRouterDom = require("react-router-dom");

var _core = require("@material-ui/core");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// array of course info
let data = [{
  name: "Software Engineering",
  courseNumber: "CS373",
  professors: "Glenn Downing",
  syllabus: "https://www.cs.utexas.edu/users/downing/cs373/",
  description: "Introduction to current knowledge, techniques, and theories in large software system design and development.",
  hours: "10am-11am, 11am-12pm",
  department: "Computer Science",
  college: "College of Natural Sciences",
  prerequisites: "Computer Science 429 or 429H with a grade of at least C-",
  img_url: "https://inteng-storage.s3.amazonaws.com/img/iea/nR6bkXZxwo/sizes/software-engineering-skills_resize_md.jpg"
}, {
  name: "Object Oriented Programming",
  courseNumber: "CS371G",
  professors: "Glenn Downing",
  syllabus: "https://www.cs.utexas.edu/users/downing/cs371p/",
  description: "Introduction to current knowledge, techniques, and theories in large software system design and development.",
  hours: "1pm-2pm",
  department: "Computer Science",
  college: "College of Natural Sciences",
  prerequisites: "Computer Science 429 or 429H with a grade of at least C-",
  img_url: "https://inteng-storage.s3.amazonaws.com/img/iea/nR6bkXZxwo/sizes/software-engineering-skills_resize_md.jpg"
}, {
  name: "Operating Systems",
  courseNumber: "CS439",
  professors: "Alison Norman",
  syllabus: "https://www.cs.utexas.edu/~ans/classes/cs439/index.html",
  description: "An introduction to computer systems software abstractions with an emphasis on the connection of these abstractions to underlying computer hardware. Key abstractions include threads, virtual memory, protection, and I/O. Requires writing of synchronized multithreaded programs and pieces of an operating system.",
  hours: "9am-11am, 11am-1pm",
  department: "Computer Science",
  college: "College of Natural Sciences",
  prerequisites: "Computer Science 429 or 429H with a grade of at least C-",
  img_url: "https://inteng-storage.s3.amazonaws.com/img/iea/nR6bkXZxwo/sizes/software-engineering-skills_resize_md.jpg"
}, {
  name: "Elements Software Engineer I",
  courseNumber: "CS330E",
  professors: "Fares Fraij",
  syllabus: "https://www.cs.utexas.edu/~fares/cs330ef20/list.html",
  description: "This is a course on software engineering using Python. It is strongly focused on using tools to improve the quality of software development, including automated builds with make, source control with git and GitLab, unit testing with unittest, code coverage with coverage, continuous integration with GitLab CI, automated documentation with pydoc, Docker.",
  hours: "9:30am-11am, 11am-12:30pm",
  department: "Computer Science",
  college: "College of Natural Sciences",
  prerequisites: "Computer Science 429 or 429H with a grade of at least C-",
  img_url: "https://inteng-storage.s3.amazonaws.com/img/iea/nR6bkXZxwo/sizes/software-engineering-skills_resize_md.jpg"
}, {
  name: "Algorithms and Complexity",
  courseNumber: "CS331",
  professors: "Fares Fraij",
  syllabus: "https://www.cs.utexas.edu/~fares/cs331s20/list.html",
  description: "An investigation of algorithmic paradigms: divide and conquer, dynamic programming, greedy algorithms, graph algorithms, randomized algorithms,undecidability, NP-completeness, and approximation algorithms.",
  hours: "11am-12:30am, 2pm-3:30pm",
  department: "Computer Science",
  college: "College of Natural Sciences",
  prerequisites: "The following coursework with a grade of at least C-: Computer Science 429 or 429H; Mathematics 362K or Statistics and Data Sciences 321; and credit with a grade of at least C- or registration for: Mathematics 340L, 341, or Statistics and Data Sciences 329C.",
  img_url: "https://inteng-storage.s3.amazonaws.com/img/iea/nR6bkXZxwo/sizes/software-engineering-skills_resize_md.jpg"
}, {
  name: "Data Structures",
  courseNumber: "CS314",
  professors: "Michael Scott",
  syllabus: "https://www.cs.utexas.edu/~scottm/cs314/index.htm",
  description: "This is a second course in computer programming. The purpose of the course is to learn how to use and implement canonical data structures such as lists, iterators, stacks, queues, priority queues, trees, binary search trees, balanced binary search trees, sets, maps, hash tables, heaps, tries, and graphs. The course also covers testing, reasoning about programs (pre/post conditions, assertions), debugging, abstraction of data, basic algorithm analysis, recursion, canonical sorting and searching algorithms, an introduction to the object oriented concepts of encapsulation, inheritance, and polymorphism, dynamic programming, and functional programming in Java. Students will be able to implement medium sized programs using the concepts listed. The course is taught using Java.",
  hours: "9am-10am, 10am-11am, 1pm-2pm",
  department: "Computer Science",
  college: "College of Natural Sciences",
  prerequisites: "Computer Science 312 or 312H with a grade of at least C-.",
  img_url: "https://inteng-storage.s3.amazonaws.com/img/iea/nR6bkXZxwo/sizes/software-engineering-skills_resize_md.jpg"
}, {
  name: "Astronomical Data Analysis",
  courseNumber: "AST382D",
  professors: "Brendan Bowler",
  syllabus: "https://astronomy.utexas.edu/images/ASTRO/pdfs/course_info/fall_2021/48370_Bowler.pdf",
  description: "Interpreting astronomical observations begins with analyzing data. From optimally extracting astellar spectrum to constraining cosmological models, data analysis lies at the heart of all aspects of astronomy. The goal of this course is to provide a practical guide to analyzing astronomical data. Topics will include applied probability theory, parameter estimation, Bayesian statistics, maximum likelihood methods, model fitting, and Markov chain Monte Carlo sampling. Students will work with a variety of real astronomical datasets to develop experience and skills for research.",
  hours: "10am-11:30am",
  department: "Astronomy",
  college: "College of Natural Sciences",
  prerequisites: "Graduate standing and consent of department.",
  img_url: "https://static.scientificamerican.com/sciam/cache/file/6A1D320D-B844-4467-B5834B78B41E32F5_source.jpg?w=590&h=800&1B368BB1-7BA0-424B-9D00E40AD581EE85"
}, {
  name: "Mechanics",
  courseNumber: "PHY301",
  professors: "John Markert",
  syllabus: "https://speedwaycourses.austin.utexas.edu/uex/coursedescriptions/PHY301.pdf",
  description: "Designed for students who intend to major in science or mathematics.",
  hours: "11am-12pm",
  department: "Physics",
  college: "College of Natural Sciences",
  prerequisites: "Credit with a grade of at least C- or registration in Mathematics 408D, 408L, or 408S, and Physics 101L; and an appropriate score on the physics assessment exam.",
  img_url: "https://media-cldnry.s-nbcnews.com/image/upload/newscms/2018_22/2451826/180601-atomi-mn-1540.jpg"
}, {
  name: "Introduction to Research-FRI",
  courseNumber: "PHY208F",
  professors: "John Markert",
  syllabus: "https://speedwaycourses.austin.utexas.edu/uex/coursedescriptions/PHY301.pdf",
  description: "Introductory laboratory experience; use of tools and test equipment; beginning apprenticeship in active physics research.",
  hours: "3pm-5pm",
  department: "Physics",
  college: "College of Natural Sciences",
  prerequisites: "Consent of instructor and the undergraduate adviser.",
  img_url: "https://media-cldnry.s-nbcnews.com/image/upload/newscms/2018_22/2451826/180601-atomi-mn-1540.jpg"
}];

function CourseInstancePage() {
  const {
    courseNumber
  } = (0, _reactRouterDom.useParams)();
  const textStyle = {
    fontFamily: "Bungee Shade",
    fontSize: 70,
    fontWeight: 600,
    display: "flex",
    justifyContent: "center",
    marginTop: "1%",
    marginBottom: "1%",
    color: "#bf5700"
  };
  const textStyleForDescription = {
    fontSize: 30,
    align: "left",
    marginBottom: 20
  };
  const textStyleForTitle = {
    fontSize: 30,
    align: "left",
    fontWeight: 600
  };
  const pageStyle = {
    width: "80%",
    display: "flex",
    align: "center",
    flexDirection: "column",
    marginLeft: "10%",
    marginRight: "10%"
  };
  let course = searchCourses(courseNumber);
  (0, _react.useEffect)(() => {
    document.body.style.background = 'white';
    document.title = course[0].courseNumber;
  });

  if (course.length >= 1) {
    return /*#__PURE__*/_react.default.createElement("div", {
      style: pageStyle
    }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
      align: "center",
      style: textStyle
    }, course[0].name), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      align: "center",
      style: textStyle
    }, course[0].courseNumber), /*#__PURE__*/_react.default.createElement("img", {
      src: course[0].img_url,
      className: "center"
    }), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForTitle
    }, "Professors:"), DisplayProfessor(course), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForTitle
    }, "Link to Syllabus:"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForDescription
    }, /*#__PURE__*/_react.default.createElement("a", {
      href: course[0].syllabus
    }, course[0].syllabus)), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForTitle
    }, "Hours:"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForDescription
    }, course[0].hours), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForTitle
    }, "Description:"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForDescription
    }, course[0].description), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForTitle
    }, "Department:"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForDescription,
      component: _reactRouterDom.Link,
      to: {
        pathname: `/Departments/${course[0].department}`
      }
    }, course[0].department), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForTitle
    }, "College:"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForDescription
    }, course[0].college), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForTitle
    }, "Prerequisites:"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForDescription
    }, course[0].prerequisites));
  } else {
    // in case there isn't info for the course, display error message
    return /*#__PURE__*/_react.default.createElement(_core.Typography, {
      align: "center",
      style: textStyle
    }, "Sorry, we couldn't find information on this class");
  }
} // this will render the multiple professors' names for a course,
// each name as a link to the professor instance page


function DisplayProfessor(course) {
  const textStyleForDescription = {
    fontSize: 30,
    align: "left",
    marginBottom: 20
  };
  let professors = course[0].professors.split(',');
  var professorList = professors.map(function (professor) {
    return /*#__PURE__*/_react.default.createElement(_core.Typography, {
      style: textStyleForDescription,
      component: _reactRouterDom.Link,
      to: {
        pathname: `/Professors/${professor}`
      }
    }, " ", professor, " ");
  });
  return professorList;
}

; // search courses in the array using the courseNumber

const searchCourses = courseNumber => {
  return data.filter(value => {
    return value.courseNumber.match(new RegExp(courseNumber, 'g'));
  });
};

exports.searchCourses = searchCourses;
var _default = CourseInstancePage;
exports.default = _default;