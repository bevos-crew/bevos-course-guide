from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, String, Float, Integer, PickleType, Text
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.ext.mutable import MutableList
from sqlalchemy.types import TypeDecorator, JSON
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field
import sqlalchemy 
import json
import os

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)
    db.init_app(app)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    if os.environ.get('AWS_DB_KEY') == None:
        f = open("credentials.txt", "r")
        app.config['SQLALCHEMY_DATABASE_URI'] = f.readline()
    else:
        app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('AWS_DB_KEY')

    return app, db

#UPDATED MODELS
class Professor2(db.Model):
    prof_name       = Column(String(), primary_key=True)
    prof_picture    = Column(String())
    #CHANGE BELOW and add new attribute (specific dept abbr)
    prof_department = Column(String())
    #prof_dept_general_name = Column(String())
    prof_dept_specific_abbr = Column(String())
    prof_college    = Column(String())
    prof_resume     = Column(String())
    prof_num_courses = Column(Integer)
    #CHANGE BELOW and add new attribute (course numbers)
    prof_courses    = Column(MutableList.as_mutable(ARRAY(String)))
    #prof_course_names = Column(MutableList.as_mutable(ARRAY(String)))
    prof_course_nums = Column(MutableList.as_mutable(ARRAY(String)))
    #MODIFICATION DUE TO MERGING OF PRIMARY KEYS FOR COURSES
    prof_course_names_nums = Column(MutableList.as_mutable(ARRAY(String)))
    prof_num_pubs   = Column(Integer)
    prof_pubs_list  = Column(MutableList.as_mutable(ARRAY(String)))
    prof_email      = Column(String())
    prof_eid        = Column(String())
    prof_class_buildings = Column(MutableList.as_mutable(ARRAY(String)))
    prof_teaching_days = Column(String())
    prof_teaching_times = Column(MutableList.as_mutable(ARRAY(String)))

    def __init__(self,
            prof_name:       str = "",
            prof_picture:    str = "",
            prof_department: str = "",
            #prof_dept_general_name: str = "",
            prof_dept_specific_abbr: str = "",
            prof_college:    str = "",
            prof_resume:     str = "",
            prof_num_courses:int = 0,
            prof_courses:    list = [],
            #prof_course_names: list = [],
            prof_course_nums: list = [],
            prof_course_names_nums: list = [],
            prof_num_pubs:   int = 0,
            prof_pubs_list:  list = [],
            prof_email:      str = "",
            prof_eid:        str = "",
            prof_class_buildings: list = [],
            prof_teaching_days: str = "",
            prof_teaching_times: list = []):
        self.prof_name       = prof_name
        self.prof_picture    = prof_picture
        self.prof_department = prof_department
        #replace with prof_department_general_name and prof_department_specific_abbr
        #self.prof_dept_general_name = prof_dept_general_name
        self.prof_dept_specific_abbr = prof_dept_specific_abbr
        self.prof_college    = prof_college
        self.prof_resume     = prof_resume
        self.prof_num_courses= prof_num_courses
        self.prof_courses    = prof_courses
        #replace with course names to be more specific:
        #self.prof_course_names = prof_course_names
        self.prof_course_nums = prof_course_nums
        self.prof_course_names_nums = prof_course_names_nums
        self.prof_num_pubs   = prof_num_pubs
        self.prof_pubs_list  = prof_pubs_list
        self.prof_email      = prof_email
        self.prof_eid        = prof_eid
        self.prof_class_buildings = prof_class_buildings
        self.prof_teaching_days = prof_teaching_days
        self.prof_teaching_times = prof_teaching_times
        return


class Section2(db.Model):
    sec_id               = Column(String(), primary_key=True)
    sec_days             = Column(String())
    sec_room             = Column(String())
    sec_starttime        = Column(String())
    sec_endtime          = Column(String())
    sec_seats            = Column(Integer)
    sec_professor        = Column(String())
    #add dept general name, course name, course num
    sec_dept_general_name = Column(String())
    sec_course_name = Column(String())
    sec_course_num = Column(String())
    sec_book_ISBN        = Column(String())
    sec_book_title       = Column(String())
    sec_book_subtitle    = Column(String())
    sec_book_authors     = Column(MutableList.as_mutable(ARRAY(String)))
    sec_book_thumbnail   = Column(String())
    sec_book_listPrice   = Column(String())
    sec_book_retailPrice = Column(String())

    def __init__(self,
            sec_id:              str = "",
            sec_days:            str = "",
            sec_room:            str = "",
            sec_starttime:       str = "",
            sec_endtime:         str = "",
            sec_seats:           int = 0,
            sec_professor:       str = "",
            sec_dept_general_name: str = "",
            sec_course_name:     str = "",
            sec_course_num:      str = "",
            sec_book_ISBN:       str = "",
            sec_book_title:      str = "",
            sec_book_subtitle:   str = "",
            sec_book_authors:    list = [], 
            sec_book_thumbnail:  str = "",
            sec_book_listPrice:  str = "",
            sec_book_retailPrice:str = ""):
        self.sec_id        = sec_id
        self.sec_days      = sec_days
        self.sec_room      = sec_room
        self.sec_starttime = sec_starttime
        self.sec_endtime   = sec_endtime
        self.sec_seats     = sec_seats
        self.sec_professor = sec_professor
        # add department (general name), course name, course number?
        self.sec_dept_general_name = sec_dept_general_name
        self.sec_course_name = sec_course_name
        self.sec_course_num = sec_course_num
        self.sec_book_ISBN = sec_book_ISBN
        self.sec_book_title = sec_book_title
        self.sec_book_subtitle = sec_book_subtitle
        self.sec_book_authors = sec_book_authors
        self.sec_book_thumbnail = sec_book_thumbnail
        self.sec_book_listPrice = sec_book_listPrice
        self.sec_book_retailPrice = sec_book_retailPrice
        return


class Course2(db.Model):
    #course_name       = Column(String(), primary_key=True)
    #course_number     = Column(String(), primary_key=True)
    #MODIFICATION OF PK DUE TO MERGING OF PRIMARY KEYS FOR COURSES
    course_num_name   = Column(String(), primary_key=True)
    course_professors = Column(MutableList.as_mutable(ARRAY(String)))
    course_syllabus   = Column(String())
    course_desc       = Column(String())
    course_hours      = Column(MutableList.as_mutable(ARRAY(String)))
    course_days       = Column(String())
    course_dept_general_name = Column(String())
    course_dept_specific_abbr = Column(String())
    course_college    = Column(String())
    course_section    = Column(MutableList.as_mutable(ARRAY(String)))
    course_rating     = Column(Integer)

    def __init__(self,
            course_num_name:   str = "",
            #course_name:       str = "",
            #course_number:     str = "",
            course_professors: list = [],
            course_syllabus:   str = "",
            course_desc:       str = "",
            course_hours:      list = [],
            course_days:       str = "",
            course_college:    str = "",
            course_dept_general_name: str = "",
            course_dept_specific_abbr: str = "",
            course_section:    list = [],
            course_rating:     int = -1):
        self.course_num_name   = course_num_name
        #self.course_name       = course_name
        #self.course_number     = course_number
        self.course_professors = course_professors
        self.course_syllabus   = course_syllabus
        self.course_desc       = course_desc
        self.course_hours      = course_hours
        #add unique course days
        self.course_days = course_days
        #fix this to have general department name and specific dept abbr
        self.course_dept_general_name = course_dept_general_name
        self.course_dept_specific_abbr = course_dept_specific_abbr
        self.course_college    = course_college
        self.course_section    = course_section
        self.course_rating     = course_rating
        return


class Rating(db.Model):
    course_num_name   = Column(String(), primary_key=True)
    course_rating     = Column(Integer)
    course_rating_amount = Column(Integer)
    def __init__(self,
            course_num_name:   str = "",
            course_rating:     int = -1,
            course_rating_amount: int = 0):
        self.course_num_name   = course_num_name
        self.course_rating     = course_rating
        self.course_rating_amount = course_rating_amount
        return


class Department2(db.Model):
    depart_name    = Column(String(), primary_key=True)
    depart_picture = Column(String())
    depart_website = Column(String())
    depart_abbrs   = Column(MutableList.as_mutable(ARRAY(String)))
    depart_additional_links = Column(MutableList.as_mutable(ARRAY(String)))
    depart_parent  = Column(String())
    depart_people  = Column(MutableList.as_mutable(ARRAY(String)))
    depart_all_classes = Column(MutableList.as_mutable(ARRAY(String)))
    #depart_all_class_names = Column(MutableList.as_mutable(ARRAY(String)))
    depart_all_class_nums = Column(MutableList.as_mutable(ARRAY(String)))
    #MODIFICATION DUE TO MERGING OF PRIMARY KEYS FOR COURSES
    depart_all_class_names_nums = Column(MutableList.as_mutable(ARRAY(String)))
    depart_all_professors = Column(MutableList.as_mutable(ARRAY(String)))
    depart_address = Column(String())
    depart_lat     = Column(String())
    depart_long    = Column(String())
    def __init__(self,
            depart_name               : str = "",
            depart_picture            : str = "",
            depart_website            : str = "",
            depart_abbrs              : list = [],
            depart_additional_links   : list = [],
            depart_parent             : str = "",
            depart_people             : list = [],
            depart_all_classes        : list = [],
            #depart_all_class_names    : list = [],
            depart_all_class_nums     : list = [],
            depart_all_class_names_nums: list = [],
            depart_all_professors     : list = [],
            depart_address            : str  = "",
            depart_lat                : str  = "",
            depart_long               : str  = "",
            ):
        self.depart_name               = depart_name
        self.depart_picture            = depart_picture
        self.depart_website            = depart_website
        self.depart_abbrs              = depart_abbrs
        self.depart_additional_links   = depart_additional_links
        self.depart_parent             = depart_parent
        self.depart_people             = depart_people
        self.depart_all_classes        = depart_all_classes
        #add all class numbers
        #self.depart_all_class_names = depart_all_class_names
        self.depart_all_class_nums = depart_all_class_nums
        self.depart_all_class_names_nums = depart_all_class_names_nums
        self.depart_all_professors     = depart_all_professors
        self.depart_address            = depart_address
        self.depart_lat                = depart_lat
        self.depart_long               = depart_long
        return

class ProfessorSchema2(SQLAlchemyAutoSchema):
    class Meta:
        model = Professor2
        include_relationships = True
        load_instance = True

class DepartmentSchema2(SQLAlchemyAutoSchema):
    class Meta:
        model = Department2
        include_relationships = True
        load_instance = True

class CourseSchema2(SQLAlchemyAutoSchema):
    class Meta:
        model = Course2
        include_relationships = True
        load_instance = True

class SectionSchema2(SQLAlchemyAutoSchema):
    class Meta:
        model = Section2
        include_relationships = True
        load_instance = True