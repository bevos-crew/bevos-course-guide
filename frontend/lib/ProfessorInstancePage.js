"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _core = require("@material-ui/core");

var _react = _interopRequireWildcard(require("react"));

require("./fonts.css");

var _styles = require("@material-ui/core/styles");

var _reactRouterDom = require("react-router-dom");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function createData(name, professor_img_url, department, professor_website_url, resume_url, classes, difficulty_rating, would_take_again_rating, quality_rating, biography, email) {
  return {
    name,
    professor_img_url,
    department,
    professor_website_url,
    resume_url,
    classes,
    difficulty_rating,
    would_take_again_rating,
    quality_rating,
    biography,
    email
  };
}

const rows = [createData("Glenn Downing", "https://www.cs.utexas.edu/sites/default/files/downing_glenn.jpg", "Computer Science", "https://www.cs.utexas.edu/users/downing/", "https://www.linkedin.com/in/glenn-downing-6524a81/", ["CS373", "CS371G"], 3.5, 92, 4.6, "I am an Assistant Professor of Instruction in the Department of Computer Science.\nI did my undergraduate work at Purdue and Illinois and my graduate work at MIT, Stanford, and Texas.\nI have worked at the Jet Propulsion Laboratory (JPL), the Los Alamos National Laboratory, and the Microelectronics and Computer Technology Corporation (MCC).\nBoth of my parents were born and raised in Granada, Nicaragua. I was born in New York, NY and grew up in Miami, FL.", "Downing@Cs.utexas.edu"), createData("Alison Norman", "https://www.cs.utexas.edu/sites/default/files/norman_alison_0.jpg", "Computer Science", "https://www.cs.utexas.edu/~ans/", "https://www.linkedin.com/in/alison-smith-norman/", ["CS439"], 4.6, 61, 3.6, "Dr. Norman is an Associate Professor of Instruction in The University of Texas at Austin Computer Science Department. She received both her M.S. in computer science and her Ph.D. in computer science from this department. Since then, she has been fortunate enough to spend her days teaching systems to undergraduates and performing outreach to underserved communities. Her research interests lie in the intersection of supercomputing and program analysis with a special interest in static techniques to improve checkpointing. She is also interested in large-scale simulation.", "ans@cs.utexas.edu"), createData("Fares Fraij", "https://www.cs.utexas.edu/sites/default/files/fraij_fares.jpg", "Computer Science", "https://www.cs.utexas.edu/~fares/", "https://www.linkedin.com/in/fares-fraij-1b512929/", ["CS331", "CS330E"], 3.5, 92, 4.6, "I am an Assistant Professor of Instruction in the Department of Computer Science at the University of Texas at Austin (UTCS). I received my PhD degree from the University of Texas at El Paso in 2005. My advisors were Steve Roach and J Strother Moore (from UTCS). Before joining UTCS, I worked as an Assistant/Associate Professor of Computer Science at Al-Hussein Bin Talal University in Jordan between 2005 and 2011 and a Visiting Associate Professor at Taif University in Saudi Arabia between 2011 and 2015. In addition to teaching, I have served in leading administrative positions including the Dean of the College of Information Technology and the Director of the Information Technology Center.", "fares@cs.utexas.edu"), createData("Michael Scott", "https://www.cs.utexas.edu/sites/default/files/scott_mike.jpg", "Computer Science", "https://www.cs.utexas.edu/~scottm/", "https://www.cs.utexas.edu/~scottm/cv.pdf", ["CS314"], 3.6, 81, 4.4, "Michael Scott is a lecturer at Texas Computer Science. He was raised in St. Charles, Missouri and was an undergraduate at Stanford University receiving a B.S. in Industrial Engineering. He later attended Rensselaer Polytechnic Institute and received a M.S. in Computer Science.", "scottm@cs.utexas.edu"), createData("Brendan Bowler", "https://astronomy.utexas.edu/images/cobalt_thumbs/image/3978/c9a61422b7383f8b5c686297f65a9c24.jpg", "Astronomy", "http://www.as.utexas.edu/~bpbowler/", "http://www.as.utexas.edu/~bpbowler/cv.pdf", ["AST382D"], 3.2, 100, 4.5, "I am an Assistant Professor in the Department of Astronomy at The University of Texas at Austin. I am interested in how planets form and evolve over time, which I address by finding exoplanets and characterizing their physical, orbital, and atmospheric properties at the individual and population levels. My research group focuses on questions related to the statistical patterns, orbital evolution, architectures, and circumplanetary (moon-forming) disks of exoplanets and brown dwarfs from an observational perspective. We use a broad range of ground- and space-based telescopes spanning the optical, infrared, and sub-mm wavelengths with an emphasis on high-contrast adaptive optics imaging, near-infrared spectroscopy, and precision radial velocities.", "bpbowler@astro.as.utexas.edu"), createData("John Markert", "https://cns.utexas.edu/images/cobalt_thumbs/image/1010/e7d3de6aac761fc936dd67fea8a2665c.jpg", "Physics", "https://web2.ph.utexas.edu/~markweb/", "https://utdirect.utexas.edu/apps/student/coursedocs/nlogon/download/2385064/", ["PHY301", "PHY208F", "PHY 308F", "PHY 115L", "PHY 116L"], 3.1, 100, 4.7, "Ph.D., Cornell University (1987)\nResearch Interests\nMagnetism and superconductivity in oxides and heavy fermion systems; high temperature superconductivity; high pressure research; magnetic resonance microscopy; spin dynamics; magentic domain dynamics.\n-Thermal Conductivity Probe\n-High Temperature Superconducting Materials\n-Nuclear Magnetic Resonance Microscopy and Spectroscopy\n-Novel charge states of Oxide Interfaces using PLD\n-High-Q Oscillator Experiments\n-Dynamics of Ferromagnets\n-Optically Switchable Metal Hydride Films", "markert@physics.utexas.edu")];

function ProfessorInstancePage() {
  // directs routing to specific prof instance page
  const {
    professorName
  } = (0, _reactRouterDom.useParams)(); // styles used for components

  const useStyle = (0, _styles.makeStyles)(() => ({
    card: {
      width: 500,
      height: 700,
      marginTop: "1%",
      marginBottom: "3%",
      elevation: 0,
      display: "flex",
      marginLeft: "4%"
    },
    infoCard: {
      width: 300,
      height: 700,
      marginTop: "1%",
      marginBottom: "3%",
      marginLeft: "4%",
      backgroundColor: "#c4c4c4"
    },
    bioBox: {
      width: 900,
      marginTop: "1%",
      marginBottom: "3%",
      marginLeft: "17%",
      backgroundColor: "#9E9999"
    }
  }));
  const titleTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 70,
    display: "flex",
    justifyContent: "center",
    marginTop: "2%",
    marginBottom: "0%",
    color: "#bf5700"
  };
  const subTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 25,
    display: "flex",
    justifyContent: "center",
    marginTop: "0%",
    marginBottom: "2%",
    marginLeft: "30%",
    marginRight: "30%",
    color: "#9E9999",
    maxWidth: "600px"
  };
  const boxTitleTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 50,
    display: "flex",
    justifyContent: "left",
    marginTop: "10%",
    marginBottom: "2%",
    marginLeft: "3%",
    marginRight: "30%",
    color: "white",
    maxWidth: "600px"
  };
  const boxTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 25,
    display: "flex",
    justifyContent: "left",
    marginLeft: "3%",
    color: "#bf5700",
    maxWidth: "600px"
  };
  const bioTextStyle = {
    fontFamily: "Bebas Neue",
    fontSize: 20,
    display: "flex",
    justifyContent: "center",
    color: "white"
  };
  const classes = useStyle(); // iterate through rows data set to get row index that matches profesor name
  // will need to later replace with filtering/mapping function

  var rowIndex = -1;

  for (var i = 0; i < rows.length; i++) {
    if (rows[i].name == professorName) {
      rowIndex = i;
      break;
    }
  } // set background color to white


  (0, _react.useEffect)(() => {
    document.body.style.background = 'white';
    document.title = rows[rowIndex].name;
  });
  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: titleTextStyle
  }, professorName), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: subTextStyle,
    align: "center",
    component: _reactRouterDom.Link,
    to: `/Departments/${rows[rowIndex].department}`
  }, "DEPARTMENT: ", rows[rowIndex].department), /*#__PURE__*/_react.default.createElement(_core.Box, {
    sx: {
      display: "flex",
      flexWrap: "wrap"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Card, {
    className: classes.infoCard
  }, /*#__PURE__*/_react.default.createElement(_core.CardContent, null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: boxTitleTextStyle
  }, "Info"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: boxTextStyle
  }, "Email: ", rows[rowIndex].email), /*#__PURE__*/_react.default.createElement("a", {
    target: "_blank",
    href: `${rows[rowIndex].professor_website_url}`
  }, "Website"), /*#__PURE__*/_react.default.createElement(_core.Typography, null), /*#__PURE__*/_react.default.createElement("a", {
    target: "_blank",
    href: `${rows[rowIndex].resume_url}`
  }, "Resume"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: boxTitleTextStyle
  }, "Classes"), rows[rowIndex].classes.map(prop => /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: boxTextStyle,
    component: _reactRouterDom.Link,
    to: {
      pathname: `/Courses/${prop}`
    }
  }, prop)))), /*#__PURE__*/_react.default.createElement(_core.Card, {
    className: classes.card
  }, /*#__PURE__*/_react.default.createElement(_core.CardMedia, {
    component: "img",
    height: "100%",
    width: "100%",
    image: rows[rowIndex].professor_img_url,
    alt: "glenn downing official professor image"
  })), /*#__PURE__*/_react.default.createElement(_core.Card, {
    className: classes.infoCard
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: boxTitleTextStyle
  }, "Ratings"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: boxTextStyle
  }, "Difficulty: ", rows[rowIndex].difficulty_rating), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: boxTextStyle
  }, "Would Take Again: ", rows[rowIndex].would_take_again_rating), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: boxTextStyle
  }, "Overall Quality: ", rows[rowIndex].quality_rating), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: boxTitleTextStyle
  }, "Comments"))), /*#__PURE__*/_react.default.createElement(_core.Box, {
    className: classes.bioBox
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    style: bioTextStyle
  }, rows[rowIndex].biography)));
}

var _default = ProfessorInstancePage;
exports.default = _default;