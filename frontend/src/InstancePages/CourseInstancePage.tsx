import React, { useEffect } from "react";
import '../fonts.css';
import { useParams, Link } from "react-router-dom";
import { Typography, Box } from '@material-ui/core';
import axios from "axios";
import { ICourse, ISection } from "../Components/Interfaces"
import { textStyleForDescription, titleTextStyle, infoTitleTextStyle } from "../Components/Style";
import { CreatePDF } from "../Components/Media";
import { getSectionInfo, SectionGrid } from "../Components/Sections";

//places data into an ICourse interface
function createCourse(
    name: string,
    courseNumber: string,
    professors: string[],
    syllabus: string,
    desc: string,
    college: string,
    section: string[],
    department: string,
    pdf: string,
    numName: string

): ICourse {
    return {
        name,
        courseNumber,
        professors,
        syllabus,
        desc,
        college,
        section,
        department,
        pdf,
        numName
    };
}


//information obtained from url
interface RouteParams {
    courseName: string;
}


//obtain a single courses information from the endpoint
const getCourseInfo = async (courseName: string) => {
    let course: ICourse = createCourse("", "", [], "", "", "", [], "", "", "");

    await axios.get<ICourse>(`https://api.bevoscourseguide.me/api/course/${courseName}`, {
        headers: {
            "Content-Type": "application/json"
        },
    })
        .then((response: { data: any; }) => {
            console.log(response.data);
            course = createCourse(response.data.name, response.data.courseNumber, response.data.professors, response.data.syllabus, response.data.desc, response.data.college, response.data.section, response.data.department, response.data.pdf, response.data["num name"]);
        }).catch((ex: { response: { status: number; }; }) => {
            const error =
                ex.response.status === 404
                    ? "Resource Not found"
                    : "An unexpected error has occurred";
            //console.log(ex);
        });

    return course;
}

//fetch data for a specific course and place it onto the page
function CourseInstancePage() {

    const { courseName } = useParams<RouteParams>();

    const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);
    const [error, setError]: [string, (error: string) => void] = React.useState("");
    let errorText: string[] = [];

    let [course, setCourse] = React.useState(createCourse("", "", [], "", "", "", [], "", "", ""));

    let defaultSections: ISection[] = []
    const [sections, setSections]: [ISection[], (sections: ISection[]) => void] = React.useState(defaultSections)

    useEffect(() => {
        document.body.style.background = 'white';
        const fetchData = async () => {
            const courseInfo = await getCourseInfo(courseName);
            setCourse(courseInfo);
            const sections = await getSectionInfo(courseInfo.section);
            setSections(sections);
            setLoading(false);
        }

        fetchData();

        document.title = course.numName;
        document.body.style.background = 'white';
    }, []);

    if (loading) {
        return (
            <>
                <Typography variant="h3" align="center" style={{ fontWeight: 600, marginBottom: 20, marginTop: 60 }}>Loading...</Typography>
            </>
        );
    }

    if (course.numName.length >= 1) {
        //place data and different types of media like textbook images and a pdf onto the page,
        //organizing sections into a grid
        return (
            <div >
                <Box alignItems="center" display="flex" flexDirection="column" >
                    <Typography align="center" style={titleTextStyle}>{course.numName}</Typography>
                    <Typography style={infoTitleTextStyle}>Department:</Typography>
                    <Typography style={textStyleForDescription} component={Link} to={{ pathname: `/Departments/${course.college}` }}>{course.college}</Typography>
                    <Typography style={infoTitleTextStyle}>Course Description:</Typography>
                    <Box maxWidth={500} textAlign="center">
                        <Typography style={textStyleForDescription}>{course.desc}</Typography>
                    </Box>
                    <Typography style={infoTitleTextStyle}>Syllabus:</Typography>
                    <CreatePDF file={course.pdf} />
                    <Typography align="center" style={infoTitleTextStyle}>Sections</Typography>
                    <SectionGrid sections={sections} />
                </Box>
            </div >
        )
    } else {
        // in case there isn't info for the course, display error message
        return <Typography align="center" style={titleTextStyle}>Sorry, we couldn't find information on this class</Typography>
    }
}


export default CourseInstancePage;
