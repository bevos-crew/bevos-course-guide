import React, { Component } from "react";
import { Viewer, Worker } from "@react-pdf-viewer/core";
import { Typography } from "@material-ui/core";
import GoogleMap from 'google-map-react';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import { IDepartment } from "./Interfaces";

//create media instance of pdf(syllabus or resume) for a class or professor
export function CreatePDF(prop: { file: string }) {
    const errorTextStyle = {
        fontFamily: "Bebas Neue",
        fontSize: 25,
        display: "flex",
        justifyContent: "center",
        color: "#bf5700",
    };

    const url = URL.createObjectURL(base64toBlob(prop.file));

    if (prop.file.length > 0) {
        return (
            <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
                <div style={{
                    height: '1010px',
                    width: '950px',
                    justifyContent: "center",
                }}>
                    <Viewer initialPage={1} fileUrl={url} />
                </div>
            </Worker>
        )
    } else {
        return <Typography align="center" style={errorTextStyle}>Sorry, we couldn't find a syllabus for this class</Typography>
    }
}

//turn endpoint into a base string of a pdf
const base64toBlob = (data: string) => {

    const bytes = atob(data);
    let length = bytes.length;
    let out = new Uint8Array(length);

    while (length--) {
        out[length] = bytes.charCodeAt(length);
    }

    return new Blob([out], { type: 'application/pdf' });
};

//class to make marker on map
class Marker extends Component {

    render() {
        return (
            <div style={{ color: "#bf5700" }}>
                <LocationOnIcon fontSize="large" />
            </div >
        );
    }
}

//map media for department page
export function DepartmentMap(props: { department: IDepartment }) {
    const { department } = props;
    return (
        <div style={{ height: '75vh', width: '100%' }}>
            <GoogleMap center={{ lat: +department.long, lng: +department.lat }} zoom={16} bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLEMAPSAPIKEY ?? "" }} yesIWantToUseGoogleMapApiInternals={true}>
                <div style={{ color: "#bf5700" }}>
                    <Marker />
                </div>
            </GoogleMap>
        </div>
    )
}