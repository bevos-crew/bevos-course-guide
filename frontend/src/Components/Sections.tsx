import { ISection } from "./Interfaces";
import axios from "axios";
import React from "react";
import { Link } from "react-router-dom";
import { Grid, Paper, Typography, Box } from "@material-ui/core";
import { pageStyle, paperTitleStyle, textStyleForDescription } from "./Style";

//place data into ISection to be used later in the page
export function createSection(
    id: string,
    professor: string,
    days: string,
    starttime: string,
    endtime: string,
    seats: string,
    title: string,
    thumbnail: string,
    subtitle: string,
    isbn: string,
    listPrice: number,
    retailPrice: number,
    authors: string[],
): ISection {
    return {
        id,
        professor,
        days,
        starttime,
        endtime,
        seats,
        title,
        thumbnail,
        subtitle,
        isbn,
        listPrice,
        retailPrice,
        authors,
    };
}

//get all sections data for a single course and place into an array
export const getSectionInfo = async (sections: string[]) => {
    let sectionInfo = [];

    const promises = sections.map(async (section) => {
        await axios.get<ISection>(`https://api.bevoscourseguide.me/api/section/${section}`).then((response) => {
            //console.log(response);
            let section: any = response.data;
            sectionInfo.push(createSection(response.data.id,
                response.data.professor,
                response.data.days,
                response.data.starttime,
                response.data.endtime,
                response.data.seats,
                section["book title"],
                section["book thumbnail"],
                section["book subtitle"],
                section["book ISBN"],
                section["book listPrice"],
                section["book retailPrice"],
                section["book authors"]));

        }).catch(ex => {
            const error =
                ex.response.status === 404
                    ? "Resource Not found"
                    : "An unexpected error has occurred";
            //console.log(ex);
        });

    });

    await Promise.all(promises);

    return sectionInfo;

}


//place all sections into a grid for a class
export function SectionGrid(props: { sections: ISection[] }) {
    const { sections } = props;
    return (
        <Grid container spacing={3} justifyContent="center">
            {sections.map((section) => (
                <Grid item alignContent='center' direction="column"  >
                    <Paper style={pageStyle}>
                        <Typography style={paperTitleStyle}>
                            Unique ID: {section.id}
                        </Typography>
                        <Typography style={paperTitleStyle} component={Link} to={`/Professors/${section.professor}`}>
                            Professor: {section.professor}
                        </Typography>
                        <Typography style={paperTitleStyle}>
                            Days: {section.days}
                        </Typography>
                        <Typography style={paperTitleStyle}>
                            Time: {section.starttime} - {section.endtime}
                        </Typography>
                        <Typography style={paperTitleStyle}>
                            Seats: {section.seats}
                        </Typography>
                        <Typography style={paperTitleStyle}>
                            Required Textbook Title: {section.title}
                        </Typography>
                        <Typography style={paperTitleStyle}>
                            Subtitle: {section.subtitle}
                        </Typography>
                        <Box alignContent="center">
                            <img src={section.thumbnail}
                                className="center" width="300" />
                        </Box>
                        <Typography style={paperTitleStyle}>
                            Author(s):
                        </Typography>
                        <Box alignItems="center">
                            {section.authors.map((author) =>
                                <Typography style={textStyleForDescription}>
                                    {author}
                                </Typography>
                            )}
                        </Box>
                        <Typography style={paperTitleStyle}>
                            ISBN: {section.isbn}
                        </Typography>
                        <Typography style={paperTitleStyle}>
                            Amazon List Price: ${section.listPrice}
                        </Typography>
                        <Typography style={paperTitleStyle}>
                            Retail Price:  ${section.retailPrice}
                        </Typography>
                    </Paper>
                </Grid>
            ))
            }
        </Grid >
    )
}