from dataclasses import dataclass
from enum import Enum

class Semester(Enum):
    FALL = 9
    SUMMER = 6
    SPRING = 2

@dataclass
class CourseData:
    year: int
    semester: Semester
    dept: str
    dept_name: str
    course_number: str
    topic: int
    unique: str
    const_sect_nbr: str
    title: str
    desc: str
    instructor: str
    instructor_eid: str
    instructor_email: str
    days: str
    start_time: str
    end_time: str
    building: str
    room: str
    max_enrollment: int
    seats_taken: int


class CourseLineParser:

    def __init__(self, line):
        self.line = line.split('\t')

    def get_string(self, idx):
        if idx > 0 and idx < len(self.line):
            return self.line[idx].strip()
        else:
            return ""

    def get_int(self, idx):
        if idx > 0 and idx < len(self.line):
            return int(self.line[idx].strip())
        else:
            return 0

    def parse_data(self):
        return CourseData(
            year = self.get_int(0),
            semester = self.get_int(1),
            dept = self.get_string(2),
            dept_name = self.get_string(3),
            course_number = self.get_string(4),
            topic = self.get_int(5),
            unique = self.get_string(6),
            const_sect_nbr = self.get_string(7),
            title = self.get_string(8),
            desc = self.get_string(9),
            instructor = self.get_string(10),
            instructor_eid = self.get_string(11),
            instructor_email = self.get_string(12),
            days = self.get_string(13),
            start_time = self.get_string(14),
            end_time = self.get_string(15),
            building = self.get_string(16),
            room = self.get_string(17),
            max_enrollment = self.get_int(18),
            seats_taken = self.get_int(19),
        )

course_data = {}

def map_all_courses():
    with open('../Current_Semester_Report') as f:
        file_lines = f.readlines()

    for line in file_lines:
        if line[0] != '2':
            continue
        parser = CourseLineParser(line)
        course = parser.parse_data()

        course_data[course.dept + course.course_number] = course


map_all_courses()

print(course_data["C S439"])
