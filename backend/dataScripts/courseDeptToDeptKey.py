import pandas as pd
from datetime import datetime, time
#from sqlalchemy import SQLAlchemyError
from sqlalchemy.ext.mutable import MutableList
import requests
import sys
import time
import os
import ast
import json

def main():
    file = open("../dataFiles/deptsDict.json", "r")
    contents = file.read()
    #keys: dept name, value keys: "Abbr", "People", "Parent", "Links"
    dept_dict = ast.literal_eval(contents)
    file.close()

    width = [5, 2, 3, 101, 6, 4, 7, 7, 31, 217, 26, 9, 101, 
            7, 5, 5, 4, 8, 4, 4, 4, 6, 183]
    header = ['Year', 'Semester', 'Dept-Abbr', 'Dept-Name', 'Course Nbr', 'Topic', 
            'Unique', 'Const Sect Nbr', 'Title', 'Crs Desc', 'Instructor', 
            'Instructor EID', 'Instructor Email', 'Days', 'From', 'To', 
            'Building',	'Room', 'Max Enrollment', 'Seats Taken', 'Total X-listings',
            'X-List Pointer', 'X-Listings']
    df = pd.read_fwf('../dataFiles/Current_Semester_Report', widths=width, dtype='str')
    df.columns=header

    dept_to_dept_dict = {}
    all_abbreviations_list = []
    for key, value in dept_dict.items():
        abbreviations_list = []
        try: abbreviations_list = value['Abbr']
        except: pass
        
        all_abbreviations_list += abbreviations_list
        for abbr in abbreviations_list:
            #print(type(abbr))
            df_dept = df.loc[(df['Dept-Abbr'] == abbr)]
            if not df_dept.empty:
                dept_to_dept_dict[df_dept['Dept-Name'].tolist()[0]] = key
            else: 
                print(abbr)
                print("couldn't find in abbr in semester report")
    
    for index, rec in df.iterrows():
        if rec['Dept-Abbr'] not in all_abbreviations_list:
            print(rec['Dept-Abbr'])
    # with open('../dataFiles/dept.txt', 'w') as f:
    #     json.dump(dept_to_dept_dict, f)


if __name__ == "__main__":
    main()