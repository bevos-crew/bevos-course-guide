import React, { useEffect, useState } from "react";

import {
    Typography,
    Button,
    Box,
    makeStyles
  } from "@material-ui/core";
import BCGVisuals from "./BCGVisualizations/BCGVisuals";
import FRVisuals from "./FRVisualizations/FRVisuals";
import { visualButtonStyle, titleStyle } from "../../Components/Style";

function Visualizations() {

    const [ourViz, setOurViz]: [boolean, (ourViz: boolean) => void] = useState<boolean>(true);

    useEffect(() => {
        document.body.style.background = 'white';
        document.title = "Departments";

    }, []);

    const classes = visualButtonStyle();

    return (
        <Box display="flex" flexDirection="column" alignItems="center">
            <Typography
                variant="h3"
                align="center"
                style={titleStyle}
            >
                Visualizations
            </Typography>

            <Box display="flex" flexDirection="row" alignItems="" sx={{ marginBottom: 50 }}>
                <Button variant={ourViz ? "contained" : "outlined"} color="primary" className={classes.leftButton}
                    onClick={() => setOurViz(true)}
                >
                    Our Visuals
                </Button>
                <Button variant={ourViz ? "outlined" : "contained"} color="primary" className={classes.rightButton}
                    onClick={() => setOurViz(false)}
                >
                    Flight Right's Visuals
                </Button>
            </Box>

            {/* unsure why but this makes the margin bottom work */}
            <Box />

            {ourViz ?
                <BCGVisuals />
                :
                <FRVisuals />
            }

        </Box>
    );
}

export default Visualizations;