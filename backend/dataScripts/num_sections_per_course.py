import pandas as pd 
import ast
import pickle
import json

def main():
    width = [5, 2, 3, 101, 6, 4, 7, 7, 31, 217, 26, 9, 101, 
            7, 5, 5, 4, 8, 4, 4, 4, 6, 183]
    header = ['Year', 'Semester', 'Dept-Abbr', 'Dept-Name', 'Course Nbr', 'Topic', 
            'Unique', 'Const Sect Nbr', 'Title', 'Crs Desc', 'Instructor', 
            'Instructor EID', 'Instructor Email', 'Days', 'From', 'To', 
            'Building',	'Room', 'Max Enrollment', 'Seats Taken', 'Total X-listings',
            'X-List Pointer', 'X-Listings']
    df = pd.read_fwf('../dataFiles/Current_Semester_Report', widths=width, dtype='str')
    df.columns=header

    course_sections_dict = {}

    for index, rec in df.iterrows():
        course_abbr_num = ''
        try: course_abbr_num = rec['Dept-Abbr'] + ' ' + rec['Course Nbr']
        except: course_abbr_num = rec['Course Nbr']
        #try appending abbr course + name
        course_num_name = course_abbr_num
        try: course_num_name += (' ' + rec['Title'])
        except: pass
        if course_sections_dict.get(course_num_name) == None:
            section_numbers = df.loc[(df['Course Nbr'] == rec['Course Nbr']) & 
                        (df['Title'] == rec['Title'])]
            course_sections_dict[course_num_name] = len(section_numbers)
    
    with open('../dataFiles/num_sections_per_course.txt', 'w') as f:
        json.dump(course_sections_dict, f)

if __name__ == "__main__":
    main()