import pandas as pd
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
import urllib
import json
import requests
import ast


#only have to store number of publications (and maybe names)

Schema = "postgres+psycopg2://skalr:sahil7katelyn3adeet6lauren0rachel2@bevocourseguide.c3xs1xqx1eit.us-east-2.rds.amazonaws.com:5432/postgres"
#Now, we want to initialize our Flask app and SQLAlchemy instance.
#if __name__ == "__main__":
#    pass
def open_db():
    app = Flask(__name__)
    app.debug = True
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    app.config['SQLALCHEMY_DATABASE_URI'] = Schema

def get_aminer_pubs(last_name: str, unique: str):
    # Get API request
    #request_url = 'http://api.worldbank.org/v2/countries?format=json&&per_page=50'
    #request_url = 'http://arnetminer.org/services/search-publication?q=factor graph&u=oyster&start=3&num=2'
    #request_url = 'http://arnetminer.org/services/search-publication?q=UT Austin&u=ashkat&start=0&num=20'
    
    #par = {'org': 'UT Austin'}
    #print(last_name, unique)
    name = ''
    try: name = last_name[:last_name.index(',')]
    except: pass
    if unique == None: unique = ''

    file = open("../dataFiles/profNameDict.txt", "r")
    contents = file.read()
    prof_name_dict = ast.literal_eval(contents)
    file.close()
    full_name = prof_name_dict.get(unique)
    full_name_found = False
    if (full_name != 'NO NAME FOUND') and (full_name != None):
        name = full_name
        full_name_found = True
    #print(name)
    par = {'query': name}
    headers = {'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
    #re = requests.get('https://api.aminer.org/api/search/pub/advanced', params=par, headers=headers)
    url = 'https://api.aminer.org/api/search/pub'
    #url = 'https://api.aminer.org/api/search/person'
    #url = 'https://api.aminer.org/api/search/person/advanced?keyword=University of Texas'
    try: 
        re = requests.get(url, params=par, headers=headers, timeout=10)
        data = json.loads(re.text)
        data = re.json()
        json_formatted_str = json.dumps(data, indent=2)
    except: return (0, [])
    #print(json_formatted_str)
    try:
        df_all_paper_results = pd.json_normalize(data['result']) 
        #print(df_all_paper_results['title'])
        drop_rows = []
        for i, author in enumerate(df_all_paper_results['authors']):
            df_single_paper_results = pd.json_normalize(author)
            #print(df_single_paper_results.columns)
            #print(d)
            try:
                df_single_paper_results = df_single_paper_results.loc[(df_single_paper_results['aff.desc'].str.contains(['University of Texas at Austin', 'UT Austin', 'University of Texas Austin', 'University of Texas']))]
                
                if df_single_paper_results.empty:
                #print('is empty')
                    drop_rows.append(i)
            except: 
                if full_name_found:
                    pass
                else:
                    drop_rows.append(i)
                    #print(author)
                #pass 
            #print(d.columns)
            #print(df_single_paper_results['aff.desc'])
            #print(i)

        df_all_paper_results = df_all_paper_results.drop(drop_rows)
        #print(df_all_paper_results)
        #print(df['affiliation'])
        #return df
        #return number of publications, and list of tuples - title, abstract
        return (len(df_all_paper_results.index), 
            [(t, a) for t,a in zip(df_all_paper_results['title'], 
            df_all_paper_results['abstract'])])
    except:
        return (0, []) 
        #print("could not find any relevant publications".upper())
        #print(json_formatted_str)
    #print(data)
    #df = pd.json_normalize(data['title'])
    #print(df)

    #par = {'name': 'Brendan Bowler'}
    #header = {'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
    #request_url = 'https://api.aminer.org/api/search/pub/advanced'
    #r = urllib.request.urlopen(request_url, data=bytes(par))
    #data = json.loads(r.read())
    #print(data)
    #df = pd.DataFrame(data)
    #print(df)

def get_reddit_json(param: str):
    headers = {'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
    params = {'q':param, 'restrict_sr':'on'}
    url = 'https://www.reddit.com/r/UTAustin/search.json'
    #url = 'https://api.aminer.org/api/search/person/advanced?keyword=University of Texas'
    re = requests.get(url, params=params, headers=headers, timeout=10)
    #data = json.loads(re.text)
    data = re.json()
    return data

def get_title_and_comment(course_abbr_num: str, professor: str, department: str, unique: str):
    #returns list of tuples - (title, descriptions)
    course_data = get_reddit_json(course_abbr_num)
    #print("course data")
    #print(course_data)
    #print("children")
    #print(((course_data['data'])['children'][0])['data'])
    try:
        #children = course_data['children'][0]
        children = (course_data['data'])['children']
        if len(children) > 0:
            return [(child['data']['title'], child['data']['selftext']) 
                    for child in children]
    except: pass
    #PROFESSOR
    name = professor[:professor.index(',')]
    file = open("../dataFiles/profNameDict.txt", "r")
    contents = file.read()
    prof_name_dict = ast.literal_eval(contents)
    file.close()
    full_name = prof_name_dict.get(unique)
    if full_name != 'NO NAME FOUND':
        name = full_name
    course_data = get_reddit_json(name)
    try:
        children = (course_data['data'])['children']
        if len(children) > 0:
            return [(child['data']['title'], child['data']['selftext']) 
                    for child in children]
    except: pass
    course_data = get_reddit_json(department)
    try:
        children = (course_data['data'])['children']
        if len(children) > 0:
            return [(child['data']['title'], child['data']['selftext']) 
                    for child in children]
    except: pass
    return [] #couldn't find anything about this course, prof, department on subreddit

#AMINER EXAMPLE
#data = get_aminer_pubs('PYREK, A', '00340')
#print(data)
#data = get_aminer_pubs('AARONSON, S', '52920')
#print(data)
#data = get_aminer_pubs('PARIKH, D', '52450')
#print(data)
#data = get_aminer_pubs('BALAKRISHNAN, A', '33985')
#print(data)

#REDDIT EXAMPLE - must take in course_abbr_num, prof, and dept
#result = get_title_and_comment('CS373', 'DOWNING, G', 'Computer Science', '52980')
#print(result)
#result = get_title_and_comment('CS356', 'HAN, M', 'Computer Science', '52905')
#print(result)

#data = get_aminer_pubs()
# data = get_reddit_json('cs439')
# #data = get_reddit_json('cs373')
# #print(data)
# df = pd.json_normalize(data['data'])
# print(df)
# print(df.shape)
# #print(df.columns)
# #print(df['children'])
# children = df['children'][0]
# print(children)
# print(len(children))
# for li in children:
#     print(li['data']['title'])

#if not class, then professor, then department

#print(df['title'])
#print(df['aff.desc'])
#df.to_csv(path_or_buf='/Users/katelynsfiles/Documents/senior_year/cs373/bevos-course-guide/backend/APIcalls/testAPI.txt')