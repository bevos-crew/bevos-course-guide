import { Box, Typography, Divider } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import '../fonts.css';
import { SearchAutocomplete } from "../Components/SearchAutocomplete";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import Pagination from "../Components/Pagination"

interface IData {
    data: IOption[];
    num_instances: number;
}

interface IOption {
    model: string;
    name: string;
    match: string;
}



function SearchResultsPage() {
    let [page, setPage] = useState(0);
    const [itemsPerPage, setItemsPerPage] = useState(12);
    const [count, setCount] = useState(10);

    interface RouteParams {
        query: string;
    }

    const { query } = useParams<RouteParams>();

    const [results, setResults]: [IOption[], (results: IOption[]) => void] = useState([]);
    const [loading, setLoading]: [boolean, (loading: boolean) => void] = useState<boolean>(true);

    const getSearchResults = async (query: string) => {
        let searchResults = [];
        await axios.get<IData>(`https://api.bevoscourseguide.me/api/search?query=${query}&quantity=${itemsPerPage}&page=${page + 1}`,
        )
            .then((response) => {
                searchResults = response.data.data;
                setCount(response.data.num_instances);
            })
            .catch((ex) => {
                // const error =
                //     ex.response.status === 404
                //     ? "Resource Not found"
                //     : "An unexpected error has occurred";
                console.log(ex);
            });

        return searchResults;
    };

    // fetching the data for the search results page
    useEffect(() => {
        document.body.style.backgroundColor = "white";
        document.title = "Search Results";

        const fetchData = async () => {
            const results = await getSearchResults(query);
            setResults(results);
            setLoading(false);
        };
        setPage(page);
        setCount(count);
        setItemsPerPage(itemsPerPage);
        fetchData();

    }, [page, itemsPerPage]);

    // display loading text if the data is still being retrieved
    if (loading) {
        return (
            <Typography variant="h3" align="center" style={{ fontWeight: 600, marginBottom: 20, marginTop: 60 }}>Loading...</Typography>
        );
    }

    return (
        <Box display="flex" flexDirection="column" alignItems="left">
            <Box sx={{ width: 600 }}>
                <SearchAutocomplete fontSize={20} borderRadius={30} size="small" initialVal={query} setResults={setResults} />
            </Box>
            <Divider />
            <Box height={10} />
            {results.map((result: IOption) => <SearchResult result={result} query={query} />)}
            <Box sx={{ flexGrow: 1 }} mr={20}>
                <Pagination page={page} setPage={setPage} itemsPerPage={itemsPerPage} setItemsPerPage={setItemsPerPage} count={count} pageOptions={[6, 12, 18, 24, 30]} />
            </Box>
        </Box>
    )
}

interface ResultProps {
    result: IOption;
    query: string;
}

// gets the individual search results and formats it to be displayed
function SearchResult({ result, query }: ResultProps) {

    function getHighlightedText(text: string, highlight: string) {
        // Split on highlight term and include term into parts, ignore case
        const parts: string[] = text.split(new RegExp(`(${highlight})`, 'gi'));
        return <span> {parts.map((part, i) =>
            <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? { backgroundColor: "yellow", padding: "0.1em 0.0em" } : {}}>
                {part}
            </span>)
        } </span>;
    }
    return (
        <Box display="flex" flexDirection="column" alignItems="left" margin={5} >
            <Typography variant="h5" component={Link} to={"/" + result.model + "s/" + result.name}>{result.name}</Typography>
            <Typography variant="body1">{getHighlightedText(result.match, query)}</Typography>
        </Box>
    )
}


export default SearchResultsPage;